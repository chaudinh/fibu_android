package com.giinger.fibu.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.giinger.fibu.R;
import com.giinger.fibu.adapter.AdvertiseAdapter;
import com.giinger.fibu.model.AdvertiseItem;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AdvertiseFragment extends Fragment {

    private ListView lvAdvertise;
    private AdvertiseAdapter adapter;
    private List<AdvertiseItem> data;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.advertise_fragment, null, false);

        lvAdvertise = (ListView) view.findViewById(R.id.lvAdvertise);

        data = new ArrayList<>();
        adapter = new AdvertiseAdapter(data);
        lvAdvertise.setAdapter(adapter);

        loadAdvertise();

        return view;
    }

    private void loadAdvertise() {
        Context context = getActivity();
        AQuery aQuery = new AQuery(context);
        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                Log.e("Advertise", object + "");
                if (object != null) {
                    try {
                        JSONObject response = object.getJSONObject(Constants.RESPONSE);
                        boolean success = response.getBoolean(Constants.SUCCESS);
                        if (success) {
                            JSONArray apps = response.getJSONArray("apps");
                            for (int i = 0; i < apps.length(); i++) {
                                JSONObject item = apps.getJSONObject(i);
                                AdvertiseItem advertise = new AdvertiseItem(item);
                                data.add(advertise);
                            }

                            adapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        String url = AppUtils.getAdvertiseUrl(context);
        aQuery.ajax(url, JSONObject.class, callback);
    }
}
