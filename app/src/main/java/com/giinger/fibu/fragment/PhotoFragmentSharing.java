package com.giinger.fibu.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.giinger.fibu.R;
import com.giinger.fibu.activity.BaseFileScreen;
import com.giinger.fibu.activity.ChatBoxScreen;
import com.giinger.fibu.adapter.PhotoAdapter;
import com.giinger.fibu.model.PhotoGroup;
import com.giinger.fibu.model.PhotoItem;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.ClearItems;
import com.giinger.fibu.util.ExternalStorage;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.OnItemSharing;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.SelectedItems;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class PhotoFragmentSharing extends Fragment implements ClearItems {

    public static final int MAX_SELECTED = 5;

    protected TextView txtCountSelectedItem;
    private RelativeLayout headerPhoto;
    private int count = 0;

    protected List<PhotoItem> selected = new ArrayList<>();

    public int getCountItemSelected() {
        return selected.size();
    }

    public int addItemSelected(PhotoItem item) {
        Fragment fragment = getParentFragment();
        if (fragment instanceof SelectedItems) {
            ((SelectedItems) fragment).onSelected(item, item.isSelected);
        }

        if (item.isSelected) {
            selected.add(item);
        } else {
            selected.remove(item);
        }

        updateCountView();

        return selected.size();
    }

    protected void updateCountView() {
        String type = "photo";
        int count = selected.size();
        String text = count + " " + (count > 1 ? type + "s" : type) + " selected";
        txtCountSelectedItem.setText(text);
    }

    private PhotoAdapter adapter;
    private List<PhotoGroup> data;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.photo_fragment, null, false);

        txtCountSelectedItem = (TextView) view.findViewById(R.id.txtCountSelectedItem);
        headerPhoto = (RelativeLayout) view.findViewById(R.id.header_photo);

        if (Constants.isChat) {
            headerPhoto.setVisibility(View.GONE);
        } else {
            headerPhoto.setVisibility(View.VISIBLE);
        }

        ListView lvPhotos = (ListView) view.findViewById(R.id.lvPhotos);
        data = new ArrayList<>();
        adapter = new PhotoAdapter(data);
        adapter.setOnItemClickListener(new PhotoAdapter.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int group, int position, View view) {
                PhotoItem child = data.get(group).items.get(position);

                FragmentActivity activity = getActivity();
                if (activity instanceof OnItemSharing) {
                    try {
                        if (group == 1 && position == 0) {
                            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                            File photo = new File(Environment.getExternalStorageDirectory(), "capture_sharing.jpg");
                            PreferenceUtils.saveString(Constants.PATH_SHARING, getActivity(), photo.getAbsolutePath());
                            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photo));
                            getActivity().startActivityForResult(intent, ChatBoxScreen.RC_CAPTURE_IMAGE);
                        } else {
                            if (child.size > BaseFileScreen.MAX_SIZE) {
                                String message = getString(R.string.msg_file_selected_too_big, "");
                                Context context = getActivity();
                                new AlertDialog.Builder(context).setMessage(message).setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }).show();
                            } else {
                                if (child.isPhoto) {
                                    JSONObject data = new JSONObject();
                                    data.put("data", child.data);
                                    ((OnItemSharing) activity).onShare("photo", data);
                                } else {
                                    JSONObject video = new JSONObject();
                                    video.put("data", child.data);
                                    ((OnItemSharing) activity).onShare("video", video);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        lvPhotos.setAdapter(adapter);
        loadAllPhotos();
        return view;
    }

    //    private List<PhotoGroup> newData = new ArrayList<>();
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
//            data.clear();
//            data.addAll(newData);
            adapter.notifyDataSetChanged();
        }
    };

    private static File PHOTOS = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

    private void loadAllPhotos() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e("Photo", PHOTOS.getAbsolutePath());

                listAllPhotos(PHOTOS);

                if (ExternalStorage.isAvailable()) {
                    File sdCard = new File("/storage/sdcard1/DCIM");
                    if (!sdCard.exists()) {
                        Map<String, File> externalLocations = ExternalStorage.getAllStorageLocations();
                        if (externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD) == null) {
                            sdCard = new File("/mnt/sdcard/ext_sd/DCIM");
                        } else {
                            sdCard = new File(externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD).toString() + "/DCIM");
                        }
                    }

                    listAllPhotos(sdCard);
                }

            }
        }).start();
    }

    private void listAllPhotos(File root) {
        if (root == null) {
            return;
        }

        File cache = BumpUtils.getCacheFolder();
        if (root.getAbsolutePath().equals(cache.getAbsolutePath())) {
            return;
        }

        final File[] files = root.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                if (file.isFile() && file.getName().toLowerCase().endsWith(".mp4")) {
                    file.setLastModified(System.currentTimeMillis());
                }

                return file.isFile() && file.getName().toLowerCase().endsWith(".jpg") || file.isFile() && file.getName().toLowerCase().endsWith(".mp4") ;
            }
        });

        if (files != null && files.length > 0) {
            String name = root.getName();

            Arrays.sort(files, new Comparator() {
                public int compare(Object o1, Object o2) {

                    if (((File) o1).lastModified() > ((File) o2).lastModified()) {
                        return -1;
                    } else if (((File) o1).lastModified() < ((File) o2).lastModified()) {
                        return +1;
                    } else {
                        return 0;
                    }
                }

            });

            String groupName = name + " (" + files.length + ")";
            final PhotoGroup header = new PhotoGroup(PhotoGroup.TYPE.HEADER, groupName, false, "", 0, root.getAbsolutePath());
            count++;
//            newData.add(header);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        data.add(header);
                        adapter.notifyDataSetChanged();
                    }
                });
            }

            PhotoGroup row = null;
            if (count == 1) {

                for (int i = 0; i < files.length + 1; i++) {
                    if (i == 0) {
                        row = new PhotoGroup(PhotoGroup.TYPE.ROW, "", false, "", 0, "");
                    } else {
                        File file = files[i - 1];
                        String path = file.getAbsolutePath();
                        boolean isPhoto = path.toLowerCase().endsWith(".jpg") ? true : false;
                        String duration = isPhoto ? "" : getDuration(path);

                        switch (i % 4) {
                            case 0:
                                row = new PhotoGroup(PhotoGroup.TYPE.ROW, path, isPhoto, duration, 0, "");
                                break;
                            case 3:
                                row.addChild(path, isPhoto, duration, file.length());
                                final PhotoGroup finalRow = row;
                                if (activity != null) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            data.add(finalRow);
                                            adapter.notifyDataSetChanged();
                                        }
                                    });
                                }
                                row = null;
                                break;
                            default:
                                row.addChild(path, isPhoto, duration, file.length());
                                break;
                        }
                    }
                }

            } else {
                for (int i = 0; i < files.length; i++) {
                    File file = files[i];
                    String path = file.getAbsolutePath();
                    boolean isPhoto = path.contains(".jpg") ? true : false;
                    String duration = isPhoto ? "" : getDuration(path);

                    switch (i % 4) {
                        case 0:
                            row = new PhotoGroup(PhotoGroup.TYPE.ROW, path, isPhoto, duration, 0, "");
                            break;
                        case 3:
                            row.addChild(path, isPhoto, duration, file.length());
                            final PhotoGroup finalRow = row;
                            if (activity != null) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        data.add(finalRow);
                                        adapter.notifyDataSetChanged();
                                    }
                                });
                            }
                            row = null;
                            break;
                        default:
                            row.addChild(path, isPhoto, duration, file.length());
                            break;
                    }
                }
            }


            if (row != null && activity != null) {
                final PhotoGroup fr = row;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        data.add(fr);
                        adapter.notifyDataSetChanged();
                    }
                });
            }

            handler.sendEmptyMessage(0);
        }

        File[] folders = root.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isDirectory() && !file.isHidden();
            }
        });

        if (folders != null) {
            for (File folder : folders) {
                listAllPhotos(folder);
            }
        }
    }

    @Override
    public void onClear() {
        HomeFragment homeFragment = (HomeFragment) getParentFragment();
        String bumpType = homeFragment.getBumpType();

        if (!"photo".equals(bumpType) && selected.size() > 0) {
            for (PhotoItem item : selected) {
                item.isSelected = false;
            }

            selected.clear();
            adapter.notifyDataSetChanged();
            updateCountView();
        }
    }

    public String getDuration(String path) {
        MediaPlayer mp = null;
        try {
            mp = MediaPlayer.create(getActivity(), Uri.parse(path));
        } catch (Exception e) {

        }

        if (mp == null) {
            return "";
        }

        int duration = mp.getDuration();
        mp.release();

        String dr = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(duration),
                TimeUnit.MILLISECONDS.toMinutes(duration) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)),
                TimeUnit.MILLISECONDS.toSeconds(duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));

        return dr;
    }
}
