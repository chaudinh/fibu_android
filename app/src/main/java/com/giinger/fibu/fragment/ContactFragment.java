package com.giinger.fibu.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.giinger.fibu.R;
import com.giinger.fibu.activity.AllThingsScreen;
import com.giinger.fibu.activity.ExtraPowerScreen;
import com.giinger.fibu.adapter.ContactAdapter;
import com.giinger.fibu.model.ContactItem;
import com.giinger.fibu.util.ClearItems;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.OnItemSharing;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.SelectedItems;
import com.giinger.fibu.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ContactFragment extends Fragment implements ClearItems, View.OnClickListener {

    public static int MAX_SELECTED_ITEMS = 10;
    public static final String ACTION = "fibu.save.contact";
    public static final String DATA = "contact.data";

    private ListView lvContact;
    private ContactAdapter adapter;
    private List<ContactItem> data;
    private RelativeLayout headerContact;
    //    public static RelativeLayout layoutActionSecurity;
    public static RelativeLayout layoutListContact;
    private RelativeLayout layoutBackup;
    private RelativeLayout layoutFistAll;
    private boolean isSelectedAll = false;
    private LinearLayout layoutButtonBF;

    protected TextView txtCountSelectedItem;
    protected List<ContactItem> selected = new ArrayList<>();
    private static final int RC_PURCHASE = 1009;

    public int getCountItemSelected() {
        return selected.size();
    }

    public int onItemSelected(ContactItem item) {
        Fragment fragment = getParentFragment();
        if (fragment instanceof SelectedItems) {
            ((SelectedItems) fragment).onSelected(item, item.isSelected);
        }

        if (item.isSelected) {
            selected.add(item);
        } else {
            selected.remove(item);
        }

        updateCountView();
        return selected.size();
    }

//    public void clearSelectedItems() {
//        selected.clear();
//        updateCountView();
//    }

    protected void updateCountView() {
        String type = getType();
        int count = selected.size();
        if (count == 0) {
            txtCountSelectedItem.setVisibility(View.INVISIBLE);
            isSelectedAll = false;
        } else {
            txtCountSelectedItem.setVisibility(View.VISIBLE);
            String text = count + " " + (count > 1 ? type + "s" : type) + " selected";
            txtCountSelectedItem.setText(text);

            if (count < data.size()) {
                isSelectedAll = false;
            } else {
                isSelectedAll = true;

            }
        }
    }

    protected String getType() {
        return "contact";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact_fragment, null, false);

        txtCountSelectedItem = (TextView) view.findViewById(R.id.txtCountSelectedItem);
        lvContact = (ListView) view.findViewById(R.id.lvContact);
        headerContact = (RelativeLayout) view.findViewById(R.id.header_contact);
//        layoutActionSecurity = (RelativeLayout) view.findViewById(R.id.layout_action_show_warrning);
        layoutListContact = (RelativeLayout) view.findViewById(R.id.layout_list_contact);
//        layoutActionSecurity.setOnClickListener(this);

        MAX_SELECTED_ITEMS = PreferenceUtils.getInt(Constants.MAX_SELECTED_CONTACT, getActivity(), 10);

        layoutBackup = (RelativeLayout) view.findViewById(R.id.layout_back_up);
        layoutFistAll = (RelativeLayout) view.findViewById(R.id.layout_fist_all);
        layoutButtonBF = (LinearLayout) view.findViewById(R.id.layout_button_all);

        layoutBackup.setOnClickListener(this);
        layoutFistAll.setOnClickListener(this);

        if (Constants.isChat) {
            headerContact.setVisibility(View.GONE);
            layoutButtonBF.setVisibility(View.GONE);
        }

        data = new ArrayList<>();
        adapter = new ContactAdapter(data);
        lvContact.setAdapter(adapter);

        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactItem item = data.get(position);
                FragmentActivity activity = getActivity();
                if (activity instanceof OnItemSharing) {
                    try {
                        JSONObject contact = new JSONObject();
                        contact.put("name", item.name);
                        contact.put("website", item.website);
                        contact.put("address", item.address);

                        JSONArray emails = new JSONArray();
                        if (item.emails != null) {
                            for (String email : item.emails) {
                                emails.put(email);
                            }
                        }

                        JSONArray phones = new JSONArray();
                        if (item.phones != null) {
                            for (String phone : item.phones) {
                                phones.put(phone);
                            }
                        }

                        contact.put("emails", emails);
                        contact.put("phones", phones);

                        ((OnItemSharing) activity).onShare("contact", contact);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    ContactAdapter.ViewHolder viewHolder = (ContactAdapter.ViewHolder) view.getTag();

                    HomeFragment homeFragment = (HomeFragment) getParentFragment();
                    if (item.isSelected) {
                        item.isSelected = false;
                        int size = onItemSelected(item);
                        viewHolder.imgSelected.setImageResource(R.drawable.contact_not);
                        if (size == 0) {
                            homeFragment.goneLayoutAnimation();
                        }
                    } else {
                        int count = getCountItemSelected();
                        if (count < MAX_SELECTED_ITEMS) {
                            if (Constants.isAnim) {
                                homeFragment.showAnimationBump();
                            }

                            item.isSelected = true;
                            onItemSelected(item);
                            viewHolder.imgSelected.setImageResource(R.drawable.select_icon);
                        } else {
                            Context context = getActivity();
                            Toast.makeText(context, "Maximum contacts can select once time", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });


//        if (!PreferenceUtils.getBoolean("show_security",getActivity(),true)) {
//            layoutListContact.setVisibility(View.VISIBLE);
//            layoutActionSecurity.setVisibility(View.GONE);
//        } else {
//            layoutListContact.setVisibility(View.GONE);
//            layoutActionSecurity.setVisibility(View.VISIBLE);
//        }

        loadContact();

        Context context = getActivity();
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);

        return view;
    }

    private int newItems = 0;

    private IntentFilter filter = new IntentFilter(ACTION);

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String str = intent.getStringExtra(DATA);
            Bitmap bm = intent.getParcelableExtra("bit_map_contact");

            Log.e("Contact", str);

            try {
                JSONObject json = new JSONObject(str);
                ContactItem item = new ContactItem(json);

                boolean ready = false;
                for (ContactItem contact : data) {
//                    if (item.phones.size() == 0 && contact.name.equals(item.name)) {
//                        ready = true;
//                        break;
//                    }
//
//                    for (String phone : item.phones) {
//                        if (contact.phones.contains(phone)) {
//                            ready = true;
//                            break;
//                        }
//                    }
//
//                    for (String email : contact.emails) {
//                        if (item.emails.contains(email)) {
//                            ready = true;
//                            break;
//                        }
//                    }
                    if (contact.name.equals(item.name)) {
                        Log.e("QueAnh_add_contact", "same name");
                        if (item.phones.size() == 0 && item.emails.size() == 0) {
                            if (contact.phones.size() == 0 && contact.emails.size() == 0) {
                                ready = true;
                                break;
                            }
                        } else {
                            Log.e("QueAnh_add_contact", "difficult name");
                            if (item.phones.size() != 0 && item.emails.size() != 0) {
                                for (String phone : item.phones) {
                                    if (contact.phones.contains(phone)) {
                                        ready = true;
                                        break;
                                    }
                                }

                                if (!ready) {
                                    for (String email : contact.emails) {
                                        if (item.emails.contains(email)) {
                                            ready = true;
                                            break;
                                        }
                                    }
                                }
                            } else {
                                if (item.phones.size() != 0) {

                                    for (String phone : item.phones) {
                                        if (contact.phones.contains(phone)) {
                                            ready = true;
                                            break;
                                        }
                                    }
                                }

                                if (!ready && item.emails.size() != 0) {
                                    for (String email : contact.emails) {
                                        if (item.emails.contains(email)) {
                                            ready = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (!ready) {
                    Utils.getInstance().saveContact(getActivity(), json, bm);
                    data.add(item);

                    Collections.sort(data, new Comparator<ContactItem>() {
                        @Override
                        public int compare(ContactItem lhs, ContactItem rhs) {
                            return lhs.name.toLowerCase().compareTo(rhs.name.toLowerCase());
                        }
                    });

                    adapter.notifyDataSetChanged();

                    Constants.contactSize = data.size();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

//    private Handler handler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            clearSelectedItems();
//            adapter.notifyDataSetChanged();
//        }
//    };


    @Override
    public void onResume() {
        super.onResume();

        Context context = getActivity();
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);

        MAX_SELECTED_ITEMS = 10;
        String type = PreferenceUtils.getString(Constants.PURCHASED, getActivity(), "");
        if (type.equals(Constants.PURCHASE.BUY_ALL.type)) {
            MAX_SELECTED_ITEMS = 10000;
        } else if (type.equals(Constants.PURCHASE.BUY_ONE.type)) {
            MAX_SELECTED_ITEMS = MAX_SELECTED_ITEMS + 5;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Context context = getActivity();
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }

    public void loadContact() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<ContactItem> items = new ArrayList<>();

                FragmentActivity activity = getActivity();
                FragmentActivity context = activity;
                ContentResolver resolver = activity.getContentResolver();

                String where = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = 1 ";
                String sorted = ContactsContract.Contacts.DISPLAY_NAME;
                Cursor cursor = resolver.query(ContactsContract.Contacts.CONTENT_URI, null, where, null, sorted);

                if (cursor != null) {
                    int count = cursor.getCount();
                    if (count > 0) {
                        while (cursor.moveToNext()) {

                            if (items.size() % 20 == 19) {
                                if (activity != null) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            data.clear();
                                            data.addAll(items);
                                            adapter.notifyDataSetChanged();
//                                        handler.sendEmptyMessage(0);
                                        }
                                    });
                                }
                            }

                            List<String> phones = new ArrayList<>();
                            List<String> emails = new ArrayList<>();

                            int idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID);
                            String id = cursor.getString(idIndex);
                            int nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                            String name = cursor.getString(nameIndex);

                            int hasPhoneIndex = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
                            String hasPhone = cursor.getString(hasPhoneIndex);
                            if (Integer.parseInt(hasPhone) > 0) {
                                Cursor pCur = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                                while (pCur.moveToNext()) {
                                    int phoneIndex = pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                                    String phone = pCur.getString(phoneIndex);
                                    phones.add(phone);
                                }

                                pCur.close();
                            }

                            Cursor emailCur = resolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                            while (emailCur.moveToNext()) {
                                int emailIndex = emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA);
                                String email = emailCur.getString(emailIndex);
                                emails.add(email);
                            }
                            emailCur.close();

                            if (name == null || name.isEmpty()) {
                                if (emails.size() > 0) {
                                    name = emails.get(0);
                                } else if (phones.size() > 0) {
                                    name = phones.get(0);
                                } else {
                                    continue;
                                }
                            } else {
                                String c = name.charAt(0) + "";
                                name = c.toUpperCase() + name.substring(1);
                            }

                            String address = null;
                            Cursor addressCur = resolver.query(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, null, ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID + " = ?", new String[]{id}, null);
                            if (addressCur.moveToNext()) {
                                address = addressCur.getString(addressCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                            }

                            addressCur.close();

                            List<String> addresses = address == null ? new ArrayList<String>() : Arrays.asList(address);

                            String website = null;
                            Cursor websiteCur = resolver.query(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, null, ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID + " = ?", new String[]{id}, null);
                            if (websiteCur.moveToNext()) {
                                address = websiteCur.getString(websiteCur.getColumnIndex(ContactsContract.CommonDataKinds.Website.DATA));
                            }
                            websiteCur.close();

                            if (!emails.isEmpty() || !phones.isEmpty()) {
                                ContactItem item = new ContactItem(name, emails, phones, addresses, null, website, id, null, null);
                                items.add(item);
                            }
                        }

                    }

                    cursor.close();
                }

                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            data.clear();
                            data.addAll(items);
                            adapter.notifyDataSetChanged();
                            Constants.contactSize = data.size();
                        }
                    });
//                handler.sendEmptyMessage(0);
                }
            }
        }).start();

    }

    @Override
    public void onClear() {
        HomeFragment homeFragment = (HomeFragment) getParentFragment();
        String bumpType = homeFragment.getBumpType();
        if (!"contact".equals(bumpType) && selected.size() > 0) {
            for (ContactItem item : selected) {
                item.isSelected = false;
            }

            selected.clear();
            adapter.notifyDataSetChanged();
            updateCountView();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.layout_action_show_warrning:
//                HomeFragment home = (HomeFragment) getParentFragment();
//                home.showLayoutSecurity();
//                break;

            case R.id.layout_back_up:
//                String typeIAP = PreferenceUtils.getString(Constants.PURCHASED, getActivity(), "");
//
//                if (typeIAP.equals(Constants.PURCHASE.BUY_ALL.type)) {
//                    Log.e("Lu", "compress email");
//                    String name = PreferenceUtils.getString("email_user",getActivity(),"user@gmail.com");
//                    Intent email = new Intent(Intent.ACTION_SEND);
//                    email.putExtra(Intent.EXTRA_EMAIL, new String[]{name});
//                    email.putExtra(Intent.EXTRA_SUBJECT, "Fibu - Backup Contacts");
//                    email.putExtra(Intent.EXTRA_TEXT, "message");
//                    email.setType("message/rfc822");
//                    startActivity(Intent.createChooser(email, "Choose an Emails :"));
//
//                } else {
//                    showPopupPurchase();
//                }
                break;

            case R.id.layout_fist_all:
                String type = PreferenceUtils.getString(Constants.PURCHASED, getActivity(), "");

                if (type.equals(Constants.PURCHASE.BUY_ALL.type)) {
                    Log.e("Lu", "fist all");
                    if (!isSelectedAll) {
                        isSelectedAll = true;
                        selected.clear();
                        for (ContactItem item : data) {
                            item.isSelected = true;
                            onItemSelected(item);
                        }
                        HomeFragment homeFragment = (HomeFragment) getParentFragment();
                        if (Constants.isAnim) {
                            homeFragment.showAnimationBump();
                        }

                        adapter.notifyDataSetChanged();
                    }


                } else {
                    showPopupPurchase();
                }

                break;
        }
    }

    private void showPopupPurchase() {
        final Context context = getActivity();
        final AlertDialog dialogPurchase = new AlertDialog.Builder(context).create();
        View view = View.inflate(context, R.layout.bought_iap_question, null);
        final String userId = PreferenceUtils.getString(Constants.USER_ID, getActivity(), "");

        dialogPurchase.setView(view);

        View tvWantIt = view.findViewById(R.id.tv_want_it);
        View tvCancel = view.findViewById(R.id.tv_cancel);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.tv_cancel:
                        dialogPurchase.dismiss();
                        break;
                    case R.id.tv_want_it:
                        dialogPurchase.dismiss();

                        Intent intent = new Intent(context, AllThingsScreen.class);
                        intent.putExtra(AllThingsScreen.EXTRA_USER_ID, userId);
                        getActivity().startActivityForResult(intent, RC_PURCHASE);

                        break;
                }
            }
        };

        tvWantIt.setOnClickListener(onClickListener);
        tvCancel.setOnClickListener(onClickListener);
        dialogPurchase.show();
    }
}
