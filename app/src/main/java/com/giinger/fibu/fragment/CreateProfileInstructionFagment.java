package com.giinger.fibu.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import com.giinger.fibu.R;
import com.giinger.fibu.activity.CreateProfileScreen;

public class CreateProfileInstructionFagment extends Fragment {

    private RelativeLayout rlCreateCard;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.create_profile_instruction_fragment, null, false);

        rlCreateCard = (RelativeLayout) view.findViewById(R.id.layout_create);
        rlCreateCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateProfileScreen.nextPage(2);
                showKeyBoard();
            }
        });

        return view;
    }

    private void showKeyBoard() {
        FragmentActivity activity = getActivity();
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }
}
