package com.giinger.fibu.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.giinger.fibu.R;
import com.giinger.fibu.activity.ChatBoxScreen;
import com.giinger.fibu.activity.HomeScreen;
import com.giinger.fibu.activity.WebScreen;
import com.giinger.fibu.adapter.HomeScreenAdapter;
import com.giinger.fibu.element.CustomButton;
import com.giinger.fibu.element.CustomViewPager;
import com.giinger.fibu.element.PopupBumpFailHolder;
import com.giinger.fibu.element.PopupConnectingHolder;
import com.giinger.fibu.element.PopupMultipleConnectedHolder;
import com.giinger.fibu.model.ChatBoxItem;
import com.giinger.fibu.model.ContactItem;
import com.giinger.fibu.model.Friend;
import com.giinger.fibu.model.PhotoItem;
import com.giinger.fibu.receiver.ParseReceiver;
import com.giinger.fibu.service.GPSTracker;
import com.giinger.fibu.util.AccelerometerManager;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.ClearItems;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.RushOrmManager;
import com.giinger.fibu.util.SelectedItems;
import com.giinger.fibu.util.Utils;
import com.koushikdutta.async.future.Future;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeFragment extends Fragment implements SelectedItems, AccelerometerManager.AccelerometerListener, View.OnClickListener, View.OnTouchListener, BumpUtils.ConnectConfirmCallback {

    public static final int TIME_OUT = 5 * 60 * 1000;

    private CustomViewPager pager;
    private CirclePageIndicator indicator;
    private View imgChatBox, imgShowSettings, imgHideSettings;
    private View popupConnecting, layoutSettings, popupBumpFail, popupMultipleConnected;
    private SeekBar sbAdjSensitive;

    private Animation animLeft;
    private Animation animRight;
    private Animation moveDow;

    private RelativeLayout layoutGoIt;
    public RelativeLayout layoutBumpAnimation;

    private RelativeLayout layoutAnimLeft;
    private RelativeLayout layoutAnimRight;

    private CustomButton btnGoIt;
    private TextView txtBadge;

    private RelativeLayout rlIndicator;
//    private RelativeLayout layoutSecurity;
//    private Button btnDeny;
//    private Button btnAllow;

    public static final String EXTRA_COUNT_BADGE = "extra_count_badge";
    public static final String ACTION = "fibu.activity.unread";

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int count = intent.getIntExtra(EXTRA_COUNT_BADGE, 0);
            if (count == 0) {
                txtBadge.setVisibility(View.INVISIBLE);
            } else {
                String text = String.valueOf(count);
                txtBadge.setText(text);
                txtBadge.setVisibility(View.VISIBLE);
                PreferenceUtils.saveInt(Constants.COUNT_BADGE, getActivity(), Integer.parseInt(txtBadge.getText().toString()));
            }
        }
    };
    //
    private IntentFilter filter = new IntentFilter(ACTION);

    private TextView txtNotification;

    private Handler handler = new Handler();

    private IntentFilter notifyFilter = new IntentFilter(ParseReceiver.NOTIFY_ACTION);

    private BroadcastReceiver notifyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            String message = intent.getStringExtra(ParseReceiver.KEY_PUSH_MESSAGE);
            txtNotification.setText(message);
            txtNotification.setVisibility(View.VISIBLE);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    txtNotification.setVisibility(View.GONE);
                }
            }, 3000);

            txtNotification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtNotification.setVisibility(View.GONE);
                    txtNotification.setOnClickListener(null);

                    String data = intent.getStringExtra(ParseReceiver.KEY_PUSH_DATA);
                    Intent fibu = new Intent("fibu.action.chat");
                    fibu.putExtra(ParseReceiver.KEY_PUSH_DATA, data);
                    context.sendBroadcast(fibu);
                }
            });


        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private HomeScreenAdapter adapter;

    private String makeFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }

    public Fragment getCurrentFragment() {
        int index = pager.getCurrentItem();
        String tag = makeFragmentName(R.id.pager, index);
        FragmentManager fragmentManager = getChildFragmentManager();
        return fragmentManager.findFragmentByTag(tag);
    }

    private View txtBumpFailure;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);

        txtBumpFailure = view.findViewById(R.id.txtBumpFailure);

        txtNotification = (TextView) view.findViewById(R.id.txtNotification);

        txtBadge = (TextView) view.findViewById(R.id.txtBadge);

        pager = (CustomViewPager) view.findViewById(R.id.pager);
        rlIndicator = (RelativeLayout) view.findViewById(R.id.rlIndicator);

        FragmentManager fragmentManager = getChildFragmentManager();
        adapter = new HomeScreenAdapter(fragmentManager);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(HomeScreenAdapter.COUNT);
        Constants.start = true;

        layoutAnimLeft = (RelativeLayout) view.findViewById(R.id.layout_anim_left);
        layoutAnimRight = (RelativeLayout) view.findViewById(R.id.layout_anim_right);

        layoutGoIt = (RelativeLayout) view.findViewById(R.id.layout_go_it);
        layoutBumpAnimation = (RelativeLayout) view.findViewById(R.id.layout_bump_action);
        btnGoIt = (CustomButton) view.findViewById(R.id.btn_got_it);
        btnGoIt.setOnClickListener(this);

        animLeft = AnimationUtils.loadAnimation(getActivity(), R.anim.left_to_right);
        animRight = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);

        moveDow = AnimationUtils.loadAnimation(getActivity(), R.anim.move_down);
        moveDow.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (popupBumpFail.getVisibility() == View.VISIBLE) {
                            popupBumpFail.setVisibility(View.GONE);
                            lockBump(false);
                        }
                    }
                }, 2000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Constants.isAnim = false;
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        indicator.setViewPager(pager);
        indicator.setStrokeWidth(0);
        Resources resources = getResources();
        int grey = resources.getColor(R.color.grey);
        int white = resources.getColor(R.color.white);
        indicator.setFillColor(white);
        indicator.setPageColor(grey);

        indicator.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                Log.e("Pager", "Position " + position);
                clearSelected();
                if (position == 0) {
                    layoutBumpAnimation.setVisibility(View.VISIBLE);
                }
//                else {
//                    if (position == 3) {
//                        if (PreferenceUtils.getBoolean("show_security", getActivity(), true)) {
//                            if (Constants.isShowWarrning) {
//                                showLayoutSecurity();
//                            }
//
//                        }
//                    } else {
//                        goneLayoutSecurity();
//                    }
//                }
            }
        });

        imgChatBox = view.findViewById(R.id.imgActivity);
        imgChatBox.setOnClickListener(this);

        imgShowSettings = view.findViewById(R.id.imgShowSettings);
        imgShowSettings.setOnClickListener(this);

        imgHideSettings = view.findViewById(R.id.imgHideSettings);
        imgHideSettings.setOnClickListener(this);

        popupConnecting = view.findViewById(R.id.layoutPopupConnecting);
        popupConnectingHolder = new PopupConnectingHolder(popupConnecting);

        popupBumpFail = view.findViewById(R.id.view_bump_fail);
        popupBumpFailHolder = new PopupBumpFailHolder(popupBumpFail);
        popupBumpFailHolder.layoutActionHide.setOnClickListener(this);
        popupBumpFailHolder.layoutAnimation.setOnClickListener(this);

        popupMultipleConnected = view.findViewById(R.id.layoutMultipleConnect);
        popupMultipleConnectedHolder = new PopupMultipleConnectedHolder(popupMultipleConnected);
        popupMultipleConnectedHolder.txtPopupCancelBumpAgain.setOnClickListener(this);

        layoutSettings = view.findViewById(R.id.layoutSettings);

        View txtInviteBySms = view.findViewById(R.id.txtInviteBySms);
        txtInviteBySms.setOnClickListener(this);

        View txtShowOnYoutube = view.findViewById(R.id.txtShowOnYoutube);
        txtShowOnYoutube.setOnClickListener(this);

        View txtShowHowItWork = view.findViewById(R.id.txtShowHowItWork);
        txtShowHowItWork.setOnClickListener(this);

        sbAdjSensitive = (SeekBar) view.findViewById(R.id.sbAdjSensitive);
        sbAdjSensitive.setOnTouchListener(this);

        Context context = getActivity();
        int progress = PreferenceUtils.getInt(Constants.SETTING_SENSITIVE, context, Constants.DEFAULT_SENSITIVE);
        sbAdjSensitive.setProgress(progress);

        Utils.getInstance().checkLocationEnable(getActivity());

        rotate = AnimationUtils.loadAnimation(context, R.anim.rotate);

//        layoutSecurity = (RelativeLayout) view.findViewById(R.id.layout_security);
//        btnDeny = (Button) view.findViewById(R.id.btn_deny);
//        btnAllow = (Button) view.findViewById(R.id.btn_allow);
//        btnDeny.setOnClickListener(this);
//        btnAllow.setOnClickListener(this);

        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);
        LocalBroadcastManager.getInstance(context).registerReceiver(notifyReceiver, notifyFilter);
        return view;
    }

    private Animation rotate;

    private int progress = 0;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                int current = sbAdjSensitive.getProgress();
                switch (current) {
                    case 0:
                    case 50:
                    case 100:
                        break;
                    default:
                        if (current < progress) {
                            current = current > 50 ? 50 : 0;
                        } else if (current > progress) {
                            current = current > 50 ? 100 : 50;
                        }
                        progress = current;
                        sbAdjSensitive.setProgress(current);
                        break;
                }

                Context context = getActivity();
                PreferenceUtils.saveInt(Constants.SETTING_SENSITIVE, context, current);
                Utils.getInstance().setSensitivity(current);
                return true;
            case MotionEvent.ACTION_DOWN:
                progress = sbAdjSensitive.getProgress();
            default:
                return false;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        FragmentActivity context = getActivity();
        switch (id) {
            case R.id.imgActivity:
                FragmentActivity activity = context;
                if (activity instanceof HomeScreen) {
                    ((HomeScreen) activity).openDrawer();
                }
                break;
            case R.id.imgShowSettings:
                layoutSettings.setVisibility(View.VISIBLE);
                break;
            case R.id.imgHideSettings:
                layoutSettings.setVisibility(View.GONE);
                break;
            case R.id.txtInviteBySms:
                Utils.sendSms(context);
                break;
            case R.id.txtShowHowItWork:
            case R.id.txtShowOnYoutube:
                String key = id == R.id.txtShowHowItWork ? Constants.HOW_IT_WORK_URL : Constants.APP_LINK;
                String url = PreferenceUtils.getString(key, context, "");
                Intent webIntent = new Intent(context, WebScreen.class);
                webIntent.putExtra(WebScreen.EXTRA_URL, url);
                startActivity(webIntent);
                break;
            case R.id.btn_got_it:
                layoutGoIt.setVisibility(View.GONE);
                ((HomeScreen) getActivity()).lockDrawer(false);
                lockBump(false);
                break;
            case R.id.view_bump_fail:
                popupBumpFail.setVisibility(View.GONE);
                lockBump(false);
                break;

            case R.id.layout_animation:
                popupBumpFail.setVisibility(View.GONE);
                lockBump(false);
                break;
            case R.id.txtPopupCancelBumpAgain:
                popupMultipleConnected.setVisibility(View.GONE);
                lockBump(false);
                break;
//            case R.id.btn_deny:
//                goneLayoutSecurity();
//                Constants.isShowWarrning = false;
//                ContactFragment.layoutListContact.setVisibility(View.GONE);
//                ContactFragment.layoutActionSecurity.setVisibility(View.VISIBLE);
//                break;
//
//            case R.id.btn_allow:
//                goneLayoutSecurity();
//                ContactFragment.layoutListContact.setVisibility(View.VISIBLE);
//                ContactFragment.layoutActionSecurity.setVisibility(View.GONE);
//                PreferenceUtils.saveBoolean("show_security", getActivity(), false);
        }
    }

    public void checkGpsService() {
        boolean checkLocationEnable = Utils.getInstance().checkLocationEnable(getActivity());
        if (checkLocationEnable) {
            Constants.canBump = true;

            if (Constants.start) {
                Constants.start = false;
                layoutBumpAnimation.setVisibility(View.VISIBLE);
                layoutAnimLeft.startAnimation(animLeft);
                layoutAnimRight.startAnimation(animRight);
            }

            if (PreferenceUtils.getBoolean(Constants.FIRST_START, getActivity(), true)) {
                layoutGoIt.setVisibility(View.VISIBLE);
                PreferenceUtils.saveBoolean(Constants.FIRST_START, getActivity(), false);
                lockBump(true);
                ((HomeScreen) getActivity()).lockDrawer(true);
            }
        } else {
            Constants.start = true;
            Constants.canBump = false;
            Utils.getInstance().forcePopupLocation(getActivity());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        boolean showVideo = PreferenceUtils.getBoolean(Constants.SHOW_VIDEO_TOUR, getContext(), true);
        if (!showVideo) {
            checkGpsService();
        }

        Context context = getActivity();
        if (AccelerometerManager.isSupported(context)) {
            Utils.getInstance().setSensitivity(PreferenceUtils.getInt(Constants.SETTING_SENSITIVE, getActivity(), Constants.DEFAULT_SENSITIVE));
            AccelerometerManager.startListening(this);
        }

        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);
        LocalBroadcastManager.getInstance(context).registerReceiver(notifyReceiver, notifyFilter);
    }

    @Override
    public void onPause() {
        super.onPause();

        Context context = getActivity();
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
        LocalBroadcastManager.getInstance(context).unregisterReceiver(notifyReceiver);

        if (AccelerometerManager.isListening()) {
            AccelerometerManager.stopListening();
        }
    }

    private static Friend friend = null;

    private void handlerBumpResponse(JSONObject data) {
        try {
            Log.e("Bump", "Response " + data);
            JSONObject response = data.getJSONObject(Constants.RESPONSE);
            boolean success = response.getBoolean(Constants.SUCCESS);

            FragmentActivity activity = getActivity();

            if (success) {
                friend = new Friend(response);

                if (activity instanceof HomeScreen) {
                    ((HomeScreen) activity).closeDrawer();
                }

                showPopupConnecting();
            } else {
                String error = response.getString("error");
                switch (error) {
                    case "friendNotFound":
                    case "bothNotShare":
//                        popupBumpFail.setVisibility(View.VISIBLE);
//                        popupBumpFailHolder.layoutAnimation.startAnimation(moveDow);

                        Constants.canBump = true;

                        if (activity instanceof HomeScreen) {
                            boolean ok = ((HomeScreen) activity).showBumpFailure();
                            if (ok) {
                                break;
                            }
                        }

                        txtBumpFailure.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                txtBumpFailure.setVisibility(View.GONE);
                            }
                        }, 3000);
                        break;
                    case "multiBumped":
                        if (activity instanceof HomeScreen) {
                            ((HomeScreen) activity).closeDrawer();
                        }

                        showPopupMultipleConnected();
                        break;
//                    case "bothNotShare":
//                        popupBumpFail.setVisibility(View.VISIBLE);
//                        popupBumpFailHolder.layoutAnimation.startAnimation(moveDow);
//                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private List<Object> selected = new ArrayList<>();

    public void clearSelected() {
        selected.clear();

        List<Fragment> fragments = getChildFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment instanceof ClearItems) {
                ((ClearItems) fragment).onClear();
            }
        }

        if (pager.getCurrentItem() != 0) {
            goneLayoutAnimation();
        }
    }

    private void lockBump(boolean lock) {
        Constants.canBump = !lock;
//        FragmentActivity activity = getActivity();
//        if (activity instanceof HomeScreen) {
//            ((HomeScreen) activity).lockDrawer(lock);
//        }
    }

    private void doBump() {
        GPSTracker gpsTracker = GPSTracker.getInstance();
        Location location = gpsTracker.getLocation();
        Log.e("Location", location + "");
        if (location == null) {
            return;
        }

        if (!Constants.canBump) {
            return;
        }

        lockBump(true);

        Context context = getActivity();
        AQuery aQuery = new AQuery(context);
        Map<String, String> params = new HashMap<>();

        params.put("latitude", location.getLatitude() + "");
        params.put("longitude", location.getLongitude() + "");

//        params.put("latitude", "10.806803" + new Random().nextInt(10));
//        params.put("longitude", "106.667936" + new Random().nextInt(10));

        String userID = PreferenceUtils.getString(Constants.USER_ID, context, null);
        params.put("user_id", userID);

        String type = getBumpType();
        params.put("my_shared_type", type);

        Log.e("Params", params + "");

        String url = AppUtils.getBumpUrl(context);
        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {

                    Fragment currentFragment = getCurrentFragment();
                    if (currentFragment instanceof ProfileFragment) {
                        boolean isEditMode = ((ProfileFragment) currentFragment).isEditMode;
                        if (isEditMode) {
                            return;
                        }
                    }

                    handlerBumpResponse(object);
                } else {
                    lockBump(false);
                }
            }
        });
    }

    @Override
    public void onShake(float force) {
        Log.e("Shake", force + "");

        Fragment currentFragment = getCurrentFragment();
        if (currentFragment instanceof ProfileFragment) {
            boolean isEditMode = ((ProfileFragment) currentFragment).isEditMode;
            if (isEditMode) {
                return;
            }
        }

        if (Constants.canBump) {
            Log.e("QueAnh", "shake");
            doBump();
        } else {
            Log.e("Shake", "Skip");
        }
    }

    public String getBumpType() {
        int currentPage = pager.getCurrentItem();
        String type = null;
        int count = selected.size();
        switch (currentPage) {
            case 0:
                type = "profile";
                break;
            case 1:
                type = count > 0 ? "photo" : null;
                break;
            case 3:
                type = count > 0 ? "contact" : null;
                break;
        }
        return type;
    }

    private PopupConnectingHolder popupConnectingHolder;
    private PopupBumpFailHolder popupBumpFailHolder;
    private PopupMultipleConnectedHolder popupMultipleConnectedHolder;

    private void showPopupConnecting() {
        popupConnectingHolder.txtFirstName.setText(friend.firstName);
        popupConnectingHolder.txtLastName.setText(friend.lastName);
        popupConnectingHolder.txtJobTitle.setText("null".equals(friend.jobTitle) ? "" : friend.jobTitle);

        popupConnectingHolder.txtPopupAction.setVisibility(View.VISIBLE);
        popupConnectingHolder.imgStatus.setVisibility(View.GONE);

        popupConnectingHolder.txtPopupCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (request != null) {
                    Log.e("Cancel", "AQuery");
                    request.cancel(true);
                }

                if (asyncTask != null) {
                    Log.e("Cancel", "Task");
                    asyncTask.cancel(true);
                }

                hidePopupConnecting();

                Context context = getActivity();
                BumpUtils.confirmConnect(null, context, friend.connectId, BumpUtils.ConnectConfirmCallback.ACTION_CANCEL, friend.id);
            }
        });

        Context context = getActivity();
        String avatarUrl = AppUtils.getAvatarByName(context, friend.avatar);
        AppUtils.loadAvatar(context, popupConnectingHolder.imgAvatar, avatarUrl, true);
        popupConnecting.setVisibility(View.VISIBLE);

        View.OnClickListener l = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupConnectingHolder.layoutPopupAction.setOnClickListener(null);
                if (friend.sendType.isEmpty() && friend.receiveType.isEmpty()) {
                    gotoChatBox();
                    hidePopupConnecting();
                } else {
                    transferData();
                }
            }
        };

        popupConnectingHolder.layoutPopupAction.setOnClickListener(l);
        popupConnectingHolder.txtPopupAction.setOnClickListener(l);
    }

    private void showPopupMultipleConnected() {
        popupMultipleConnected.setVisibility(View.VISIBLE);
        View.OnClickListener l = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMultipleConnected.setVisibility(View.GONE);
                lockBump(false);
                doBump();
            }
        };

        popupMultipleConnectedHolder.layoutPopupAction.setOnClickListener(l);
        popupMultipleConnectedHolder.txtPopupBumpAgain.setOnClickListener(l);
    }

    public View getSettingPopup() {
        return layoutSettings;
    }

    private void transferData() {
        popupConnectingHolder.txtPopupAction.setVisibility(View.GONE);
        popupConnectingHolder.imgStatus.setVisibility(View.VISIBLE);
        popupConnectingHolder.txtPopupAction.setOnClickListener(null);
        popupConnectingHolder.imgStatus.startAnimation(rotate);
        popupConnectingHolder.txtPopupCancel.setOnClickListener(null);

        Context context = getActivity();
        BumpUtils.confirmConnect(this, context, friend.connectId, BumpUtils.ConnectConfirmCallback.ACTION_CONNECT, friend.id);
    }

    public void doTransferData() {
        if (!friend.sendType.isEmpty()) {
            upload();
        } else {
            connectHandler.sendEmptyMessage(1);
        }

        if (!friend.receiveType.isEmpty()) {
            download();
        } else {
            connectHandler.sendEmptyMessage(1);
        }
    }

    private AsyncTask asyncTask;
    private Future request;

    private Handler connectHandler = new Handler() {
        private boolean firstTime = true;

        @Override
        public void handleMessage(Message msg) {
            Log.e("Handler", "Received " + firstTime + " " + msg.what);
            if (msg.what == -1) {
                hidePopupConnecting();
                firstTime = true;
                return;
            }

            if (firstTime) {
                firstTime = false;
            } else {
                firstTime = true;
                gotoChatBox();
                hidePopupConnecting();
            }
        }
    };

    private void upload() {
        ArrayList<String> selected = new ArrayList<>();

        switch (friend.sendType) {
            case "profile":
                // do nothing
                break;
            case "photo":
                for (Object item : this.selected) {
                    if (item instanceof PhotoItem) {
                        String path = ((PhotoItem) item).data;
                        selected.add(path);
                    }
                }
                break;
            case "contact":
                Collections.sort(this.selected, new Comparator<Object>() {
                    @Override
                    public int compare(Object lhs, Object rhs) {
                        ContactItem c1 = (ContactItem) lhs;
                        ContactItem c2 = (ContactItem) rhs;
                        return c1.name.toLowerCase().compareTo(c2.name.toLowerCase());
                    }
                });

                for (Object item : this.selected) {
                    if (item instanceof ContactItem) {
                        ContactItem contactItem = (ContactItem) item;

                        try {
                            JSONObject contact = new JSONObject();
                            contact.put("name", contactItem.name);
                            contact.put("website", contactItem.website);
                            contact.put("address", contactItem.address);

                            JSONArray emails = new JSONArray();
                            if (contactItem.emails != null) {
                                for (String email : contactItem.emails) {
                                    emails.put(email);
                                }
                            }

                            JSONArray phones = new JSONArray();
                            if (contactItem.phones != null) {
                                for (String phone : contactItem.phones) {
                                    phones.put(phone);
                                }
                            }

                            contact.put("emails", emails);
                            contact.put("phones", phones);

                            String data = contact.toString();
                            selected.add(data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
        }

        Context context = getActivity();
        BumpUtils.upload(context, friend, selected, new BumpUtils.Callback() {
            @Override
            public void callback() {
                connectHandler.sendEmptyMessage(0);
            }
        });

    }

    private void download() {
        Context context = getActivity();
        BumpUtils.download(context, friend, new BumpUtils.Callback() {
            @Override
            public void callback() {
                connectHandler.sendEmptyMessage(0);
            }
        });
    }

    public void hidePopupConnecting() {
        request = null;
        asyncTask = null;

        lockBump(false);
        popupConnecting.setVisibility(View.GONE);
        popupConnectingHolder.imgStatus.clearAnimation();
    }

    private void gotoChatBox() {
        clearSelected();

        if (!PreferenceUtils.getString(Constants.FRIEND_ID, getActivity(), "").equals("friend" + friend.id)) {
            //add friend profile
            ChatBoxItem itemFriend = new ChatBoxItem();
            JSONObject jsonFriend = Utils.getInstance().getProfileFriend(friend);

            itemFriend.setFriendId(friend.id);
            itemFriend.setJsonData(jsonFriend);
            RushOrmManager.getInstance().storeSingleChatItem(itemFriend);

            //add seft profile
            ChatBoxItem itemSelf = new ChatBoxItem();
            String selfInfo = PreferenceUtils.getString(Constants.USER_INFO_JSON, getActivity(), "{}");
            try {
                JSONObject selfJson = new JSONObject(selfInfo);
                JSONObject jsonSeft = Utils.getInstance().getSelfProfile(friend.id, selfJson, 0);

                itemSelf.setFriendId(friend.id);
                itemSelf.setJsonData(jsonSeft);
                RushOrmManager.getInstance().storeSingleChatItem(itemSelf);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //store friend id
            PreferenceUtils.saveString(Constants.FRIEND_ID, getActivity(), "friend" + friend.id);
        }


        Context context = getActivity();
        int count = PreferenceUtils.getInt(Constants.TIMES_BUMP_SUCCESS, context, 0) + 1;
        PreferenceUtils.saveInt(Constants.TIMES_BUMP_SUCCESS, context, count);

        Intent intent = new Intent(context, ChatBoxScreen.class);
        intent.putExtra(ChatBoxScreen.EXTRA_ID, friend.id);
        intent.putExtra(ChatBoxScreen.EXTRA_NAME, friend.firstName + " " + friend.lastName);
        intent.putExtra(ChatBoxScreen.EXTRA_AVATAR, friend.avatar);
        intent.putExtra(ChatBoxScreen.EXTRA_FROM_BUM, true);
        startActivity(intent);
    }

    @Override
    public void onSelected(Object item, boolean select) {
        Log.e("Selected", item + " " + select);
        if (select) {
            selected.add(item);
        } else {
            selected.remove(item);
        }
    }

    public void showAnimationBump() {
        layoutBumpAnimation.setVisibility(View.VISIBLE);
        layoutAnimLeft.startAnimation(animLeft);
        layoutAnimRight.startAnimation(animRight);

    }

    public void goneLayoutAnimation() {
        layoutBumpAnimation.setVisibility(View.GONE);
        Constants.isAnim = true;
    }

    public void disableViewpager() {
        pager.setSwipe(false);
        rlIndicator.setVisibility(View.GONE);
    }

    public void enableViewpager() {
        pager.setSwipe(true);
        rlIndicator.setVisibility(View.VISIBLE);
    }

//    public void showLayoutSecurity() {
//        layoutSecurity.setVisibility(View.VISIBLE);
//    }
//
//    public void goneLayoutSecurity() {
//        layoutSecurity.setVisibility(View.GONE);
//    }
}
