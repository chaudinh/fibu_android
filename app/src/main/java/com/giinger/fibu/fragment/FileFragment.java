package com.giinger.fibu.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.giinger.fibu.R;
import com.giinger.fibu.activity.FileDocumentScreen;
import com.giinger.fibu.activity.FileMusicScreen;
import com.giinger.fibu.activity.FilePhysicalScreen;
import com.giinger.fibu.activity.FileVideoScreen;
import com.giinger.fibu.util.Constants;

public class FileFragment extends Fragment implements View.OnClickListener {

    private RelativeLayout headerFile;
    public static FrameLayout layoutContent;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.file_fragment, null, false);

        headerFile = (RelativeLayout) view.findViewById(R.id.header_files);
        layoutContent = (FrameLayout) view.findViewById(R.id.content_frame_file);

        ImageView imgMusic = (ImageView) view.findViewById(R.id.imgChooseMusic);
        ImageView imgVideo = (ImageView) view.findViewById(R.id.imgChooseVideo);

        if (Constants.isChat) {
            headerFile.setVisibility(View.GONE);
            imgMusic.setVisibility(View.INVISIBLE);
            imgVideo.setImageResource(R.drawable.choose_music);
        } else {
            headerFile.setVisibility(View.VISIBLE);
            imgMusic.setVisibility(View.VISIBLE);
            imgVideo.setImageResource(R.drawable.choose_video);
            imgMusic.setImageResource(R.drawable.choose_music);
        }

        View[] views = {
                view.findViewById(R.id.imgChooseVideo),
                view.findViewById(R.id.imgChooseMusic),
                view.findViewById(R.id.imgChooseDropbox),
                view.findViewById(R.id.imgChooseFolder)
        };

        for (View v: views) {
            v.setOnClickListener(this);
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Activity activity = getActivity();

        switch (id) {
            case R.id.imgChooseVideo:

                if (Constants.isChat) {
//                    Fragment fragment = new FileVideoFragment();
//                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.content_frame_file, fragment).commit();
//                    layoutContent.setVisibility(View.VISIBLE);
                    Fragment fragment = new FileMusicFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame_file, fragment).commit();
                    layoutContent.setVisibility(View.VISIBLE);
                } else {
                    Intent chooseVideoIntent = new Intent(activity, FileVideoScreen.class);
                    startActivity(chooseVideoIntent);
                }

                break;
            case R.id.imgChooseMusic:

                if (Constants.isChat) {
                    Fragment fragment = new FileMusicFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame_file, fragment).commit();
                    layoutContent.setVisibility(View.VISIBLE);
                } else {
                    Intent chooseMusicIntent = new Intent(activity, FileMusicScreen.class);
                    startActivity(chooseMusicIntent);
                }

                break;
            case R.id.imgChooseDropbox:

                if (Constants.isChat) {
                    Fragment fragment = new FileDropBoxFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame_file, fragment).commit();
                    layoutContent.setVisibility(View.VISIBLE);
                } else {
                    Intent documentIntent = new Intent(activity, FileDocumentScreen.class);
                    startActivity(documentIntent);
                }
                break;

            case R.id.imgChooseFolder:

                if (Constants.isChat) {
                    Fragment fragment = new FilePhysicalFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame_file, fragment).commit();
                    layoutContent.setVisibility(View.VISIBLE);
                } else {
                    Intent physicalIntent = new Intent(activity, FilePhysicalScreen.class);
                    startActivity(physicalIntent);
                }
                break;

        }
    }

    public static void backAction(FragmentManager fragmentManager) {
        layoutContent.setVisibility(View.GONE);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(fragmentManager.findFragmentById(R.id.content_frame_file)).commit();
    }
}
