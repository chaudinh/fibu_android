package com.giinger.fibu.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;

import com.giinger.fibu.R;
import com.giinger.fibu.adapter.TabsAdapter;
import com.giinger.fibu.util.Constants;

public class SharingBarFragment extends Fragment {

    private TabHost mTab;
    private ViewPager mViewPager;
    private TabsAdapter mTabsAdapter;
    public String currentTab;
    private Button btnOff;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.sharing_bar_layout, container, false);
        btnOff = (Button) view.findViewById(R.id.btn_off_bar);

        btnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.isChat = true;
                Constants.offBar = true;
                getActivity().onBackPressed();
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.isChat = true;
                Constants.offBar = true;
                getActivity().onBackPressed();
            }
        });

        mTab = (TabHost) view.findViewById(android.R.id.tabhost);
        mTab.setup();

        mTab.getTabWidget().setStripEnabled(false);

        mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mTabsAdapter = new TabsAdapter(getActivity(), mTab, mViewPager);
        mViewPager.setOffscreenPageLimit(4);

        mTabsAdapter.addTab(mTab.newTabSpec("Photo").setIndicator("", getResources().getDrawable(R.drawable.photo_in_menu)), PhotoFragmentSharing.class, null);
        mTabsAdapter.addTab(mTab.newTabSpec("File").setIndicator("", getResources().getDrawable(R.drawable.file_in_menu)), FileFragment.class, null);
        mTabsAdapter.addTab(mTab.newTabSpec("Contanct").setIndicator("", getResources().getDrawable(R.drawable.contact_in_menu)), ContactFragment.class, null);
        mTabsAdapter.addTab(mTab.newTabSpec("Location").setIndicator("", getResources().getDrawable(R.drawable.location_in_menu)), LocationFragment.class, null);

        mTabsAdapter.notifyDataSetChanged();

        TabWidget widget = mTab.getTabWidget();
        for(int i = 0; i < widget.getChildCount(); i++) {
            View v = widget.getChildAt(i);

            ImageView img = (ImageView)v.findViewById(android.R.id.icon);
            img.setScaleType(ImageView.ScaleType.FIT_XY);

            v.setBackgroundResource(R.drawable.tab_indicator);
        }

        return view;
    }

    public int getPosition(){
        return mTabsAdapter.getPositionTab();
    }
}
