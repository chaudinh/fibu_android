package com.giinger.fibu.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.giinger.fibu.R;
import com.giinger.fibu.activity.FileMusicScreen;
import com.giinger.fibu.adapter.FileMusicAdapter;
import com.giinger.fibu.model.FileItem;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.ExternalStorage;
import com.giinger.fibu.util.OnItemSharing;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class FileMusicFragment extends Fragment {

    public static final int MAX_SELECTED_ITEMS = 5;

    private ListView lvChooseMusic;
    private List<FileItem> musicData;
    private RelativeLayout headerMusic;
    private FileMusicAdapter musicAdapter;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Collections.sort(musicData, new Comparator<FileItem>() {
                public int compare(FileItem f1, FileItem f2) {
                    return f1.name.toLowerCase().compareTo(f2.name.toLowerCase());
                }
            });
            musicAdapter.notifyDataSetChanged();
        }
    };

    private void loadAllFiles() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e("Music", FileMusicScreen.MUSICS.getAbsolutePath());
                listAllFile(FileMusicScreen.MUSICS);

                File appFolder = BumpUtils.getAppFolder();
                listAllFile(appFolder);

                if (ExternalStorage.isAvailable()) {
                    File sdCard = new File("/storage/sdcard1/Music");
                    if (!sdCard.exists()) {
                        Map<String, File> externalLocations = ExternalStorage.getAllStorageLocations();
                        if (externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD) == null) {
                            sdCard = new File("/mnt/sdcard/ext_sd/Music");
                        } else {
                            sdCard = new File(externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD).toString() + "/Music");
                        }
                    }

                    listAllFile(sdCard);
                }
                handler.sendEmptyMessage(0);
            }
        }).start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.file_music_screen, container, false);

        lvChooseMusic = (ListView) view.findViewById(R.id.lvChooseMusic);

        headerMusic = (RelativeLayout) view.findViewById(R.id.header_file_music);

        if (Constants.isChat) {
            headerMusic.setVisibility(View.GONE);
        } else {
            headerMusic.setVisibility(View.VISIBLE);
        }

        musicData = new ArrayList<>();

        musicAdapter = new FileMusicAdapter(musicData);
        lvChooseMusic.setAdapter(musicAdapter);
        lvChooseMusic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FragmentActivity activity = getActivity();
                if (activity instanceof OnItemSharing) {
                    FileItem child = musicData.get(position);
                    try {
                        JSONObject data = new JSONObject();
                        data.put("data", child.path);
                        ((OnItemSharing) activity).onShare("music", data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        loadAllFiles();

        return view;
    }

    private void listAllFile(File root) {
        File[] files = root.listFiles();
        if (files == null) {
            return;
        }

        File cache = BumpUtils.getCacheFolder();
        if (root.getAbsolutePath().equals(cache.getAbsolutePath())) {
            return;
        }

        for (File file : files) {
            boolean isDir = file.isDirectory();
            if (isDir) {
                listAllFile(file);
            } else {
                String name = file.getName();
                boolean isMusic = name.toLowerCase().endsWith(".mp3");
                if (isMusic) {
                    String path = file.getAbsolutePath();
                    FileItem item = new FileItem(path, name, file.length());
                    musicData.add(item);
                }
            }
        }
    }

}
