package com.giinger.fibu.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.giinger.fibu.R;
import com.giinger.fibu.activity.BaseFileScreen;
import com.giinger.fibu.activity.ChatBoxScreen;
import com.giinger.fibu.adapter.PhotoAdapter;
import com.giinger.fibu.model.PhotoGroup;
import com.giinger.fibu.model.PhotoItem;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.ClearItems;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.ExternalStorage;
import com.giinger.fibu.util.OnItemSharing;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.SelectedItems;
import com.giinger.fibu.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class PhotoFragment extends Fragment implements ClearItems {

    public static int MAX_SELECTED = 6;

    protected TextView txtCountSelectedItem;
    private RelativeLayout headerPhoto;
    private String imgPath;

    protected List<PhotoItem> selected = new ArrayList<>();
    private long size = 0;

    public int getCountItemSelected() {
        return selected.size();
    }

    public int addItemSelected(PhotoItem item) {
        if (item.isSelected) {
            long newSize = size + item.size;
            if (newSize > BaseFileScreen.MAX_SIZE) {
                showAlertBigFiles();
                return -1;
            }
        }

        Fragment fragment = getParentFragment();
        if (fragment instanceof SelectedItems) {
            ((SelectedItems) fragment).onSelected(item, item.isSelected);
        }

        if (item.isSelected) {
            size += item.size;
            selected.add(item);
        } else {
            selected.remove(item);
            size -= item.size;
        }

        updateCountView();

        return selected.size();
    }

    protected void updateCountView() {
        String type = "photo";
        int count = selected.size();
        if (count == 0) {
            txtCountSelectedItem.setVisibility(View.INVISIBLE);
        } else {
            txtCountSelectedItem.setVisibility(View.VISIBLE);
            String text = count + " " + (count > 1 ? type + "s" : type) + " selected";
            txtCountSelectedItem.setText(text);
        }
    }

    private PhotoAdapter adapter;
    private List<PhotoGroup> data;

    private void showAlertBigFiles() {
        String message = getString(R.string.msg_file_selected_too_big, "");
        Context context = getActivity();
        new AlertDialog.Builder(context).setMessage(message).setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.photo_fragment, null, false);

        txtCountSelectedItem = (TextView) view.findViewById(R.id.txtCountSelectedItem);
        headerPhoto = (RelativeLayout) view.findViewById(R.id.header_photo);

        if (Constants.isChat) {
            headerPhoto.setVisibility(View.GONE);
        } else {
            headerPhoto.setVisibility(View.VISIBLE);
        }

        ListView lvPhotos = (ListView) view.findViewById(R.id.lvPhotos);
        data = new ArrayList<>();
        adapter = new PhotoAdapter(data);
        adapter.setOnItemClickListener(new PhotoAdapter.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int group, int position, View view) {
                if (group == 1 && position == 0) {
                    capture = true;
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    String groupFolder = data.get(group).groupFolder;
//                    Log.e("Lu _ group_folder : ", groupFolder);
                    File imagesFolder = new File(groupFolder);
                    imagesFolder.mkdirs();
                    String nameImg = System.currentTimeMillis() + ".jpg";
                    imgPath = groupFolder + "/" + nameImg;
                    File image = new File(imagesFolder, nameImg);
                    Uri uriSavedImage = Uri.fromFile(image);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);

                    getActivity().startActivityForResult(intent, RC_CAPTURE_IMAGE);
                } else {
                    PhotoItem child = data.get(group).items.get(position);
                    int count = getCountItemSelected();

                    FragmentActivity activity = getActivity();
                    if (activity instanceof OnItemSharing) {
                        if (child.size > BaseFileScreen.MAX_SIZE) {
                            showAlertBigFiles();
                            return;
                        }

                        try {
                            JSONObject data = new JSONObject();
                            data.put("data", child.data);
                            ((OnItemSharing) activity).onShare("photo", data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (count < MAX_SELECTED || child.isSelected) {
                            View selected = view.findViewById(R.id.imgSelected);
                            child.isSelected = !child.isSelected;
                            int result = addItemSelected(child);

                            if (result == -1) {
                                child.isSelected = !child.isSelected;
                                return;
                            }

                            HomeFragment homeFragment = (HomeFragment) getParentFragment();
                            if (child.isSelected) {
                                if (Constants.isAnim) {
                                    homeFragment.showAnimationBump();
                                }
                            } else {
                                if (result == 0)
                                    homeFragment.goneLayoutAnimation();
                            }

                            selected.setVisibility(child.isSelected ? View.VISIBLE : View.INVISIBLE);
                            Log.e("Selected", child.data);
                        } else {
                            showAlertMaxFiles();
                        }
                    }
                }
            }
        });

        lvPhotos.setAdapter(adapter);
//        loadAllPhotos();
        return view;
    }

    private void showAlertMaxFiles() {
        Context context = getActivity();
        String message = "Maximum photos can be selected once time";
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    private static final int RC_CAPTURE_IMAGE = 1001;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RC_CAPTURE_IMAGE:
                if (resultCode == Activity.RESULT_OK) {
                    String pth = imgPath;

                    File file = new File(pth);
                    PhotoItem photoItem = adapter.insertPath(file);

                    int count = getCountItemSelected();
                    if (count < MAX_SELECTED) {
                        photoItem.isSelected = true;

                        int result = addItemSelected(photoItem);

                        if (result == -1) {
                            photoItem.isSelected = false;
                        } else {
                            HomeFragment homeFragment = (HomeFragment) getParentFragment();
                            if (Constants.isAnim) {
                                homeFragment.showAnimationBump();
                            }
                        }
                    } else {
                        showAlertMaxFiles();
                    }

                    for (PhotoItem item : selected) {
                        selectedPaths.add(item.data);
                    }

                    selected.clear();
                    updateCountView();

                    this.data.clear();
                    loadAllPhotos();
                    adapter.notifyDataSetChanged();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            adapter.notifyDataSetChanged();

            if (msg.what == -2) {
                updateCountView();
                selectedPaths.clear();
            }
        }
    };

    private static File PHOTOS = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

    private void loadAllPhotos() {
        count = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e("Photo", PHOTOS.getAbsolutePath());

                listAllPhotos(PHOTOS);
                if (ExternalStorage.isAvailable()) {
                    File sdCard = new File("/storage/sdcard1/DCIM");
                    if (!sdCard.exists()) {
                        Map<String, File> externalLocations = ExternalStorage.getAllStorageLocations();
                        if (externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD) == null) {
                            sdCard = new File("/mnt/sdcard/ext_sd/DCIM");
                        } else {
                            sdCard = new File(externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD).toString() + "/DCIM");
                        }
                    }

                    listAllPhotos(sdCard);
                }

                handler.sendEmptyMessage(-2);

            }
        }).start();
    }

    private int count = 0;

    private void listAllPhotos(File root) {
        if (root == null) {
            return;
        }

        File cache = BumpUtils.getCacheFolder();
        if (root.getAbsolutePath().equals(cache.getAbsolutePath())) {
            return;
        }

        final File[] files = root.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isFile() && file.getName().toLowerCase().endsWith(".jpg");
            }
        });

        if (files != null && files.length > 0) {
            String name = root.getName();

            Arrays.sort(files, new Comparator() {
                public int compare(Object o1, Object o2) {

                    if (((File) o1).lastModified() > ((File) o2).lastModified()) {
                        return -1;
                    } else if (((File) o1).lastModified() < ((File) o2).lastModified()) {
                        return +1;
                    } else {
                        return 0;
                    }
                }

            });

            String groupName = name + " (" + files.length + ")";
            final PhotoGroup header = new PhotoGroup(PhotoGroup.TYPE.HEADER, groupName, false, "", 0,root.getAbsolutePath());
//            newData.add(header);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        data.add(header);
                        adapter.notifyDataSetChanged();
                    }
                });
            }

            count++;
            PhotoGroup row = null;
            if (count == 1) {
                for (int i = 0; i < files.length + 1; i++) {
                    if (i == 0) {
                        row = new PhotoGroup(PhotoGroup.TYPE.ROW, "", false, "", 0, root.getAbsolutePath());
                    } else {
                        File file = files[i - 1];
                        String path = file.getAbsolutePath();

                        boolean select = selectedPaths.contains(path);

                        long size = file.length();
                        PhotoItem photoItem;

                        switch (i % 4) {
                            case 0:
                                row = new PhotoGroup(PhotoGroup.TYPE.ROW, path, true, "", size, select);
                                photoItem = row.items.get(0);
                                break;
                            case 3:
                                photoItem = row.addChild(path, true, "", size, select);
                                final PhotoGroup finalRow = row;
                                if (activity != null) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            data.add(finalRow);
                                            adapter.notifyDataSetChanged();
                                        }
                                    });
                                }
                                row = null;
                                break;
                            default:
                                photoItem = row.addChild(path, true, "", size, select);
                                break;
                        }

                        if (select) {
                            selected.add(photoItem);
                        }
                    }
                }
            } else {
                for (int i = 0; i < files.length; i++) {
                    File file = files[i];
                    String path = file.getAbsolutePath();

                    boolean select = selectedPaths.contains(path);

                    PhotoItem photoItem;

                    long size = file.length();
                    switch (i % 4) {
                        case 0:
                            row = new PhotoGroup(PhotoGroup.TYPE.ROW, path, true, "", size, select);
                            photoItem = row.items.get(0);
                            break;
                        case 3:
                            photoItem = row.addChild(path, true, "", size, select);
                            final PhotoGroup finalRow = row;
                            if (activity != null) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        data.add(finalRow);
                                        adapter.notifyDataSetChanged();
                                    }
                                });
                            }
                            row = null;
                            break;
                        default:
                            photoItem = row.addChild(path, true, "", size, select);
                            break;
                    }

                    if (select) {
                        selected.add(photoItem);
                    }
                }
            }

            if (row != null && activity != null) {
                final PhotoGroup fr = row;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        data.add(fr);
                        adapter.notifyDataSetChanged();
                    }
                });
            }

            handler.sendEmptyMessage(0);
        }

        File[] folders = root.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isDirectory() && !file.isHidden();
            }
        });

        if (folders != null) {
            for (File folder : folders) {
                listAllPhotos(folder);
            }
        }
    }

    @Override
    public void onClear() {
        HomeFragment homeFragment = (HomeFragment) getParentFragment();
        String bumpType = homeFragment.getBumpType();

        if (!"photo".equals(bumpType) && selected.size() > 0) {
            for (PhotoItem item : selected) {
                item.isSelected = false;
            }

            selected.clear();
            size = 0;
            adapter.notifyDataSetChanged();
            updateCountView();
        }
    }

    private boolean capture = false;

    private List<String> selectedPaths = new ArrayList<>();

    @Override
    public void onResume() {
        super.onResume();

        MAX_SELECTED = 6;
        String type = PreferenceUtils.getString(Constants.PURCHASED, getActivity(), "");

        if (type.equals(Constants.PURCHASE.BUY_ALL.type) || type.equals(Constants.PURCHASE.BUY_ONE.type)) {
           MAX_SELECTED = MAX_SELECTED + 5;
        }

        if (Constants.toForeground && !capture) {
            Constants.toForeground = false;
            for (PhotoItem item : selected) {
                selectedPaths.add(item.data);
            }

            selected.clear();
            updateCountView();

            data.clear();
            loadAllPhotos();
        }

        if (capture) {
            capture = false;
        }

    }
}
