package com.giinger.fibu.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputType;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.giinger.fibu.R;
import com.giinger.fibu.activity.ExtraPowerScreen;
import com.giinger.fibu.activity.HomeScreen;
import com.giinger.fibu.adapter.AddFieldAdapter;
import com.giinger.fibu.element.CircleImageView;
import com.giinger.fibu.element.CustomEditText;
import com.giinger.fibu.element.CustomTextView;
import com.giinger.fibu.model.AdditionalFieldModel;
import com.giinger.fibu.model.ChatBoxItem;
import com.giinger.fibu.service.UpDownLoadService;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.OnRefreshListener;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.RushOrmManager;
import com.giinger.fibu.util.Utils;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.parse.ParsePush;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import co.uk.rushorm.core.RushSearch;

public class ProfileFragment extends Fragment implements OnRefreshListener, View.OnClickListener {

    public static CircleImageView imgAvatar, imgAvatar4Edit;

    private CustomTextView tvFirstName;
    private CustomTextView tvLastName;
    private CustomTextView tvJobTitle;
    private CustomTextView tvMobile;
    private CustomTextView tvEmail;

    private RelativeLayout rlMobileView;
    private RelativeLayout rlEmailView;

    private ScrollView layoutViewEdit;
    private View layoutView, layoutEdit, marginTop;
    private LinearLayout layoutFieldEdit;
    private LinearLayout layoutFieldView;
    private LinearLayout layoutBaseView;
    private RelativeLayout layoutBaseEdit;

    private View btnOff, btnEdit, btnSave;
    private View imgSepProfileInfo;

    private CustomEditText edtMobile;
    private CustomEditText edtEmail;

    private CustomEditText edtFirstName;
    private CustomEditText edtLastName;
    private CustomEditText edtJobTitle;

    private RelativeLayout rlAddressEdit;
    private RelativeLayout layoutActionAddField;
    private LinearLayout layoutContainAdditional;
    private ListView listAdditionalField;

    private ImageView imgEyeMobile;
    private ImageView imgEyeEmail;

    private View progressBar;

    private AddFieldAdapter adapter;
    private ArrayList<AdditionalFieldModel> dataAdditional;
    private ArrayList<AdditionalFieldModel> dataAdditionalSelected;
    private int countView = 0;
    private ArrayList<String> hidden;
    private Map<String, String> params;
    private LayoutInflater mInflater;
    private ArrayList<String> additionalFieldDisable;
    private boolean isValid = true;
    private boolean showAnimation = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        params = new HashMap<>();

        dataAdditional = new ArrayList<>();
        dataAdditionalSelected = new ArrayList<>();
        hidden = new ArrayList<>();
        additionalFieldDisable = new ArrayList<>();
        adapter = new AddFieldAdapter(dataAdditional);

        mCallbackManagerFacebook = CallbackManager.Factory.create();
        Log.e("profile_info", PreferenceUtils.getUserInfo(getActivity()).toString());
    }

    private View bumpCardLayout;
    private String avatarName;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.profile_fragment, null, false);
        mInflater = inflater;

        progressBar = view.findViewById(R.id.layout_progress);

        tvFirstName = (CustomTextView) view.findViewById(R.id.tv_first_name);
        tvLastName = (CustomTextView) view.findViewById(R.id.tv_last_name);
        tvJobTitle = (CustomTextView) view.findViewById(R.id.tv_job_title);
        tvMobile = (CustomTextView) view.findViewById(R.id.tv_mobile);
        tvEmail = (CustomTextView) view.findViewById(R.id.tv_email);

        layoutView = view.findViewById(R.id.scroll_view);
        layoutViewEdit = (ScrollView) view.findViewById(R.id.layout_scroll_edit);

        marginTop = view.findViewById(R.id.marginTop);
        layoutEdit = view.findViewById(R.id.layout_edit);
        layoutFieldView = (LinearLayout) view.findViewById(R.id.layout_field);

        rlMobileView = (RelativeLayout) view.findViewById(R.id.layout_info_mobile);
        rlEmailView = (RelativeLayout) view.findViewById(R.id.layout_info_email);

        imgEyeMobile = (ImageView) view.findViewById(R.id.img_eye);
        imgEyeEmail = (ImageView) view.findViewById(R.id.img_eye_email);

        imgEyeMobile.setOnClickListener(this);
        imgEyeEmail.setOnClickListener(this);

        layoutBaseView = (LinearLayout) view.findViewById(R.id.base_profile_view);
        layoutBaseEdit = (RelativeLayout) view.findViewById(R.id.base_edit_profile);

        edtFirstName = (CustomEditText) view.findViewById(R.id.edt_first_name);
        edtLastName = (CustomEditText) view.findViewById(R.id.edt_last_name);
        edtJobTitle = (CustomEditText) view.findViewById(R.id.edt_job_title);

        layoutContainAdditional = (LinearLayout) view.findViewById(R.id.layout_contain_field);
        btnOff = view.findViewById(R.id.btn_show_off);
        btnOff.setOnClickListener(this);
        layoutActionAddField = (RelativeLayout) view.findViewById(R.id.layout_click_add);
        layoutActionAddField.setOnClickListener(this);
        listAdditionalField = (ListView) view.findViewById(R.id.list_field);
        listAdditionalField.setAdapter(adapter);

        listAdditionalField.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (dataAdditional.get(position).getSelected()) {
                    addFieldEdit(position, mInflater);
                    dataAdditional.get(position).setSelected(false);
                    dataAdditionalSelected.add(dataAdditional.get(position));
                    showSelectFieldView(false);

//                    HomeFragment home = (HomeFragment) getParentFragment();
//                    home.enableViewpager();
//                    HomeScreen.instance.lockNavigation(false);

                    countView++;
                    adapter.notifyDataSetChanged();
                }
            }
        });

        btnEdit = view.findViewById(R.id.imgEditProfileCard);
        btnEdit.setOnClickListener(this);
        btnSave = view.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);

        imgSepProfileInfo = view.findViewById(R.id.imgSepProfileInfo);

        edtMobile = (CustomEditText) view.findViewById(R.id.edt_mobile);
        edtEmail = (CustomEditText) view.findViewById(R.id.edt_email);

        rlAddressEdit = (RelativeLayout) view.findViewById(R.id.layout_edt_address);
        layoutFieldEdit = (LinearLayout) view.findViewById(R.id.layout_field_edit);

        imgAvatar = (CircleImageView) view.findViewById(R.id.img_avatar);
        imgAvatar4Edit = (CircleImageView) view.findViewById(R.id.img_avatar_4edit);

        File file = BumpUtils.getFile("avatar.jpg", true);
        final FragmentActivity context = getActivity();
        avatarName = PreferenceUtils.getString(Constants.AVATAR_URL, context, null);
        if (file.exists()) {
            String path = file.getAbsolutePath();
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            if (bitmap != null) {
                imgAvatar.setImageBitmap(bitmap);
                imgAvatar4Edit.setImageBitmap(bitmap);
            } else if (!TextUtils.isEmpty(avatarName)) {
                loadAvatar(avatarName, true, false, false);
            } else {
                imgAvatar.setImageResource(R.drawable.empty_ava);
                imgAvatar4Edit.setImageResource(R.drawable.empty_ava);
            }
        } else if (!TextUtils.isEmpty(avatarName)) {
            loadAvatar(avatarName, true, false, false);
        } else {
            imgAvatar.setImageResource(R.drawable.empty_ava);
            imgAvatar4Edit.setImageResource(R.drawable.empty_ava);
        }

        imgAvatar4Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = getActivity();

                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                Utils.getInstance().showLayoutDialogEditAvatar(view, activity);
            }
        });

        initView();

        bumpCardLayout = view.findViewById(R.id.bumpCardLayout);
        bumpCardLayout.setVisibility(PreferenceUtils.getBoolean(Constants.SHOW_BUMP_CARD, context, true) ? View.VISIBLE : View.GONE);

        bumpCardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bumpCardLayout.setVisibility(View.GONE);
                PreferenceUtils.saveBoolean(Constants.SHOW_BUMP_CARD, context, false);
            }
        });

        boolean showVideo = PreferenceUtils.getBoolean(Constants.SHOW_VIDEO_TOUR, context, true);
        if (showVideo) {
            Constants.canBump = false;

            PreferenceUtils.saveBoolean(Constants.SHOW_VIDEO_TOUR, context, false);

            HomeFragment home = (HomeFragment) getParentFragment();
            home.disableViewpager();
            HomeScreen.instance.lockNavigation(true);

            videoLayout = view.findViewById(R.id.videoLayout);
            videoLayout.setVisibility(View.VISIBLE);

            //TODO start dowload
            File folder = BumpUtils.getCacheFolder();
            String fileName = "video-tour.mp4";
            File video = new File(folder, fileName);

            loadingLayout = videoLayout.findViewById(R.id.loadingLayout);

            ImageView loadingIcon = (ImageView) loadingLayout.findViewById(R.id.loadingIcon);
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.rotate);
            loadingIcon.startAnimation(animation);

            tvProgress = (TextView) loadingLayout.findViewById(R.id.tvProgress);

            videoView = (VideoView) videoLayout.findViewById(R.id.videoView);
            videoViewWrapper = videoLayout.findViewById(R.id.videoViewWrapper);

            String url = PreferenceUtils.getString(Constants.TOUR_VIDEO, context, null);
            String saved = PreferenceUtils.getString(Constants.TOUR_VIDEO_SAVED, context, null);

            if (video.exists() && !UpDownLoadService.isDownloadingVideoTour && url != null && url.equals(saved)) {
                playVideo();
            } else {
                if (!UpDownLoadService.isDownloadingVideoTour) {
                    Intent intent = new Intent(context, UpDownLoadService.class);
                    intent.putExtra(UpDownLoadService.TYPE, UpDownLoadService.TYPE_DOWNLOAD_VIDEO);
                    context.startService(intent);

                    tvProgress.setText("0%");
                }
            }
        } else {
            if (PreferenceUtils.getBoolean(Constants.SHOW_BUMP_CARD, getActivity(), true)) {
                count = 5 * 4;
                beginCountDown();
            }
        }

        initializeFacebook(view);
        checkConnectFB();

        return view;
    }

    private void enableSeparatorView() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean hasChild = (layoutFieldView.getChildCount() > 0) && (rlEmailView.isShown() || rlMobileView.isShown());
                imgSepProfileInfo.setVisibility(hasChild ? View.VISIBLE : View.GONE);
            }
        }, 500);
    }

    private void checkConnectFB() {
        if (TextUtils.isEmpty(PreferenceUtils.getString(Constants.FB_ID, getActivity(), ""))) {
            mSignInFacebook.setVisibility(View.VISIBLE);
        } else {
            mSignInFacebook.setVisibility(View.GONE);
        }
        enableSeparatorView();
    }

    private CallbackManager mCallbackManagerFacebook;
    public static final String[] FACEBOOK_PERMISSIONS = new String[]{"email"};
    private LoginButton mSignInFacebook;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RC_PURCHASE:
                if (resultCode == Activity.RESULT_OK) {
                    showPopupEnterPassCode();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                mCallbackManagerFacebook.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void initializeFacebook(View view) {
        mSignInFacebook = (LoginButton) view.findViewById(R.id.fb_login);
        mSignInFacebook.setReadPermissions(FACEBOOK_PERMISSIONS);
        mSignInFacebook.setFragment(this);
        mSignInFacebook.setBackgroundResource(R.drawable.facebook_login);
        mSignInFacebook.registerCallback(mCallbackManagerFacebook,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        ((HomeScreen) getActivity()).showProgressDialog(false);
                        final AccessToken accessToken = loginResult.getAccessToken();
                        GraphRequest request = GraphRequest.newMeRequest(
                                accessToken,
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        handleFacebookInfo(object);
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, name, link, email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException e) {

                    }

                });
    }

    public void showLoginFBSuccess() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Linked successfully!");
        alertDialog.setMessage("Stop looking for his/her name. It’s now a hassle free to add someone as a Facebook friend."
                + "\n1- Bump together"
                + "\n2- Connect"
                + "\n3- Tap Add friend"
                + "\nDone !");
        alertDialog.setPositiveButton("Got it",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        alertDialog.show();
    }

    private String avatarFB = "";
    private String idFB = "";
    private String emailFB = "";
    private String restoreUserId = "";

    private void handleFacebookInfo(JSONObject object) {
        try {
            idFB = object.getString("id");
            avatarFB = "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large";
            emailFB = object.getString("email");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ((HomeScreen) getActivity()).closeProgressDialog();

        progressBar.setVisibility(View.VISIBLE);

        final Context context = getActivity();
        AQuery mAQuery = new AQuery(context);
        getInfoAdditionField();

        if (!isValid) {
            isValid = true;
            progressBar.setVisibility(View.GONE);
            return;
        }

        params.put("first_name", tvFirstName.getText().toString().trim());
        params.put("last_name", tvLastName.getText().toString().trim());
        params.put("job_title", tvJobTitle.getText().toString().trim());
        params.put("mobile", tvMobile.getText().toString().trim());
        if (TextUtils.isEmpty(tvEmail.getText().toString().trim())) {
            params.put("email", emailFB);
        } else {
            params.put("email", tvEmail.getText().toString().trim());
        }
        params.put("facebook", idFB);
        params.put("id", PreferenceUtils.getString(Constants.USER_ID, getActivity(), ""));

        int countHidden = hidden.size();
        String hiddenFields = "";

        for (int i = 0; i < countHidden; i++) {
            if (i == 0) {
                hiddenFields = hidden.get(0);
            } else {
                hiddenFields = hiddenFields + "," + hidden.get(i);
            }
        }
        Log.e("QueANh_hidden_fields : ", hiddenFields);

//        if (!TextUtils.isEmpty(hiddenFields)) {
//            params.put("hidden_fields", hiddenFields);
//        }
        params.put("hidden_fields", hiddenFields);

        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, final JSONObject object, AjaxStatus status) {
                progressBar.setVisibility(View.GONE);

                try {
                    final JSONObject obj = object.getJSONObject("response");
                    if (obj.getString("success").equals("true")) {
                        showEditView(false);

                        JSONArray userInfo = obj.getJSONArray("user");
                        JSONObject userData = PreferenceUtils.saveUserInfo(context, userInfo);

                        String id = userData.getJSONObject("id").getString("value");
                        PreferenceUtils.saveString(Constants.USER_ID, context, id);

                        PreferenceUtils.saveString(Constants.FB_ID, getActivity(), idFB);
                        idFB = "";
                        checkConnectFB();
                        if (TextUtils.isEmpty(avatarName)) {
                            avatarName = avatarFB;
                            if (!TextUtils.isEmpty(avatarName)) {
                                loadAvatar(avatarName, false, true, true);
                            }
                        }

                        reloadProfileData(userData);
                        showLoginFBSuccess();
                    } else {
                        LoginManager.getInstance().logOut();
                        try {
                            String error = obj.getString("error");
                            switch (error) {
                                case "existingEmail":
                                    existingEmail(obj.getString("userId"));
                                    break;
                                case "editExistingEmail":
                                    existingEmail(obj.getString("duplicateUserId"));
                                    break;
                                case "editNonExistingEmail":
                                    showPopupAlertEditEmail();
                                    break;
                                case "incorrectPasscode":
                                    Toast.makeText(getActivity(), "Passcode is incorrect. Please try again!", Toast.LENGTH_SHORT).show();
                                    break;
                                default:
                                    Toast.makeText(getActivity(), "Server error!", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "API Error!", Toast.LENGTH_SHORT).show();
                }
            }
        };

        String url = AppUtils.getSaveProfileUrl(context);
        mAQuery.ajax(url, params, JSONObject.class, callback);
    }

    private void reloadProfileData(JSONObject userData) {
        initView();

        HomeFragment he = (HomeFragment) getParentFragment();
        he.layoutBumpAnimation.setVisibility(View.VISIBLE);
        Constants.canBump = true;

        //update self profile
        try {
            List<ChatBoxItem> profiles = new RushSearch().whereStartsWith("id", "self").find(ChatBoxItem.class);
            if (profiles != null) {
                JSONObject jsonSelf = Utils.getInstance().getSelfProfile(null, userData, 0);

                for (ChatBoxItem selfItem : profiles) {
                    if (selfItem.timeStamp == 0) {
                        jsonSelf.put("gmtTimestamp", String.valueOf(System.currentTimeMillis() / 1000));
                    } else {
                        jsonSelf.put("gmtTimestamp", String.valueOf(selfItem.timeStamp));
                    }

                    jsonSelf.put("id", "self" + selfItem.friendId);

                    selfItem.setJsonData(jsonSelf);
                    RushOrmManager.getInstance().deleteItem(selfItem);
                    RushOrmManager.getInstance().storeSingleChatItem(selfItem);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private TextView tvProgress;
    private View loadingLayout, videoLayout, videoViewWrapper;
    private VideoView videoView;

    private void loadAvatar(String avatarName, final boolean isAddPrefix, final boolean isDownloadAvatar, final boolean isUploadAvatar) {
        String avatarUrl = "";
        if (isAddPrefix) {
            avatarUrl = AppUtils.getAvatarByName(getActivity(), avatarName);
        } else {
            avatarUrl = avatarName;
        }
        new AQuery(getActivity()).id(imgAvatar).image(avatarUrl, true, true, 0, R.drawable.empty_ava, new BitmapAjaxCallback() {
            @Override
            protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                super.callback(url, iv, bm, status);
                imgAvatar4Edit.setImageBitmap(bm);
                if (isDownloadAvatar) {
                    FileOutputStream outputStream = null;
                    try {
                        File fileAvatar = BumpUtils.getFile("avatar.jpg", true);
                        outputStream = new FileOutputStream(new File(fileAvatar.getAbsolutePath().toString()), true);
                        bm.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                        outputStream.close();

                        if (isUploadAvatar) {
                            Constants.imgUri = fileAvatar.getAbsolutePath();
                            byte[] data = Utils.getInstance().getFileData(fileAvatar);
                            Utils.getInstance().postAvatar(getActivity(), data);
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void getAdditionalField() {
        AQuery mAQuery = new AQuery(getActivity());
        mAQuery.ajax(AppUtils.getAdditionalFieldUrl(getActivity()), null, JSONObject.class,
                new AjaxCallback<JSONObject>() {
                    public void callback(String url, JSONObject object,
                                         com.androidquery.callback.AjaxStatus status) {
                        try {
                            JSONArray array = object.getJSONObject("response").getJSONArray("attributes");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject js = array.getJSONObject(i);
                                AdditionalFieldModel model = new AdditionalFieldModel();

                                model.setName(js.getString("label"));
                                model.setType(js.getString("name"));
                                model.setSelected(true);

                                String icon = js.getString("icon").replace(".png", "");
                                model.setIcon(icon);
                                int img = getResources().getIdentifier(icon, "drawable", getActivity().getPackageName());
                                model.setImg(img);

                                for (int j = 0; j < additionalFieldDisable.size(); j++) {
                                    if (model.getType().equals(additionalFieldDisable.get(j))) {
                                        model.setSelected(false);
                                    }
                                }

                                dataAdditional.add(model);
                            }

                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void playVideo() {
        if (videoViewWrapper != null && loadingLayout != null) {
            loadingLayout.setVisibility(View.GONE);
            videoViewWrapper.setVisibility(View.VISIBLE);

            String fileName = "video-tour.mp4";
            File file = BumpUtils.getFile(fileName, true);
            videoView.setVideoPath(file.getAbsolutePath());
            videoView.start();

            showSkipVideo();
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopVideo();
                }
            });
        }
    }

    private void stopVideo() {
        if (videoLayout != null && videoView != null) {
            videoLayout.setVisibility(View.GONE);
            videoView.pause();

            HomeFragment home = (HomeFragment) getParentFragment();
            home.enableViewpager();
            HomeScreen.instance.lockNavigation(false);
            Constants.canBump = true;

            home.checkGpsService();

            if (PreferenceUtils.getBoolean(Constants.SHOW_BUMP_CARD, getActivity(), true)) {
                count = 5 * 4;
                beginCountDown();
            }
        }
    }

    private int currentProgress = 0;

    private IntentFilter filter = new IntentFilter(UpDownLoadService.DOWNLOAD_VIDEO_TOUR_ACTION);

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (tvProgress != null) {
                int progress = intent.getIntExtra(UpDownLoadService.DOWNLOAD_VIDEO_TOUR_PROGRESS, -1);
                if (progress == 100) {
                    playVideo();
                } else if (progress > currentProgress) {
                    currentProgress = progress;
                    tvProgress.setText(progress + "%");
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        Context context = getActivity();
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();

        Context context = getActivity();
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onSetImageAvatar(String path) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgEditProfileCard:
                params.clear();
                Constants.canBump = false;
                showEditView(true);
                HomeFragment he = (HomeFragment) getParentFragment();
                he.goneLayoutAnimation();
                break;

            case R.id.btn_save:

                if (TextUtils.isEmpty(edtFirstName.getText().toString().trim()) || TextUtils.isEmpty(edtLastName.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "Please fill first name and last name!", Toast.LENGTH_LONG).show();
                } else {
                    if (TextUtils.isEmpty(edtEmail.getText().toString().trim()) || Utils.getInstance().isEmailValid(edtEmail.getText().toString().trim())) {
                        Activity activity = getActivity();
                        hideKeyBoard(activity);
                        saveProfile(false);
                    } else {
                        Toast.makeText(getActivity(), "Wrong email format!", Toast.LENGTH_LONG).show();
                    }
                }

                break;

            case R.id.btn_show_off:

//                HomeFragment homeFragment = (HomeFragment) getParentFragment();
//                homeFragment.enableViewpager();
//                HomeScreen.instance.lockNavigation(false);
                showSelectFieldView(false);
                break;

            case R.id.layout_click_add:
//                HomeFragment home = (HomeFragment) getParentFragment();
//                home.disableViewpager();
//                HomeScreen.instance.lockNavigation(true);
                showSelectFieldView(true);
                break;

            case R.id.img_eye:
                String mobileKey = "mobile";
                if (v.getTag() != null && v.getTag().toString().equals(mobileKey)) {
                    imgEyeMobile.setImageResource(R.drawable.eye_view_icon);
                    v.setTag(null);
                    hidden.remove(mobileKey);
                } else {
                    imgEyeMobile.setImageResource(R.drawable.hidden_icon);
                    v.setTag(mobileKey);
                    hidden.add(mobileKey);
                }
                break;

            case R.id.img_eye_email:
                String emailKey = "email";
                if (v.getTag() != null && v.getTag().toString().equals(emailKey)) {
                    imgEyeEmail.setImageResource(R.drawable.eye_view_icon);
                    v.setTag(null);
                    hidden.remove(emailKey);
                } else {
                    imgEyeEmail.setImageResource(R.drawable.hidden_icon);
                    v.setTag(emailKey);
                    hidden.add(emailKey);
                }
                break;

        }
    }

    public static void hideKeyBoard(Activity context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            View view = context.getCurrentFocus();
            if (view == null) {
                view = new View(context);
            }

            IBinder windowToken = view.getWindowToken();
            inputMethodManager.hideSoftInputFromWindow(windowToken, InputMethodManager.HIDE_NOT_ALWAYS);
        }

//        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public void initView() {
        try {
            hidden.clear();

            layoutFieldView.removeAllViews();

            while (layoutFieldEdit.getChildCount() > 1) {
                layoutFieldEdit.removeViewAt(layoutFieldEdit.getChildCount() - 1);
            }


            Context context = getActivity();
            JSONObject userInfo = PreferenceUtils.getUserInfo(context);

            JSONObject firstName = userInfo.optJSONObject("first_name");
            if (firstName != null) {
                String text = firstName.getString("value");
                tvFirstName.setText(text);
                edtFirstName.setText(text);
            }

            JSONObject lastName = userInfo.optJSONObject("last_name");
            if (firstName != null) {
                String text = lastName.getString("value");
                tvLastName.setText(text);
                edtLastName.setText(text);
            }

            JSONObject jobTitle = userInfo.optJSONObject("job_title");
            if (firstName != null) {
                String text = jobTitle.getString("value");
                tvJobTitle.setText(text);
                edtJobTitle.setText(text);
            }

            String mobileKey = "mobile";
            JSONObject mobile = userInfo.optJSONObject(mobileKey);
            boolean mobileHide = true;
            if (mobile != null) {
                mobileHide = mobile.getBoolean("hidden");
            }

            if (mobile == null || mobileHide) {
                rlMobileView.setVisibility(View.GONE);

                if (mobile != null) {
                    imgEyeMobile.setImageResource(R.drawable.hidden_icon);
                    hidden.add(mobileKey);
                    imgEyeMobile.setTag(mobileKey);

                    String text = mobile.getString("value");
                    edtMobile.setText(text);
                }
            } else {
                rlMobileView.setVisibility(View.VISIBLE);
                imgEyeMobile.setTag(null);
                imgEyeMobile.setImageResource(R.drawable.eye_view_icon);

                String text = mobile.getString("value");
                tvMobile.setText(text);
                edtMobile.setText(text);
            }

            String emailKey = "email";
            JSONObject email = userInfo.optJSONObject(emailKey);
            PreferenceUtils.saveString("email_user", getActivity(), email.getString("value"));

            boolean emailHide = true;
            if (email != null) {
                emailHide = email.getBoolean("hidden");
            }

            if (email == null || emailHide) {
                rlEmailView.setVisibility(View.GONE);

                if (email != null) {
                    imgEyeEmail.setImageResource(R.drawable.hidden_icon);
                    hidden.add(emailKey);
                    imgEyeEmail.setTag(emailKey);

                    String text = email.getString("value");
                    edtEmail.setText(text);
                }
            } else {
                rlEmailView.setVisibility(View.VISIBLE);
                imgEyeEmail.setTag(null);
                imgEyeEmail.setImageResource(R.drawable.eye_view_icon);

                String text = email.getString("value");
                tvEmail.setText(text);
                edtEmail.setText(text);
            }

            addFieldView(mInflater, userInfo);

            enableSeparatorView();

            addFiledEditAvailable(userInfo, mInflater);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addFieldView(LayoutInflater inflater, JSONObject info) {
        Iterator<String> keys = info.keys();
        while (keys.hasNext()) {
            String key = keys.next();

            if (PreferenceUtils.isBaseField(key)) {
                continue;
            }

            try {
                JSONObject data = info.getJSONObject(key);
                boolean isHidden = data.getBoolean("hidden");

                if (!isHidden) {
                    boolean isAddress = key.contains(Constants.FIELD_ADDRESS);
                    if (isAddress) {
                        final View view = inflater.inflate(R.layout.item_view_profile, null);
                        ImageView img = (ImageView) view.findViewById(R.id.img);
                        CustomTextView tv = (CustomTextView) view.findViewById(R.id.tv);

                        String icon = data.getString("icon").replace(".png", "");

                        int im = getResources().getIdentifier(icon, "drawable", getActivity().getPackageName());
                        img.setImageBitmap(BitmapFactory.decodeResource(getResources(), im));

                        String value = data.getString("value");
                        JSONObject address = new JSONObject(value);
                        String add = address.optString("address");
                        String street = address.optString("street");
                        String city = address.optString("city");
                        String code = address.optString("postal_code");

                        if (!TextUtils.isEmpty(add) || !TextUtils.isEmpty(street) || !TextUtils.isEmpty(city) || !TextUtils.isEmpty(code)) {
                            String adressStreet = TextUtils.isEmpty(add) ? "" : add + (TextUtils.isEmpty(street) ? "" : "\n" + street);
                            String cityCode = TextUtils.isEmpty(city) ? "" : "\n" + city + (TextUtils.isEmpty(code) ? "" : "\n" + code);
                            String total = adressStreet + cityCode;
                            tv.setText(total);
                            layoutFieldView.addView(view);
                        }
                    } else {
                        final View view = inflater.inflate(R.layout.item_view_profile, null);
                        ImageView img = (ImageView) view.findViewById(R.id.img);
                        final CustomTextView tv = (CustomTextView) view.findViewById(R.id.tv);

                        String icon = data.getString("icon").replace(".png", "");

                        int im = getResources().getIdentifier(icon, "drawable", getActivity().getPackageName());
                        img.setImageBitmap(BitmapFactory.decodeResource(getResources(), im));
                        final String text = data.getString("value");
                        boolean isAddView = true;
                        if (key.contains(Constants.FIELD_FACEBOOK)) {
                            if (AccessToken.getCurrentAccessToken() == null) {
                                isAddView = false;
                            } else {
//                                Ion.with(this).load("https://graph.facebook.com/?ids=" + text
//                                    + "&access_token=1524270291224917|c38c8ef6632f8452a1b0de330985d028")
//                                    .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
//                                @Override
//                                public void onCompleted(Exception e, JsonObject result) {
//                                    if (e != null || result == null) {
//                                        return;
//                                    }
//                                    if (result.has(text)) {
//                                        JsonObject response = result.getAsJsonObject(text);
//                                        tv.setText(response.getAsJsonPrimitive("name").getAsString());
//                                    }
//                                }
//                            });
                                new GraphRequest(
                                        AccessToken.getCurrentAccessToken(),
                                        "/" + text,
                                        null,
                                        HttpMethod.GET,
                                        new GraphRequest.Callback() {
                                            public void onCompleted(GraphResponse response) {
                                                if (response.getJSONObject() != null) {
                                                    try {
                                                        tv.setText(response.getJSONObject().getString("name"));
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                ).executeAsync();
                            }
                        } else {
                            tv.setText(text);
                        }

                        if (isAddView) {
                            layoutFieldView.addView(view);
                        }
                    }
                }

                additionalFieldDisable.add(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void addFieldEdit(final int position, LayoutInflater inflater) {

        final AdditionalFieldModel model = dataAdditional.get(position);

        if (model.getType().contains(Constants.FIELD_ADDRESS)) {
            final View view = inflater.inflate(R.layout.item_additional_address, null);
            view.setTag(model.getType());

            final ImageView imgEye = (ImageView) view.findViewById(R.id.img_eye);
            final ImageView imgHidden = (ImageView) view.findViewById(R.id.img_hidden);
            ImageView imgDelete = (ImageView) view.findViewById(R.id.img_add_delete);
            ImageView img = (ImageView) view.findViewById(R.id.img_address);
            CustomEditText edt = (CustomEditText) view.findViewById(R.id.edt_add);
            edt.setHint(model.getName());

            img.setImageBitmap(BitmapFactory.decodeResource(getResources(), model.getImg()));
            layoutFieldEdit.addView(view);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopupAlertDeleteField(false, model.getType(), view, position);
                }
            });

            imgHidden.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imgHidden.setVisibility(View.GONE);
                    imgEye.setVisibility(View.VISIBLE);
                    hidden.remove(model.getType());
                }
            });

            imgEye.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imgHidden.setVisibility(View.VISIBLE);
                    imgEye.setVisibility(View.GONE);
                    hidden.add(model.getType());
                }
            });

        } else {
            final View view = inflater.inflate(R.layout.item_additional_normal, null);
            view.setTag(model.getType());

            final ImageView imgEye = (ImageView) view.findViewById(R.id.img_eye);
            final ImageView imgHidden = (ImageView) view.findViewById(R.id.img_hidden);
            ImageView imgDelete = (ImageView) view.findViewById(R.id.img_add_delete);
            ImageView img = (ImageView) view.findViewById(R.id.img_add);
            CustomEditText edt = (CustomEditText) view.findViewById(R.id.edt_add);

            if (model.getType().contains(Constants.FIELD_PHONE)) {
                edt.setInputType(InputType.TYPE_CLASS_PHONE);
            } else if (model.getType().contains(Constants.FIELD_EMAIL)) {
                edt.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
            } else if (model.getType().contains(Constants.FIELD_FACEBOOK)) {
                edt.setEnabled(false);
            } else {
                edt.setInputType(InputType.TYPE_CLASS_TEXT);
            }

            edt.setHint(model.getName());
            img.setImageBitmap(BitmapFactory.decodeResource(getResources(), model.getImg()));

            layoutFieldEdit.addView(view);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopupAlertDeleteField(false, model.getType(), view, position);
                }
            });

            imgHidden.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imgHidden.setVisibility(View.GONE);
                    imgEye.setVisibility(View.VISIBLE);
                    hidden.remove(model.getType());
                }
            });

            imgEye.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imgHidden.setVisibility(View.VISIBLE);
                    imgEye.setVisibility(View.GONE);
                    hidden.add(model.getType());
                }
            });
        }

        autoScroll();

        final CustomEditText edt = (CustomEditText) layoutFieldEdit.getChildAt(layoutFieldEdit.getChildCount() - 1).findViewById(R.id.edt_add);
        layoutFieldEdit.post(new Runnable() {
            @Override
            public void run() {
                boolean b = edt.requestFocus();
                Log.e("Focus", b + "");

                InputMethodManager keyboard = (InputMethodManager)
                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(edt, 0);
//                autoScroll();
            }
        });

        Log.e("QueAnh_count_view : ", layoutFieldEdit.getChildCount() + "_" + countView);
    }

    public void addFiledEditAvailable(JSONObject info, LayoutInflater inflater) {

        Iterator<String> keys = info.keys();
        while (keys.hasNext()) {
            final String key = keys.next();

            if (PreferenceUtils.isBaseField(key)) {
                continue;
            }

            try {
                JSONObject data = info.getJSONObject(key);
                boolean isHidden = data.getBoolean("hidden");

                boolean isAddress = key.contains(Constants.FIELD_ADDRESS);
                if (isAddress) {
                    String icon = data.getString("icon").replace(".png", "");

                    JSONObject address = new JSONObject(data.getString("value"));
                    String add = address.getString("address");
                    String street = address.getString("street");
                    String city = address.getString("city");
                    String code = address.getString("postal_code");

                    int im = getResources().getIdentifier(icon, "drawable", getActivity().getPackageName());


                    final View view = inflater.inflate(R.layout.item_additional_address, null);
                    view.setTag(key);

                    final ImageView imgEye = (ImageView) view.findViewById(R.id.img_eye);
                    final ImageView imgHidden = (ImageView) view.findViewById(R.id.img_hidden);
                    ImageView imgDelete = (ImageView) view.findViewById(R.id.img_add_delete);
                    ImageView img = (ImageView) view.findViewById(R.id.img_address);
                    CustomEditText edtAddress = (CustomEditText) view.findViewById(R.id.edt_add);
                    CustomEditText edtStreet = (CustomEditText) view.findViewById(R.id.edt_street);
                    CustomEditText edtCity = (CustomEditText) view.findViewById(R.id.edt_city);
                    CustomEditText edtCode = (CustomEditText) view.findViewById(R.id.edt_code);

                    edtAddress.setTag(icon);

                    img.setImageBitmap(BitmapFactory.decodeResource(getResources(), im));
                    edtAddress.setText(add);
                    edtStreet.setText(street);
                    edtCity.setText(city);
                    edtCode.setText(code);

                    if (isHidden) {
                        imgEye.setVisibility(View.GONE);
                        imgHidden.setVisibility(View.VISIBLE);
                        hidden.add(key);
                    } else {
                        imgEye.setVisibility(View.VISIBLE);
                        imgHidden.setVisibility(View.GONE);
                    }

                    imgDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showPopupAlertDeleteField(true, key, view, 0);
                        }
                    });

                    imgHidden.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imgHidden.setVisibility(View.GONE);
                            imgEye.setVisibility(View.VISIBLE);
                            hidden.remove(key);
                        }
                    });

                    imgEye.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imgHidden.setVisibility(View.VISIBLE);
                            imgEye.setVisibility(View.GONE);
                            hidden.add(key);
                        }
                    });

                    layoutFieldEdit.addView(view);
                    countView++;
                } else {
                    final View view = inflater.inflate(R.layout.item_additional_normal, null);

                    final ImageView imgEye = (ImageView) view.findViewById(R.id.img_eye);
                    final ImageView imgHidden = (ImageView) view.findViewById(R.id.img_hidden);
                    ImageView imgDelete = (ImageView) view.findViewById(R.id.img_add_delete);
                    ImageView img = (ImageView) view.findViewById(R.id.img_add);
                    final CustomEditText edt = (CustomEditText) view.findViewById(R.id.edt_add);

                    String icon = data.getString("icon").replace(".png", "");
                    edt.setTag(icon);
                    view.setTag(key);

                    int im = getResources().getIdentifier(icon, "drawable", getActivity().getPackageName());
                    img.setImageBitmap(BitmapFactory.decodeResource(getResources(), im));

                    String text = data.getString("value");
                    edt.setText(text);

                    boolean isAddView = true;

                    if (key.contains(Constants.FIELD_PHONE)) {
                        edt.setInputType(InputType.TYPE_CLASS_PHONE);
                    } else if (key.contains(Constants.FIELD_EMAIL)) {
                        edt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    } else if (key.contains(Constants.FIELD_FACEBOOK)) {
                        edt.setEnabled(false);
                        if (AccessToken.getCurrentAccessToken() == null) {
                            isAddView = false;
                        } else {
//                                Ion.with(this).load("https://graph.facebook.com/?ids=" + text
//                                    + "&access_token=1524270291224917|c38c8ef6632f8452a1b0de330985d028")
//                                    .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
//                                @Override
//                                public void onCompleted(Exception e, JsonObject result) {
//                                    if (e != null || result == null) {
//                                        return;
//                                    }
//                                    if (result.has(text)) {
//                                        JsonObject response = result.getAsJsonObject(text);
//                                        tv.setText(response.getAsJsonPrimitive("name").getAsString());
//                                    }
//                                }
//                            });
                            new GraphRequest(
                                    AccessToken.getCurrentAccessToken(),
                                    "/" + text,
                                    null,
                                    HttpMethod.GET,
                                    new GraphRequest.Callback() {
                                        public void onCompleted(GraphResponse response) {
                                            if (response.getJSONObject() != null) {
                                                try {
                                                    edt.setText(response.getJSONObject().getString("name"));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                            ).executeAsync();
                        }
                    } else {
                        edt.setInputType(InputType.TYPE_CLASS_TEXT);
                    }

                    if (isHidden) {
                        imgHidden.setVisibility(View.VISIBLE);
                        imgEye.setVisibility(View.GONE);
                        hidden.add(key);
                    } else {
                        imgHidden.setVisibility(View.GONE);
                        imgEye.setVisibility(View.VISIBLE);
                    }

                    imgDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showPopupAlertDeleteField(true, key, view, 0);
                        }
                    });

                    imgHidden.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imgHidden.setVisibility(View.GONE);
                            imgEye.setVisibility(View.VISIBLE);
                            hidden.remove(key);
                        }
                    });

                    imgEye.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imgHidden.setVisibility(View.VISIBLE);
                            imgEye.setVisibility(View.GONE);
                            hidden.add(key);
                        }
                    });

                    if (isAddView) {
                        layoutFieldEdit.addView(view);
                        countView++;
                    }
                }
            } catch (JSONException exception) {
                exception.printStackTrace();
            }
        }

        Log.e("Count_v_available : ", "" + countView);
    }

    public void getInfoAdditionField() {
        for (int i = 1; i < layoutFieldEdit.getChildCount(); i++) {
            View view = layoutFieldEdit.getChildAt(i);
            String type = (String) view.getTag();
            AdditionalFieldModel model = getModel(type);

            if (type.contains(Constants.FIELD_ADDRESS)) {
                CustomEditText address = (CustomEditText) view.findViewById(R.id.edt_add);
                CustomEditText street = (CustomEditText) view.findViewById(R.id.edt_street);
                CustomEditText code = (CustomEditText) view.findViewById(R.id.edt_code);
                CustomEditText city = (CustomEditText) view.findViewById(R.id.edt_city);

                if (model == null) {
                    getAddress(address, street, code, city, type, address.getTag().toString());
                } else {
                    getAddress(address, street, code, city, type, model.getIcon());
                }


            } else {
                CustomEditText edt = (CustomEditText) view.findViewById(R.id.edt_add);
                if (!TextUtils.isEmpty(edt.getText().toString().trim())) {
                    if (type.contains(Constants.FIELD_EMAIL)) {
                        if (Utils.getInstance().isEmailValid(edt.getText().toString().trim())) {
                            params.put(type, edt.getText().toString().trim());
                        } else {
                            Toast.makeText(getActivity(), "Email is invalid format! ", Toast.LENGTH_LONG).show();
                            isValid = false;
                            params.clear();
                            return;
                        }
                    } else if (type.equals(Constants.FIELD_LINK)) {
                        if (Patterns.WEB_URL.matcher(edt.getText().toString().trim()).matches()) {
                            params.put(type, edt.getText().toString().trim());
                        } else {
                            Toast.makeText(getActivity(), "Link is invalid format!", Toast.LENGTH_LONG).show();
                            isValid = false;
                            params.clear();
                            return;
                        }
                    } else if (type.equals(Constants.FIELD_FACEBOOK)) {
//                        String[] fbLink = edt.getText().toString().trim().split(Pattern.quote("/"));
//                        params.put(type, fbLink[fbLink.length - 1]);
                        params.put(type, PreferenceUtils.getString(Constants.FB_ID, getActivity(), ""));
                    } else {
                        params.put(type, edt.getText().toString().trim());
                    }

                    try {

                        JSONObject job = new JSONObject();
                        job.put(type, edt.getText().toString().trim());

                        if (model == null) {
                            job.put("icon", edt.getTag().toString());
                        } else {
                            job.put("icon", model.getIcon());
                        }

                        job.put("hidden", checkHiddenField(type));
                        job.put("type", type);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.e("Params_add_field :", "type : " + type + "value : " + edt.getText().toString().trim());
                } else {
                    hidden.remove(type);
                    getModel(type).setSelected(true);
                }
            }
        }

        adapter.notifyDataSetChanged();
    }

    public void getAddress(CustomEditText edtAddress, CustomEditText edtStreet, CustomEditText edtCode, CustomEditText edtCity, String type, String icon) {
        if (!TextUtils.isEmpty(edtCity.getText().toString().trim()) ||
                !TextUtils.isEmpty(edtStreet.getText().toString().trim()) ||
                !TextUtils.isEmpty(edtAddress.getText().toString().trim()) ||
                !TextUtils.isEmpty(edtCode.getText().toString().trim())) {

            JSONObject jsonObject = new JSONObject();
            try {

                jsonObject.put("address", edtAddress.getText().toString().trim());
                jsonObject.put("street", edtStreet.getText().toString().trim());
                jsonObject.put("city", edtCity.getText().toString().trim());
                jsonObject.put("postal_code", edtCode.getText().toString().trim());

                JSONObject ob = new JSONObject();
                params.put(type, jsonObject.toString());

                ob.put("address", edtAddress.getText().toString().trim());
                ob.put("street", edtStreet.getText().toString().trim());
                ob.put("city", edtCity.getText().toString().trim());
                ob.put("postal_code", edtCode.getText().toString().trim());
                ob.put("icon", icon);
                ob.put("hidden", checkHiddenField(type));
                ob.put("type", type);

                Log.e("QueANh Address : ", jsonObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            hidden.remove(type);
            AdditionalFieldModel model = getModel(type);
            if (model != null) {
                model.setSelected(true);
            }
        }
    }

//    public void storeDataProfile() {
//        try {
//            root.put("base_info", baseInfo);
//            root.put("additional", additional);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        PreferenceUtils.saveString(Constants.USER_INFO_JSON, getActivity(), root.toString());
//    }

    public AdditionalFieldModel getModel(String type) {
        AdditionalFieldModel model = null;
        for (int i = 0; i < dataAdditional.size(); i++) {
            if (dataAdditional.get(i).getType().equals(type)) {
                model = dataAdditional.get(i);
                break;
            }
        }

        return model;
    }

    public boolean checkHiddenField(String type) {
        boolean hide = false;
        for (int i = 0; i < hidden.size(); i++) {
            if (hidden.get(i).toString().equals(type)) {
                hide = true;
                break;
            }
        }

        return hide;
    }

    public boolean isEditMode = false;
    public boolean isSelectFieldMode = false;

    public void showSelectFieldView(boolean show) {
        layoutContainAdditional.setVisibility(show ? View.VISIBLE : View.GONE);
        isSelectFieldMode = show;
        if (show && dataAdditional.size() == 0) {
            getAdditionalField();
        }
    }

    public void unselectedAdditionalField() {
        for (AdditionalFieldModel item : dataAdditionalSelected) {
            dataAdditional.get(dataAdditional.indexOf(item)).setSelected(true);
        }
        adapter.notifyDataSetChanged();
    }

    public void showEditView(boolean show) {
        layoutView.setVisibility(show ? View.GONE : View.VISIBLE);
        layoutEdit.setVisibility(show ? View.VISIBLE : View.GONE);
        marginTop.setVisibility(show ? View.GONE : View.VISIBLE);

        HomeFragment home = (HomeFragment) getParentFragment();
        if (show) {
            home.disableViewpager();
        } else {
            home.enableViewpager();
        }

        HomeScreen.instance.lockNavigation(show);
        isEditMode = show;
        dataAdditionalSelected.clear();
    }

    private String passCode = "";

    public void saveProfile(boolean isNew) {

        progressBar.setVisibility(View.VISIBLE);

        final Context context = getActivity();
        AQuery mAQuery = new AQuery(context);
        getInfoAdditionField();

        if (!isValid) {
            isValid = true;
            progressBar.setVisibility(View.GONE);
            return;
        }

        params.put("first_name", edtFirstName.getText().toString().trim());
        params.put("last_name", edtLastName.getText().toString().trim());
        params.put("job_title", edtJobTitle.getText().toString().trim());
        params.put("mobile", edtMobile.getText().toString().trim());
        if (TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
            params.put("email", emailFB);
        } else {
            params.put("email", edtEmail.getText().toString().trim());
        }

        if (!TextUtils.isEmpty(passCode)) {
            params.put("passcode", passCode);
            passCode = "";
        }

        if (!TextUtils.isEmpty(idFB)) {
            params.put("facebook", idFB);
        }

        if (!isNew) {
            String id = PreferenceUtils.getString(Constants.USER_ID, getActivity(), "");
            params.put("id", id);
        } else {
            params.remove("id");
        }

        int countHidden = hidden.size();
        String hiddenFields = "";

        for (int i = 0; i < countHidden; i++) {
            if (i == 0) {
                hiddenFields = hidden.get(0);
            } else {
                hiddenFields = hiddenFields + "," + hidden.get(i);
            }
        }
        Log.e("QueANh_hidden_fields : ", hiddenFields);

//        if (!TextUtils.isEmpty(hiddenFields)) {
//            params.put("hidden_fields", hiddenFields);
//        }
        params.put("hidden_fields", hiddenFields);

        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, final JSONObject object, AjaxStatus status) {
                progressBar.setVisibility(View.GONE);

                try {
                    final JSONObject obj = object.getJSONObject("response");
                    if (obj.getString("success").equals("true")) {
                        showEditView(false);

                        JSONArray userInfo = obj.getJSONArray("user");
                        JSONObject userData = PreferenceUtils.saveUserInfo(context, userInfo);

                        String id = userData.getJSONObject("id").getString("value");
                        PreferenceUtils.saveString(Constants.USER_ID, context, id);

                        if (isRestoreAccount) {
                            isRestoreAccount = false;
                            dialogEnterPassCode.dismiss();
                            dataAdditional.clear();
                            additionalFieldDisable.clear();
                            restoreAccount(userData);
                            ((HomeScreen) getActivity()).closeProgressDialog();
                        }

                        if (TextUtils.isEmpty(PreferenceUtils.getString(Constants.FB_ID, getActivity(), ""))) {
//                            if (userData.has(Constants.FIELD_FACEBOOK)) {
//                                PreferenceUtils.saveString(Constants.FB_ID, getActivity(), userData.getJSONObject("facebook").getString("value"));
//                            } else {
//                                PreferenceUtils.saveString(Constants.FB_ID, getActivity(), idFB);
//                                idFB = "";
//                            }
                            idFB = "";
                        } else {
                            if (userData.has(Constants.FIELD_FACEBOOK)) {
                                if (!userData.getJSONObject("facebook").getString("value").equals(PreferenceUtils.getString(Constants.FB_ID, getActivity(), ""))) {
                                    LoginManager.getInstance().logOut();
//                                    PreferenceUtils.saveString(Constants.FB_ID, getActivity(), userData.getJSONObject("facebook").getString("value"));
                                    PreferenceUtils.saveString(Constants.FB_ID, getActivity(), "");
                                }
                            } else {
                                LoginManager.getInstance().logOut();
                                PreferenceUtils.saveString(Constants.FB_ID, getActivity(), "");
                            }
                        }

                        checkConnectFB();
                        reloadProfileData(userData);
                    } else {
                        try {
                            String error = obj.getString("error");
                            switch (error) {
                                case "existingEmail":
                                    existingEmail(obj.getString("userId"));
                                    break;
                                case "editExistingEmail":
                                    existingEmail(obj.getString("duplicateUserId"));
                                    break;
                                case "editNonExistingEmail":
                                    showPopupAlertEditEmail();
                                    break;
                                case "incorrectPasscode":
                                    isRestoreAccount = false;
                                    Toast.makeText(getActivity(), "Passcode is incorrect. Please try again!", Toast.LENGTH_SHORT).show();
                                    break;
                                default:
                                    Toast.makeText(getActivity(), "Server error!", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "API Error!", Toast.LENGTH_SHORT).show();
                }
            }
        };

        String url = AppUtils.getSaveProfileUrl(context);
        mAQuery.ajax(url, params, JSONObject.class, callback);

    }

    private void existingEmail(String restoreId) {
//        Toast.makeText(getActivity(), "Email has been used by another user!", Toast.LENGTH_SHORT).show();
        edtEmail.requestFocus();
        restoreUserId = restoreId;
        showPopupExistingEmail();
    }

    private void restoreAccount(JSONObject userData) {
        try {
            boolean rated = userData.getJSONObject("rated").getBoolean("value");
            PreferenceUtils.saveBoolean(Constants.RATED, getActivity(), rated);

            String purchase = userData.getJSONObject("purchase_type").getString("value");
            if (purchase != null && !purchase.equals("null")) {
                PreferenceUtils.saveString(Constants.PURCHASED, getActivity(), purchase);
            }

            String avatar = userData.getJSONObject("avatar").getString("value");
            if (avatar != null && !avatar.isEmpty() && !avatar.equals("null")) {
                avatarName = avatar;
                PreferenceUtils.saveString(Constants.AVATAR_URL, getActivity(), avatar);
                File file = BumpUtils.getFile("avatar.jpg", true);
                file.delete();

                loadAvatar(avatar, true, true, false);
            }

            String channel = "fibu_" + PreferenceUtils.getString(Constants.USER_ID, getActivity(), "");
            ParsePush.subscribeInBackground(channel);

            ProfileFragment.hideKeyBoard(getActivity());

            PreferenceUtils.saveBoolean(Constants.IS_RESTORE_ACCOUNT, getActivity(), true);
            Fragment fragment = getFragmentManager().findFragmentByTag(
                    "android:switcher:" + R.id.pager + ":5");
            if (fragment != null) {
                ((ExtraPowerFragment) fragment).refreshProAccount();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private AlertDialog dialogEnterPassCode;
    private boolean isRestoreAccount;

    private void showPopupEnterPassCode() {
        if (dialogEnterPassCode != null) {
            dialogEnterPassCode.dismiss();
        }

        final Activity activity = getActivity();
        dialogEnterPassCode = new AlertDialog.Builder(activity).create();

        final View view = View.inflate(activity, R.layout.enter_restore_code_popup, null);

        final EditText txtPassCode = (EditText) view.findViewById(R.id.txtPassCode);
        View txtResend = view.findViewById(R.id.txtResend);
        View txtRestore = view.findViewById(R.id.txtRestore);

        txtPassCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    String code = textView.getText().toString().trim();
                    if (code.isEmpty()) {
                        Toast.makeText(activity, "Please enter passcode", Toast.LENGTH_SHORT).show();
                        return true;
                    }

                    ((HomeScreen) getActivity()).showLoadingDialog(false);
                    isRestoreAccount = true;
                    passCode = code;
                    saveProfile(false);
                }

                return false;
            }
        });

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.txtResend:
                        String url = AppUtils.getResendCodeUrl(activity);

                        Builders.Any.B request = Ion.with(activity).load(url);
                        request.setBodyParameter("user_id", restoreUserId);
                        request.asJsonObject().setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (e == null && result != null) {
                                    JsonObject response = result.getAsJsonObject("response");
                                    boolean success = response.getAsJsonPrimitive("success").getAsBoolean();
                                    if (success) {
                                        String code = response.getAsJsonPrimitive("passcode").getAsString();
                                        Log.e("Pass Code", code);

                                        Toast.makeText(activity, "Passcode has been resent successfully. Please check your email and continue.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        String error = response.getAsJsonPrimitive("error").getAsString();
                                        switch (error) {
                                            case "notProAccount":
                                                new AlertDialog.Builder(activity).setMessage("You have not purchased the Pro account yet.")
                                                        .setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                hideKeyboard(activity, view);

                                                                ProfileFragment.hideKeyBoard(activity);
                                                                Intent intent = new Intent(activity, ExtraPowerScreen.class);
                                                                intent.putExtra(ExtraPowerScreen.EXTRA_USER_ID, restoreUserId);
                                                                startActivityForResult(intent, RC_PURCHASE);
                                                            }
                                                        }).show();
                                                break;
                                        }
                                    }
                                }
                            }
                        });
                        break;
                    case R.id.txtRestore:
                        String code = txtPassCode.getText().toString().trim();
                        if (code.isEmpty()) {
                            Toast.makeText(activity, "Please enter passcode", Toast.LENGTH_SHORT).show();
                        } else {
                            hideKeyboard(activity, txtPassCode);
                            passCode = code;
                            isRestoreAccount = true;
                            ((HomeScreen) getActivity()).showLoadingDialog(false);
                            saveProfile(false);
                        }
                        break;
                }
            }
        };

        txtResend.setOnClickListener(onClickListener);
        txtRestore.setOnClickListener(onClickListener);

        dialogEnterPassCode.setView(view);
        dialogEnterPassCode.show();

        txtPassCode.requestFocus();
        showKey();
    }

    public void showKey() {
        FragmentActivity context = getActivity();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void autoScroll() {
        layoutViewEdit.post(new Runnable() {
            @Override
            public void run() {
                layoutViewEdit.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    public void showPopupAlertDeleteField(final boolean isAvailable, final String type, final View view, final int position) {

        AlertDialog alertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Confirm delete this field?");
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        try {
                            layoutFieldEdit.removeView(view);

                            countView--;
                            params.remove(type);

                            if (isAvailable) {
                                additionalFieldDisable.remove(type);
                                getModel(type).setSelected(true);
                            } else {
                                dataAdditional.get(position).setSelected(true);
                            }

                            adapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

        alertDialog = builder.show();
        TextView messageText = (TextView) alertDialog
                .findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    private void showPopupExistingEmail() {
        final Context context = getActivity();
        final AlertDialog dialogRestore = new AlertDialog.Builder(context).create();
        View view = View.inflate(context, R.layout.restore_acount_popup, null);

        dialogRestore.setView(view);

        View txtYes = view.findViewById(R.id.txtYes);
        View txtNo = view.findViewById(R.id.txtNo);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.txtYes:
                        dialogRestore.dismiss();
                        showPopupEnterPassCode();
                        break;
                    case R.id.txtNo:
                        dialogRestore.dismiss();

                        String purchase = PreferenceUtils.getString(Constants.PURCHASED, context, null);
                        if (purchase == null) {
                            edtEmail.requestFocus();
                            Activity activity = getActivity();
                            hideKeyboard(activity, edtEmail);

                            Intent intent = new Intent(context, ExtraPowerScreen.class);
                            intent.putExtra(ExtraPowerScreen.EXTRA_USER_ID, restoreUserId);
                            startActivityForResult(intent, RC_PURCHASE);
                        }
                        break;
                }
            }
        };

        txtYes.setOnClickListener(onClickListener);
        txtNo.setOnClickListener(onClickListener);
        dialogRestore.show();
    }

    private static final int RC_PURCHASE = 1001;

    public void showPopupAlertEditEmail() {

        AlertDialog alertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setNegativeButton("Cancel", null);

        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveProfile(true);
                    }
                });

        TextView messageText = new TextView(getActivity());
        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        messageText.setText(R.string.msg_error_edit_non_existing_email);
        messageText.setPadding(10, 10, 10, 10);
        messageText.setTextSize(18);
        builder.setView(messageText);

        TextView title = new TextView(getActivity());
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setText("Do you want to change your main email address?");
        title.setPadding(10, 10, 10, 10);
        title.setTextSize(20);
        title.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        builder.setCustomTitle(title);

        alertDialog = builder.show();

    }

    private Handler handler = new Handler();
    private int count;
    private boolean stop = false;

    public void pause() {
        stop = true;
    }

    public void resume() {
        if (count > 0 && stop) {
            stop = false;
            beginCountDown();
        }
    }

    public void beginCountDown() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (--count <= 0) {
                    bumpCardLayout.setVisibility(View.GONE);
                    PreferenceUtils.saveBoolean(Constants.SHOW_BUMP_CARD, getActivity(), false);
                } else if (!stop) {
                    beginCountDown();
                }
            }
        }, 1000 / 4);
    }

    public void hideKeyboard(Activity activity, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showSkipVideo() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                TextView txtSkip = (TextView) videoLayout.findViewById(R.id.txtSkip);
                SpannableString content = new SpannableString("Skip");
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                txtSkip.setText(content);
                txtSkip.setVisibility(View.VISIBLE);

                txtSkip.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        stopVideo();
                    }
                });
            }
        }, 10000);
    }
}
