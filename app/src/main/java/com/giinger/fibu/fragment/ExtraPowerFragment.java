package com.giinger.fibu.fragment;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.giinger.fibu.R;
import com.giinger.fibu.activity.BaseFileScreen;
import com.giinger.fibu.activity.ExtraPowerScreen;
import com.giinger.fibu.activity.FileMusicScreen;
import com.giinger.fibu.activity.HomeScreen;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.PreferenceUtils;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class ExtraPowerFragment extends Fragment implements View.OnClickListener {

    public static final String EXTRA_USER_ID = "extra_user_id";

    private IInAppBillingService iapService;
    private ServiceConnection iapConnection;

    private View popupLayout, imgIcon;
    private View txtBuyAll, txtBuyOne, txtNoThanks;

    private TextView txtNowUcan, txtTitle, txtGoPro;

    private String userId;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.extra_power_fragment, container, false);

        popupLayout = view.findViewById(R.id.popupLayout);

        txtNowUcan = (TextView) view.findViewById(R.id.txtNowUcan);
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);

        imgIcon = view.findViewById(R.id.imgIcon);
        txtGoPro = (TextView) view.findViewById(R.id.txtGoPro);

        txtBuyAll = view.findViewById(R.id.txtBuyAll);
        txtBuyOne = view.findViewById(R.id.txtBuyOne);

        txtNoThanks = view.findViewById(R.id.txtNoThanks);

        txtGoPro.setOnClickListener(this);

        txtNoThanks.setOnClickListener(this);

        txtBuyAll.setOnClickListener(this);
        txtBuyOne.setOnClickListener(this);

        Bundle args = getArguments();
        if (args != null) {
            userId = args.getString(EXTRA_USER_ID);
        }

        if (userId == null) {
            Activity activity = getActivity();
            userId = PreferenceUtils.getString(Constants.USER_ID, activity, "");
        }

        String purchased = PreferenceUtils.getString(Constants.PURCHASED, getContext(), null);
        if (purchased != null) {
            purchasedView();
        } else {
            bindIAPService();
        }

        return view;
    }

    private void purchasedView() {
        txtGoPro.setVisibility(View.GONE);
        imgIcon.setVisibility(View.VISIBLE);
        txtTitle.setText(R.string.text_extra_already_bought_pro);
        txtNowUcan.setText(R.string.text_extra_now_you_can);
    }

    public boolean hidePopupLayout() {
        if (popupLayout != null && popupLayout.isShown()) {
            popupLayout.setVisibility(View.GONE);
            if (getActivity() instanceof HomeScreen) {
                HomeFragment fragment = (HomeFragment) getParentFragment();
                fragment.enableViewpager();
                ((HomeScreen) getActivity()).lockDrawer(false);
            }

            return true;
        }

        return false;
    }

    private class GetIapDetail extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                ArrayList<String> skuList = new ArrayList<>();
                Context context = getContext();
                String sku = PreferenceUtils.getString(Constants.IAP_ITEM, context, "");
                skuList.add(sku);
                Bundle query = new Bundle();
                query.putStringArrayList("ITEM_ID_LIST", skuList);

                String packageName = context.getPackageName();
                Bundle details = iapService.getSkuDetails(3,
                        packageName, "inapp", query);

                if (details != null) {
                    int code = details.getInt("RESPONSE_CODE", -1);
                    if (code == 0) {
                        ArrayList<String> items = details.getStringArrayList("DETAILS_LIST");
                        if (items != null && !items.isEmpty()) {
                            String item = items.get(0);
                            JSONObject json = new JSONObject(item);
                            return json.getString("price");
                        }
                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String price) {
            if (price != null) {
                String text = getString(R.string.text_go_pro_with_price, price);
                txtGoPro.setText(text);
            }
        }
    }

    private void bindIAPService() {
        iapConnection = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                iapService = null;
            }

            @Override
            public void onServiceConnected(ComponentName name,
                                           IBinder service) {
                iapService = IInAppBillingService.Stub.asInterface(service);

                new GetIapDetail().execute();
            }
        };

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");

        Activity activity = getActivity();
        activity.bindService(serviceIntent, iapConnection, Context.BIND_AUTO_CREATE);
    }

    //    private static final int RC_PURCHASE_ONE = 6001;
    private static final int RC_PURCHASE_ALL = 6002;

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
//            case RC_PURCHASE_ONE:
            case RC_PURCHASE_ALL:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        if (userId != null) {
                            final Activity activity = getActivity();
                            final String type = requestCode == RC_PURCHASE_ALL ? Constants.PURCHASE.BUY_ALL.type : Constants.PURCHASE.BUY_ONE.type;

                            PreferenceUtils.saveString(Constants.PURCHASED, activity, type);
                            if (type.equals(Constants.PURCHASE.BUY_ALL.type)) {
                                ContactFragment.MAX_SELECTED_ITEMS = 10000;
                                PhotoFragment.MAX_SELECTED = 11;
                                FileMusicScreen.MAX_SELECTED_ITEMS = 7;
                                BaseFileScreen.MAX_SIZE = 30 * 1024 * 1024;
                            } else if (type.equals(Constants.PURCHASE.BUY_ONE.type)) {
                                ContactFragment.MAX_SELECTED_ITEMS = 15;
                                PhotoFragment.MAX_SELECTED = 11;
                                FileMusicScreen.MAX_SELECTED_ITEMS = 7;
                                BaseFileScreen.MAX_SIZE = 30 * 1024 * 1024;
                            }

                            purchasedView();

                            String url = AppUtils.getConfirmUrl(activity);

                            Builders.Any.B request = Ion.with(this).load(url);
                            request.setBodyParameter("userId", userId);

                            request.setBodyParameter("action", type);

                            request.asJsonObject().setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {
                                    Log.e("Purchase", result + "");

                                    if (e == null && result != null) {
                                        if (activity instanceof ExtraPowerScreen) {
                                            activity.setResult(Activity.RESULT_OK);
                                            activity.finish();
                                        }
                                    }
                                }
                            });

                            if (activity instanceof HomeScreen) {
                                HomeFragment fragment = (HomeFragment) getParentFragment();
                                fragment.enableViewpager();
                                ((HomeScreen) activity).lockDrawer(false);

                            }
                        }
                        break;
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (iapService != null) {
            Activity activity = getActivity();
            activity.unbindService(iapConnection);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        Activity activity = getActivity();

        switch (id) {
//            case R.id.txtNoThanks:
            case R.id.txtGoPro:
//                boolean popupShown = id == R.id.txtGoPro;
//                popupLayout.setVisibility(popupShown ? View.VISIBLE : View.GONE);
//
//                if (getActivity() instanceof HomeScreen) {
//                    HomeFragment fragment = (HomeFragment) getParentFragment();
//                    if (popupShown) {
//                        fragment.disableViewpager();
//                        ((HomeScreen) getActivity()).lockDrawer(true);
//                    } else {
//                        fragment.enableViewpager();
//                        ((HomeScreen) getActivity()).lockDrawer(false);
//                    }
//                }
//
//                break;
//            case R.id.txtBuyAll:
//            case R.id.txtBuyOne:
                int requestCode = RC_PURCHASE_ALL;
                String purchaseType = Constants.PURCHASE.BUY_ALL.type;
                String sku = PreferenceUtils.getString(Constants.IAP_ITEM, activity, "");

                try {
                    String packageName = activity.getPackageName();
                    String payload = "inapp_purchase_fibu_v2";
                    String type = "inapp";

                    Bundle buyIntent = iapService.getBuyIntent(3, packageName, sku, type, payload);

                    int responseCode = buyIntent.getInt("RESPONSE_CODE");
                    if (responseCode == 0) {
                        PendingIntent intent = buyIntent.getParcelable("BUY_INTENT");

                        IntentSender sender = intent.getIntentSender();
                        activity.startIntentSenderForResult(sender, requestCode, new Intent(), 0, 0, 0);
                    } else if (responseCode == 7) { //TODO: consume for testing
                        iapService.consumePurchase(3, packageName,
                                "inapp:" + packageName + ":"
                                        + sku);

                        onClick(view);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.txtRestore:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        popupLayout.setVisibility(View.GONE);
    }

    public void refreshProAccount() {
        userId = PreferenceUtils.getString(Constants.USER_ID, getActivity(), "");
        ContactFragment.MAX_SELECTED_ITEMS = 10000;
        PhotoFragment.MAX_SELECTED = 11;
        FileMusicScreen.MAX_SELECTED_ITEMS = 7;
        BaseFileScreen.MAX_SIZE = 30 * 1024 * 1024;
        purchasedView();
    }
}
