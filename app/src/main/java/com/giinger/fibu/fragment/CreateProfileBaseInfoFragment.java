package com.giinger.fibu.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import com.giinger.fibu.R;
import com.giinger.fibu.activity.CreateProfileScreen;
import com.giinger.fibu.element.CircleImageView;
import com.giinger.fibu.element.CustomButton;
import com.giinger.fibu.element.CustomEditText;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.OnRefreshListener;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.Utils;

public class CreateProfileBaseInfoFragment extends Fragment implements OnRefreshListener {

    private CustomEditText edtFirstName;
    private CustomEditText edtLastName;
    private CustomEditText edtJobTitle;
    private CustomButton btnNext;
    private CircleImageView imgAvatar;

    private RelativeLayout layoutAddAvatar;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.create_profile_base_info_fragment, null, false);

        edtFirstName = (CustomEditText) view.findViewById(R.id.edt_first_name);
        edtLastName = (CustomEditText) view.findViewById(R.id.edt_last_name);
        edtJobTitle = (CustomEditText) view.findViewById(R.id.edt_job_title);
        btnNext = (CustomButton) view.findViewById(R.id.btn_next_base_info);
        imgAvatar = (CircleImageView) view.findViewById(R.id.img_avatar);
        edtFirstName.requestFocus();

        layoutAddAvatar = (RelativeLayout) view.findViewById(R.id.layout_avatar);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edtFirstName.getText().toString()) || TextUtils.isEmpty(edtLastName.getText().toString())) {
                    Context context = getActivity();
                    String message = context.getString(R.string.msg_require_first_name_and_last_name);
                    String ok = context.getString(R.string.text_ok);
                    new AlertDialog.Builder(context).setMessage(message).setPositiveButton(ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
                } else {
                    String firstName = edtFirstName.getText().toString();
                    String lastName = edtLastName.getText().toString();
                    String jobTitle = edtJobTitle.getText().toString();

                    PreferenceUtils.saveString(Constants.FIRST_NAME, getActivity(), firstName);
                    PreferenceUtils.saveString(Constants.LAST_NAME, getActivity(), lastName);
                    PreferenceUtils.saveString(Constants.JOB_TITLE, getActivity(), jobTitle);

                    CreateProfileDetailInfoFragment fragment = (CreateProfileDetailInfoFragment) getFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + 2);
                    fragment.onRefresh();

                    fragment.onSetImageAvatar(Constants.imgUri);
                    CreateProfileScreen.nextPage(2);
                    showKeyBoard();
                }
            }
        });

        layoutAddAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAvatar(view);
            }
        });

        imgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAvatar(view);
            }
        });

        return view;
    }

    private void showKeyBoard() {
        FragmentActivity activity = getActivity();
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public void editAvatar(View view) {
        FragmentActivity activity = getActivity();

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        Utils.getInstance().showLayoutDialogEditAvatar(view, activity);
        btnNext.setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {
    }

    @Override
    public void onSetImageAvatar(String path) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        imgAvatar.setImageBitmap(bitmap);
        layoutAddAvatar.setVisibility(View.GONE);
        imgAvatar.setVisibility(View.VISIBLE);
    }
}
