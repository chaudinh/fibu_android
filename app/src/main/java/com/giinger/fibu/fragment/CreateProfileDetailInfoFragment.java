package com.giinger.fibu.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.giinger.fibu.R;
import com.giinger.fibu.activity.ExtraPowerScreen;
import com.giinger.fibu.activity.HomeScreen;
import com.giinger.fibu.adapter.AddFieldAdapter;
import com.giinger.fibu.element.CircleImageView;
import com.giinger.fibu.element.CustomButton;
import com.giinger.fibu.element.CustomEditText;
import com.giinger.fibu.element.CustomTextView;
import com.giinger.fibu.model.AdditionalFieldModel;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.HttpClient;
import com.giinger.fibu.util.OnRefreshListener;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.Utils;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.parse.ParsePush;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CreateProfileDetailInfoFragment extends Fragment implements OnRefreshListener, View.OnClickListener, View.OnFocusChangeListener, CustomEditText.BackPressedListener {

    private CustomEditText tvFirstName;
    private CustomEditText tvLastName;
    private CustomEditText tvJobTitle;

    private CustomEditText edtMobile;
    private CustomEditText edtEmail;

    private ImageView imgEyePhone;
    private ImageView imgEyeMail;
    private ImageView imgEyeAddress;

    private CustomButton btnBump, btnBump2;
    private CircleImageView imgAvatar;
    private RelativeLayout layoutSettingUp;

    private ProgressBar progressBar;

    private String firstName;
    private String lastName;
    private String jobTitle;

    private File file;
    private ArrayList<String> hidden;
    private Map<String, String> params;
    private JSONObject baseInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseInfo = new JSONObject();
        params = new HashMap<>();
        hidden = new ArrayList<>();
    }

    private void setKeyboardIsShown(boolean shown) {
//        btnBump2.setVisibility(shown ? View.VISIBLE : View.INVISIBLE);
//        btnBump.setVisibility(shown ? View.INVISIBLE : View.VISIBLE);
    }

    public void hideKeyboard(Activity activity, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

            setKeyboardIsShown(false);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.create_profile_detail_info_fragment, null, false);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity activity = getActivity();
                hideKeyboard(activity, view);
            }
        });

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        tvFirstName = (CustomEditText) view.findViewById(R.id.tv_first_name);
        tvLastName = (CustomEditText) view.findViewById(R.id.tv_last_name);
        tvJobTitle = (CustomEditText) view.findViewById(R.id.tv_job_title);

        edtMobile = (CustomEditText) view.findViewById(R.id.edt_mobile);
        edtMobile.setOnClickListener(this);
        edtMobile.setBackPressedListener(this);
        edtMobile.setOnFocusChangeListener(this);

        edtEmail = (CustomEditText) view.findViewById(R.id.edt_email);
        edtEmail.setOnClickListener(this);
        edtEmail.setBackPressedListener(this);
        edtEmail.setOnFocusChangeListener(this);

        edtEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    performSaveProfile(v);
                    return true;
                }

                return false;
            }
        });

        imgEyePhone = (ImageView) view.findViewById(R.id.img_eye);
        imgEyeMail = (ImageView) view.findViewById(R.id.img_eye_email);

        imgEyePhone.setOnClickListener(this);
        imgEyeMail.setOnClickListener(this);

        layoutSettingUp = (RelativeLayout) view.findViewById(R.id.layout_setting_up);

        imgAvatar = (CircleImageView) view.findViewById(R.id.img_avatar);

        imgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setKeyboardIsShown(false);
                editAvatar(view);
            }
        });

        btnBump = (CustomButton) view.findViewById(R.id.btn_bump);
        btnBump2 = (CustomButton) view.findViewById(R.id.btn_bump2);

        btnBump.setOnClickListener(this);
        btnBump2.setOnClickListener(this);

        tvFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().isEmpty()) {
                    btnBump2.setVisibility(View.INVISIBLE);
                } else if (tvLastName.getText().toString().trim().isEmpty()) {
                    btnBump2.setVisibility(View.INVISIBLE);
                } else {
                    btnBump2.setVisibility(View.VISIBLE);
                }
            }
        });

        tvLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().isEmpty()) {
                    btnBump2.setVisibility(View.INVISIBLE);
                } else if (tvFirstName.getText().toString().trim().isEmpty()) {
                    btnBump2.setVisibility(View.INVISIBLE);
                } else {
                    btnBump2.setVisibility(View.VISIBLE);
                }
            }
        });

        return view;
    }

    private void lockBump(boolean lock) {
        btnBump.setOnClickListener(lock ? null : this);
        btnBump.setOnClickListener(lock ? null : this);
    }

    private void performSaveProfile(View v) {
        lockBump(true);

        Activity activity = getActivity();
//        String email = edtEmail.getText().toString();
//        if (TextUtils.isEmpty(edtMobile.getText().toString()) || TextUtils.isEmpty(email)) {
//            Toast.makeText(activity, "Please fill email and mobile field!", Toast.LENGTH_LONG).show();
//            lockBump(false);
//        } else {
//            boolean emailValid = Utils.getInstance().isEmailValid(email);
//            if (emailValid) {
                firstName = tvFirstName.getText().toString();
                lastName = tvLastName.getText().toString();
                jobTitle = tvJobTitle.getText().toString();

                if (firstName.isEmpty() || lastName.isEmpty()) {
                    String message = getString(R.string.msg_require_first_name_and_last_name);
                    String ok = getString(R.string.text_ok);
                    new AlertDialog.Builder(activity).setMessage(message).setPositiveButton(ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
                } else {
                    hideKeyboard(activity, v);
                    saveProfile();
                }
//            } else {
//                Toast.makeText(activity, "Wrong format Email!", Toast.LENGTH_LONG).show();
//                lockBump(true);
//            }
//        }
    }

    public void editAvatar(View view) {
        FragmentActivity activity = getActivity();

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        btnBump.setVisibility(View.GONE);
        Utils.getInstance().showLayoutDialogEditAvatar(view, activity);
    }

    public void saveProfile(String... passCode) {

        final Activity activity = getActivity();
        AQuery mAQuery = new AQuery(activity);

        layoutSettingUp.setVisibility(View.VISIBLE);

        params.put("first_name", firstName);
        params.put("last_name", lastName);
        params.put("job_title", jobTitle);
        params.put("mobile", edtMobile.getText().toString());
        params.put("email", edtEmail.getText().toString());

        params.put("passcode", passCode.length > 0 ? passCode[0] : null);

        try {
            baseInfo.put("first_name", firstName);
            baseInfo.put("last_name", lastName);
            baseInfo.put("job_title", jobTitle);
            baseInfo.put("mobile", edtMobile.getText().toString());
            baseInfo.put("mobile_hidden", checkHiddenField("mobile"));
            baseInfo.put("email", edtEmail.getText().toString());
            baseInfo.put("email_hidden", checkHiddenField("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        int countHidden = hidden.size();
        String hiddenFields = "";

        for (int i = 0; i < countHidden; i++) {
            if (i == 0) {
                hiddenFields = hidden.get(0);
            } else {
                hiddenFields = hiddenFields + "," + hidden.get(i);
            }
        }
        Log.e("QueANh_hidden_fields : ", hiddenFields);

        if (!TextUtils.isEmpty(hiddenFields)) {
            params.put("hidden_fields", hiddenFields);
        }

        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                try {
                    Log.e("QueAnh : ", object + "");

                    JSONObject obj = object.getJSONObject("response");
                    if (obj.getString("success").equals("true")) {

                        JSONArray userInfo = obj.getJSONArray("user");
                        JSONObject userData = PreferenceUtils.saveUserInfo(activity, userInfo);

                        String id = userData.getJSONObject("id").getString("value");
                        PreferenceUtils.saveString(Constants.USER_ID, activity, id);

                        boolean rated = userData.getJSONObject("rated").getBoolean("value");
                        PreferenceUtils.saveBoolean(Constants.RATED, activity, rated);

                        String purchase = userData.getJSONObject("purchase_type").getString("value");
                        if (purchase != null && !purchase.equals("null")) {
                            PreferenceUtils.saveString(Constants.PURCHASED, activity, purchase);
                        }

                        String avatar = userData.getJSONObject("avatar").getString("value");
                        if (avatar != null && !avatar.isEmpty() && !avatar.equals("null")) {
                            PreferenceUtils.saveString(Constants.AVATAR_URL, activity, avatar);
                            File file = BumpUtils.getFile("avatar.jpg", true);
                            file.delete();

                            Utils.saveAvatar(activity, avatar);
                        }

                        String channel = "fibu_" + id;
                        ParsePush.subscribeInBackground(channel);

                        if (TextUtils.isEmpty(Constants.imgUri)) {
                            activity.startActivity(new Intent(activity, HomeScreen.class));
                            activity.finish();
                        } else {
                            uploadAvatar(id);
                        }

                        ProfileFragment.hideKeyBoard(activity);

                        PreferenceUtils.saveBoolean(Constants.IS_RESTORE_ACCOUNT, activity, true);
                    } else {
                        String error = obj.getString("error");
                        switch (error) {
                            case "existingEmail":
                                userId = obj.getString("userId");
                                showPopupExistingEmail();
                                break;
                            case "incorrectPasscode":
                                Toast.makeText(activity, "Passcode is incorrect. Please try again!", Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                Toast.makeText(activity, "Server error!", Toast.LENGTH_SHORT).show();
                                break;
                        }

                        layoutSettingUp.setVisibility(View.GONE);
                        lockBump(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    Toast.makeText(activity, "Error!", Toast.LENGTH_SHORT).show();
                    layoutSettingUp.setVisibility(View.GONE);
                    lockBump(false);
                }
            }
        };

        String url = AppUtils.getSaveProfileUrl(activity);
        mAQuery.ajax(url, params, JSONObject.class, callback);

    }

    private void showPopupExistingEmail() {
        final Context context = getActivity();
        final AlertDialog dialogRestore = new AlertDialog.Builder(context).create();
        View view = View.inflate(context, R.layout.restore_acount_popup, null);

        dialogRestore.setView(view);

        View txtYes = view.findViewById(R.id.txtYes);
        View txtNo = view.findViewById(R.id.txtNo);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.txtYes:
                        dialogRestore.dismiss();
                        showPopupEnterPassCode();
                        break;
                    case R.id.txtNo:
                        dialogRestore.dismiss();

                        String purchase = PreferenceUtils.getString(Constants.PURCHASED, context, null);
                        if (purchase == null) {
                            edtEmail.requestFocus();
                            Activity activity = getActivity();
                            hideKeyboard(activity, edtEmail);

                            Intent intent = new Intent(context, ExtraPowerScreen.class);
                            intent.putExtra(ExtraPowerScreen.EXTRA_USER_ID, userId);
                            startActivityForResult(intent, RC_PURCHASE);
                        }
                        break;
                }
            }
        };

        txtYes.setOnClickListener(onClickListener);
        txtNo.setOnClickListener(onClickListener);
        dialogRestore.show();
    }

    private AlertDialog dialogEnterPassCode;

    @Override
    public void onStop() {
        super.onStop();

        if (dialogEnterPassCode != null) {
            dialogEnterPassCode.dismiss();
        }
    }

    private void showPopupEnterPassCode() {
        if (dialogEnterPassCode != null) {
            dialogEnterPassCode.dismiss();
        }

        final Activity activity = getActivity();
        dialogEnterPassCode = new AlertDialog.Builder(activity).create();

        final View view = View.inflate(activity, R.layout.enter_restore_code_popup, null);

        final EditText txtPassCode = (EditText) view.findViewById(R.id.txtPassCode);
        View txtResend = view.findViewById(R.id.txtResend);
        View txtRestore = view.findViewById(R.id.txtRestore);

        txtPassCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    String code = textView.getText().toString();
                    if (code.isEmpty()) {
                        Toast.makeText(activity, "Please enter passcode", Toast.LENGTH_SHORT).show();
                        return true;
                    }

                    saveProfile(code);
                }

                return false;
            }
        });

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.txtResend:
                        String url = AppUtils.getResendCodeUrl(activity);

                        Builders.Any.B request = Ion.with(activity).load(url);
                        request.setBodyParameter("user_id", userId);
                        request.asJsonObject().setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (e == null && result != null) {
                                    JsonObject response = result.getAsJsonObject("response");
                                    boolean success = response.getAsJsonPrimitive("success").getAsBoolean();
                                    if (success) {
                                        String code = response.getAsJsonPrimitive("passcode").getAsString();
                                        Log.e("Pass Code", code);

                                        Toast.makeText(activity, "Passcode has been resent successfully. Please check your email and continue.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        String error = response.getAsJsonPrimitive("error").getAsString();
                                        switch (error) {
                                            case "notProAccount":
                                                new AlertDialog.Builder(activity).setMessage("You have not purchased the Pro account yet.")
                                                        .setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                hideKeyboard(activity, view);

                                                                ProfileFragment.hideKeyBoard(activity);
                                                                Intent intent = new Intent(activity, ExtraPowerScreen.class);
                                                                intent.putExtra(ExtraPowerScreen.EXTRA_USER_ID, userId);
                                                                startActivityForResult(intent, RC_PURCHASE);
                                                            }
                                                        }).show();
                                                break;
                                        }
                                    }
                                }
                            }
                        });
                        break;
                    case R.id.txtRestore:
                        String code = txtPassCode.getText().toString();
                        if (code.isEmpty()) {
                            Toast.makeText(activity, "Please enter passcode", Toast.LENGTH_SHORT).show();
                        } else {
                            hideKeyboard(activity, txtPassCode);
                            saveProfile(code);
                        }
                        break;
                }
            }
        };

        txtResend.setOnClickListener(onClickListener);
        txtRestore.setOnClickListener(onClickListener);

        dialogEnterPassCode.setView(view);
        dialogEnterPassCode.show();

        txtPassCode.requestFocus();
        showKey();
    }

    public void uploadAvatar(String id) {
        Activity activity = getActivity();
        file = new File(Constants.imgUri);
        UpAvatarTask upAvatarTask = new UpAvatarTask(activity);
        upAvatarTask.execute(id);
    }

    private static final int RC_PURCHASE = 1001;

    private String userId = null;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case RC_PURCHASE:
                if (resultCode == Activity.RESULT_OK) {
                    showPopupEnterPassCode();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    public void onRefresh() {
        firstName = PreferenceUtils.getString(Constants.FIRST_NAME, getActivity(), "");
        lastName = PreferenceUtils.getString(Constants.LAST_NAME, getActivity(), "");
        jobTitle = PreferenceUtils.getString(Constants.JOB_TITLE, getActivity(), "");

        tvFirstName.setText(firstName);
        tvLastName.setText(lastName);
        tvJobTitle.setText(jobTitle);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                edtMobile.requestFocus();
            }
        });
    }

    @Override
    public void onSetImageAvatar(String path) {
        if (TextUtils.isEmpty(path)) {
            imgAvatar.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.empty_ava));
        } else {
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            imgAvatar.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.edt_email:
            case R.id.edt_mobile:
                setKeyboardIsShown(true);
                break;
            case R.id.btn_bump:
            case R.id.btn_bump2:
                performSaveProfile(v);
                break;
            case R.id.img_eye:
                if (v.getTag() != null && v.getTag().toString().equals("mobile")) {
                    imgEyePhone.setImageResource(R.drawable.eye_view_icon);
                    v.setTag(null);
                    hidden.remove("mobile");
                } else {
                    imgEyePhone.setImageResource(R.drawable.hidden_icon);
                    v.setTag("mobile");
                    hidden.add("mobile");
                }
                break;
            case R.id.img_eye_email:
                if (v.getTag() != null && v.getTag().toString().equals("email")) {
                    imgEyeMail.setImageResource(R.drawable.eye_view_icon);
                    v.setTag(null);
                    hidden.remove("email");
                } else {
                    imgEyeMail.setImageResource(R.drawable.hidden_icon);
                    v.setTag("email");
                    hidden.add("email");
                }
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean focus) {
        if (focus) {
            setKeyboardIsShown(true);
        }
    }

    @Override
    public void onImeBack(CustomEditText editText) {
        setKeyboardIsShown(false);
    }

    @Override
    public void onPause() {
        super.onPause();

        setKeyboardIsShown(false);
    }

    class UpAvatarTask extends AsyncTask<String, Void, JSONObject> {

        private Activity activity;

        public UpAvatarTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject data = null;
            try {
                String url = AppUtils.getUploadAvatarUrl(activity);
                HttpClient client = new HttpClient(url);

                client.connectForMultipart();
                client.addFormPart("user_id", params[0]);
                client.addFilePart("avatar", "avatar.jpg", Utils.getInstance()
                        .getFileData(file));

                client.finishMultipart();
                data = new JSONObject(client.getResponse());
            } catch (Throwable t) {
                t.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {
                if (result != null && result.getJSONObject("response").getString("success").equals("true")) {
                    Log.e("QueANh", "upload avatar json : " + result);

                    if (result.getJSONObject("response").has("avatar")) {
                        PreferenceUtils.saveString(Constants.AVATAR_URL, getActivity(), result.getJSONObject("response").getString("avatar"));
                        Constants.imgUri = "";
                    }

                    activity.startActivity(new Intent(activity, HomeScreen.class));
                    activity.finish();
                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
                layoutSettingUp.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Fail upload avatar!", Toast.LENGTH_LONG).show();
            }
//            proDialog.dismiss();
        }
    }

    public boolean checkHiddenField(String type) {
        boolean hide = false;
        for (int i = 0; i < hidden.size(); i++) {
            if (hidden.get(i).toString().equals(type)) {
                hide = true;
                break;
            }
        }

        return hide;
    }

    public void showKey() {
        FragmentActivity context = getActivity();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

}
