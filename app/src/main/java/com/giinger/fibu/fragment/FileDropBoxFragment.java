package com.giinger.fibu.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.exception.DropboxUnlinkedException;
import com.dropbox.client2.session.AppKeyPair;
import com.giinger.fibu.R;
import com.giinger.fibu.adapter.FileDocumentAdapter;
import com.giinger.fibu.model.FileItem;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.OnItemSharing;
import com.giinger.fibu.util.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by geekup on 4/1/15.
 */
public class FileDropBoxFragment extends Fragment {

    private ListView lvChooseDocument;
    private List<FileItem> data;
    private RelativeLayout headerDrop;

    private FileDocumentAdapter adapter;
    private View lnListItems, lnEmpty;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.file_document_screen, container, false);

        lnListItems = view.findViewById(R.id.lnListItems);
        lnEmpty = view.findViewById(R.id.lnEmpty);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        lvChooseDocument = (ListView) view.findViewById(R.id.lvChooseDocument);
        headerDrop = (RelativeLayout) view.findViewById(R.id.header_dropbox);

        if (Constants.isChat) {
            headerDrop.setVisibility(View.GONE);
        } else {
            headerDrop.setVisibility(View.VISIBLE);
        }

        data = new ArrayList<>();
        adapter = new FileDocumentAdapter(data);
        lvChooseDocument.setAdapter(adapter);

        lvChooseDocument.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FileItem item = data.get(position);
                Log.e("QueAnh ", "File dropbox clicked");
                switch (item.type) {
                    case MEDIA_FILE:
                    case DOCUMENT_FILE:
                        FileDocumentAdapter.ViewHolder viewHolder = (FileDocumentAdapter.ViewHolder) view.getTag();
                        if (item.isSelected) {
                            item.isSelected = false;
                            viewHolder.imgSelected.setImageResource(R.drawable.not_select_icon);
                        } else {
                            item.isSelected = true;
                            viewHolder.imgSelected.setImageResource(R.drawable.select_icon);

                            FragmentActivity activity = getActivity();
                            if (activity instanceof OnItemSharing) {
                                try {
                                    JSONObject data = new JSONObject();
                                    data.put("data", item.path);
                                    ((OnItemSharing) activity).onShare("dropbox", data);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        break;
                    case BACK_FOLDER:
                    case FOLDER:
                    case ROOT_FOLDER:
                        goToFolder(item);
                        break;
                }
            }
        });

        String key = getString(R.string.dropbox_key);
        String secret = getString(R.string.dropbox_secret);
        AppKeyPair appKeys = new AppKeyPair(key, secret);
        AndroidAuthSession session = new AndroidAuthSession(appKeys);
        String token = PreferenceUtils.getString(Constants.DROPBOX_TOKEN, getActivity(), null);
        Log.e("Token", token + "");
        if (token != null) {
            session.setOAuth2AccessToken(token);
            dropboxAPI = new DropboxAPI<>(session);

            FileItem item = new FileItem("/", null);
            goToFolder(item);
        } else {
            dropboxAPI = new DropboxAPI<>(session);
            isAuth = true;
            dropboxAPI.getSession().startOAuth2Authentication(getActivity());
        }

        return view;
    }

    private boolean isAuth = false;

    public void onResume() {
        super.onResume();

        if (dropboxAPI.getSession().authenticationSuccessful()) {
            isAuth = false;
            try {
                dropboxAPI.getSession().finishAuthentication();

                String accessToken = dropboxAPI.getSession().getOAuth2AccessToken();
                Log.e("Token", accessToken);
                PreferenceUtils.saveString(Constants.DROPBOX_TOKEN, getActivity(), accessToken);

                FileItem item = new FileItem("/", null);
                goToFolder(item);
            } catch (IllegalStateException e) {
                Log.i("DbAuthLog", "Error authenticating", e);
                handler.sendEmptyMessage(EMPTY);
            }
        } else if (isAuth) {
            isAuth = false;
            handler.sendEmptyMessage(EMPTY);
        }
    }

    private static DropboxAPI<AndroidAuthSession> dropboxAPI;

    public static String getLinkForShare(final String path) {
        DropboxAPI.DropboxLink link = null;
        try {
            link = dropboxAPI.share(path);
        } catch (DropboxException e) {
            e.printStackTrace();
        }

        return link.url;
    }

    private final String ROOT_PATH = "/";

    private static final int EMPTY = 0;
    private static final int RELOAD = 1;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EMPTY:
                    lnListItems.setVisibility(View.GONE);
                    lnEmpty.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    break;
                case RELOAD:
                    adapter.notifyDataSetChanged();
                    lvChooseDocument.smoothScrollToPosition(0);
                    lnListItems.setVisibility(View.VISIBLE);
                    lnEmpty.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    break;
            }
        }
    };

    public void goToFolder(final FileItem root) {

        Log.e("Goto", root.path);
        progressBar.setVisibility(View.VISIBLE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<FileItem> items = new ArrayList<>();
                try {
                    DropboxAPI.Entry metadata = dropboxAPI.metadata(root.path, 0, null, true, null);
                    List<DropboxAPI.Entry> entries = metadata.contents;

                    if (!ROOT_PATH.equals(root.path)) {
                        String path = metadata.parentPath();
                        String name = "Up to " + metadata.fileName();
                        FileItem fileItem = new FileItem(path, name, FileItem.TYPE.BACK_FOLDER);
                        items.add(fileItem);
                    }

                    for (DropboxAPI.Entry item : entries) {
                        String path = item.path;
                        String name = item.fileName();

                        FileItem fileItem = new FileItem(path, name);
                        if (ROOT_PATH.equals(root.path)) {
                            fileItem.type = item.isDir ? FileItem.TYPE.ROOT_FOLDER : FileItem.TYPE.DOCUMENT_FILE;
                        } else {
                            fileItem.type = item.isDir ? FileItem.TYPE.FOLDER : FileItem.TYPE.DOCUMENT_FILE;
                            boolean isMediaFile = item.mimeType == null ? false : item.mimeType.contains("");
                            if (isMediaFile) {

                            }
                        }

                        items.add(fileItem);
                    }

                    data.clear();
                    data.addAll(items);
                    Collections.sort(data);

                    handler.sendEmptyMessage(RELOAD);
                } catch (DropboxException e) {
                    e.printStackTrace();

                    if (e instanceof DropboxUnlinkedException) {
                        isAuth = true;
                        dropboxAPI.getSession().startOAuth2Authentication(getActivity());
                    } else {
                        handler.sendEmptyMessage(EMPTY);
                    }
                }
            }
        }).start();

    }

//    public void getLinkDropBox(final String path) {
//        Thread mThread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                final String url = getLinkForShare(path);
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Log.e("QueANh_link_drop_box : ", url);
//                        Constants.message = url;
//                        getActivity().onBackPressed();
//                    }
//                });
//            }
//        });
//
//        mThread.start();
//    }
}
