package com.giinger.fibu.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.giinger.fibu.R;
import com.giinger.fibu.activity.BaseFileScreen;
import com.giinger.fibu.adapter.FileDocumentAdapter;
import com.giinger.fibu.model.FileItem;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.OnItemSharing;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by geekup on 4/1/15.
 */
public class FilePhysicalFragment extends Fragment {

    public static final File ROOT = Environment.getExternalStorageDirectory().getParentFile().getParentFile();
    private static final int EMPTY = 0;
    private static final int RELOAD = 1;
    public static final int MAX_SELECTED_ITEMS = 5;
    private String rootPath;

    private FileDocumentAdapter adapter;

    private ListView lvChooseDocument;
    private List<FileItem> data;
    private RelativeLayout headerPhysical;
    private View lnListItems, lnEmpty;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.file_document_screen, container, false);

        lnListItems = view.findViewById(R.id.lnListItems);
        lnEmpty = view.findViewById(R.id.lnEmpty);
        lvChooseDocument = (ListView) view.findViewById(R.id.lvChooseDocument);
        headerPhysical = (RelativeLayout) view.findViewById(R.id.header_dropbox);

        if (Constants.isChat) {
            headerPhysical.setVisibility(View.GONE);
        } else {
            headerPhysical.setVisibility(View.VISIBLE);
        }

        data = new ArrayList<>();
        adapter = new FileDocumentAdapter(data);
        lvChooseDocument.setAdapter(adapter);

        lvChooseDocument.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FileItem item = data.get(position);
                switch (item.type) {
                    case MEDIA_FILE:
                    case DOCUMENT_FILE:
                        FragmentActivity activity = getActivity();
                        if (activity instanceof OnItemSharing) {
                            if (item.size > BaseFileScreen.MAX_SIZE) {
                                String message = getString(R.string.msg_file_selected_too_big, "");
                                Context context = getActivity();
                                new AlertDialog.Builder(context).setMessage(message).setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }).show();
                                return;
                            }

                            try {
                                JSONObject data = new JSONObject();
                                data.put("data", item.path);
                                ((OnItemSharing) activity).onShare("document", data);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    case BACK_FOLDER:
                    case FOLDER:
                    case ROOT_FOLDER:
                        goToFolder(item);
                        break;
                }
            }
        });

        rootPath = ROOT.getAbsolutePath();
        FileItem root = new FileItem(rootPath, null);
        goToFolder(root);

        return view;
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EMPTY:
                    lnListItems.setVisibility(View.GONE);
                    lnEmpty.setVisibility(View.VISIBLE);
                    break;
                case RELOAD:
                    adapter.notifyDataSetChanged();
                    lvChooseDocument.smoothScrollToPosition(0);
                    lnListItems.setVisibility(View.VISIBLE);
                    lnEmpty.setVisibility(View.GONE);
                    break;
            }
        }
    };

    public void goToFolder(final FileItem root) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<FileItem> items = new ArrayList<>();
                File file = new File(root.path);

                if (!rootPath.equals(root.path)) {
                    String path = file.getParentFile().getAbsolutePath();
                    String name = "Up to " + file.getName();
                    FileItem fileItem = new FileItem(path, name, FileItem.TYPE.BACK_FOLDER);
                    items.add(fileItem);
                }

                File[] files = file.listFiles();
                if (files != null) {
                    for (File item : files) {
                        String path = item.getAbsolutePath();
                        String name = item.getName();

                        FileItem fileItem = new FileItem(path, name);
                        boolean isDir = item.isDirectory();
                        if (rootPath.equals(root.path)) {
                            fileItem.type = isDir ? FileItem.TYPE.ROOT_FOLDER : FileItem.TYPE.DOCUMENT_FILE;
                        } else {
                            fileItem.type = isDir ? FileItem.TYPE.FOLDER : FileItem.TYPE.DOCUMENT_FILE;
                        }

                        items.add(fileItem);
                    }
                }

                data.clear();
                data.addAll(items);
                Collections.sort(data);

                handler.sendEmptyMessage(RELOAD);
            }
        }).start();

    }
}
