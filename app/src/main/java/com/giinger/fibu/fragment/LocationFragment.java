package com.giinger.fibu.fragment;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.giinger.fibu.R;
import com.giinger.fibu.activity.SearchScreen;
import com.giinger.fibu.element.CustomTextView;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.OnItemSharing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LocationFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, View.OnClickListener {


    private TextView txtLocation;
    private Location mLocation;
    private GoogleApiClient googleApiClient;

    private RelativeLayout locationOne;
    private RelativeLayout locationTwo;

    private CustomTextView tvPlaceOne;
    private CustomTextView tvPlaceTwo;

    private ImageView imgSearch;
    //    private CustomTextView edtSearch;
    private RelativeLayout layoutActionSearch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.location_tab, container, false);

        txtLocation = (TextView) view.findViewById(R.id.tv_location);
        tvPlaceOne = (CustomTextView) view.findViewById(R.id.tv_loc_1);
        tvPlaceTwo = (CustomTextView) view.findViewById(R.id.tv_loc_2);

        locationOne = (RelativeLayout) view.findViewById(R.id.location_one);
        locationTwo = (RelativeLayout) view.findViewById(R.id.location_two);

        layoutActionSearch = (RelativeLayout) view.findViewById(R.id.action_search_screen);
        layoutActionSearch.setOnClickListener(this);

//        imgSearch = (ImageView) view.findViewById(R.id.img_search);
//        edtSearch = (CustomEditText) view.findViewById(R.id.edt_search);
//        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                Log.e("QueAnh_send_emty", actionId + "");
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    if (!TextUtils.isEmpty(edtSearch.getText().toString())) {
//                        hideKeyBoard();
//                        searchPlace(edtSearch.getText().toString());
//                        edtSearch.setText("");
//                    }
//
//                    return true;
//                }
//
//                return false;
//            }
//        });

//        imgSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (TextUtils.isEmpty(edtSearch.getText().toString())) {
//                    Toast.makeText(getActivity(), "Please enter name-place to search bar!", Toast.LENGTH_LONG).show();
//                } else {
//                    hideKeyBoard();
//                    searchPlace(edtSearch.getText().toString());
//                }
//                getActivity().startActivity(new Intent(getActivity(), SearchScreen.class));
//            }
//        });

        SupportMapFragment fragment = new SupportMapFragment();
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map, fragment).commit();

        fragment.getMapAsync(this);

//      getCurrentLocation();

        Context context = getActivity();
        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
        return view;
    }

    private void hideKeyBoard() {
        FragmentActivity activity = getActivity();
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    private void getCurrentLocation() {
        Context context = getActivity();

        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    try {
                        JSONArray results = object.getJSONArray("results");
                        JSONObject result = results.getJSONObject(0);
                        String address = result.getString("formatted_address");
                        txtLocation.setText(address);
                        txtLocation.setOnClickListener(LocationFragment.this);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        String url = AppUtils.getGeocoderUrl(mLocation);
        new AQuery(context).ajax(url, JSONObject.class, callback);
    }

    private GoogleMap mMap;

    @Override
    public void onMapReady(GoogleMap map) {
        map.setMyLocationEnabled(false);
        mMap = map;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.e("Google Api", "Connected");
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (lastLocation != null) {
            mLocation = lastLocation;
            getCurrentLocation();

            double lat = mLocation.getLatitude();
            double lng = mLocation.getLongitude();
            LatLng here = new LatLng(lat, lng);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(here, 18));

            MarkerOptions marker = new MarkerOptions();
            marker.position(here);
            mMap.addMarker(marker);

            getNearbyLocation();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public void getNearbyLocation() {

        Context context = getActivity();
        String url = AppUtils.getNearbyUrl(mLocation, getActivity());

        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    try {
                        Log.e("QueAnh_url_foursquare : ", url);
                        Log.e("QueANh list_place : ", object.toString());
                        JSONArray venues = object.getJSONObject("response").getJSONArray("venues");

                        for (int i = 0; i < venues.length(); i++) {
                            JSONObject location = venues.getJSONObject(i).getJSONObject("location");
                            if (i == 0) {
                                tvPlaceOne.setText(venues.getJSONObject(i).getString("name"));
                                tvPlaceOne.setTag(location);
                                locationOne.setOnClickListener(LocationFragment.this);
                            } else {
                                tvPlaceTwo.setText(venues.getJSONObject(i).getString("name"));
                                tvPlaceTwo.setTag(location);
                                locationTwo.setOnClickListener(LocationFragment.this);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        new AQuery(context).ajax(url, JSONObject.class, callback);
    }

//    public void searchPlace(String name) {
//
//        Context context = getActivity();
//        String url = AppUtils.getGeocoderNameUrl(name);
//
//        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
//            @Override
//            public void callback(String url, JSONObject object, AjaxStatus status) {
//                if (object != null) {
//                    try {
//                        Log.e("QueAnh_url_get_geo_ : ", url);
//
//                        JSONObject location = object.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location");
//                        String loc = "lat : " + location.getString("lat") + "," + "lng : " + location.getString("lng");
//                        Log.e("QueANh_location_search_place : ", loc);
//                        FragmentActivity activity = getActivity();
//
//                        JSONObject lct = new JSONObject();
//                        lct.put("longitude", location.getString("lng"));
//                        lct.put("latitude", location.getString("lat"));
//                        lct.put("name",object.getJSONArray("results").getJSONObject(0).getString("formatted_address"));
//
//                        ((OnItemSharing) activity).onShare("location", lct);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        Toast.makeText(getActivity(), "Place not found!", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//        };
//
//        new AQuery(context).ajax(url, JSONObject.class, callback);
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_location:
                try {
                    JSONObject lct = new JSONObject();
                    lct.put("longitude", mLocation.getLongitude());
                    lct.put("latitude", mLocation.getLatitude());
                    lct.put("name", txtLocation.getText());

                    ((OnItemSharing) getActivity()).onShare("location", lct);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.location_one:
                try {
                    JSONObject location = (JSONObject) tvPlaceOne.getTag();
                    JSONObject lct = new JSONObject();
                    lct.put("longitude", location.getString("lng"));
                    lct.put("latitude", location.getString("lat"));
                    lct.put("name", tvPlaceOne.getText());

                    ((OnItemSharing) getActivity()).onShare("location", lct);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            case R.id.location_two:
                try {
                    JSONObject location = (JSONObject) tvPlaceTwo.getTag();
                    JSONObject lct = new JSONObject();
                    lct.put("longitude", location.getString("lng"));
                    lct.put("latitude", location.getString("lat"));
                    lct.put("name", tvPlaceTwo.getText());

                    ((OnItemSharing) getActivity()).onShare("location", lct);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;

            case R.id.action_search_screen:
                Intent searchItent = new Intent(getActivity(), SearchScreen.class);
                searchItent.putExtra("lat_lng", mLocation.getLatitude() + "," + mLocation.getLongitude());
                getActivity().startActivity(searchItent);
                break;
        }
    }
}
