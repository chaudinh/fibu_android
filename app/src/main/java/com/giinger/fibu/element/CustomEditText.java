package com.giinger.fibu.element;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

import com.giinger.fibu.R;

import github.ankushsachdeva.emojicon.EmojiconEditText;

public class CustomEditText extends EmojiconEditText {

    public static final int LIGHT = 0;
    public static final int REGULAR = 1;

    public CustomEditText(Context context) {
        super(context);

        setFont();
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode()) {
            return;
        }

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);

        int customType = -1;
        try {
            customType = a.getInteger(R.styleable.CustomTextView_custom_style, -1);
        } finally {
            a.recycle();
        }

        setFont(customType);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);

        int customType = -1;
        try {
            customType = a.getInteger(R.styleable.CustomTextView_custom_style, -1);
        } finally {
            a.recycle();
        }

        setFont(customType);
    }

    private void setFont(int type) {
        if (isInEditMode()) {
            return;
        }

        String fontLocation;
        switch (type) {
            case LIGHT:
                fontLocation = "fonts/Lato-Light.ttf";
                break;

            case REGULAR:
                fontLocation = "fonts/Lato-Regular.ttf";
                break;

            default:
                fontLocation = "fonts/Lato-Light.ttf";
                break;
        }

        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                fontLocation);

        setTypeface(font);
    }

    private void setFont() {
        if (isInEditMode()) {
            return;
        }

        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Lato-Light.ttf");
        setTypeface(font);
    }

    private BackPressedListener mOnImeBack;

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            if (mOnImeBack != null) mOnImeBack.onImeBack(this);
        }
        return super.dispatchKeyEvent(event);
    }

    public void setBackPressedListener(BackPressedListener listener) {
        mOnImeBack = listener;
    }

    public interface BackPressedListener {
        void onImeBack(CustomEditText editText);
    }

}
