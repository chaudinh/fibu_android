package com.giinger.fibu.element;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.giinger.fibu.R;

public class CustomButton extends Button {

    public static final int LIGHT = 0;
    public static final int REGULAR = 1;

    public CustomButton(Context context) {
        super(context);

        if (isInEditMode()) {
            return;
        }

        setFont();
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode()) {
            return;
        }

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);

        int customType = -1;
        try {
            customType = a.getInteger(R.styleable.CustomTextView_custom_style, -1);
        } finally {
            a.recycle();
        }

        setFont(customType);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (isInEditMode()) {
            return;
        }

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);

        int customType = -1;
        try {
            customType = a.getInteger(R.styleable.CustomTextView_custom_style, -1);
        } finally {
            a.recycle();
        }
        setFont(customType);
    }

    private void setFont(int type) {
        String fontLocation;
        switch (type) {
            case LIGHT:
                fontLocation = "fonts/Lato-Light.ttf";
                break;

            case REGULAR:
                fontLocation = "fonts/Lato-Regular.ttf";
                break;

            default:
                fontLocation = "fonts/Lato-Light.ttf";
                break;
        }

        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                fontLocation);

        setTypeface(font);
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Lato-Light.ttf");
        setTypeface(font);
    }

}
