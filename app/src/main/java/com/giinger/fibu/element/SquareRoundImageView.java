package com.giinger.fibu.element;

import android.content.Context;
import android.util.AttributeSet;

import com.joooonho.SelectableRoundedImageView;

public class SquareRoundImageView extends SelectableRoundedImageView {

    public SquareRoundImageView(Context context) {
        super(context);
    }

    public SquareRoundImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareRoundImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }
}
