package com.giinger.fibu.element;

import android.view.View;

import com.giinger.fibu.R;

/**
 * Created by H.Anh on 11/05/2015.
 */
public class PopupMultipleConnectedHolder {

    public View txtPopupBumpAgain, txtPopupCancelBumpAgain, layoutPopupAction;

    public PopupMultipleConnectedHolder(View view) {
        txtPopupBumpAgain = view.findViewById(R.id.txtPopupBumpAgain);
        txtPopupCancelBumpAgain = view.findViewById(R.id.txtPopupCancelBumpAgain);
        layoutPopupAction = view.findViewById(R.id.layoutPopupAction);
    }

}
