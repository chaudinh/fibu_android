package com.giinger.fibu.element;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.giinger.fibu.R;

import github.ankushsachdeva.emojicon.EmojiconTextView;

public class CustomTextView extends EmojiconTextView {

    public static final int LIGHT = 0;
    public static final int REGULAR = 1;

    public CustomTextView(Context context) {
        super(context);
        setFont();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);

        int customType = -1;
        try {
            customType = a.getInteger(R.styleable.CustomTextView_custom_style, -1);
        } finally {
            a.recycle();
        }

        setFont(customType);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);

        int customType = -1;
        try {
            customType = a.getInteger(R.styleable.CustomTextView_custom_style, -1);
        } finally {
            a.recycle();
        }


        setFont(customType);
    }

    private void setFont(int type) {
        if (isInEditMode()) {
            return;
        }

        String fontLocation;
        switch (type) {
            case LIGHT:
                fontLocation = "fonts/Lato-Light.ttf";
                break;

            case REGULAR:
                fontLocation = "fonts/Lato-Regular.ttf";
                break;

            default:
                fontLocation = "fonts/Lato-Light.ttf";
                break;
        }

        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                fontLocation);

        setTypeface(font);
    }

    private void setFont() {
        if (isInEditMode()) {
            return;
        }

        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Lato-Light.ttf");
        setTypeface(font);
    }

}
