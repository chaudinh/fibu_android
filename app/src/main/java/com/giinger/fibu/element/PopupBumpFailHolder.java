package com.giinger.fibu.element;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.giinger.fibu.R;

public class PopupBumpFailHolder {

    public LinearLayout layoutActionHide;
    public RelativeLayout layoutAnimation;

    public PopupBumpFailHolder(View view) {
        layoutActionHide = (LinearLayout) view.findViewById(R.id.view_bump_fail);
        layoutAnimation = (RelativeLayout) view.findViewById(R.id.layout_animation);
    }

}
