package com.giinger.fibu.element;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.giinger.fibu.R;

public class PopupConnectingHolder {

    public ImageView imgAvatar, imgStatus;
    public TextView txtFirstName, txtLastName, txtJobTitle;
    public TextView txtPopupCancel, txtPopupAction;
    public View layoutPopupAction;

    public PopupConnectingHolder(View view) {
        txtFirstName = (TextView) view.findViewById(R.id.txtFirstName);
        txtLastName = (TextView) view.findViewById(R.id.txtLastName);
        txtJobTitle = (TextView) view.findViewById(R.id.txtJobTitle);

        imgAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
        imgStatus = (ImageView) view.findViewById(R.id.imgStatus);

        txtPopupAction = (TextView) view.findViewById(R.id.txtPopupAction);
        layoutPopupAction = view.findViewById(R.id.layoutPopupAction);
        txtPopupCancel = (TextView) view.findViewById(R.id.txtPopupCancel);
    }

}
