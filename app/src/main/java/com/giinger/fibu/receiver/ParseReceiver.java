package com.giinger.fibu.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.giinger.fibu.R;
import com.giinger.fibu.activity.BasePreviewScreen;
import com.giinger.fibu.activity.ChatBoxScreen;
import com.giinger.fibu.activity.HomeScreen;
import com.giinger.fibu.activity.SplashScreen;
import com.giinger.fibu.fragment.HomeFragment;
import com.giinger.fibu.model.ChatBoxItem;
import com.giinger.fibu.service.UpDownLoadService;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.EmojiMapUtil;
import com.giinger.fibu.util.Foreground;
import com.giinger.fibu.util.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.leolin.shortcutbadger.ShortcutBadger;

public class ParseReceiver extends BroadcastReceiver {

    public static final String NOTIFY_ACTION = "fibu.action.notification";
    public static final String KEY_PUSH_DATA = "com.parse.Data";
    public static final String KEY_PUSH_MESSAGE = "com.fibu.message";

    @Override
    public void onReceive(final Context context, Intent intent) {
        String data = intent.getStringExtra(KEY_PUSH_DATA);
        Log.e("Notification", "Receive data" + data);

        boolean isBackground = Foreground.get().isBackground();

        Log.e("State", "isBackground " + isBackground);
        try {
            JSONObject json = new JSONObject(data);
            if (!json.has("additionalData")) {
                return;
            }

            JSONObject info = json.getJSONObject("additionalData");

            String chatType = info.getString("chatType");
            if ("3".equals(chatType)) {
                String msg = info.getString("message");
                JSONObject object = new JSONObject(msg);
                String type = object.getString("type");
                if ("contact".equals(type)) {
                    JSONArray items = object.getJSONArray("items");
                    if (items.length() == 0) {
                        return;
                    }

                    final JSONObject item = items.getJSONObject(0);
                    String avatar = item.has("avatar") ? item.getString("avatar") : null;

                    JSONObject dt = item;
                    if (item.has("name")) {
                        dt = new JSONObject();
                        JSONArray phones = item.getJSONArray("phones");
                        JSONArray emails = item.getJSONArray("emails");
                        String name = item.getString("name");
                        dt.put("first_name", name);

                        if (phones.length() > 0) {
                            String phone = phones.getString(0);
                            dt.put("mobile", phone);
                        }

                        if (emails.length() > 0) {
                            String email = emails.getString(0);
                            dt.put("email", email);
                        }
                    }

                    String avatarUrl = AppUtils.getAvatarByName(context, avatar);

                    if (avatarUrl == null) {
                        ChatBoxScreen.saveContact(context, dt, null);
                    } else {
                        final JSONObject finalDt = dt;
                        BitmapAjaxCallback callback = new BitmapAjaxCallback() {
                            @Override
                            protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                                ChatBoxScreen.saveContact(context, finalDt, bm);
                            }
                        };

                        ImageView img = new ImageView(context);
                        new AQuery(context).id(img).image(avatarUrl, true, false, 0, 0, callback);
                    }
                }
            }

            ChatBoxItem chatBoxItem = parse(info);
            if (info.has("uploadedFile")) {
                String uploadedFile = info.getString("uploadedFile");
                Log.e("Uploaded File", uploadedFile);
                Intent download = new Intent(context, UpDownLoadService.class);
                download.putExtra(UpDownLoadService.TYPE, UpDownLoadService.TYPE_DOWNLOAD);
                download.putExtra(UpDownLoadService.EXTRA_DOWNLOAD_FILE, uploadedFile);
                download.putExtra(ChatBoxScreen.EXTRA_LOADING_ID, chatBoxItem.id);
                context.startService(download);
            }

            String notify = json.getString("alert");
            notify = EmojiMapUtil.replaceCheatSheetEmojis(notify);
            if (isBackground) {
                int badgeCount = PreferenceUtils.getInt(Constants.COUNT_BADGE, context, 0) + 1;
                PreferenceUtils.saveInt(Constants.COUNT_BADGE, context, badgeCount);
                ShortcutBadger.with(context).count(badgeCount);
                createNotification(context, json, notify);
            } else {
                if (ChatBoxScreen.instance != null) {
                    String friendId = ChatBoxScreen.instance.getFriendId();
                    if (chatBoxItem.friendId.equals(friendId) && BasePreviewScreen.instance == null) {
                        ChatBoxScreen.instance.addToListChat(friendId, info);
                        return;
                    }
                }

                int totalBadge = PreferenceUtils.getInt(Constants.COUNT_BADGE, context, 0) + 1;
                PreferenceUtils.saveInt(Constants.COUNT_BADGE, context, totalBadge);
                Intent i = new Intent(HomeFragment.ACTION);
                i.putExtra(HomeFragment.EXTRA_COUNT_BADGE, totalBadge);
                LocalBroadcastManager.getInstance(context).sendBroadcast(i);

                Log.e("State", "On Top");

                Intent fibu = new Intent(NOTIFY_ACTION);
                fibu.putExtra(KEY_PUSH_DATA, data);
                fibu.putExtra(KEY_PUSH_MESSAGE, notify);
                LocalBroadcastManager.getInstance(context).sendBroadcast(fibu);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private ChatBoxItem parse(JSONObject info) {
        try {
            String friendId = info.getString("senderId");
            String chatType = info.getString("chatType");
            String messageId = info.getString("messageId");

            info.put("id", messageId);
            info.put("side", "friend");
            info.put("seen", 0);
            info.put("liked", 0);

            switch (chatType) {
                case "2":
                    chatType = "message";
                    break;
                case "3":
                    chatType = "share";
                    String msg = info.getString("message");
                    JSONObject object = new JSONObject(msg);
                    String type = object.getString("type");
                    if (!"photo".equals(type) && !"location".equals(type) && !"dropbox".equals(type)) {
                        JSONArray items = object.getJSONArray("items");
                        if (items.length() == 0) {
                            return null;
                        }

                        JSONObject item = items.getJSONObject(0);
                        object.put("items", null);
                        object.put("message", item);
                        info.put("message", object.toString());
                    }
                    break;
            }

            info.put("type", chatType);

            String chatTime = info.getString("chatTime");
            info.put("time", chatTime);

            ChatBoxItem item = new ChatBoxItem(friendId, info);
            item.isLoading = item.isNeedDownload();
            item.save();

            return item;
        } catch (JSONException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    private void createNotification(Context context, JSONObject data, String message) {
        try {
            NotificationManager mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);

            String title = context.getString(R.string.app_name);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                    context).setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setVibrate(new long[]{1000, 1000}).setAutoCancel(true)
                    .setContentText(message)
                    .setTicker(message);

            Intent resultIntent = new Intent(context, SplashScreen.class);
            resultIntent.putExtra(SplashScreen.EXTRA_TYPE, SplashScreen.TYPE_CHAT);

            JSONObject info = data.getJSONObject("additionalData");
            String id = info.getString("senderId");
            String name = info.getString("senderName");
            String avatar = info.getString("senderAvatar");
            resultIntent.putExtra(ChatBoxScreen.EXTRA_ID, id);
            resultIntent.putExtra(ChatBoxScreen.EXTRA_NAME, name);
            resultIntent.putExtra(ChatBoxScreen.EXTRA_AVATAR, avatar);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(HomeScreen.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);

            Notification notification = mBuilder.build();
            mNotificationManager.notify(0, notification);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
