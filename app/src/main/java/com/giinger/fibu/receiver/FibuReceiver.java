package com.giinger.fibu.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.giinger.fibu.activity.BasePreviewScreen;
import com.giinger.fibu.activity.ChatBoxScreen;
import com.giinger.fibu.activity.HomeScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FibuReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("Receiver", "OK");

        try {
            String data = intent.getStringExtra(ParseReceiver.KEY_PUSH_DATA);
            JSONObject json = new JSONObject(data);

            Intent i = new Intent(context, ChatBoxScreen.class);
            JSONObject info = json.getJSONObject("additionalData");
            String id = info.getString("senderId");
            String name = info.getString("senderName");
            String avatar = info.getString("senderAvatar");
            i.putExtra(ChatBoxScreen.EXTRA_ID, id);
            i.putExtra(ChatBoxScreen.EXTRA_NAME, name);
            i.putExtra(ChatBoxScreen.EXTRA_AVATAR, avatar);

            if (ChatBoxScreen.instance != null) {
                String friendId = ChatBoxScreen.instance.getFriendId();
                if (id.equals(friendId)) {
                    if (BasePreviewScreen.instance != null) {
                        BasePreviewScreen.instance.finish();
                    }

                    String messageId = info.getString("messageId");
                    info.put("id", messageId);

                    info.put("side", "friend");
                    info.put("seen", 0);
                    info.put("liked", 0);

                    String chatType = info.getString("chatType");
                    switch (chatType) {
                        case "2":
                            chatType = "message";
                            break;
                        case "3":
                            chatType = "share";
                            String message = info.getString("message");
                            JSONObject object = new JSONObject(message);
                            String type = object.getString("type");
                            if (!"photo".equals(type) && !"location".equals(type) && !"dropbox".equals(type)) {
                                JSONArray items = object.getJSONArray("items");
                                if (items.length() == 0) {
                                    return;
                                }

                                JSONObject item = items.getJSONObject(0);
                                object.put("items", null);
                                object.put("message", item);
                                info.put("message", object.toString());
                            }
                            break;
                    }

                    info.put("type", chatType);

                    String chatTime = info.getString("chatTime");
                    info.put("time", chatTime);

                    ChatBoxScreen.instance.addToListChat(friendId, info);
                } else {
                    ChatBoxScreen.instance.loadFriend(i);
//                    ChatBoxScreen.instance.finish();
//                    HomeScreen.instance.startActivity(i);
                }
            } else if (HomeScreen.instance != null) {
                HomeScreen.instance.startActivity(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
