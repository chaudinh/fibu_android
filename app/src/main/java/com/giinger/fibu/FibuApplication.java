package com.giinger.fibu;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.giinger.fibu.util.Foreground;
import com.giinger.fibu.util.RushOrmManager;
import com.parse.Parse;

public class FibuApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        Foreground.init(this);

        String parseId = getString(R.string.parse_app_id);
        String clientKey = getString(R.string.parse_client_key);
        Parse.initialize(this, parseId, clientKey);
        RushOrmManager.getInstance().setUpRushOrm(getApplicationContext());
    }

}
