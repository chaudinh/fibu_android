package com.giinger.fibu.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.giinger.fibu.activity.ChatBoxScreen;
import com.giinger.fibu.activity.FileDocumentScreen;
import com.giinger.fibu.fragment.FileDropBoxFragment;
import com.giinger.fibu.model.ChatBoxItem;
import com.giinger.fibu.receiver.ParseReceiver;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.RushOrmManager;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UpDownLoadService extends Service {

    public static final int TIME_OUT = 5 * 60 * 1000;

    public static final String TYPE = "extra_type";
    public static final int TYPE_UPLOAD = 1001;
    public static final int TYPE_DOWNLOAD = 1002;
    public static final int TYPE_BUMP_RECEIVE = 1003;
    public static final int TYPE_BUMP_SEND = 1004;
    public static final int TYPE_RESEND = 1005;
    public static final int TYPE_DOWNLOAD_VIDEO = 1006;

    public static final String EXTRA_DOWNLOAD_FILE = "extra_download_file";
    public static final String EXTRA_SIDE_IS_ME = "extra_side";

    public static final String EXTRA_UPLOAD_USER_ID = "extra_upload_user_id";
    public static final String EXTRA_FRIEND_ID = "extra_upload_friend_id";
    public static final String EXTRA_UPLOAD_TYPE = "extra_upload_type";
    public static final String EXTRA_UPLOAD_INFO = "extra_upload_info";

    public static final String EXTRA_RESEND_FILE = "extra_resend_file";
    public static final String EXTRA_RESEND_MESSAGE = "extra_resend_message";

    public static final String EXTRA_BUMP_RECEIVE_TOKEN = "extra_bump_receive_token";
    public static final String EXTRA_BUMP_SEND_TOKEN = "extra_bump_send_token";
    public static final String EXTRA_BUMP_SEND_SELECTED = "extra_bump_send_selected";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private static Map<String, Integer> inProgress = new HashMap<>();

    public static boolean isInProgressing(String id) {
        String loadingId = newId2LoadingId.get(id);
        if (loadingId == null) {
            loadingId = id;
        }

        Log.e("Check in progress", id + " " + loadingId);

        return inProgress.get(loadingId) != null;
    }

    public static int getProgress(String id) {
        String loadingId = newId2LoadingId.get(id);

        if (loadingId == null) {
            loadingId = id;
        }

        return inProgress.get(loadingId);
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        if (intent != null) {
            final int type = intent.getIntExtra(TYPE, -1);
            final String loadingId = intent.getStringExtra(ChatBoxScreen.EXTRA_LOADING_ID);
            final String friendId = intent.getStringExtra(EXTRA_FRIEND_ID);
            String receiveToken = intent.getStringExtra(EXTRA_BUMP_RECEIVE_TOKEN);

            switch (type) {
                case TYPE_DOWNLOAD:
                    boolean isMe = intent.getBooleanExtra(EXTRA_SIDE_IS_ME, false);
                    String uploadedFile = intent.getStringExtra(EXTRA_DOWNLOAD_FILE);
                    downloadFile(uploadedFile, loadingId, isMe);
                    break;
                case TYPE_UPLOAD:
                case TYPE_RESEND:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String userId = intent.getStringExtra(EXTRA_UPLOAD_USER_ID);

                                if (type == TYPE_UPLOAD) {
                                    String info = intent.getStringExtra(EXTRA_UPLOAD_INFO);
                                    String tp = intent.getStringExtra(EXTRA_UPLOAD_TYPE);
                                    JSONObject json = new JSONObject(info);
                                    uploadFile(loadingId, userId, friendId, tp, json, type);
                                } else {
                                    String filePath = intent.getStringExtra(EXTRA_RESEND_FILE);
                                    String message = intent.getStringExtra(EXTRA_RESEND_MESSAGE);
                                    resendFile(loadingId, userId, friendId, message, filePath, type);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    break;
                case TYPE_BUMP_RECEIVE:
                    bumpReceive(loadingId, friendId, receiveToken);
                    break;
                case TYPE_BUMP_SEND:
                    final String sendToken = intent.getStringExtra(EXTRA_BUMP_SEND_TOKEN);
                    final String bumpType = intent.getStringExtra(EXTRA_UPLOAD_TYPE);
                    final List<String> selected = intent.getStringArrayListExtra(EXTRA_BUMP_SEND_SELECTED);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            bumpSend(loadingId, friendId, sendToken, bumpType, selected);
                        }
                    }).start();
                    break;
                case TYPE_DOWNLOAD_VIDEO:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            downloadVideo();
                        }
                    }).start();
                    break;
            }

        }

        return super.onStartCommand(intent, flags, startId);
    }

    public static boolean isDownloadingVideoTour = false;

    private void downloadVideo() {

        final String url = PreferenceUtils.getString(Constants.TOUR_VIDEO, this, null);
        final String saved = PreferenceUtils.getString(Constants.TOUR_VIDEO_SAVED, this, null);

        final Context context = getApplicationContext();

        File folder = BumpUtils.getCacheFolder();
        String fileName = "video-tour.mp4";
        final File output = new File(folder, fileName);
        if (url != null && !url.equals(saved) || !output.exists()) {
            isDownloadingVideoTour = true;

            Ion.with(context).load(url).progress(new ProgressCallback() {
                @Override
                public void onProgress(long downloaded, long total) {
                    int percent = (int) (downloaded * 100 / total);
                    updateVideoTourDownload(percent);
                    Log.e("Video tour", String.valueOf(percent));
                }
            }).write(output).setCallback(new FutureCallback<File>() {
                @Override
                public void onCompleted(Exception e, File result) {
                    Log.e("Video tour", "Done");
                    if (e != null || result == null) {
                        output.delete();
                    }

                    isDownloadingVideoTour = false;
                    PreferenceUtils.saveString(Constants.TOUR_VIDEO_SAVED, context, url);
                }
            });
        }
    }

    public static final String DOWNLOAD_VIDEO_TOUR_ACTION = "download_video_tour_action";
    public static final String DOWNLOAD_VIDEO_TOUR_PROGRESS = "download_video_tour_progress";

    private void updateVideoTourDownload(int percent) {
        Intent intent = new Intent(DOWNLOAD_VIDEO_TOUR_ACTION);
        intent.putExtra(DOWNLOAD_VIDEO_TOUR_PROGRESS, percent);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void bumpSend(final String loadingId, final String friendId, String sendToken, String bumpType, List<String> selected) {
        File zip = null;

        inProgress.put(loadingId, ChatBoxItem.PROGRESS_INIT);

        if ("contact".equals(bumpType)) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            final Context context = getApplicationContext();
            String url = AppUtils.getSendFileUrl(context);

            Builders.Any.B request = Ion.with(context).load(url).uploadProgress(new ProgressCallback() {
                @Override
                public void onProgress(long downloaded, long total) {
                    int progress = (int) (100 * downloaded / total);
                    updateProgress(loadingId, progress, TYPE_BUMP_SEND);
                }
            });

            String userId = PreferenceUtils.getString(Constants.USER_ID, context, "");
            request.setMultipartParameter("sender_id", userId);
            request.setMultipartParameter("receiver_id", friendId);
            request.setMultipartParameter("token", sendToken);

            JSONObject info = new JSONObject();

            info.put("type", bumpType);

            switch (bumpType) {
                case "profile":
                    // do nothing
                    break;
                case "photo":
                    List<String> filesPath = selected;

                    JSONObject file = BumpUtils.compressFiles(filesPath, userId);
                    zip = (File) file.get("zip");

                    JSONArray items = file.getJSONArray("items");
                    info.put("items", items);
                    request.setMultipartFile("file", zip);
                    break;
                case "contact":
                    JSONArray contacts = new JSONArray();


                    for (String item : selected) {
                        JSONObject json = new JSONObject(item);
                        contacts.put(json);
                    }
                    int size = selected.size();
                    String countParam = String.valueOf(size);
                    if (Constants.contactSize <= size) {
                        countParam = "all";
                    }

                    info.put("items", contacts);
                    info.put("count", countParam);
                    break;
                case "video":
                case "music":
                case "document":
                    JSONObject compressFiles = BumpUtils.compressFiles(selected, userId);
                    zip = (File) compressFiles.get("zip");

                    JSONArray files = compressFiles.getJSONArray("items");
                    info.put("items", files);

                    request.setMultipartFile("file", zip);
                    break;
                case "dropbox":
                    String path = selected.get(0);
                    String link = FileDocumentScreen.getLinkForShare(path);
                    info.put("type", "dropbox");
                    info.put("link", link);
                    break;
            }

            String message = info.toString();
            request.setMultipartParameter("file_transfer_info", message);
            ChatBoxScreen.updateSendItem(loadingId, zip, message);

            final File finalZip = zip;
            request.asJsonObject().setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject object) {
                    Log.e("Upload", object + " ");

                    if (e != null || object == null) {
                        updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, TYPE_BUMP_SEND);
                        return;
                    }

                    JsonObject response = AppUtils.getResponse(object);
                    if (response == null) {
                        updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, TYPE_BUMP_SEND);
                        return;
                    }

                    try {
                        updateProgress(loadingId, ChatBoxItem.PROGRESS_EXTRACTING, TYPE_BUMP_SEND);
                        BumpUtils.extractFiles(context, finalZip, true);

                        JsonObject chatItem = response.getAsJsonObject("chatItem");
                        String data = chatItem.toString();

                        JSONObject json = new JSONObject(data);
                        String newId = ChatBoxScreen.updateBumpItem(context, loadingId, friendId, json);
                        if (newId != null) {
                            loadingId2NewId.put(loadingId, newId);
                            newId2LoadingId.put(newId, loadingId);

                            updateProgress(loadingId, ChatBoxItem.PROGRESS_SUCCESS, TYPE_BUMP_SEND);
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                        updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, TYPE_BUMP_SEND);
                    }

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, TYPE_BUMP_SEND);
        }
    }

    private void bumpReceive(final String loadingId, final String friendId, String
            receiveToken) {

        inProgress.put(loadingId, ChatBoxItem.PROGRESS_INIT);

        final Context context = getApplicationContext();
        final String userId = PreferenceUtils.getString(Constants.USER_ID, context, "");
        String url = AppUtils.getReceiveFileUrl(context, userId, friendId, receiveToken);

        Log.e("Shake", "Download " + url);

        Ion.with(context).load(url).setTimeout(TIME_OUT).asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject object) {
                Log.e("Downloaded", object + "");

                if (e != null || object == null) {
                    updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, TYPE_BUMP_RECEIVE);
                    return;
                }

                JsonObject response = AppUtils.getResponse(object);
                if (response == null) {
                    updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, TYPE_BUMP_RECEIVE);
                    return;
                }

                JsonObject fileInfo = response.getAsJsonObject("chatItem");
                try {
                    String data = fileInfo.toString();
                    JSONObject json = new JSONObject(data);

                    String newId = ChatBoxScreen.updateBumpItem(context, loadingId, friendId, json);
                    if (newId != null) {
                        loadingId2NewId.put(loadingId, newId);
                        newId2LoadingId.put(newId, loadingId);

                        boolean hasFileServer = response.has("serverFile");
                        if (hasFileServer) {
                            String serverFile = response.getAsJsonPrimitive("serverFile").getAsString();
                            downloadFile(serverFile, loadingId, false);
                        } else {
//                        BumpUtils.updateBumpResult(context, friendId, userId);
                            updateProgress(loadingId, ChatBoxItem.PROGRESS_SUCCESS, TYPE_BUMP_RECEIVE);
                        }
                    }
                } catch (JSONException exception) {
                    exception.printStackTrace();
                    updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, TYPE_BUMP_RECEIVE);
                }
            }
        });
    }

    private void resendFile(String loadingId, String userId, String friendId, String info, String filePath, int type) {
        File zip = filePath == null ? null : new File(filePath);
        onSharing(loadingId, userId, friendId, info, zip, type);
    }

    private void onSharing(final String loadingId, String userId, final String friendId, String message, File zip, final int type) {
        inProgress.put(loadingId, ChatBoxItem.PROGRESS_INIT);

        final Context context = getApplicationContext();
        String url = AppUtils.getChatSendUrl(context);
        Builders.Any.B request = Ion.with(context).load(url);

        request.setMultipartParameter("user_id", userId);
        request.setMultipartParameter("friend_id", friendId);
        request.setMultipartParameter("chat_type", "3");
        request.setMultipartParameter("message", message);

        if (zip != null) {
            request.setMultipartFile("file", zip);
        }

        Log.e("QueANh_json_share : ", message);

        final File finalZip1 = zip;
        request.uploadProgress(new ProgressCallback() {
            @Override
            public void onProgress(long downloaded, long total) {
                int progress = (int) (downloaded * 100 / total);
                updateProgress(loadingId, progress, type);
            }
        }).asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e != null || result == null) {
                    updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, type);
                    return;
                }

                try {
                    BumpUtils.extractFiles(context, finalZip1, true);

                    JsonObject response = AppUtils.getResponse(result);
                    if (response == null) {
                        updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, type);
                        return;
                    }

                    JsonObject item = response.getAsJsonObject("item");

                    boolean seen = item.getAsJsonPrimitive("seen").getAsBoolean();
                    item.remove("seen");

                    boolean liked = item.getAsJsonPrimitive("liked").getAsBoolean();
                    item.remove("liked");

                    long gmtTimestamp = item.getAsJsonPrimitive("gmtTimestamp").getAsLong();
                    item.remove("gmtTimestamp");

                    String data = item.toString();

                    JSONObject json = new JSONObject(data);
                    json.put("seen", seen ? 1 : 0);
                    json.put("liked", liked ? 1 : 0);
                    json.put("chat_time", gmtTimestamp);

                    String newId = ChatBoxScreen.updateBumpItem(context, loadingId, friendId, json);
                    if (newId != null) {
                        loadingId2NewId.put(loadingId, newId);
                        newId2LoadingId.put(newId, loadingId);

                        updateProgress(loadingId, ChatBoxItem.PROGRESS_SUCCESS, type);
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                    updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, type);
                }
            }
        });
    }

    private void uploadFile(String loadingId, String userId, String friendId, String type, JSONObject json, int progressType) {
        File zip = null;

        inProgress.put(loadingId, ChatBoxItem.PROGRESS_INIT);

        try {
            JSONObject info = new JSONObject();
            info.put("type", type);

            switch (type) {
                case "photo":
                case "video":
                case "music":
                case "document":
                    List<String> filesPath = new ArrayList<>();
                    String path = json.getString("data");
                    filesPath.add(path);

                    JSONObject file = BumpUtils.compressFiles(filesPath, userId);
                    zip = (File) file.get("zip");

                    JSONArray items = file.getJSONArray("items");
                    info.put("items", items);
                    break;
                case "contact":
                    JSONArray contacts = new JSONArray();
                    contacts.put(json);
                    info.put("items", contacts);
                    break;
                case "dropbox":
                    String dropbox = json.getString("data");
                    String link = FileDropBoxFragment.getLinkForShare(dropbox);
                    info.put("link", link);
                    break;
                case "location":
                    info.put("item", json);
                    break;
            }

            String message = info.toString();
            ChatBoxScreen.updateSendItem(loadingId, zip, message);
//            updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, progressType);

            onSharing(loadingId, userId, friendId, message, zip, progressType);
        } catch (JSONException e) {
            e.printStackTrace();
            updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, progressType);
        }
    }

    private static Map<String, String> newId2LoadingId = new HashMap<>();
    private static Map<String, String> loadingId2NewId = new HashMap<>();

    private static List<String> done = new ArrayList<>();

    public static boolean isDone(String id) {
        String loadingId = newId2LoadingId.get(id);
        if (loadingId == null) {
            loadingId = id;
        }

        return done.contains(loadingId);
    }

    private void updateProgress(String loadingId, int progress, int type) {
        String newId = loadingId2NewId.get(loadingId);
        if (newId == null) {
            newId = loadingId;
        }

        RushOrmManager.getInstance().updateProgress(newId, progress);

        Log.e("Service update", loadingId + " " + progress);

        Intent broadcast = new Intent("fibu.action.loading");
        broadcast.putExtra(ChatBoxScreen.EXTRA_LOADING_ID, loadingId);
        broadcast.putExtra(TYPE, type);
        broadcast.putExtra(ChatBoxScreen.EXTRA_LOADING_PROGRESS, progress);

        LocalBroadcastManager instance = LocalBroadcastManager.getInstance(this);
        instance.sendBroadcast(broadcast);

        switch (progress) {
            case ChatBoxItem.PROGRESS_SUCCESS:
            case ChatBoxItem.PROGRESS_FAIL:
                inProgress.remove(loadingId);
                done.add(loadingId);
                break;
            default:
                inProgress.put(loadingId, progress);
                break;
        }
    }

    private void downloadFile(String uploadedFile, final String loadingId, final boolean isMe) {
        inProgress.put(loadingId, ChatBoxItem.PROGRESS_INIT);

        String url = AppUtils.getFileUrlByName(this, uploadedFile);

        Log.e("Download Service", url);

        File folder = BumpUtils.getAppFolder();
        String fileName = "bump-" + System.currentTimeMillis() + ".zip";
        File output = new File(folder, fileName);

        Context context = getApplicationContext();
        Ion.with(context).load(url).progress(new ProgressCallback() {
            @Override
            public void onProgress(long downloaded, long total) {
                int progress = (int) (downloaded * 100 / total);
                updateProgress(loadingId, progress, TYPE_DOWNLOAD);
            }
        }).setTimeout(TIME_OUT).write(output).setCallback(new FutureCallback<File>() {
            @Override
            public void onCompleted(Exception e, File file) {
                if (e != null || file == null) {
                    updateProgress(loadingId, ChatBoxItem.PROGRESS_FAIL, TYPE_DOWNLOAD);
                    return;
                }

                Log.e("Done", "" + file);
                updateProgress(loadingId, ChatBoxItem.PROGRESS_EXTRACTING, TYPE_DOWNLOAD);

                BumpUtils.extractFiles(UpDownLoadService.this, file, isMe);
                updateProgress(loadingId, ChatBoxItem.PROGRESS_SUCCESS, TYPE_DOWNLOAD);
            }
        });
    }

}
