package com.giinger.fibu.model;

public class FileItem implements Comparable<FileItem> {

    public int progress = -1;

    @Override
    public int compareTo(FileItem another) {

        boolean mIsBack = isBackFolder();
        if (mIsBack) {
            return -1;
        }

        boolean isBack = another.isBackFolder();
        if (isBack) {
            return 1;
        }

        boolean isFolder = another.isFolder();
        boolean mIsFolder = isFolder();

        if (isFolder) {
            if (mIsFolder) {
                return name.compareTo(another.name);
            }

            return 1;
        }

        if (mIsFolder) {
            return -1;
        }

        return name.compareTo(another.name);
    }

    public static enum TYPE {
        ROOT_FOLDER, BACK_FOLDER, FOLDER, MEDIA_FILE, DOCUMENT_FILE
    }

    public boolean isBackFolder() {
        return type == TYPE.BACK_FOLDER;
    }

    public boolean isFile() {
        return type == TYPE.DOCUMENT_FILE || type == TYPE.MEDIA_FILE;
    }

    public boolean isFolder() {
        return type == TYPE.FOLDER || type == TYPE.ROOT_FOLDER;
    }

    public String name, path;
    public String date, time;
    public boolean isSelected = false;
    public TYPE type = TYPE.ROOT_FOLDER;
    public long size = 0;
    private long mili = 0;

    public FileItem(String path, String name, long size) {
        this.path = path;
        this.name = name;
        this.size = size;
    }

    public FileItem(String path, String name) {
        this.path = path;
        this.name = name;
    }

    public FileItem(String path, String name, TYPE type) {
        this.path = path;
        this.name = name;
        this.type = type;
    }

    public FileItem(String path, String name, String date, String time, long size) {
        this.path = path;
        this.name = name;
        this.type = TYPE.ROOT_FOLDER;
        this.date = date;
        this.time = time;
        this.size = size;
    }

    public void setMili(long mili) {
        this.mili = mili;
    }

    public long getMili(){
        return mili;
    }

}
