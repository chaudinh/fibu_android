package com.giinger.fibu.model;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ContactItem implements Comparable<ContactItem> {

    @Override
    public int compareTo(ContactItem another) {
        if (name == null) {
            return -1;
        }

        if (another.name == null) {
            return 1;
        }

        return name.compareTo(another.name);
    }

    public String name, website, id, jobTitle, avatar;
    public List<String> emails, phones, address, social;
    public boolean isSelected = false;

    public ContactItem(String name, List<String> emails, List<String> phones, List<String> address, List<String> social, String website, String id, String jobTitle, String avatar) {
        this.name = name;
        this.website = website;
        this.emails = emails;
        this.address = address;
        this.phones = phones;
        this.social = social;
        this.id = id;
        this.jobTitle = jobTitle;
        this.avatar = avatar;
    }

    public ContactItem(JSONObject data) {
        try {
            this.phones = new ArrayList<>();
            this.emails = new ArrayList<>();

            if (data.has("name")) {
                this.name = data.getString("name");

                JSONArray phones = data.getJSONArray("phones");
                for (int i = 0; i < phones.length(); i++) {
                    String phone = phones.getString(i);
                    if (!TextUtils.isEmpty(phone)) {
                        this.phones.add(phone);
                    }
                }

                JSONArray emails = data.getJSONArray("emails");
                for (int i = 0; i < emails.length(); i++) {
                    String email = emails.getString(i);
                    if (!TextUtils.isEmpty(email)) {
                        this.emails.add(email);
                    }
                }
            } else {
                this.name = data.getString("first_name") + (data.has("last_name") ? " " + data.getString("last_name") : "");
                this.jobTitle = data.has("job_title") ? data.getString("job_title") : null;
                this.avatar = data.has("avatar") ? data.getString("avatar") : null;
                this.website = data.has("link") ? data.getString("link") : null;

                String[] phonesKey = {"mobile", "home_phone", "office_phone", "other_phone"};
                for (String key : phonesKey) {
                    if (data.has(key)) {
                        phones.add(data.getString(key));
                    }
                }

                String[] emailsKey = {"email", "other_email"};
                for (String key : emailsKey) {
                    if (data.has(key)) {
                        String email = data.getString(key);
                        emails.add(email);
                    }
                }

                this.address = new ArrayList<>();
                String[] addressesKey = {"home_address", "office_address", "other_address"};
                for (String key : addressesKey) {
                    if (data.has(key)) {
                        String address = data.getString(key);
                        this.address.add(address);
                    }
                }

                this.social = new ArrayList<>();
                String[] socialsKey = {"facebook", "twitter", "linkedin"};
                for (String key : socialsKey) {
                    if (data.has(key)) {
                        String account = data.getString(key);
                        social.add(account);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
