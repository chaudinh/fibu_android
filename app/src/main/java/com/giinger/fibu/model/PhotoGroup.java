package com.giinger.fibu.model;

import java.util.ArrayList;
import java.util.List;

public class PhotoGroup {
    public static enum TYPE {HEADER, ROW}

    public TYPE type;
    public List<PhotoItem> items;
    public String groupFolder;

    public PhotoGroup(TYPE type, String data, boolean isPhoto, String duration, long size, String groupFolder) {
        this.type = type;
        this.groupFolder = groupFolder;
        items = new ArrayList<>();
        items.add(new PhotoItem(data, isPhoto, duration, size));
    }

    public PhotoGroup(TYPE type, String data, boolean isPhoto, String duration, long size, boolean selected) {
        this.type = type;
        items = new ArrayList<>();
        items.add(new PhotoItem(data, isPhoto, duration, size, selected));
    }

    public PhotoItem addChild(String data, boolean isPhoto, String duration, long size) {
        PhotoItem photoItem = new PhotoItem(data, isPhoto, duration, size);
        items.add(photoItem);
        return photoItem;
    }

    public PhotoItem addChild(String data, boolean isPhoto, String duration, long size, boolean selected) {
        PhotoItem photoItem = new PhotoItem(data, isPhoto, duration, size, selected);
        items.add(photoItem);
        return photoItem;
    }

}
