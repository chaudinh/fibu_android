package com.giinger.fibu.model;

public class PhotoItem {

    public String data;
    public boolean isSelected;
    public boolean isPhoto;
    public String duration;
    public long size;
    public Integer direction = null;

    public PhotoItem(String data, boolean isPhoto, String duration, long size) {
        this.data = data;
        this.isPhoto = isPhoto;
        this.duration = duration;
        this.size = size;
    }

    public PhotoItem(String data, boolean isPhoto, String duration, long size, boolean isSelected) {
        this.data = data;
        this.isPhoto = isPhoto;
        this.duration = duration;
        this.size = size;
        this.isSelected = isSelected;
    }

}
