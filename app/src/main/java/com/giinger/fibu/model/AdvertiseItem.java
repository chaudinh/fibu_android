package com.giinger.fibu.model;

import org.json.JSONException;
import org.json.JSONObject;

public class AdvertiseItem {

    public String videoUrl, appUrl, title, description;

    public AdvertiseItem(String videoUrl, String appUrl, String title, String description) {
        this.videoUrl = videoUrl;
        this.appUrl = appUrl;
        this.title = title;
        this.description = description;
    }

    public AdvertiseItem(JSONObject item) {
        try {
            title = item.getString("app_name");
            description = item.getString("description");
            videoUrl = item.getString("youtube_link");
            appUrl = item.getString("gp_link");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
