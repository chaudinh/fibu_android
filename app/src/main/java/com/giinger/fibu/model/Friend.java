package com.giinger.fibu.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Friend {

    public String connectId;
    public String id;
    public String receiveType, sendType;

    public String lastName, firstName;
    public String avatar, jobTitle;
    public String mobile;
    public JSONObject friendProfile;

    public String sendToken, receiveToken;
//    public boolean isSender;

//    public JSONObject profile;

    public Friend(JSONObject response) {
        try {
            connectId = response.getString("connectId");
            sendToken = response.getString("sendToken");
            receiveToken = response.getString("receiveToken");

            receiveType = response.getString("receiveType");
            if (receiveType.equals("null")) {
                receiveType = "";
            }

            sendType = response.getString("sendType");
            if (sendType.equals("null")) {
                sendType = "";
            }

//            isSender = "receiver".equals(response.getString("yourRole"));

            JSONObject friend = response.getJSONObject("friend");
            id = friend.getString("id");
            firstName = friend.getString("first_name");
            lastName = friend.getString("last_name");
            jobTitle = friend.getString("job_title");
            avatar = friend.getString("avatar");
            mobile = friend.getString("mobile");
            friendProfile = friend;

//            if (response.has("profile")) {
//                profile = response.getJSONObject("profile");
//            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
