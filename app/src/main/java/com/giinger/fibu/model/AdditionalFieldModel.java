package com.giinger.fibu.model;

/**
 * Created by geekup on 3/19/15.
 */
public class AdditionalFieldModel {

    private int img;
    private String name;
    private String type;
    private boolean isSelected;
    private String icon;

    public AdditionalFieldModel(){
    }

    public void setImg(int img){
        this.img = img;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getImg(){
        return img;
    }

    public String getName(){
        return name;
    }

    public String getType() {
        return type;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public String getIcon() {
        return icon;
    }
}
