package com.giinger.fibu.model;

import com.giinger.fibu.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FriendItem {

    public String badge;
    public String name;
    public String message;
    public String time;
    public String avatar;
    public String id;

    public FriendItem(String name, String message, String time, String avatar, String badge) {
        this.name = name;
        this.message = message;
        this.time = time;
        this.avatar = avatar;
        this.badge = badge;
    }

    public FriendItem(JSONObject data) {
        try {
            JSONObject friend = data.getJSONObject("friend");
            id = friend.getString("id");
            name = friend.getString("first_name") + " " + friend.getString("last_name");
            avatar = friend.getString("avatar");

            if (data.has("gmtTimestamp")) {
                long timeStamp = data.getLong("gmtTimestamp") * 1000;
                Date date = new Date(timeStamp);
                String th = Utils.getThOfDate(date);
                SimpleDateFormat format = new SimpleDateFormat("MMM dd-");
                time = format.format(date).replace("-", th);
            } else {
                time = data.getString("time");
            }

            message = data.getString("message");
            badge = friend.getString("unread");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
