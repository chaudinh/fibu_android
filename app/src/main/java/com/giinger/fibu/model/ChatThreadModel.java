package com.giinger.fibu.model;

import co.uk.rushorm.core.RushObject;
import co.uk.rushorm.core.annotations.RushTableAnnotation;

/**
 * Created by luannguyen on 7/29/15.
 */

@RushTableAnnotation
public class ChatThreadModel extends RushObject {
    private String chatId;
    private String side;
    private String type;
    private String message;
    private String messageId;
    private String friendId;
    private String tokenBump;
    private long timeStamp;
    private boolean seen;
    private boolean liked;


    public ChatThreadModel(){

    }

    public void setId(String chatId){
        this.chatId = chatId;
    }

    public void setSide(String side){
        this.side = side;
    }

    public void setType(String type){
        this.type = type;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public void setTokenBump(String tokenBump) {
        this.tokenBump = tokenBump;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public String getChatId(){
        return chatId;
    }

    public String getMessage(){
        return message;
    }

}
