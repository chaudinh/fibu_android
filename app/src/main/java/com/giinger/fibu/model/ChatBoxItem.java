package com.giinger.fibu.model;

import android.util.Log;

import com.giinger.fibu.service.UpDownLoadService;
import com.giinger.fibu.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import co.uk.rushorm.core.RushObject;
import co.uk.rushorm.core.annotations.RushTableAnnotation;

@RushTableAnnotation
public class ChatBoxItem extends RushObject implements Comparable<ChatBoxItem> {

    public static final int PROGRESS_EXTRACTING = 101;
    public static final int PROGRESS_SUCCESS = 102;
    public static final int PROGRESS_FAIL = 103;

    public static final int PROGRESS_WAITING = -1;
    public static final int PROGRESS_COMPRESSING = -2;
    public static final int PROGRESS_GETTING_LINK = -3;
    public static final int PROGRESS_INIT = -4;

    public boolean _isLoading() {
        return isLoading && !UpDownLoadService.isDone(id);
    }

    public boolean isFailed() {
        boolean inProgressing = UpDownLoadService.isInProgressing(id);
        Log.e("Check failed", id + " " + progress + " " + inProgressing + " " + isLoading);
        return this.progress == PROGRESS_FAIL || _isLoading() && !inProgressing;
    }

    public boolean isLoading() {
        boolean inProgressing = UpDownLoadService.isInProgressing(id);
        Log.e("Check loading", id + " " + isLoading + " " + inProgressing + " " + hasServerFile());
        return _isLoading() && (inProgressing || hasServerFile());
    }

    public ChatBoxItem() {
    }

    public ChatBoxItem(String friendId, boolean isSender) {
        this.isSender = isSender;
        this.friendId = friendId;
    }

    public ChatBoxItem(String friendId) {
        this.friendId = friendId;
    }

    @Override
    public int compareTo(ChatBoxItem item) {
        if (item.id != null && (item.id.startsWith("friend") || item.id.startsWith("self"))) {
            return 1;
        }

        return new Long(timeStamp).compareTo(item.timeStamp);
    }

//    @Override
//    public boolean equals(Object o) {
//        if (o instanceof ChatBoxItem) {
//            String itemId = ((ChatBoxItem) o).id;
//            return id.equals(itemId);
//        }
//
//        return false;
//    }

    public enum TYPE {
        MESSAGE, PHOTO, VIDEO, MUSIC, CONTACT, DROPBOX, DOCUMENT, SEPARATE, LOCATION
    }

    /*
     MESSAGE : 1
     PHOTO : 2
     VIDEO : 3
     MUSIC : 4
     CONTACT : 5
     DROPBOX : 6
     DOCUMENT : 7
     SEPARATE : 8
     LOCATION : 9
     */

    public int type;

    public boolean isNeedDownload() {
        return type == 2 || type == 3 || type == 4 || type == 7;
    }

    public String time, id;
    public boolean isSender, isSeen, isLiked;
    public JSONObject data;
    public boolean isLoading = false;
    public Map<Integer, Integer> direction = new HashMap<>();
    public String serverFile;
    public int progress = PROGRESS_INIT;
    public String friendId;
    public long timeStamp;
    public String stringData;
    public int count = 0;

    //For resend file
    public String fileToResend, messageForResend;

    public void setResendInfo(String file, String info) {
        this.fileToResend = file;
        this.messageForResend = info;
    }

    public boolean hasServerFile() {
        return serverFile != null;
    }

    public static String formatTime(long timeStamp) {
        Date date = new Date(timeStamp * 1000);
        String th = Utils.getThOfDate(date);
        SimpleDateFormat format = new SimpleDateFormat("MMM dd- HH:mm");
        return format.format(date).replace("-", th);
    }

    public void parse(JSONObject data) {
        try {

            Log.e("data",data.toString());
            id = data.getString("id");

            isSeen = data.getString("seen").equals("true");
            isLiked = data.getString("liked").equals("true");
//            messageId = data.getString("id");

            if (data.has("gmtTimestamp")) {
                timeStamp = data.getLong("gmtTimestamp");
                time = formatTime(timeStamp * 1000);
            } else {
                time = data.getString("time");
            }

            String side = data.getString("side");
            isSender = "me".equals(side);

            String type = data.getString("type");

            switch (type) {
                case "share":
                case "bump":
                    String message = data.getString("message");
                    JSONObject object = new JSONObject(message);
                    serverFile = object.optString("serverFile", null);

                    String sf = data.optString("server_file", "null");
                    if (!sf.isEmpty() && !sf.equals("null")) {
                        serverFile = sf;
                    }

                    getTypeAndData(object, data);

                    break;
                case "message":
                    this.type = 1;
                    this.data = data;
                    this.stringData = data.toString();
                    break;
            }

            Log.e("time stamp", timeStamp + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getTypeAndData(JSONObject object, JSONObject data) throws JSONException {
        String bumpType = object.getString("type");

        switch (bumpType) {
            case "photo":
                this.type = 2;
                this.data = object;
                break;
            case "profile":
                this.type = 5;
                this.data = object;
                break;
            case "contact":
                this.type = 5;
                this.data = object.getJSONObject("message");
                break;
            case "video":
                this.type = 3;
                this.data = object.getJSONObject("message");
                break;
            case "music":
                this.type = 4;
                this.data = object.getJSONObject("message");
                break;
            case "document":
                this.type = 7;
                this.data = object.getJSONObject("message");
                break;
            case "dropbox":
                this.type = 6;
                this.data = data;
                break;
            case "location":
                this.type = 9;
                this.data = data;
                Log.e("QueANh_share_location", data + "");
                break;
        }
        this.stringData = this.data.toString();
    }

    public ChatBoxItem(String friendId, JSONObject data) {
        this.friendId = friendId;
        parse(data);
    }

    public ChatBoxItem(String friendId, int type, JSONObject data) {
        this.friendId = friendId;
        this.type = type;
        this.id = toString();


        this.timeStamp = System.currentTimeMillis() / 1000;
        this.time = "Just now";

        this.isSender = true;
        this.isSeen = false;
        this.isLiked = false;

        this.data = data;
        this.stringData = data.toString();
    }

    public ChatBoxItem(String friendId, int type, boolean isSender) {
        this.friendId = friendId;
        this.type = type;
        this.id = toString();

        this.time = "Just now";
        this.timeStamp = System.currentTimeMillis() / 1000;

        this.isLoading = true;
        this.isSender = isSender;
        this.isSeen = false;
        this.isLiked = false;
    }

    public void update(JSONObject json) {
        try {
            id = json.getString("id");

            isSeen = json.getString("seen").equals("true");
            isLiked = json.getString("liked").equals("true");

            timeStamp = json.getLong("chat_time");
            time = formatTime(timeStamp * 1000);

            String message = json.getString("message");
            JSONObject object = new JSONObject(message);

            serverFile = object.optString("serverFile", null);
            getTypeAndData(object, json);

            //store item
//            RushOrmManager.getInstance().storeSingleChatItem(this);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //
    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public void setJsonData(JSONObject json) {
        parse(json);
    }

    public JSONObject getData() {
        if (stringData == null) {
            return null;
        }

        try {
            return new JSONObject(stringData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

}
