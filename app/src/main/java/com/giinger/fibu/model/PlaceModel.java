package com.giinger.fibu.model;

/**
 * Created by geekup on 4/24/15.
 */
public class PlaceModel {

    private String name;
    private String address;
    private String lat;
    private String lng;
    private String img;

    public PlaceModel(){

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName(){
        return name;
    }

    public String ggetAddress(){
        return address;
    }

    public String getLat(){
        return lat;
    }

    public String getLng(){
        return lng;
    }

    public String getImg() {
        return img;
    }
 }
