package com.giinger.fibu.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.util.Log;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.giinger.fibu.R;
import com.giinger.fibu.service.UpDownLoadService;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

public class AppUtils {
//    public static final String HOST = "http://beta.app.getfibu.com/";
    public static final String HOST = "http://fibu.geekup.vn/";

    public static final String ROOT_URL = HOST + "api/v1/";
    public static final String API_SAVE_PROFILE = "user/saveProfile";
    public static final String API_VIEW_PROFILE = "user/viewProfile";
    public static final String API_GET_ADVERTISE = "app/advertisingApps";
    public static final String API_UPLOAD_AVATAR = "user/uploadAvatar";
    public static final String API_GET_CONFIG = "app/config";
    public static final String API_GET_ADDITIONAL_FIELD = "user/additionalFields";
    public static final String API_SEND_FILE = "bump/send";
    public static final String API_RECEIVE_FILE = "bump/receive";
    public static final String API_FRIENDS_ACTIVITY = "chat/friendActivities";
    public static final String API_CHAT_THREAD = "chat/friendThread";
    public static final String API_CHAT_SEND = "chat/send";
    public static final String API_CHAT_LIKE = "chat/like";
    public static final String API_UPDATE_BUMP_RESULT = "bump/updateBumpResult";
    public static final String API_REMOVE_USER = "user/remove";
    public static final String API_BLOCK_USER = "user/block";
    public static final String API_BUMP = "bump/bump";
    public static final String API_CONFIRM_ACTIVITY = "user/confirmActivity";
    public static final String API_RESEND_CODE = "user/resendPasscode";
    public static final String API_CONFIRM_CONNECT = "bump/confirmConnect";
    public static final String API_COLLECTION_INFO = "user/submitDeviceInformation";

    public static String getConfirmConnectUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_CONFIRM_CONNECT;
    }

    public static String getResendCodeUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_RESEND_CODE;
    }

    public static String getConfirmUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_CONFIRM_ACTIVITY;
    }

    public static String getAppConfigUrl() {
        return ROOT_URL + API_GET_CONFIG;
    }

    public static void saveAppConfig(Context context, JSONObject data) {
        Log.e("Config", data + "");
        try {
            JSONObject appConfig = data.getJSONObject("$appConfig");
            String rootUrl = appConfig.getString("rootUrl");
            PreferenceUtils.saveString(Constants.ROOT_URL, context, rootUrl);
            String avatarUrl = appConfig.getString("avatarUrl");
            PreferenceUtils.saveString(Constants.ROOT_AVATAR_URL, context, avatarUrl);
            String uploadedUrl = appConfig.getString("uploadedUrl");
            PreferenceUtils.saveString(Constants.ROOT_UPLOADED_URL, context, uploadedUrl);
            String appLink = appConfig.getString("appLink");
            PreferenceUtils.saveString(Constants.APP_LINK, context, appLink);

            String inviteMsg = appConfig.getString("inviteMsg");
            PreferenceUtils.saveString(Constants.INVITE_MESSAGE, context, inviteMsg);

            String smsSharingTemplate = appConfig.getString("smsSharingTemplate");
            PreferenceUtils.saveString(Constants.SMS_TEMPLATE, context, smsSharingTemplate);

            String emailSharingTemplate = appConfig.getString("emailSharingTemplate");
            PreferenceUtils.saveString(Constants.EMAIL_TEMPLATE, context, emailSharingTemplate);

            String gpUrl = appConfig.getString("gpUrl");
            PreferenceUtils.saveString(Constants.GP_URL, context, gpUrl);

            String feedbackEmail = appConfig.getString("feedbackEmail");
            PreferenceUtils.saveString(Constants.EMAIL_FEEDBACK, context, feedbackEmail);

            String howItWorkUrl = appConfig.getString("howItWorkUrl");
            PreferenceUtils.saveString(Constants.HOW_IT_WORK_URL, context, howItWorkUrl);

            String firstTourVideoUrl = appConfig.getString("firstTourVideoUrl");
            PreferenceUtils.saveString(Constants.TOUR_VIDEO, context, firstTourVideoUrl);

            String iapItem = appConfig.getString("iapItem");
            PreferenceUtils.saveString(Constants.IAP_ITEM, context, iapItem);

            JSONObject parseConfig = appConfig.getJSONObject("parseConfig");
            String appId = parseConfig.getString("appId");
            PreferenceUtils.saveString(Constants.Parse.APP_ID, context, appId);
            String restKey = parseConfig.getString("restKey");
            PreferenceUtils.saveString(Constants.Parse.REST_KEY, context, restKey);
            String masterKey = parseConfig.getString("masterKey");
            PreferenceUtils.saveString(Constants.Parse.MASTER_KEY, context, masterKey);
            String clientKey = parseConfig.getString("clientKey");
            PreferenceUtils.saveString(Constants.Parse.CLIENT_KEY, context, clientKey);

            Intent intent = new Intent(context, UpDownLoadService.class);
            intent.putExtra(UpDownLoadService.TYPE, UpDownLoadService.TYPE_DOWNLOAD_VIDEO);
            context.startService(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String getRootUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL);
    }

    public static String getSaveProfileUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_SAVE_PROFILE;
    }

    public static String getAdvertiseUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_GET_ADVERTISE;
    }

    public static String getUploadAvatarUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_UPLOAD_AVATAR;
    }

    public static String getAvatarByName(Context context, String name) {
        String url = PreferenceUtils.getString(Constants.ROOT_AVATAR_URL, context, HOST + "uploads/avatars/");
        return name == null || "null".equals(name) ? null : url + name;
    }

    public static void loadAvatar(final Context context, ImageView img, String url, final boolean setBackground) {
        if (url == null) {
            img.setImageResource(R.drawable.empty_ava);
        } else {
            AQuery aQuery = new AQuery(context);
            Bitmap cachedImage = aQuery.getCachedImage(url);
            if (cachedImage == null) {
                Log.e("Avatar", "Load " + url);
                new AQuery(context).id(img).image(url, true, true, 0, R.drawable.empty_ava, new BitmapAjaxCallback() {
                    @Override
                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                        super.callback(url, iv, bm, status);
                        if (setBackground) {
                            int color = context.getResources().getColor(R.color.blue_on_setting);
                            iv.setBackgroundColor(color);
                        }
                    }
                });
            } else {
                Log.e("Avatar", "Cached " + url);
                img.setImageBitmap(cachedImage);
            }
        }
    }

    public static String getAdditionalFieldUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_GET_ADDITIONAL_FIELD;
    }

    public static String getBumpUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_BUMP;
    }

    public static String getSendFileUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_SEND_FILE;
    }

    public static String getReceiveFileUrl(Context context, String userId, String friendId, String token) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_RECEIVE_FILE + "?receiver_id=" + userId + "&sender_id=" + friendId + "&receive_token=" + token;
    }

    public static String getFriendsActivityUrl(Context context, String userId) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_FRIENDS_ACTIVITY + "?user_id=" + userId;
    }

    public static String getViewProfileUrl(Context context, String userId) {
        return getProfileUrl(context, userId, "view");
    }

    public static String getEditProfileUrl(Context context, String userId) {
        return getProfileUrl(context, userId, "edit");
    }

    private static String getProfileUrl(Context context, String userId, String mode) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_VIEW_PROFILE + "?user_id=" + userId + "&mode=" + mode;
    }

    public static String getChatThread(Context context, String userId, String friendId) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_CHAT_THREAD + "?user_id=" + userId + "&friend_id=" + friendId;
    }

    public static String getChatSendUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_CHAT_SEND;
    }

    public static String getChatLikeUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_CHAT_LIKE;
    }

    public static String getUpdateBumpResultUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_UPDATE_BUMP_RESULT;
    }

    public static String getRemoveUserUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_REMOVE_USER;
    }

    public static String getBlockUserUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_BLOCK_USER;
    }

    public static String getGeocoderUrl(Location location) {
        return "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + location.getLatitude() + "," + location.getLongitude();
    }

    public static String getNearbyUrl(Location location, Activity mActivity) {
        return "https://api.foursquare.com/v2/venues/search?client_id=" + mActivity.getResources().getString(R.string.foursquare_client) + "&client_secret=" + mActivity.getResources().getString(R.string.foursquare_secret) +
                "&ll=" + location.getLatitude() + "," + location.getLongitude() + "&v=" + System.currentTimeMillis() + "&query=coffee&limit=2";
    }

    public static String getGeocoderNameUrl(String name) {
        return "https://maps.googleapis.com/maps/api/geocode/json?address=" + name;
    }

    public static String getFileUrlByName(Context context, String name) {
        String url = PreferenceUtils.getString(Constants.ROOT_UPLOADED_URL, context, HOST + "uploads/files/");
        return name == null ? url : url + name;
    }

    public static String staticMapUrl(String location) {
        return "https://maps.googleapis.com/maps/api/staticmap?center=" + location + "&zoom=17&size=400x400&markers=size:mid%7CFcolor:0xFFFF00%7Clabel:F%7C" + location;
    }

    public static String getUrlTextPlace(String location, String textPlace) {
        return "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + textPlace + "&sensor=true&location=" + location + "&radius=50000&key=AIzaSyDeOv5zzB3JukAYWkvaUK_9QWODVhrWMHk";
    }

    public static JsonObject getMeta(JsonObject json) {
        JsonObject meta = json.getAsJsonObject("meta");
        boolean success = meta.getAsJsonPrimitive("success").getAsInt() == 1;
        return success ? meta : null;
    }

    public static JsonObject getResponse(JsonObject json) {
        JsonObject meta = getMeta(json);

        if (meta == null) {
            return null;
        }

        JsonObject response = json.getAsJsonObject("response");
        boolean success = response.getAsJsonPrimitive("success").getAsBoolean();

        return success ? response : null;
    }

    public static String getCollectionInfoUrl(Context context) {
        return PreferenceUtils.getString(Constants.ROOT_URL, context, ROOT_URL) + API_COLLECTION_INFO;
    }

}
