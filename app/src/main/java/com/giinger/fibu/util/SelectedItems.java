package com.giinger.fibu.util;

public interface SelectedItems {
    void onSelected(Object item, boolean select);
}
