package com.giinger.fibu.util;

public interface OnRefreshListener {
    void onRefresh();

    void onSetImageAvatar(String path);
}
