package com.giinger.fibu.util;

public class Constants {
    // RESPONSE
    public static final String META = "meta";
    public static final String RESPONSE = "response";
    public static final String SUCCESS = "success";

    public static final String FACEBOOK_LINK = "https://www.facebook.com/";

    // TOKEN DROPBOX
    public static final String DROPBOX_TOKEN = "dropbox_token";

    // KEY PREFERENCE
    public static final String LOGIN = "login";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String JOB_TITLE = "job_title";
    public static final String USER_ID = "user_id";
    public static final String SETTING_SENSITIVE = "setting_sensitive";
    public static final String APP_LINK = "app_link";
    public static final String INVITE_MESSAGE = "invite_message";
    public static final String SMS_TEMPLATE = "sms_template";
    public static final String EMAIL_TEMPLATE = "email_template";
    public static final String GP_URL = "google_place_url";
    public static final String EMAIL_FEEDBACK = "email_feekback";
    public static final String HOW_IT_WORK_URL = "how_it_work_url";
    public static final String TOUR_VIDEO = "tour_video";
    public static final String TOUR_VIDEO_SAVED = "tour_video_stored";

    public static final String ROOT_AVATAR_URL = "root_avatar_url";
    public static final String ROOT_URL = "root_url";
    public static final String USER_INFO_JSON = "json_info";
    public static final String FIRST_START = "first_start";
    public static final String AVATAR_URL = "avatar_url";
    public static final String USER_PROFILE = "user_profile";
    public static final String COUNT_BADGE = "count_badge";
    public static final String PATH_SHARING = "path_sharing";
    public static final String JSON_LOC = "json_location";
    public static final String ROOT_UPLOADED_URL = "uploaded_url";
    public static final String IAP_ITEM = "iapItem";
    public static final String TIMES_BUMP_SUCCESS = "times_bump_success";
    public static final String MAX_SELECTED_CONTACT = "max_selected_contact";
    public static final String MAX_SELECTED_PHOTO = "max_selected_photo";
    public static final String MAX_SELECTED_MUSIC = "max_selected_music";
    public static final String FRIEND_ID = "friend_id";
    public static final String SHOW_VIDEO_TOUR = "show_video_tour";
    public static final String SHOW_BUMP_CARD = "show_bump_card";
    public static final int DEFAULT_SENSITIVE = 0;
    public static final String IS_RESTORE_ACCOUNT = "is_restore_account";

    public static final String FB_ID = "fb_id";
    public static final String FB_ADD_FRIEND = "fb_add_friend";

    // INTEGER
    public static int position = 0;
    public static int contactSize = 0;

    //BOOLEAN
    public static boolean canBump = true;

    public static boolean started = false;
    public static boolean start = true;
    public static boolean isChat = false;
    public static boolean isAnim = true;
    public static boolean offBar = false;
    public static boolean toForeground = false;
    public static boolean isShowWarrning = true;
    public static String imgUri = "";

    public static final String RATED = "rated";
    public static String PURCHASED = "purchased";

    public enum PURCHASE {
        BUY_ALL("purchasedUnlimit"), BUY_ONE("purchasedPro");

        public String type;

        PURCHASE(String type) {
            this.type = type;
        }
    }

    public static class Parse {
        public static final String APP_ID = "parse_app_id";
        public static final String REST_KEY = "parse_rest_key";
        public static final String MASTER_KEY = "parse_master_key";
        public static final String CLIENT_KEY = "parse_client_key";
    }

    public static final String FIELD_FACEBOOK = "facebook";
    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_ADDRESS = "address";
    public static final String FIELD_PHONE = "phone";
    public static final String FIELD_LINK = "link";
}
