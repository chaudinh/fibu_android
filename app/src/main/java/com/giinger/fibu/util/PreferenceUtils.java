package com.giinger.fibu.util;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PreferenceUtils {

    private static String REFERENCE_NAME = "SharePreferences";

    public static void saveBoolean(String key, Context myContext,
                                   boolean value) {
        if (myContext != null) {
            SharedPreferences prefs = myContext.getSharedPreferences(
                    REFERENCE_NAME, Context.MODE_PRIVATE);
            prefs.edit().putBoolean(key, value).commit();
        }
    }

    public static boolean getBoolean(String key, Context myContext,
                                     boolean value) {
        if (myContext == null) {
            return value;
        }

        SharedPreferences prefs = myContext.getSharedPreferences(
                REFERENCE_NAME, Context.MODE_PRIVATE);

        return prefs.getBoolean(key, value);
    }

    public static void saveString(String key, Context myContext, String value) {
        if (myContext != null) {
            SharedPreferences prefs = myContext.getSharedPreferences(
                    REFERENCE_NAME, Context.MODE_PRIVATE);
            prefs.edit().putString(key, value).commit();
        }
    }

    public static String getString(String key, Context myContext,
                                   String value) {
        if (myContext == null) {
            return value;
        }

        SharedPreferences prefs = myContext.getSharedPreferences(
                REFERENCE_NAME, Context.MODE_PRIVATE);

        return prefs.getString(key, value);
    }

    public static void saveInt(String key, Context myContext, int value) {
        if (myContext != null) {
            SharedPreferences prefs = myContext.getSharedPreferences(
                    REFERENCE_NAME, Context.MODE_PRIVATE);
            prefs.edit().putInt(key, value).commit();
        }
    }

    public static int getInt(String key, Context myContext, int value) {
        if (myContext == null) {
            return value;
        }

        SharedPreferences prefs = myContext.getSharedPreferences(
                REFERENCE_NAME, Context.MODE_PRIVATE);
        return prefs.getInt(key, value);
    }

    private static final List<String> BASE_FIELD = Arrays.asList("id", "first_name", "last_name", "job_title", "mobile", "email", "avatar", "rated", "purchase_type");

    public static boolean isBaseField(String key) {
        return BASE_FIELD.contains(key);
    }

    public static JSONObject convertJson(JSONArray array) {
        int length = array.length();

        JSONObject result = new JSONObject();

        for (int i = 0; i < length; i++) {
            try {
                JSONObject item = array.getJSONObject(i);

                String field = item.getString("field");
                item.remove("field");

                result.put(field, item);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return result;
    }

    public static JSONObject getUserInfo(Context context) {
        String data = PreferenceUtils.getString(Constants.USER_INFO_JSON, context, "{}");

        try {
            return new JSONObject(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new JSONObject();
    }

    public static JSONObject saveUserInfo(Context context, JSONArray userInfo) {
        JSONObject userData = PreferenceUtils.convertJson(userInfo);
        PreferenceUtils.saveString(Constants.USER_INFO_JSON, context, userData.toString());
        return userData;
    }
}
