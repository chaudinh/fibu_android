package com.giinger.fibu.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.giinger.fibu.R;
import com.giinger.fibu.activity.ChatBoxScreen;
import com.giinger.fibu.model.ChatBoxItem;
import com.giinger.fibu.model.Friend;
import com.giinger.fibu.service.UpDownLoadService;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.utils.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.uk.rushorm.core.RushCallback;

public class BumpUtils {

    private static final String FOLDER_NAME = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/Fibu/";
    private static final String FOLDER_CACHE = FOLDER_NAME + "cache/";

    public static File getFile(String name, boolean cache) {
        getCacheFolder();
        String path = cache ? FOLDER_CACHE : FOLDER_NAME;
        return new File(path, name);
    }

    public static File getCacheFolder() {
        getAppFolder();

        File folder = new File(FOLDER_CACHE);
        if (!folder.exists()) {
            folder.mkdir();
        }

        return folder;
    }

    public static File getAppFolder() {
        File folder = new File(FOLDER_NAME);
        if (!folder.exists()) {
            folder.mkdir();
        }

        return folder;
    }

    public static boolean extractFiles(Context context, File zip, boolean cache) {
        Log.e("Extract", "Start");

        if (zip == null) {
            Log.e("Extract", "File is null");
            return false;
        }

        List<File> files = new ArrayList<>();
        try {
            InputStream input = new FileInputStream(zip);
            ArchiveInputStream inputStream = new ArchiveStreamFactory().createArchiveInputStream("zip", input);
            do {
                ZipArchiveEntry zipEntry = (ZipArchiveEntry) inputStream.getNextEntry();
                if (zipEntry == null) {
                    break;
                }

                String name = zipEntry.getName();
                Log.e("Extract", "File " + name);

                File file = getFile(name, cache);
                OutputStream output = new FileOutputStream(file);
                IOUtils.copy(inputStream, output);
                output.close();
                files.add(file);
            } while (true);

            inputStream.close();
            zip.delete();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ArchiveException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.e("Extract", "Finish");

//        if (mediaScannerConnection != null) {
//            mediaScannerConnection.disconnect();
//        }
//
//        mediaScannerConnection = new MediaScannerConnection(context, new MediaScannerConnection.MediaScannerConnectionClient() {
//            @Override
//            public void onMediaScannerConnected() {
//                mediaScannerConnection.scanFile(FOLDER_NAME, null);
//            }
//
//            @Override
//            public void onScanCompleted(String path, Uri uri) {
//                mediaScannerConnection.disconnect();
//            }
//        });
//        mediaScannerConnection.connect();

        for (File file : files) {
            addFileToGallery(context, file);
        }

        return !files.isEmpty();
    }

    private static void addFileToGallery(Context context, File file) {
        Uri contentUri = Uri.fromFile(file);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

//    private static MediaScannerConnection mediaScannerConnection;

    public static JSONObject compressFiles(List<String> filesPath, String userId) {
        Log.e("Compress", "Start");

        File folder = getCacheFolder();
        String basePath = folder.getAbsolutePath();

        long timeStamp = System.currentTimeMillis();
        String path = basePath + "/bump-" + userId + "-" + timeStamp + ".zip";
        File output = new File(path);
        OutputStream outputFile = null;
        ArchiveOutputStream outputStream = null;

        JSONArray files = new JSONArray();

        try {
            outputFile = new FileOutputStream(output);
            outputStream = new ArchiveStreamFactory().createArchiveOutputStream("zip", outputFile);
            for (String filePath : filesPath) {
                File f = new File(filePath);
                FileInputStream inputStream = new FileInputStream(filePath);
//                String name = userId + "-bump-" + timeStamp + "-" + f.getName();
                String name = f.getName();
                Log.e("Compress", "Add " + name);
                ZipArchiveEntry zipEntry = new ZipArchiveEntry(name);
                outputStream.putArchiveEntry(zipEntry);
                IOUtils.copy(inputStream, outputStream);
                outputStream.closeArchiveEntry();

                JSONObject file = new JSONObject();
                file.put("name", name);
                file.put("size", f.length());
                files.put(file);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ArchiveException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (outputFile != null) {
                try {
                    outputFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        Log.e("Compress", "Finish");

        try {
            JSONObject result = new JSONObject();
            result.put("items", files);
            result.put("zip", output);
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public interface Callback {
        void callback();
    }

    public static void upload(Context context, Friend friend, ArrayList<String> selected, final Callback callback) {
        String friendId = String.valueOf(friend.id);
        final ChatBoxItem chatBoxItem = new ChatBoxItem(friendId, 3, true);
        switch (friend.sendType) {
            case "dropbox":
                chatBoxItem.progress = ChatBoxItem.PROGRESS_GETTING_LINK;
                break;
            default:
                chatBoxItem.progress = ChatBoxItem.PROGRESS_COMPRESSING;
                break;
        }

        Intent intent = new Intent(context, UpDownLoadService.class);
        intent.putExtra(UpDownLoadService.TYPE, UpDownLoadService.TYPE_BUMP_SEND);
        intent.putExtra(ChatBoxScreen.EXTRA_LOADING_ID, chatBoxItem.id);
        intent.putExtra(UpDownLoadService.EXTRA_FRIEND_ID, friend.id);
        intent.putExtra(UpDownLoadService.EXTRA_UPLOAD_TYPE, friend.sendType);
        intent.putExtra(UpDownLoadService.EXTRA_BUMP_SEND_TOKEN, friend.sendToken);
        intent.putStringArrayListExtra(UpDownLoadService.EXTRA_BUMP_SEND_SELECTED, selected);
        context.startService(intent);

        if ("profile".equals(friend.sendType)) {
            callback.callback();
        } else {
            chatBoxItem.save(new RushCallback() {
                @Override
                public void complete() {
                    ChatBoxScreen.addBumpItem(chatBoxItem);
                    callback.callback();
                }
            });
        }
    }

    public static void download(Context context, Friend friend, final Callback callback) {
        String friendId = String.valueOf(friend.id);
        final ChatBoxItem chatBoxItem = new ChatBoxItem(friendId, 3, false);

        if ("profile".equals(friend.receiveType)) {
            callback.callback();
            return;
        }

        chatBoxItem.progress = ChatBoxItem.PROGRESS_WAITING;

        Intent intent = new Intent(context, UpDownLoadService.class);
        intent.putExtra(UpDownLoadService.TYPE, UpDownLoadService.TYPE_BUMP_RECEIVE);
        intent.putExtra(ChatBoxScreen.EXTRA_LOADING_ID, chatBoxItem.id);
        intent.putExtra(UpDownLoadService.EXTRA_FRIEND_ID, friend.id);
        intent.putExtra(UpDownLoadService.EXTRA_BUMP_RECEIVE_TOKEN, friend.receiveToken);
        context.startService(intent);

        chatBoxItem.save(new RushCallback() {
            @Override
            public void complete() {
                ChatBoxScreen.addBumpItem(chatBoxItem);
                callback.callback();
            }
        });
    }

    public static void updateBumpResult(Context context, String senderId, String receiverId) {
        AQuery aQuery = new AQuery(context);

        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                Log.e("Update Bump result", object + " " + url);
            }
        };

        Map<String, String> params = new HashMap<>();
        params.put("sender_id", senderId);
        params.put("receiver_id", receiverId);

        String url = AppUtils.getUpdateBumpResultUrl(context);
        aQuery.ajax(url, params, JSONObject.class, callback);
    }

    public interface ConnectConfirmCallback {
        int TIME_OUT = 30000;

        String ACTION_CONNECT = "connect";
        String ACTION_CANCEL = "cancel";

        void doTransferData();

        void hidePopupConnecting();
    }

    public static void confirmConnect(final ConnectConfirmCallback connectConfirmCallback, final Context context, String connectId, final String action, final String friendId) {
        String url = AppUtils.getConfirmConnectUrl(context);
        Builders.Any.B request = Ion.with(context).load(url);

        final String userId = PreferenceUtils.getString(Constants.USER_ID, context, null);
        request.setBodyParameter("userId", userId);
        request.setBodyParameter("connectId", connectId);
        request.setBodyParameter("action", action);

        request.setTimeout(ConnectConfirmCallback.TIME_OUT).asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {

                if (connectConfirmCallback == null || ConnectConfirmCallback.ACTION_CANCEL.equals(action)) {
                    return;
                }

                if (e != null || result == null) {
                    Toast.makeText(context, R.string.connect_timeout_message, Toast.LENGTH_SHORT).show();
                    connectConfirmCallback.hidePopupConnecting();
                    return;
                }

                JsonObject meta = AppUtils.getMeta(result);
                if (meta == null) {
                    Toast.makeText(context, R.string.connect_timeout_message, Toast.LENGTH_SHORT).show();
                    connectConfirmCallback.hidePopupConnecting();
                    return;
                }

                JsonObject response = result.getAsJsonObject("response");
                boolean success = response.getAsJsonPrimitive("success").getAsBoolean();
                if (success) {
                    connectConfirmCallback.doTransferData();
                    BumpUtils.updateBumpResult(context, friendId, userId);
                } else {
                    String r = response.getAsJsonPrimitive("result").getAsString();
                    switch (r) {
                        case "friendCancel":
                            Toast.makeText(context, R.string.connect_friend_cancel_message, Toast.LENGTH_SHORT).show();
                            break;
                        case "friendConnectTimeout":
                            Toast.makeText(context, R.string.connect_timeout_message, Toast.LENGTH_SHORT).show();
                            break;
                    }

                    connectConfirmCallback.hidePopupConnecting();
                }

            }
        });
    }
}
