package com.giinger.fibu.util;

import org.json.JSONObject;

public interface OnItemSharing {
    public void onShare(String type, JSONObject data);
}
