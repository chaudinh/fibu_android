package com.giinger.fibu.util;

import android.content.Context;
import android.util.Log;

import com.giinger.fibu.model.ChatBoxItem;
import com.giinger.fibu.model.ChatThreadModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import co.uk.rushorm.android.AndroidInitializeConfig;
import co.uk.rushorm.core.RushCallback;
import co.uk.rushorm.core.RushCore;
import co.uk.rushorm.core.RushSearch;

/**
 * Created by luannguyen on 7/29/15.
 */
public class RushOrmManager {
    public static RushOrmManager instance;

    public static RushOrmManager getInstance() {
        if (instance == null) {
            instance = new RushOrmManager();
        }

        return instance;
    }

    public void setUpRushOrm(Context context) {
        AndroidInitializeConfig config = new AndroidInitializeConfig(context);
        RushCore.initialize(config);
    }

    public void storeSingleChatItem(final ChatBoxItem model) {
        boolean isAvailable = checkItemAvailable(model);
        if (isAvailable) {
            Log.e("Lu", "item stored");
            final ChatBoxItem item = getSingleChatItem(model.id);

            item.isSeen = model.isSeen;
            item.isLiked = model.isLiked;
            item.save(new RushCallback() {
                @Override
                public void complete() {
                    Log.e("Update item", item.stringData);
                }
            });
        } else {
            model.save(new RushCallback() {
                @Override
                public void complete() {
                    Log.e("Lu", "save chat object - timestamp : " + model.timeStamp + " - id : " + model.id);
                }
            });
        }
    }

    public void storeListChatItems(List<ChatBoxItem> listChat) {
        RushCore.getInstance().save(listChat, new RushCallback() {
            @Override
            public void complete() {
                Log.e("Lu", "save list chat thread success");
            }
        });
    }

    public ChatBoxItem getSingleChatItem(String messageId) {
        ChatBoxItem model = new RushSearch().whereEqual("id", messageId).findSingle(ChatBoxItem.class);
        return model;
    }

    public List<ChatBoxItem> getListChatItems(String friendId) {
        List<ChatBoxItem> listChat = new RushSearch().whereEqual("friendId", friendId).orderDesc("timeStamp").limit(10).find(ChatBoxItem.class);
        Collections.reverse(listChat);
        return listChat;
    }

    public List<ChatBoxItem> getListChatItemsPaging(String friendId, long lastTimeStamp) {
        Log.e("RushOrm", "last time stamp : " + lastTimeStamp);
        List<ChatBoxItem> listChat = new RushSearch().whereEqual("friendId", friendId).and().whereLessThan("timeStamp", lastTimeStamp).orderDesc("timeStamp").limit(10).find(ChatBoxItem.class);
        Collections.reverse(listChat);
        return listChat;
    }

    public void deleteChatWithFriend(final String friendId) {
        List<ChatBoxItem> items = new RushSearch().whereEqual("friendId", friendId).find(ChatBoxItem.class);
        RushCore.getInstance().delete(items, new RushCallback() {
            @Override
            public void complete() {
                Log.e("Deleted", "Chat with " + friendId);
            }
        });
    }

    public void deleteTableChatThread() {
        RushCore.getInstance().deleteAll(ChatBoxItem.class);
    }

    public boolean checkItemAvailable(ChatBoxItem chatBoxItem) {
        RushSearch rushSearch = new RushSearch();
        ChatBoxItem item = rushSearch
                .whereEqual("id", chatBoxItem.id)
                .and()
                .whereEqual("count", chatBoxItem.count)
                .findSingle(ChatBoxItem.class);

        if (item == null) {
            return false;
        }

        return true;
    }

    public void updateProgress(String loadingId, int progress) {
        RushSearch rushSearch = new RushSearch();
        final ChatBoxItem item = rushSearch.whereEqual("id", loadingId).findSingle(ChatBoxItem.class);
        if (item != null) {
            Log.e("Save", loadingId + " " + progress);

            switch (progress) {
                case ChatBoxItem.PROGRESS_SUCCESS:
                case ChatBoxItem.PROGRESS_FAIL:
                    item.isLoading = false;
                    break;
            }

            item.progress = progress;
            item.save();
        }
    }

    public int checkFriendId(String friendId) {
        RushSearch rushSearch = new RushSearch();
        int size = rushSearch.whereEqual("friendId", friendId).findSingle(ChatBoxItem.class) == null ? 0 : 1;

        return size;
    }

    public void deleteItem(ChatBoxItem item) {
        RushCore.getInstance().delete(item);
    }
}
