package com.giinger.fibu.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.MediaStore.Images;
import android.provider.Settings;
import android.support.v4.content.CursorLoader;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.giinger.fibu.R;
import com.giinger.fibu.activity.CameraScreen;
import com.giinger.fibu.element.CustomButton;
import com.giinger.fibu.model.ChatBoxItem;
import com.giinger.fibu.model.Friend;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static Utils mInstance;
    public AlertDialog alertDialog;

    public static void sendSms(Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);

        String content = PreferenceUtils.getString(Constants.SMS_TEMPLATE, context, "");
        intent.putExtra("sms_body", content);
        intent.setType("vnd.android-dir/mms-sms");

        context.startActivity(intent);
    }

    public static void sendEmail(Context context, String subject, String text, String... emails) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        intent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");

        if (subject == null) {
            subject = context.getString(R.string.app_name);
        }

        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.putExtra(Intent.EXTRA_EMAIL, emails);

        String title = context.getString(R.string.send_email_with);
        Intent chooser = Intent.createChooser(intent, title);
        context.startActivity(chooser);
    }

    public static Utils getInstance() {
        if (mInstance == null)
            mInstance = new Utils();

        return mInstance;
    }

    public void showSoftKey(Context context, EditText edt) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edt, InputMethodManager.SHOW_FORCED);
    }

    public byte[] getFileData(File file) {
        byte[] byteArray = null;
        try {
            InputStream inputStream = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024 * 20];
            int bytesRead = 0;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return byteArray;
    }

    public void showEditDialogAvatar(final Activity mActivity) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.dialog_edit_avatar);
        dialog.getWindow().getAttributes().width = LayoutParams.FILL_PARENT;

        LinearLayout takePhoto = (LinearLayout) dialog
                .findViewById(R.id.take_photo);
        LinearLayout takeGallery = (LinearLayout) dialog
                .findViewById(R.id.take_gallery);
        LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);

        takePhoto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity, CameraScreen.class);
                mActivity.startActivity(i);
                dialog.dismiss();
            }
        });

        takeGallery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        Images.Media.EXTERNAL_CONTENT_URI);
                mActivity.startActivityForResult(intent, 1);
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public float convertDpToPixel(float dp, Context c) {
        Resources resources = c.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public void showLayoutDialogEditAvatar(View view, final Activity activity) {
        final RelativeLayout layoutChooseEdit;
        RelativeLayout layoutTakePhoto;
        RelativeLayout layoutTakeGallery;
        RelativeLayout layoutCancel;
        CustomButton btnNext;

        layoutChooseEdit = (RelativeLayout) view.findViewById(R.id.layout_choose_edit_avatar);
        layoutTakeGallery = (RelativeLayout) view.findViewById(R.id.layout_take_gallery);
        layoutTakePhoto = (RelativeLayout) view.findViewById(R.id.layout_take_photo);
        layoutCancel = (RelativeLayout) view.findViewById(R.id.layout_cancel);
        btnNext = (CustomButton) view.findViewById(R.id.btn_next_base_info);
        if (btnNext == null) {
            btnNext = (CustomButton) view.findViewById(R.id.btn_bump);
        }

        layoutChooseEdit.setVisibility(View.VISIBLE);

        final CustomButton finalBtnNext = btnNext;
        layoutTakeGallery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutChooseEdit.setVisibility(View.GONE);
                if (finalBtnNext != null)
                    finalBtnNext.setVisibility(View.VISIBLE);

                Intent intent = new Intent(Intent.ACTION_PICK,
                        Images.Media.INTERNAL_CONTENT_URI);
                activity.startActivityForResult(intent, 1);
            }
        });

        layoutTakePhoto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutChooseEdit.setVisibility(View.GONE);
                if (finalBtnNext != null)
                    finalBtnNext.setVisibility(View.VISIBLE);

                Intent i = new Intent(activity, CameraScreen.class);
                activity.startActivity(i);
            }
        });

        layoutCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.imgUri = null;
                layoutChooseEdit.setVisibility(View.GONE);
                if (finalBtnNext != null)
                    finalBtnNext.setVisibility(View.VISIBLE);
            }
        });
    }

    public String getRealPathFromURI(Uri contentUri, Activity activity) {
        String[] projection = {Images.Media.DATA};

        CursorLoader cursorLoader = new CursorLoader(activity, contentUri, projection,
                null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        int column_index = cursor
                .getColumnIndexOrThrow(Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static String getImageUri(Bitmap inImage, boolean scale) {
        File file = BumpUtils.getFile("avatar.jpg", true);

        try {
            if (scale) {
                Bitmap scaledBitmap = Bitmap.createScaledBitmap(inImage, 300, 300, false);
                FileOutputStream out = new FileOutputStream(file);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            } else {
                FileOutputStream out = new FileOutputStream(file);
                inImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return file.getAbsolutePath();
    }

    public void forcePopupLocation(final Context context) {
//        if (!checkLocationEnable(context)) {
        dismissLocationDialog();

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage(context.getResources().getString(
                R.string.msg_force_location));
        dialog.setPositiveButton(
                context.getResources().getString(R.string.text_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(myIntent);

                    }
                });
        alertDialog = dialog.show();
        alertDialog.setCancelable(false);
//        }
    }

    public static void saveAvatar(Context context, final String avatarName) {
        String url = AppUtils.getAvatarByName(context, avatarName);
        File folder = BumpUtils.getCacheFolder();
        File file = new File(folder, "avatar.jpg");

        new AQuery(context).download(url, file, new AjaxCallback<File>() {
            @Override
            public void callback(String url, File object, AjaxStatus status) {
                super.callback(url, object, status);
                Log.e("Dowloaded", "Avatar " + avatarName);
            }
        });
    }

    public boolean checkLocationEnable(Context context) {
        LocationManager lm = null;
        boolean gps_enabled = false;
        boolean network_enabled = false;

        if (lm == null) {
            lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        }

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gps_enabled || network_enabled;
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public void postAvatar(final Activity activity, final byte[] data) {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                String url = AppUtils.getUploadAvatarUrl(activity);
                final HttpClient client = new HttpClient(url);

                try {
                    client.connectForMultipart();
                    client.addFormPart("user_id", PreferenceUtils.getString(Constants.USER_ID, activity, ""));
                    client.addFilePart("avatar", "avatar.jpg", data);

                    client.finishMultipart();

                    final JSONObject data = new JSONObject(client.getResponse());

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Log.e("QueANh_upload_avatar : ", data.toString());
                                if (data.getJSONObject("response").has("avatar")) {
                                    String avatarUrl = data.getJSONObject("response").getString("avatar");
                                    PreferenceUtils.saveString(Constants.AVATAR_URL, activity, avatarUrl);
                                    Constants.imgUri = "";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        thread.start();
    }

    public void setSensitivity(int seek) {
        float sensitive = 15.0f;
        switch (seek) {
            case 0:
                sensitive = 5.0f;
                break;
            case 50:
                sensitive = 15.0f;
                break;
            case 100:
                sensitive = 25.0f;
                break;
        }

        AccelerometerManager.setSensitivity(sensitive);
    }

    public void dismissLocationDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    public static String getThOfDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int d = calendar.get(Calendar.DATE);
        int l = d % 10;
        String t = "th";
        switch (l) {
            case 1:
                if (d != 11) {
                    t = "st";
                }
                break;
            case 2:
                if (d != 12) {
                    t = "nd";
                }
                break;
            case 3:
                if (d != 13) {
                    t = "rd";
                }
        }

        return t;
    }

    public static String[] getCurrentTime(long time) {
        Date date = time == 0 ? new Date() : new Date(time);
        String t = getThOfDate(date);
        DateFormat dateFormat = new SimpleDateFormat("LLL dd-, yyyy-hh:mm:ss:a");
        String[] dt = dateFormat.format(date).split("-");

        return new String[]{dt[2], dt[0] + t + dt[1]};
    }

    public void saveContact(Context context, JSONObject friendData, Bitmap bm) {
        Log.e("Save Contact", friendData + " " + bm);

        try {
            String[] phones = {friendData.has("mobile") ? friendData.getString("mobile") : null};
            String[] emails = {friendData.has("email") ? friendData.getString("email") : null};

            String jobTile = null;
            if (friendData.has("job_title")) {
                jobTile = friendData.getString("job_title");
            }

            String[] address = null;
            if (friendData.has("home_address")) {
                String dt = friendData.getString("home_address");
                JSONObject homeAddress = new JSONObject(dt);
                address = new String[3];
                address[0] = homeAddress.getString("address") + ", " + homeAddress.getString("street");
                address[1] = homeAddress.getString("postal_code");
                address[2] = homeAddress.getString("city");
            }

            ContactResult result = checkInContactsList(context, phones[0], emails[0], jobTile, address);

            ContentResolver contentResolver = context.getContentResolver();

            ArrayList<ContentProviderOperation> opts = new ArrayList<>();
            if (result == null) {
                // Add to contact list

                ContentProviderOperation contact = ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                        .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                        .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build();
                opts.add(contact);

                String firstName = friendData.getString("first_name");
                String lastName = friendData.has("last_name") ? friendData.getString("last_name") : null;

                ContentProviderOperation.Builder nameBuilder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, firstName);
                if (lastName != null) {
                    nameBuilder.withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, lastName);
                }

                ContentProviderOperation contactName = nameBuilder.build();
                opts.add(contactName);
            }

            if (result == null || !result.phone) {

                int[] types = {
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,
                        ContactsContract.CommonDataKinds.Phone.TYPE_HOME,
                        ContactsContract.CommonDataKinds.Phone.TYPE_WORK,
                        ContactsContract.CommonDataKinds.Phone.TYPE_OTHER
                };


                for (int i = 0; i < phones.length; i++) {
                    String phone = phones[i];
                    if (phone == null) {
                        continue;
                    }

                    ContentProviderOperation.Builder phoneBuilder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                    if (result == null) {
                        phoneBuilder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
                    } else {
                        phoneBuilder.withValue(ContactsContract.Data.RAW_CONTACT_ID, result.id);
                    }

                    int type = i < types.length ? types[i] : ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM;

                    phoneBuilder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                            .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, type)
                            .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone);

                    if (type == ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM) {
                        phoneBuilder
                                .withValue(ContactsContract.CommonDataKinds.Phone.LABEL, "Phone " + (i + 1));
                    }

                    ContentProviderOperation contactPhone = phoneBuilder.build();
                    opts.add(contactPhone);
                }
            }

            if (result == null || !result.email) {
                for (int i = 0; i < emails.length; i++) {
                    String email = emails[0];
                    if (email == null) {
                        continue;
                    }

                    ContentProviderOperation.Builder emailBuilder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                    if (result == null) {
                        emailBuilder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
                    } else {
                        emailBuilder.withValue(ContactsContract.Data.RAW_CONTACT_ID, result.id);
                    }

                    emailBuilder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                            .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_CUSTOM)
                            .withValue(ContactsContract.CommonDataKinds.Email.LABEL, "Email " + (i + 1))
                            .withValue(ContactsContract.CommonDataKinds.Email.DATA, email);
                    ContentProviderOperation contactEmail = emailBuilder.build();
                    opts.add(contactEmail);
                }
            }

            if (jobTile != null && (result == null || !result.job)) {
                ContentProviderOperation.Builder jobBuilder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                if (result == null) {
                    jobBuilder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
                } else {
                    jobBuilder.withValue(ContactsContract.Data.RAW_CONTACT_ID, result.id);
                }

                jobBuilder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.Organization.TITLE, jobTile)
                        .withValue(ContactsContract.CommonDataKinds.Organization.TYPE, ContactsContract.CommonDataKinds.Organization.TYPE_WORK);
                ContentProviderOperation contactJob = jobBuilder.build();
                opts.add(contactJob);
            }

            if (address != null && (result == null || !result.address)) {
                ContentProviderOperation.Builder addressBuilder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                if (result == null) {
                    addressBuilder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
                } else {
                    addressBuilder.withValue(ContactsContract.Data.RAW_CONTACT_ID, result.id);
                }

                addressBuilder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.StructuredPostal.STREET, address[0])
                        .withValue(ContactsContract.CommonDataKinds.StructuredPostal.CITY, address[1])
                        .withValue(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE, address[2])
                        .withValue(ContactsContract.CommonDataKinds.StructuredPostal.LABEL, "Address 1")
                        .withValue(ContactsContract.CommonDataKinds.StructuredPostal.TYPE, ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM);
                ContentProviderOperation contactAddress = addressBuilder.build();
                opts.add(contactAddress);
            }

            if (result == null && bm != null) {
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 80, buffer);

                byte[] bytes = buffer.toByteArray();

                ContentProviderOperation contactPhoto = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, bytes).build();
                opts.add(contactPhoto);
            }

            contentResolver.applyBatch(ContactsContract.AUTHORITY, opts);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class ContactResult {
        boolean phone, email, job, address;
        long id;

        public ContactResult(long id) {
            this.id = id;
        }
    }

    public static ContactResult checkInContactsList(Context context, String phone, String email, String jobTitle, String[] address) {
        ContactResult result = null;

        ContentResolver contentResolver = context.getContentResolver();
        if (phone != null) {
            String[] phones = {phone.replace(" ", "")};
            Cursor phoneCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "REPLACE(" + ContactsContract.CommonDataKinds.Phone.NUMBER + ", ' ', '') = ?", phones, null);
            if (phoneCursor.getCount() > 0) {
                if (phoneCursor.moveToFirst()) {
                    int index = phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
                    long id = phoneCursor.getLong(index);

                    result = new ContactResult(id);
                    result.phone = true;
                }
            }
        }

        if (email != null) {
            String[] args = new String[result != null ? 2 : 1];
            args[0] = email;
            String selection = ContactsContract.CommonDataKinds.Email.DATA + " = ?";

            if (result != null) {
                selection += " AND " + ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?";
                args[1] = String.valueOf(result.id);
            }

            Cursor emailCursor = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, selection, args, null);
            if (emailCursor.getCount() > 0) {
                if (emailCursor.moveToFirst()) {
                    int index = emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
                    long id = emailCursor.getLong(index);

                    if (result == null) {
                        result = new ContactResult(id);
                    }

                    result.email = true;
                }
            }
        }

        if (result != null && jobTitle != null) {
            String[] args = {
                    ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE,
                    jobTitle,
                    String.valueOf(result.id)
            };

            String selection = ContactsContract.Data.MIMETYPE + " = ? AND " +
                    ContactsContract.CommonDataKinds.Organization.TITLE + " = ? AND " +
                    ContactsContract.CommonDataKinds.Organization.CONTACT_ID + " = ?";

            Cursor jobCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, null, selection, args, null);
            if (jobCursor.getCount() > 0) {
                if (jobCursor.moveToFirst()) {
                    result.job = true;
                }
            }
        }

        if (result != null && address != null) {
            String[] args = {
                    ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE,
                    address[0],
                    address[1],
                    address[2],
                    String.valueOf(result.id)
            };

            String selection = ContactsContract.Data.MIMETYPE + " = ? AND " +
                    ContactsContract.CommonDataKinds.StructuredPostal.STREET + " = ? AND " +
                    ContactsContract.CommonDataKinds.StructuredPostal.CITY + " = ? AND " +
                    ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE + " = ? AND " +
                    ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID + " = ?";

            Cursor jobCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, null, selection, args, null);
            if (jobCursor.getCount() > 0) {
                if (jobCursor.moveToFirst()) {
                    result.address = true;
                }
            }
        }

        return result;
    }

    public static int getCameraPhotoOrientation(Context context, String imagePath) {
        int rotate = 0;
        try {
//            context.getContentResolver().notifyChange(imageUri, null);
//            File imageFile = new File(imagePath);

            ExifInterface exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Log.i("RotateImage", "Exif orientation: " + orientation);
            Log.i("RotateImage", "Rotate value: " + rotate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public static int getScreenWidth(Context context) {
        int columnWidth;
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (NoSuchMethodError ignore) {
            // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnWidth = point.x;

        return columnWidth;
    }

    /**
     * getScreenHeight
     *
     * @description get screen height of device
     */
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static int getScreenHeight(Context context) {
        int columnHeight;
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (NoSuchMethodError ignore) {
            // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnHeight = point.y;

        return columnHeight;
    }

    public static Bitmap getImageBitmapFromPath(String path) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        if (bitmap == null) {
            Log.e("QueAnh", "Can not get image from path "
                    + path);
            return null;
        }
        return bitmap;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int exifOrientation) {

        Matrix matrix = new Matrix();
        switch (exifOrientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0,
                    bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File createImageFile(String imagefilePath,
                                       byte[] imageByteArray) {
        try {
            File imageFile = new File(imagefilePath);
            if (!imageFile.exists()) {
                imageFile.createNewFile();
            } else {
                imageFile.delete();
                imageFile.createNewFile();
            }
            // write the bytes in file
            FileOutputStream fos = new FileOutputStream(imageFile, true);
            fos.write(imageByteArray);
            fos.flush();
            fos.close();
            return imageFile;
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] convertBitmapToByteArray(Bitmap bmpSource) {
        if (bmpSource == null) {
            return null;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmpSource.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        return baos.toByteArray();
    }

    public String getDeviceId(Context context) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return deviceId;
    }

    public String getNameModel() {
        String model = Build.MODEL;
        return model;
    }

    public String getOSVersion() {
        String currentOs = Build.VERSION.SDK_INT + "";
        return currentOs;
    }

    public JSONObject getProfileFriend(Friend friend) {

        JSONObject friendProfile = new JSONObject();
        JSONObject profile = new JSONObject();

        try {

            friendProfile.put("id", "friend" + friend.id);
            friendProfile.put("type", "profile");
            friendProfile.put("gmtTimestamp", String.valueOf(System.currentTimeMillis() / 1000));
            friendProfile.put("seen", "true");
            friendProfile.put("liked", "false");
            friendProfile.put("side", "friend");
            friendProfile.put("type", "bump");
            profile = friend.friendProfile.put("id", "friend" + friend.id);
            profile.put("type", "profile");

            friendProfile.put("message", profile);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return friendProfile;
    }

    public JSONObject getSelfProfile(String friendId, JSONObject selfInfo, long time) {
        JSONObject selfProfile = new JSONObject();
        JSONObject profile = new JSONObject();

        try {

            selfProfile.put("type", "profile");
            if (time == 0) {
                selfProfile.put("gmtTimestamp", String.valueOf(System.currentTimeMillis() / 1000));
            } else {
                selfProfile.put("gmtTimestamp", String.valueOf(time));
            }

            selfProfile.put("seen", "true");
            selfProfile.put("liked", "false");
            selfProfile.put("side", "me");
            selfProfile.put("type", "bump");
            selfProfile.put("id", "self" + friendId);

            profile.put("type", "profile");
            profile.put("first_name", selfInfo.getJSONObject("first_name").getString("value"));
            profile.put("last_name", selfInfo.getJSONObject("last_name").getString("value"));
            profile.put("job_title", selfInfo.getJSONObject("job_title").getString("value"));
            profile.put("avatar", selfInfo.getJSONObject("avatar").getString("value"));

            JSONObject selfEmail = selfInfo.getJSONObject("email");
            JSONObject selfMobile = selfInfo.getJSONObject("mobile");
            JSONObject selfAddress = selfInfo.has("home_address") ? selfInfo.getJSONObject("home_address") : null;
            JSONObject selfFacebook = selfInfo.has("facebook") ? selfInfo.getJSONObject("facebook") : null;
            JSONObject selfLink = selfInfo.has("link") ? selfInfo.getJSONObject("link") : null;

            boolean hideEmail = Boolean.parseBoolean(selfEmail.getString("hidden"));
            boolean hideMobile = Boolean.parseBoolean(selfMobile.getString("hidden"));
            boolean hideAddress = selfAddress == null ? true : Boolean.parseBoolean(selfAddress.getString("hidden"));
            boolean hideFacebook = selfFacebook == null ? true : Boolean.parseBoolean(selfFacebook.getString("hidden"));
            boolean hideLink = selfLink == null ? true : Boolean.parseBoolean(selfLink.getString("hidden"));

            if (!hideEmail) {
                profile.put("email", selfEmail.getString("value"));
            }

            if (!hideMobile) {
                profile.put("mobile", selfMobile.getString("value"));
            }

            if (!hideAddress) {
                profile.put("home_address", selfAddress.getString("value"));
            }

            if (!hideFacebook) {
                profile.put("facebook", selfFacebook.getString("value"));
            }

            if (!hideLink) {
                profile.put("link", selfLink.getString("value"));
            }

            selfProfile.put("message", profile);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return selfProfile;
    }

    public void updateLikedChatItem(ChatBoxItem item) {
        String liked = item.isLiked ? "1" : "0";
        String data = item.stringData;
        RushOrmManager.getInstance().deleteItem(item);
        try {
            JSONObject object = new JSONObject(data);
            object.put("liked", liked);
            item.setFriendId(item.friendId);
            item.setJsonData(object);

            RushOrmManager.getInstance().storeSingleChatItem(item);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
