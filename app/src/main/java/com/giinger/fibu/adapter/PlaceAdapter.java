package com.giinger.fibu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.giinger.fibu.R;
import com.giinger.fibu.element.CustomTextView;
import com.giinger.fibu.model.PlaceModel;

import java.util.ArrayList;

/**
 * Created by geekup on 4/24/15.
 */
public class PlaceAdapter extends BaseAdapter {

    private ArrayList<PlaceModel> data;
    private Context context;

    public PlaceAdapter(ArrayList<PlaceModel> data,Context context){
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null) {
            vh = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.place_item,null);

            vh.iconPlace = (ImageView) convertView.findViewById(R.id.place_icon);
            vh.namePlace = (CustomTextView) convertView.findViewById(R.id.name);
            vh.addressPlace = (CustomTextView) convertView.findViewById(R.id.address);

            convertView.setTag(vh);

        } else {
             vh = (ViewHolder)convertView.getTag();
        }

        if(position == 0) {
            vh.namePlace.setText("Use " + "\"" + data.get(0).getName() + "\"");
            vh.addressPlace.setVisibility(View.GONE);
        } else {
            vh.namePlace.setText(data.get(position).getName());
            vh.addressPlace.setVisibility(View.VISIBLE);
        }

        vh.addressPlace.setText(data.get(position).ggetAddress());
        new AQuery(context).id(vh.iconPlace).image(data.get(position).getImg(), true, true, 0, R.drawable.place_icon);

        return convertView;
    }

    static class ViewHolder{
        ImageView iconPlace;
        CustomTextView namePlace;
        CustomTextView addressPlace;
    }
}
