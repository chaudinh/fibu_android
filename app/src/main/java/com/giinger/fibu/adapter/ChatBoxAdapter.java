package com.giinger.fibu.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.giinger.fibu.R;
import com.giinger.fibu.activity.ChatBoxScreen;
import com.giinger.fibu.activity.PreviewPhoto;
import com.giinger.fibu.activity.PreviewVideo;
import com.giinger.fibu.activity.WebScreen;
import com.giinger.fibu.element.CustomTextView;
import com.giinger.fibu.model.ChatBoxItem;
import com.giinger.fibu.service.UpDownLoadService;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.EmojiMapUtil;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.Utils;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatBoxAdapter extends BaseAdapter {

    private List<ChatBoxItem> data;
    private ChatBoxScreen activity;

    public ChatBoxAdapter(List<ChatBoxItem> data, ChatBoxScreen activity) {
        this.data = data;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ChatBoxItem item = data.get(position);

        int resourceId;

        JSONObject data = item.getData();
        switch (item.type) {
            case 5:
                resourceId = item.isSender ? R.layout.chatbox_item_right_card : R.layout.chatbox_item_left_card;
                convertView = View.inflate(activity, resourceId, null);

                ChatBoxCardHolder cardHolder = new ChatBoxCardHolder(convertView);
                cardHolder.setInfo(item);
                cardHolder.fillData(data, this.activity);
//                new ContactItem(item.data);
                break;
            case 6:
                resourceId = item.isSender ? R.layout.chatbox_item_right_music : R.layout.chatbox_item_left_music;
                convertView = View.inflate(activity, resourceId, null);

                try {
                    ChatBoxImageHolder dropboxHolder = new ChatBoxImageHolder(convertView);
                    dropboxHolder.setInfo(item);

                    if (item.isFailed()) {
                        dropboxHolder.resendView(item);
                    } else if (item.isLoading()) {
                        activity.addToLoadingList(item);
                        dropboxHolder.loadingView(item.progress);
                    } else if (data == null) {
                        dropboxHolder.resendView(item);
                    } else {
                        String message = data.getString("message");
                        JSONObject obj = new JSONObject(message);
                        final String link = obj.getString("link");
                        dropboxHolder.fillData(link, R.drawable.dropbox_chat_icon);
                        dropboxHolder.layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = Uri.parse(link);
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                activity.showPreview(intent);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case 7:
                resourceId = item.isSender ? R.layout.chatbox_item_right_music : R.layout.chatbox_item_left_music;
                convertView = View.inflate(activity, resourceId, null);

                ChatBoxImageHolder documentHolder = new ChatBoxImageHolder(convertView);
                documentHolder.setInfo(item);

                try {
                    if (item.isFailed()) {
                        documentHolder.resendView(item);
                    } else if (item.isLoading()) {
                        this.activity.addToLoadingList(item);
                        documentHolder.loadingView(item.progress);
                    } else if (data == null) {
                        documentHolder.resendView(item);
                    } else {
                        String p;
                        String name;
                        if (data.has("path")) {
                            p = data.getString("path");
                            File f = new File(p);
                            name = f.getName();
                        } else {
                            name = data.getString("name");
                            File f = BumpUtils.getFile(name, item.isSender);
                            p = f.getAbsolutePath();
                        }

                        Log.e("File", p + " " + name);
                        final String path = p;

                        File file = new File(p);
                        if (!file.exists()) {
                            if (item.hasServerFile()) {
                                this.activity.addToLoadingList(item);
                                documentHolder.loadingView(item.progress);
                            } else {
                                documentHolder.setAvailableAsset(false);
                            }
                        } else {
                            documentHolder.fillData(name, R.drawable.folder_chat);
                            documentHolder.layout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showFile(path);
                                }
                            });
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case 1:
                resourceId = item.isSender ? R.layout.chatbox_item_right_message : R.layout.chatbox_item_left_message;
                convertView = View.inflate(activity, resourceId, null);

                try {
                    ChatBoxTextHolder messageHolder = new ChatBoxTextHolder(convertView);
                    String message = data.getString("message");
                    message = EmojiMapUtil.replaceCheatSheetEmojis(message);
                    messageHolder.setInfo(item);
                    messageHolder.fillData(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case 4:
                resourceId = item.isSender ? R.layout.chatbox_item_right_music : R.layout.chatbox_item_left_music;
                convertView = View.inflate(activity, resourceId, null);

                ChatBoxImageHolder musicHolder = new ChatBoxImageHolder(convertView);
                musicHolder.setInfo(item);

                try {
                    if (item.isFailed()) {
                        musicHolder.resendView(item);
                    } else if (item.isLoading()) {
                        activity.addToLoadingList(item);
                        musicHolder.loadingView(item.progress);
                    } else if (data == null) {
                        musicHolder.resendView(item);
                    } else {
                        String p;
                        String name;
                        if (data.has("path")) {
                            p = data.getString("path");
                            File f = new File(p);
                            name = f.getName();
                        } else {
                            name = data.getString("name");
                            File f = BumpUtils.getFile(name, item.isSender);
                            p = f.getAbsolutePath();
                        }

                        final String path = p;
                        Log.e("File", name + " " + item.isLoading);

                        File file = new File(p);
                        if (!file.exists()) {
                            if (item.hasServerFile()) {
                                this.activity.addToLoadingList(item);
                                musicHolder.loadingView(item.progress);
                            } else {
                                musicHolder.setAvailableAsset(false);
                            }
                        } else {
                            musicHolder.fillData(name, R.drawable.send_music_icon);
                            musicHolder.layout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showFile(path);
                                }
                            });
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case 8:
                resourceId = R.layout.chatbox_item_separate;
                convertView = View.inflate(activity, resourceId, null);

                // for separate
                break;
            case 2:
                resourceId = item.isSender ? R.layout.chatbox_item_right_photo : R.layout.chatbox_item_left_photo;
                convertView = View.inflate(activity, resourceId, null);

                ChatBoxPhotoHolder photoHolder = new ChatBoxPhotoHolder(convertView);
                photoHolder.setInfo(item);

                if (item.isFailed()) {
                    photoHolder.resendView(item);
                } else if (item.isLoading()) {
                    activity.addToLoadingList(item);
                    photoHolder.loadingView(item.progress);
                } else if (data == null) {
                    photoHolder.resendView(item);
                } else {
                    photoHolder.fillData(item);
                }
                break;
            case 3:
                resourceId = item.isSender ? R.layout.chatbox_item_right_video : R.layout.chatbox_item_left_video;
                convertView = View.inflate(activity, resourceId, null);

                ChatBoxVideoHolder videoHolder = new ChatBoxVideoHolder(convertView);
                videoHolder.setInfo(item);

                if (item.isFailed()) {
                    videoHolder.resendView(item);
                } else if (item.isLoading()) {
                    activity.addToLoadingList(item);
                    videoHolder.loadingView(item.progress);
                } else if (data == null) {
                    videoHolder.resendView(item);
                } else {
                    videoHolder.fillData(item);
                }
                break;
            case 9:
//                Log.e("QueANh: ", item.data.toString());

                resourceId = item.isSender ? R.layout.chatbox_item_right_photo : R.layout.chatbox_item_left_photo;
                convertView = View.inflate(activity, resourceId, null);

                ChatBoxLocationHolder locationHolder = new ChatBoxLocationHolder(convertView);
                locationHolder.setInfo(item);

                locationHolder.tvNameLocation.setVisibility(View.VISIBLE);
                locationHolder.fillData(data);
                break;
        }

        return convertView;
    }

    private String getExtension(String name) {
        int index = name.lastIndexOf('.');
        String ext = name.substring(index + 1).toLowerCase();
        return ext;
    }

    private void showFile(String path) {
        String extension = getExtension(path);
        String mimeType = MimeTypeMap.getSingleton()
                .getMimeTypeFromExtension(extension);
        Intent mediaIntent = new Intent(Intent.ACTION_VIEW);
        File file = new File(path);
        mediaIntent.setDataAndType(Uri.fromFile(file),
                mimeType);
        Intent chooser = Intent.createChooser(mediaIntent, "Open");
        activity.showPreview(chooser);
    }

    public static void loading(final TextView txtStatus, final int state) {
        txtStatus.postDelayed(new Runnable() {
            @Override
            public void run() {
                Object tag = txtStatus.getTag(R.string.is_loading);
                if (tag instanceof Boolean && (Boolean) tag) {
                    int resourceId = (int) txtStatus.getTag(R.string.resource_id);

                    Context context = txtStatus.getContext();
                    String text = context.getString(resourceId);
                    String[] dots = context.getResources().getStringArray(R.array.loading);
                    txtStatus.setText(text + dots[state % 4]);

                    loading(txtStatus, state + 1);
                }
            }
        }, 250);

    }

    public class ChatBoxHolder {
        public TextView txtTime, txtStatus;
        public ImageView imgLike, imgSeen;
        public View layout, loadingLayout, contentLayout, rlLike, imgFailed;
        public ProgressBar progressBar;
        public View availableAsset, unavailableAsset;

        public ChatBoxHolder(View view) {
            layout = view.findViewById(R.id.layout);

            loadingLayout = view.findViewById(R.id.loadingLayout);
            contentLayout = view.findViewById(R.id.contentLayout);
            imgFailed = view.findViewById(R.id.imgFailed);

            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            txtStatus = (TextView) view.findViewById(R.id.txtStatus);

            txtTime = (TextView) view.findViewById(R.id.txtTime);
            imgLike = (ImageView) view.findViewById(R.id.imgLike);
            imgSeen = (ImageView) view.findViewById(R.id.imgSeen);

            rlLike = view.findViewById(R.id.rl_like);

            availableAsset = view.findViewById(R.id.availableAsset);
            unavailableAsset = view.findViewById(R.id.unavailableAsset);
        }

        public void setAvailableAsset(boolean available) {
            if (availableAsset != null && unavailableAsset != null) {
                availableAsset.setVisibility(available ? View.VISIBLE : View.GONE);
                unavailableAsset.setVisibility(available ? View.GONE : View.VISIBLE);
            }
        }

        public void resendView(final ChatBoxItem item) {
            if (imgFailed != null && loadingLayout != null && contentLayout != null && rlLike != null) {
                contentLayout.setVisibility(View.INVISIBLE);
                loadingLayout.setVisibility(View.VISIBLE);

                rlLike.setVisibility(View.GONE);

                txtStatus.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);

                txtStatus.setText(item.isSender ? R.string.send_failed_message : R.string.receive_failed_message);
                imgFailed.setVisibility(View.VISIBLE);

                if (item.isSender) {
                    layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            view.setOnClickListener(null);
                            item.progress = ChatBoxItem.PROGRESS_INIT;
                            item.isLoading = true;
                            item.save();

                            if (item.messageForResend != null) {
                                activity.addToLoadingList(item);

                                Intent intent = new Intent(activity, UpDownLoadService.class);
                                intent.putExtra(UpDownLoadService.TYPE, UpDownLoadService.TYPE_RESEND);
                                intent.putExtra(ChatBoxScreen.EXTRA_LOADING_ID, item.id);

                                String userId = PreferenceUtils.getString(Constants.USER_ID, activity, "");
                                intent.putExtra(UpDownLoadService.EXTRA_UPLOAD_USER_ID, userId);

                                intent.putExtra(UpDownLoadService.EXTRA_FRIEND_ID, item.friendId);
                                intent.putExtra(UpDownLoadService.EXTRA_RESEND_FILE, item.fileToResend);
                                intent.putExtra(UpDownLoadService.EXTRA_RESEND_MESSAGE, item.messageForResend);
                                activity.startService(intent);
                            }
                        }
                    });
                }
            }
        }

        public void loadingView(final int progress) {
            if (loadingLayout != null && contentLayout != null) {
                loadingLayout.setVisibility(View.VISIBLE);
                contentLayout.setVisibility(View.INVISIBLE);

                switch (progress) {
                    case ChatBoxItem.PROGRESS_WAITING:
                        progressBar.setVisibility(View.GONE);
                        txtStatus.setVisibility(View.VISIBLE);
                        txtStatus.setText(R.string.waiting_sp);

                        txtStatus.setTag(R.string.is_loading, true);
                        txtStatus.setTag(R.string.resource_id, R.string.waiting);
                        loading(txtStatus, 0);
                        break;
                    case ChatBoxItem.PROGRESS_COMPRESSING:
                        txtStatus.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        txtStatus.setText(R.string.compressing_sp);

                        txtStatus.setTag(R.string.is_loading, true);
                        txtStatus.setTag(R.string.resource_id, R.string.compressing);
                        loading(txtStatus, 0);
                        break;
                    case ChatBoxItem.PROGRESS_GETTING_LINK:
                        txtStatus.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        txtStatus.setText(R.string.getting_link_sp);

                        txtStatus.setTag(R.string.is_loading, true);
                        txtStatus.setTag(R.string.resource_id, R.string.getting_link);
                        loading(txtStatus, 0);
                        break;
                    default:
                        progressBar.setVisibility(View.VISIBLE);
                        txtStatus.setVisibility(View.GONE);
                        progressBar.setProgress(progress);
                        txtStatus.setTag(R.string.is_loading, false);
                        break;
                }
            }

        }

        public void setInfo(final ChatBoxItem item) {
            imgSeen.setVisibility(item.isSeen && item.isSender ? View.VISIBLE : View.INVISIBLE);

            if (item.isLoading || item.isSender && !item.isLiked) {
                rlLike.setVisibility(View.INVISIBLE);
            } else {
                if (item.isSender) {
                    rlLike.setVisibility(View.VISIBLE);
                    imgLike.setImageResource(R.drawable.like_msg_icon);
                } else {
                    imgLike.setImageResource(item.isLiked ? R.drawable.like_msg_icon : R.drawable.not_like_msg_icon);
                }
            }

            String time;
            if (item.timeStamp > 0) {
                time = ChatBoxItem.formatTime(item.timeStamp);
            } else {
                time = item.time;
            }

            txtTime.setText(time);

            if (item.isSender) {
                return;
            }

            rlLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!item.isSender) {
                        AQuery aQuery = new AQuery(activity);

                        item.isLiked = !item.isLiked;
                        imgLike.setImageResource(item.isLiked ? R.drawable.like_msg_icon : R.drawable.not_like_msg_icon);

                        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
                            @Override
                            public void callback(String url, JSONObject object, AjaxStatus status) {
                                if (object != null) {
                                    try {
                                        JSONObject response = object.getJSONObject(Constants.RESPONSE);
                                        boolean success = response.getBoolean(Constants.SUCCESS);
                                        Utils.getInstance().updateLikedChatItem(item);
                                        if (success) {
                                            return;
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                item.isLiked = !item.isLiked;
                                imgLike.setImageResource(item.isLiked ? R.drawable.like_msg_icon : R.drawable.not_like_msg_icon);
                                String text = "Can't " + (item.isLiked ? "" : "dis") + "like this message";
                                Toast.makeText(activity, text, Toast.LENGTH_LONG).show();
                            }
                        };

                        Map<String, String> params = new HashMap<>();
                        params.put("id", item.id);
                        params.put("liked", item.isLiked ? "1" : "0");

                        String url = AppUtils.getChatLikeUrl(activity);
                        aQuery.ajax(url, params, JSONObject.class, callback);
                    }
                }
            });
        }
    }

    public class ChatBoxTextHolder extends ChatBoxHolder {

        public TextView text;

        public ChatBoxTextHolder(View view) {
            super(view);

            text = (TextView) view.findViewById(R.id.text);
        }

        public void fillData(String data) {
            text.setText(data);
        }
    }

    public class ChatBoxImageHolder extends ChatBoxTextHolder {

        public View layoutMusic;
        public ImageView imgIcon;

        public ChatBoxImageHolder(View view) {
            super(view);

            imgIcon = (ImageView) view.findViewById(R.id.imgIcon);
            layoutMusic = view.findViewById(R.id.contentLayout);
        }

        public void fillData(String data, int icon) {
            text.setText(data);
            imgIcon.setImageResource(icon);
        }
    }

    public class ChatBoxCardHolder extends ChatBoxHolder {

        public ImageView imgAvatar;
        public TextView txtFirstName, txtLastName, txtJobTitle;
        public View layoutPhone, layoutEmail, layoutWebsite, layoutAddress, layoutFacebook, layoutAddFriendFB, layoutOther;
        public TextView txtPhone, txtEmail, txtWebsite, txtAddress, txtFacebookName;

        public ChatBoxCardHolder(View view) {
            super(view);

            imgAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
            txtFirstName = (TextView) view.findViewById(R.id.txtFirstName);
            txtLastName = (TextView) view.findViewById(R.id.txtLastName);
            txtJobTitle = (TextView) view.findViewById(R.id.txtJobTitle);

            layoutPhone = view.findViewById(R.id.layoutPhone);
            txtPhone = (TextView) view.findViewById(R.id.txtPhone);

            layoutEmail = view.findViewById(R.id.layoutEmail);
            txtEmail = (TextView) view.findViewById(R.id.txtEmail);

            layoutWebsite = view.findViewById(R.id.layoutLink);
            txtWebsite = (TextView) view.findViewById(R.id.txtLink);

            layoutAddress = view.findViewById(R.id.layoutAddress);
            txtAddress = (TextView) view.findViewById(R.id.txtAddress);

            layoutFacebook = view.findViewById(R.id.layoutFacebook);
            txtFacebookName = (TextView) view.findViewById(R.id.txtFacebookName);
            layoutAddFriendFB = view.findViewById(R.id.layoutAddFriendFB);

            layoutOther = view.findViewById(R.id.layoutOther);
        }

        public void fillData(final JSONObject data, final ChatBoxScreen activity) {
            try {
                boolean hasData = false;

                if (data.has("name")) {
                    imgAvatar.setVisibility(View.GONE);
                    txtJobTitle.setVisibility(View.GONE);

                    final String firstName = data.getString("name");
                    txtFirstName.setText(firstName);
                    txtLastName.setVisibility(View.GONE);

                    JSONArray phones = data.getJSONArray("phones");
                    if (phones.length() > 0) {
                        String phone = phones.getString(0);
                        if (!TextUtils.isEmpty(phone) && !phone.equals("null")) {
                            txtPhone.setText(phone);
                            layoutPhone.setVisibility(View.VISIBLE);
                            hasData = true;
                        } else {
                            layoutPhone.setVisibility(View.GONE);
                        }
                    } else {
                        layoutPhone.setVisibility(View.GONE);
                    }

                    final JSONArray emails = data.getJSONArray("emails");
                    if (emails.length() > 0) {
                        String email = emails.getString(0);
                        if (!TextUtils.isEmpty(email) && !email.equals("null")) {
                            txtEmail.setText(email);
                            layoutEmail.setVisibility(View.VISIBLE);
                            hasData = true;
                        } else {
                            layoutEmail.setVisibility(View.GONE);
                        }
                    } else {
                        layoutEmail.setVisibility(View.GONE);
                    }

                    layoutWebsite.setVisibility(View.GONE);
                    layoutAddress.setVisibility(View.GONE);

                    layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                JSONObject friend = new JSONObject();
                                friend.put("first_name", firstName);

                                if (data.has("phones")) {
                                    final JSONArray phones = data.getJSONArray("phones");
                                    if (phones.length() > 0) {
                                        String phone = phones.getString(0);
                                        if (!TextUtils.isEmpty(phone) && !phone.equals("null")) {
                                            friend.put("mobile", phone);
                                        }
                                    }
                                }

                                if (data.has("emails")) {
                                    final JSONArray emails = data.getJSONArray("emails");
                                    if (emails.length() > 0) {
                                        String email = emails.getString(0);
                                        if (!TextUtils.isEmpty(email) && !email.equals("null")) {
                                            friend.put("email", email);
                                        }
                                    }
                                }

                                activity.showProfileFriend(friend);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    if (data.has("avatar")) {
                        String avatar = data.getString("avatar");
                        String avatarUrl = AppUtils.getAvatarByName(activity, avatar);
                        AppUtils.loadAvatar(activity, imgAvatar, avatarUrl, false);
                        imgAvatar.setVisibility(View.VISIBLE);
                    } else {
                        imgAvatar.setVisibility(View.GONE);
                    }

                    String firstName = data.getString("first_name");
                    txtFirstName.setText(firstName);

                    if (data.has("last_name")) {
                        String lastName = data.getString("last_name");
                        txtLastName.setText(lastName);
                        txtLastName.setVisibility(View.VISIBLE);
                    } else {
                        txtLastName.setVisibility(View.GONE);
                    }

                    if (data.has("job_title")) {
                        String jobTitle = data.getString("job_title");
                        txtJobTitle.setText(jobTitle);
                        txtJobTitle.setVisibility(View.VISIBLE);
                    } else {
                        txtJobTitle.setVisibility(View.GONE);
                    }

                    if (data.has("mobile")) {
                        String phone = data.getString("mobile");
                        if (!TextUtils.isEmpty(phone) && !phone.equals("null")) {
                            txtPhone.setText(phone);
                            layoutPhone.setVisibility(View.VISIBLE);
                            hasData = true;
                        } else {
                            layoutPhone.setVisibility(View.GONE);
                        }
                    } else {
                        layoutPhone.setVisibility(View.GONE);
                    }

                    if (data.has("email")) {
                        String email = data.getString("email");
                        if (!TextUtils.isEmpty(email) && !email.equals("null")) {
                            txtEmail.setText(email);
                            layoutEmail.setVisibility(View.VISIBLE);
                            hasData = true;
                        } else {
                            layoutEmail.setVisibility(View.GONE);
                        }
                    } else {
                        layoutEmail.setVisibility(View.GONE);
                    }

                    if (data.has("link")) {
                        String website = data.getString("link");
                        txtWebsite.setText(website);
                        layoutWebsite.setVisibility(View.VISIBLE);
                        hasData = true;
                    } else {
                        layoutWebsite.setVisibility(View.GONE);
                    }

                    if (data.has("facebook") && AccessToken.getCurrentAccessToken() != null) {
                        final String facebookId = data.getString("facebook");
//                        Ion.with(activity).load("https://graph.facebook.com/?ids=" + facebookId
//                                + "&access_token=1524270291224917|c38c8ef6632f8452a1b0de330985d028")
//                                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
//                            @Override
//                            public void onCompleted(Exception e, JsonObject result) {
//                                if (e != null || result == null) {
//                                    return;
//                                }
//                                if (result.has(facebookId)) {
//                                    JsonObject response = result.getAsJsonObject(facebookId);
//                                    txtFacebookName.setText(response.getAsJsonPrimitive("name").getAsString());
//                                }
//                            }
//                        });
                        new GraphRequest(
                                AccessToken.getCurrentAccessToken(),
                                "/" + facebookId,
                                null,
                                HttpMethod.GET,
                                new GraphRequest.Callback() {
                                    public void onCompleted(GraphResponse response) {
                                        if (response.getJSONObject() != null) {
                                            try {
                                                txtFacebookName.setText(response.getJSONObject().getString("name"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                        ).executeAsync();
                        layoutAddFriendFB.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent webIntent = new Intent(activity, WebScreen.class);
                                webIntent.putExtra(WebScreen.EXTRA_URL, Constants.FACEBOOK_LINK + facebookId);
                                activity.startActivity(webIntent);
                            }
                        });
                        if (!PreferenceUtils.getString(Constants.FB_ID, activity, "").equals(facebookId)) {
                            layoutFacebook.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent webIntent = new Intent(activity, WebScreen.class);
                                    webIntent.putExtra(WebScreen.EXTRA_URL, Constants.FACEBOOK_LINK + facebookId);
                                    activity.startActivity(webIntent);
                                }
                            });
                        }
                        layoutFacebook.setVisibility(View.VISIBLE);
                        hasData = true;
                    } else {
                        layoutFacebook.setVisibility(View.GONE);
                    }

                    if (data.has("home_address")) {
                        String json = data.getString("home_address");
                        JSONObject obj = new JSONObject(json);
                        String address = obj.optString("address") + " " + obj.optString("postal_code") + " " + obj.optString("city") + " " + obj.optString("street");
                        txtAddress.setText(address);
                        layoutAddress.setVisibility(View.VISIBLE);
                        hasData = true;
                    } else {
                        layoutAddress.setVisibility(View.GONE);
                    }

                    layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            activity.showProfileFriend(data);
                        }
                    });
                }

                layoutOther.setVisibility(hasData || true ? View.GONE : View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class ChatBoxVideoHolder extends ChatBoxHolder {
        public ImageView imgVideo, imgPlayVideo;

        public ChatBoxVideoHolder(View view) {
            super(view);

            imgVideo = (ImageView) view.findViewById(R.id.imgVideo);
            imgPlayVideo = (ImageView) view.findViewById(R.id.imgPlayVideo);
        }

        public void fillData(ChatBoxItem item) {
            try {
                String p;
                if (item.getData().has("path")) {
                    p = item.getData().getString("path");
                } else {
                    String fileName = item.getData().getString("name");
                    File f = BumpUtils.getFile(fileName, item.isSender);
                    p = f.getAbsolutePath();
                }

                final File file = new File(p);
                if (!file.exists()) {
                    if (item.hasServerFile()) {
                        activity.addToLoadingList(item);
                        loadingView(item.progress);
                    } else {
                        setAvailableAsset(false);
                    }
                    return;
                }

                final String path = p;

                new AsyncTask<Void, Void, Bitmap>() {
                    @Override
                    protected Bitmap doInBackground(Void... params) {
                        Bitmap bitmap = videoCached.get(path);
                        if (bitmap == null) {
                            bitmap = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND);
                            videoCached.put(path, bitmap);
                        }

                        return bitmap;
                    }

                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        imgVideo.setImageBitmap(bitmap);
                    }
                }.execute();

                imgPlayVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int index = path.lastIndexOf('.');
                        String ext = "";
                        if (index != -1) {
                            ext = path.substring(index + 1);
                            ext = ext.trim().toUpperCase();
                        }

                        if ("mp4".equals(ext.toLowerCase())) {

                            Intent intent = new Intent(activity, PreviewVideo.class);
                            intent.putExtra(PreviewVideo.EXTRA, path);
                            activity.showPreview(intent);
                        } else {
                            String msg = " video is not supported";
                            if (ext.isEmpty()) {
                                msg = "This" + msg;
                            } else {
                                msg = ext + msg;
                            }

                            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, Bitmap> videoCached = new HashMap<>();

    public class ChatBoxLocationHolder extends ChatBoxPhotoHolder {

        public ChatBoxLocationHolder(View view) {
            super(view);
        }

        public void fillData(JSONObject data) {

            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    items[i][j].layout.setVisibility(View.GONE);
                }
            }

            items[0][0].layout.setVisibility(View.VISIBLE);

            try {
                Log.e("QueAnh", data.toString());
                JSONObject message = new JSONObject(data.getString("message"));
                JSONObject locationOb = message.getJSONObject("item");
                final String location = locationOb.getString("latitude") + "," + locationOb.getString("longitude");
                tvNameLocation.setText(locationOb.getString("name"));

                String staticMap = AppUtils.staticMapUrl(location);
                new AQuery(activity).id(items[0][0].image).image(staticMap, true, true, 0, R.drawable.pic_icon_bg);

                items[0][0].layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri uri = Uri.parse("geo:" + location + "?z=17&q=" + location);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        activity.showPreview(intent);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class ChatBoxPhotoHolder extends ChatBoxHolder {

        public void fillData(final ChatBoxItem item) {

            try {
                JSONArray array = item.getData().getJSONArray("items");
                int size = array.length();

                final ArrayList<String> paths = new ArrayList<>();
                final ArrayList<Integer> dirs = new ArrayList<>();
                for (int i = 0; i < size; i++) {
                    JSONObject photo = array.getJSONObject(i);

                    String path;
                    if (photo.has("path")) {
                        path = photo.getString("path");
                    } else {
                        String fileName = photo.getString("name");
                        File f = BumpUtils.getFile(fileName, item.isSender);
                        path = f.getAbsolutePath();
                    }

                    File file = new File(path);
                    if (!file.exists()) {
                        if (item.hasServerFile()) {
                            activity.addToLoadingList(item);
                            loadingView(item.progress);
                        } else {
                            setAvailableAsset(false);
                        }
                        return;
                    }

                    Integer direction = item.direction.get(i);
                    if (direction == null) {
                        direction = Utils.getCameraPhotoOrientation(activity, path);
                        item.direction.put(i, direction);
                    }

                    dirs.add(direction);
                    paths.add(path);
                }

                layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, PreviewPhoto.class);
                        intent.putStringArrayListExtra(PreviewPhoto.EXTRA_PATHS, paths);
                        intent.putIntegerArrayListExtra(PreviewPhoto.EXTRA_DIRS, dirs);
                        activity.showPreview(intent);
                    }
                });

                int bound = size == 1 ? 1 : (size < 5 ? 2 : 3);

                int index = 0;
                for (int i = 0; i < 3; i++) {
                    int visible = View.INVISIBLE;
                    for (int j = 0; j < 3; j++) {
                        if (i < bound && j < bound) {
                            if (index < size) {
                                final String path = paths.get(index);
                                final int finalIndex = index;

                                Bitmap bitmap = videoCached.get(path);
                                if (bitmap == null) {
                                    new AQuery(activity).id(items[i][j].image).image(path, true, true, 300 / bound, 0, new BitmapAjaxCallback() {
                                        @Override
                                        protected void callback(String url, final ImageView iv, final Bitmap bm, AjaxStatus status) {
                                            new AsyncTask<Void, Void, Bitmap>() {
                                                @Override
                                                protected Bitmap doInBackground(Void... params) {
                                                    Integer direction = item.direction.get(finalIndex);
                                                    if (direction != 0) {
                                                        Matrix matrix = new Matrix();
                                                        matrix.postRotate(direction);
                                                        int w = bm.getWidth(), h = bm.getHeight();
                                                        Bitmap thumb = Bitmap.createBitmap(bm, 0, 0, w, h, matrix, true);
                                                        videoCached.put(path, thumb);
                                                        return thumb;
                                                    }

                                                    return bm;
                                                }

                                                @Override
                                                protected void onPostExecute(Bitmap bitmap) {
                                                    iv.setImageBitmap(bitmap);
                                                }
                                            }.execute();
                                        }
                                    });
                                } else {
                                    items[i][j].image.setImageBitmap(bitmap);
                                }

                                index++;
                            } else {
                                if (j == 0) {
                                    visible = View.GONE;
                                }

                                items[i][j].layout.setVisibility(visible);
                            }
                        } else {
                            items[i][j].layout.setVisibility(View.GONE);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public class Item {
            public View layout;
            public ImageView image;
        }

        public Item[][] items;
        public CustomTextView tvNameLocation;

        public ChatBoxPhotoHolder(View view) {
            super(view);
            tvNameLocation = (CustomTextView) view.findViewById(R.id.tv_name_location);

            items = new Item[3][];

            LinearLayout layoutPhoto = (LinearLayout) view.findViewById(R.id.contentLayout);

            for (int i = 0; i < 3; i++) {
                items[i] = new Item[3];
                LinearLayout row = (LinearLayout) layoutPhoto.getChildAt(i);
                for (int j = 0; j < 3; j++) {
                    RelativeLayout cell = (RelativeLayout) row.getChildAt(j);
                    Item item = new Item();
                    item.layout = cell;
                    item.image = (ImageView) cell.getChildAt(0);
                    items[i][j] = item;
                }
            }
        }
    }

}
