package com.giinger.fibu.adapter;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.giinger.fibu.R;
import com.giinger.fibu.model.ContactItem;
import com.giinger.fibu.util.Constants;

import java.io.IOException;
import java.util.List;

public class ContactAdapter extends BaseAdapter {

    private List<ContactItem> data;

    public ContactAdapter(List<ContactItem> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static final class ViewHolder {

        public ImageView imgIcon, imgSelected;
        public TextView txtName;

        public ViewHolder(View view) {
            imgIcon = (ImageView) view.findViewById(R.id.imgIcon);
            imgSelected = (ImageView) view.findViewById(R.id.imgSelected);
            txtName = (TextView) view.findViewById(R.id.txtName);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.contact_item, null);

            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        final ContactItem item = data.get(position);

        final ViewHolder viewHolder = (ViewHolder) convertView.getTag();
//        viewHolder.imgIcon.setImageResource(null);
        viewHolder.txtName.setText(item.name);

        if (Constants.isChat) {
            viewHolder.imgSelected.setVisibility(View.GONE);
        } else {
            viewHolder.imgSelected.setVisibility(View.VISIBLE);

            viewHolder.imgSelected.setVisibility(View.VISIBLE);
            int resourceId = item.isSelected ? R.drawable.contact_select : R.drawable.contact_not;
            viewHolder.imgSelected.setImageResource(resourceId);
        }


        if (item.id != null && !item.id.isEmpty()) {
            Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(item.id));
            uri = Uri.withAppendedPath(uri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

            ContentResolver resolver = context.getContentResolver();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(resolver, uri);
                if (bitmap != null) {
                    viewHolder.imgIcon.setImageBitmap(bitmap);
                } else {
                    viewHolder.imgIcon.setImageResource(R.drawable.contact_thumb);
                }
            } catch (IOException e) {
                viewHolder.imgIcon.setImageResource(R.drawable.contact_thumb);
            }
        } else {
            viewHolder.imgIcon.setImageResource(R.drawable.contact_thumb);
        }

        return convertView;
    }

}
