package com.giinger.fibu.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.giinger.fibu.R;
import com.giinger.fibu.model.FileItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FileVideoAdapter extends BaseAdapter {

    private List<FileItem> data;

    public FileVideoAdapter(List<FileItem> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static final class ViewHolder {

        public int position;

        @Bind(R.id.txtDate)
        public TextView txtDate;

        @Bind(R.id.txtTime)
        public TextView txtTime;

        @Bind(R.id.imgSelected)
        public ImageView imgSelected;
        @Bind(R.id.imgThumb)
        public ImageView imgThumb;

        @Bind(R.id.progressBar)
        public ProgressBar progressBar;

        public ViewHolder(View view, int position) {
            this.position = position;

            ButterKnife.bind(this, view);
        }

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            Context context = parent.getContext();
            convertView = View.inflate(context, R.layout.file_video_item, null);
            ViewHolder viewHolder = new ViewHolder(convertView, position);
            convertView.setTag(viewHolder);
        }

        final FileItem item = data.get(position);

        final ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.position = position;

        viewHolder.txtDate.setText(item.date);
        viewHolder.txtTime.setText(item.time);

        int resourceId = item.isSelected ? R.drawable.video_selected : R.drawable.video_not;
        viewHolder.imgSelected.setImageResource(resourceId);
        viewHolder.imgThumb.setImageResource(R.drawable.video_thumb);

        if (item.progress > 0) {
            viewHolder.progressBar.setVisibility(View.VISIBLE);
            viewHolder.progressBar.setProgress(item.progress);
        } else {
            viewHolder.progressBar.setVisibility(View.GONE);
        }

        Bitmap bitmap = cached.get(item.path);
        if (bitmap != null) {
            viewHolder.imgThumb.setImageBitmap(bitmap);
        } else {
            new AsyncTask<Void, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Void... params) {
                    Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(item.path, MediaStore.Images.Thumbnails.MINI_KIND);
                    if (bitmap != null) {
                        cached.put(item.path, bitmap);
                    }
                    return bitmap;
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    if (viewHolder.position == position) {
                        viewHolder.imgThumb.setImageBitmap(bitmap);
                    }
                }
            }.execute();
        }

        return convertView;
    }

    private static Map<String, Bitmap> cached = new HashMap<>();
}
