package com.giinger.fibu.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.giinger.fibu.R;
import com.giinger.fibu.element.CustomTextView;
import com.giinger.fibu.model.PhotoGroup;
import com.giinger.fibu.model.PhotoItem;
import com.giinger.fibu.util.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhotoAdapter extends BaseAdapter {

    private Map<String, Bitmap> videoCached = new HashMap<>();

    public interface OnItemSelectedListener {
        void onItemSelected(int group, int position, View child);
    }

    private List<PhotoGroup> data;
    private OnItemSelectedListener listener;

    public void setOnItemClickListener(OnItemSelectedListener listener) {
        this.listener = listener;
    }

    public PhotoItem insertPath(File file) {
        String path = file.getAbsolutePath();
        long size = file.length();
        PhotoItem item = new PhotoItem(path, true, "", size);

        int count = data.size();
        int position = 1;

        PhotoItem photoItem = item;

        PhotoGroup groupItem = data.get(0);
        PhotoItem header = groupItem.items.get(0);
        String title = header.data;
        String first = title.substring(0, title.indexOf("("));
        String total = title.substring(title.indexOf("(") +1,title.indexOf(")"));

        int lastTotal = Integer.parseInt(total) + 1;
        header.data = first + " (" + lastTotal + ")";

        for (int i = 0; i < count; i++) {
            PhotoGroup group = data.get(i);
            if (group.type == PhotoGroup.TYPE.HEADER) {
                continue;
            }

            int last = group.items.size() - 1;
            if (last < 4) {
                photoItem = null;
                break;
            }

            position = 0;
            photoItem = group.items.remove(last);
        }

        if (photoItem != null) {
            PhotoGroup group = new PhotoGroup(PhotoGroup.TYPE.ROW, photoItem.data, photoItem.isPhoto, photoItem.duration, photoItem.size, "");
            data.add(group);
        }



        return item;
    }

    public PhotoAdapter(List<PhotoGroup> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public TextView txtGroupName;
        public LinearLayout listChildren;
        private List<AQuery> tasks;

        public void addTask(AQuery task) {
            tasks.add(task);
        }

        public void removeAllTask() {
            for (AQuery task : tasks) {
                task.ajaxCancel();
            }
            tasks.clear();
        }

        public static class Child {
            public ImageView imgPhoto, imgSelected, imgVideo;
            public int group, position;
            public RelativeLayout rlCamera;
            public CustomTextView tvTimeVideo;

            public Child(View view) {
                imgPhoto = (ImageView) view.findViewById(R.id.imgPhoto);
                imgSelected = (ImageView) view.findViewById(R.id.imgSelected);
                rlCamera = (RelativeLayout) view.findViewById(R.id.layout_item_camera);
                imgVideo = (ImageView) view.findViewById(R.id.img_icon_video);
                tvTimeVideo = (CustomTextView) view.findViewById(R.id.tv_time_video);
            }
        }

        public ViewHolder(View view) {
            txtGroupName = (TextView) view.findViewById(R.id.txtGroupName);
            listChildren = (LinearLayout) view.findViewById(R.id.listChildren);
            tasks = new ArrayList<>();
        }
    }

    @Override
    public View getView(final int group, View convertView, ViewGroup parent) {
        final Context context = parent.getContext();
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.photo_item, null);
            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        final ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.removeAllTask();

        final PhotoGroup item = data.get(group);
        switch (item.type) {
            case HEADER:
                viewHolder.txtGroupName.setVisibility(View.VISIBLE);
                viewHolder.listChildren.setVisibility(View.GONE);

                PhotoItem header = item.items.get(0);
                viewHolder.txtGroupName.setText(header.data);
                break;
            case ROW:
                viewHolder.txtGroupName.setVisibility(View.GONE);
                viewHolder.listChildren.setVisibility(View.VISIBLE);
                int childCount = viewHolder.listChildren.getChildCount();
                int count = item.items.size();
                for (int i = 0; i < childCount; i++) {
                    final View view = viewHolder.listChildren.getChildAt(i);
                    if (i < count) {
                        view.setVisibility(View.VISIBLE);
                        Object tag = view.getTag();
                        if (tag == null || !(tag instanceof ViewHolder.Child)) {
                            ViewHolder.Child child = new ViewHolder.Child(view);
                            view.setTag(child);
                            tag = child;
                        }

                        final ViewHolder.Child child = (ViewHolder.Child) tag;
                        child.group = group;
                        child.position = i;

                        final PhotoItem photoItem = item.items.get(i);
                        child.imgSelected.setVisibility(photoItem.isSelected ? View.VISIBLE : View.INVISIBLE);
                        final int index = i;
                        child.imgPhoto.setVisibility(View.INVISIBLE);

                        if (TextUtils.isEmpty(photoItem.data)) {
                            child.rlCamera.setVisibility(View.VISIBLE);
                        } else {
                            child.rlCamera.setVisibility(View.GONE);

                            if (photoItem.isPhoto) {
                                child.tvTimeVideo.setVisibility(View.GONE);
                                child.imgVideo.setVisibility(View.GONE);

                                if (photoItem.direction == null) {
                                    photoItem.direction = Utils.getCameraPhotoOrientation(context, photoItem.data);
                                }

                                AQuery task = new AQuery(context).id(child.imgPhoto).image(photoItem.data, false, true, 200, 0, new BitmapAjaxCallback() {
                                    @Override
                                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                                        if (child.group == group && child.position == index) {
                                            if (photoItem.direction != 0) {
                                                Matrix matrix = new Matrix();
                                                matrix.postRotate(photoItem.direction);
                                                int w = bm.getWidth(), h = bm.getHeight();
                                                bm = Bitmap.createBitmap(bm, 0, 0, w, h, matrix, true);
                                            }

                                            iv.setImageBitmap(bm);
                                        }

                                        iv.setVisibility(View.VISIBLE);
                                    }
                                });
                                viewHolder.addTask(task);
                            } else {
                                child.imgPhoto.setVisibility(View.INVISIBLE);
                                Log.e("QueAnh_video", photoItem.data);
                                child.tvTimeVideo.setVisibility(View.VISIBLE);
                                child.tvTimeVideo.setText(photoItem.duration);
                                child.imgVideo.setVisibility(View.VISIBLE);

                                new AsyncTask<Void, Void, Bitmap>() {
                                    @Override
                                    protected Bitmap doInBackground(Void... params) {
                                        Bitmap bitmap = videoCached.get(photoItem.data);
                                        if (bitmap == null) {
                                            bitmap = ThumbnailUtils.createVideoThumbnail(photoItem.data, MediaStore.Images.Thumbnails.MINI_KIND);
                                            videoCached.put(photoItem.data, bitmap);
                                        }
                                        return bitmap;
                                    }

                                    @Override
                                    protected void onPostExecute(Bitmap bitmap) {
                                        child.imgPhoto.setImageBitmap(bitmap);
                                        child.imgPhoto.setVisibility(View.VISIBLE);
                                    }
                                }.execute();
                            }
                        }

                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                listener.onItemSelected(group, index, v);
                            }
                        });
                    } else {
                        view.setVisibility(View.INVISIBLE);
                    }
                }
                break;
        }

        return convertView;
    }
}
