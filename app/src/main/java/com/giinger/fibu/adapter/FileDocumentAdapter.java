package com.giinger.fibu.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.giinger.fibu.R;
import com.giinger.fibu.model.FileItem;

import java.util.List;

public class FileDocumentAdapter extends BaseAdapter {

    private List<FileItem> data;

    public FileDocumentAdapter(List<FileItem> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static final class ViewHolder {

        public ImageView imgIcon, imgSelected;
        public TextView txtName;

        public ViewHolder(View view) {
            imgIcon = (ImageView) view.findViewById(R.id.imgIcon);
            imgSelected = (ImageView) view.findViewById(R.id.imgSelected);
            txtName = (TextView) view.findViewById(R.id.txtName);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.file_document_item, null);

            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        int bgWhite = context.getResources().getColor(R.color.white);
        int bgBlueLight = context.getResources().getColor(R.color.file_child_item_bg);
        int bgColor = bgWhite;
        int iconId = R.drawable.folder_grey_icon;
        boolean selectable = false;

        final FileItem item = data.get(position);
        switch (item.type) {
            case ROOT_FOLDER:
                break;
            case BACK_FOLDER:
                iconId = R.drawable.up_folder_icon;
                break;
            case FOLDER:
                bgColor = bgBlueLight;
                break;
            case DOCUMENT_FILE:
                bgColor = bgBlueLight;
                iconId = R.drawable.text_file_icon;
                selectable = true;
                break;
            case MEDIA_FILE:
                bgColor = bgBlueLight;
                iconId = R.drawable.image_file_icon;
                selectable = true;
                break;
        }

        convertView.setBackgroundColor(bgColor);
        final ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.imgIcon.setImageResource(iconId);
        viewHolder.txtName.setText(item.name);

        if (selectable) {
            viewHolder.imgSelected.setVisibility(View.VISIBLE);
            int resourceId = item.isSelected ? R.drawable.select_icon : R.drawable.not_select_icon;
            viewHolder.imgSelected.setImageResource(resourceId);
        } else {
            viewHolder.imgSelected.setVisibility(View.GONE);
        }

        return convertView;
    }

}
