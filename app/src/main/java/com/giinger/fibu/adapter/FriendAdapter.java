package com.giinger.fibu.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.giinger.fibu.R;
import com.giinger.fibu.model.FriendItem;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.EmojiMapUtil;

import java.util.List;

public class FriendAdapter extends BaseAdapter {

    private List<FriendItem> data;

    public FriendAdapter(List<FriendItem> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static final class ViewHolder {
        public TextView txtName, txtStatus, txtTime, txtBadge;
        public ImageView imgAvatar;

        public ViewHolder(View view) {
            txtName = (TextView) view.findViewById(R.id.txtName);
            txtStatus = (TextView) view.findViewById(R.id.txtStatus);
            txtTime = (TextView) view.findViewById(R.id.txtTime);
            txtBadge = (TextView) view.findViewById(R.id.txtBadge);
            imgAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.friend_item, null);
            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        FriendItem item = data.get(position);

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        String url = AppUtils.getAvatarByName(context, item.avatar);
        AppUtils.loadAvatar(context, viewHolder.imgAvatar, url, false);

        viewHolder.txtName.setText(item.name);
        String message = EmojiMapUtil.replaceCheatSheetEmojis(item.message);
        viewHolder.txtStatus.setText(message);
        viewHolder.txtTime.setText(item.time);
        if (item.badge == null || item.badge.isEmpty() || item.badge.equals("0")) {
            viewHolder.txtBadge.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.txtBadge.setText(item.badge);
            viewHolder.txtBadge.setVisibility(View.VISIBLE);
        }

        return convertView;
    }
}
