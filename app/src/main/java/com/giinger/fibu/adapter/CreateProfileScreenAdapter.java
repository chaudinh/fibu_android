package com.giinger.fibu.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.giinger.fibu.fragment.CreateProfileBaseInfoFragment;
import com.giinger.fibu.fragment.CreateProfileDetailInfoFragment;
import com.giinger.fibu.fragment.CreateProfileInstructionFagment;

public class CreateProfileScreenAdapter extends FragmentPagerAdapter {

    private static final int COUNT = 3;

    public CreateProfileScreenAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Log.e("View Pager", position + "");

        switch (position) {
            case 0:
                return new CreateProfileInstructionFagment();
            case 1:
                return new CreateProfileBaseInfoFragment();
            case 2:
                return new CreateProfileDetailInfoFragment();
            default:
                return null;
        }
    }

}
