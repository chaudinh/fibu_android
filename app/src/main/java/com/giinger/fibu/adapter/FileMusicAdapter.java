package com.giinger.fibu.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.giinger.fibu.R;
import com.giinger.fibu.model.FileItem;

import java.util.List;

public class FileMusicAdapter extends BaseAdapter {

    private List<FileItem> data;

    public FileMusicAdapter(List<FileItem> data) {
        this.data = data;
    }

    public static final class ViewHolder {
        public TextView txtName;
        public ImageView imgSelected;

        public ViewHolder(View view) {
            txtName = (TextView) view.findViewById(R.id.txtName);
            imgSelected = (ImageView) view.findViewById(R.id.imgSelected);
        }

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final FileItem child = data.get(position);
        if (convertView == null) {
            Context context = parent.getContext();
            convertView = View.inflate(context, R.layout.file_music_item, null);

            final ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        final ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.txtName.setText(child.name);
        int resourceId = child.isSelected ? R.drawable.select_icon : R.drawable.not_select_icon;
        viewHolder.imgSelected.setImageResource(resourceId);

        return convertView;
    }

}
