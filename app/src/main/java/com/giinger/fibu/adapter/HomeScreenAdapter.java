package com.giinger.fibu.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.giinger.fibu.fragment.AdvertiseFragment;
import com.giinger.fibu.fragment.ContactFragment;
import com.giinger.fibu.fragment.ExtraPowerFragment;
import com.giinger.fibu.fragment.FileFragment;
import com.giinger.fibu.fragment.PhotoFragment;
import com.giinger.fibu.fragment.ProfileFragment;

public class HomeScreenAdapter extends FragmentPagerAdapter {

    public static final int COUNT = 6;

    public HomeScreenAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Log.e("View Pager", position + "");

        switch (position) {
            case 0:
                return new ProfileFragment();
            case 1:
                return new PhotoFragment();
            case 2:
                return new FileFragment();
            case 3:
                return new ContactFragment();
            case 4:
                return new AdvertiseFragment();
            case 5:
                return new ExtraPowerFragment();
            default:
                return null;
        }
    }

}
