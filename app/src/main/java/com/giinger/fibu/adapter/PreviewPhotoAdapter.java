package com.giinger.fibu.adapter;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.giinger.fibu.R;

import java.util.List;

public class PreviewPhotoAdapter extends FragmentPagerAdapter {

    private List<String> paths;
    private List<Integer> dirs;

    public PreviewPhotoAdapter(FragmentManager fm, List<String> paths, List<Integer> dirs) {
        super(fm);
        this.paths = paths;
        this.dirs = dirs;
    }

    @Override
    public Fragment getItem(int position) {

        PreviewPhotoFragment fragment = new PreviewPhotoFragment();
        Bundle args = new Bundle();
        String path = paths.get(position);
        Integer dir = dirs.get(position);
        args.putString(PreviewPhotoFragment.EXTRA_PATH, path);
        args.putInt(PreviewPhotoFragment.EXTRA_DIR, dir);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        return paths.size();
    }

    public static class PreviewPhotoFragment extends Fragment {

        public static final String EXTRA_PATH = "extra_path";
        public static final String EXTRA_DIR = "extra_dir";

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

            Bundle arguments = getArguments();
            final String path = arguments.getString(EXTRA_PATH);
            final int dir = arguments.getInt(EXTRA_DIR);

            final ImageView image = (ImageView) inflater.inflate(R.layout.preview_photo_item, container, false);

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });

//            final Uri uri = Uri.parse(path);

            new AsyncTask<Void, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Void... params) {
                    Bitmap bitmap = BitmapFactory.decodeFile(path);
                    int w = bitmap.getWidth(), h = bitmap.getHeight();

                    Matrix matrix = new Matrix();
                    if (dir != 0) {
                        matrix.postRotate(dir);
                    }

                    float ratio = (float) 512.0 / w;
                    matrix.postScale(ratio, ratio);

                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);

                    return bitmap;
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    if (bitmap != null) {
                        image.setImageBitmap(bitmap);
                    }
                }
            }.execute();

            return image;
        }
    }
}
