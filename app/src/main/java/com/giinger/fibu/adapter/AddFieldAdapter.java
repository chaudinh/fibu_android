package com.giinger.fibu.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.giinger.fibu.R;
import com.giinger.fibu.element.CustomTextView;
import com.giinger.fibu.model.AdditionalFieldModel;

import java.util.ArrayList;

public class AddFieldAdapter extends BaseAdapter {

    private ArrayList<AdditionalFieldModel> data;

    public AddFieldAdapter(ArrayList<AdditionalFieldModel> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.list_item_add_field, null);

            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        AdditionalFieldModel model = data.get(position);
        Bitmap bitmap = BitmapFactory.decodeResource(parent.getContext().getResources(), model.getImg());

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.imgType.setImageBitmap(bitmap);
        viewHolder.tvType.setText(model.getName());

        if (model.getSelected()) {
            convertView.setBackgroundResource(R.color.white);
        } else {
            convertView.setBackgroundResource(R.color.grey_cancel_action);
        }

        return convertView;
    }

    public static class ViewHolder {
        ImageView imgType;
        CustomTextView tvType;

        public ViewHolder(View view) {
            imgType = (ImageView) view.findViewById(R.id.img_type);
            tvType = (CustomTextView) view.findViewById(R.id.tv_type);
        }
    }

    public ArrayList<AdditionalFieldModel> genData() {
        data = new ArrayList<AdditionalFieldModel>();

        String name[] = {"home phone number", "office phone number", "other phone number", "home address", "office address", "other address", "email address", "link", "Facebook", "Twitter", "Linkeld"};
        String type[] = {"Linkeld", "Twitter", "Facebook", "link", "other email", "other address", "office address", "home address", "other phone number", "office phone number", "home phone number"};
        int img[] = {R.drawable.home_number_icon, R.drawable.office_icon, R.drawable.twitter_icon, R.drawable.home_icon, R.drawable.office_icon, R.drawable.twitter_icon, R.drawable.email_icon, R.drawable.web_icon, R.drawable.fb_icon, R.drawable.twitter_icon, R.drawable.linkedin_icon};

        for (int i = 0; i < name.length; i++) {
            AdditionalFieldModel model = new AdditionalFieldModel();

            model.setName(name[i]);
            model.setImg(img[i]);
            model.setType(type[i]);

            data.add(model);
        }

        return data;

    }

    public ArrayList<AdditionalFieldModel> getData() {
        return data;
    }
}
