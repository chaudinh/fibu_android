package com.giinger.fibu.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.giinger.fibu.R;
import com.giinger.fibu.activity.WebScreen;
import com.giinger.fibu.model.AdvertiseItem;

import java.util.List;

public class AdvertiseAdapter extends BaseAdapter {

    private List<AdvertiseItem> data;

    public AdvertiseAdapter(List<AdvertiseItem> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static final class ViewHolder {

        public ImageView imgThumb;
        public TextView txtTitle, txtDescription, btnDownload;

        public ViewHolder(View view) {
            imgThumb = (ImageView) view.findViewById(R.id.imgThumb);
            txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            txtDescription = (TextView) view.findViewById(R.id.txtDescription);
            btnDownload = (TextView) view.findViewById(R.id.btnDownload);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Context context = parent.getContext();
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.advertise_item, null);

            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        final AdvertiseItem item = data.get(position);

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.txtTitle.setText(item.title);
        viewHolder.txtDescription.setText(item.description);

        viewHolder.btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri uri = Uri.parse(item.appUrl);
                intent.setData(uri);
                context.startActivity(intent);
            }
        });

        viewHolder.imgThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebScreen.class);
                intent.putExtra(WebScreen.EXTRA_URL, item.videoUrl);
                context.startActivity(intent);
            }
        });

        if (item.videoUrl != null && !item.videoUrl.isEmpty()) {
            String id = item.videoUrl.split("v=")[1];
            String thumb = String.format("http://img.youtube.com/vi/%s/mqdefault.jpg", id);
            new AQuery(context).id(viewHolder.imgThumb).image(thumb, true, true, 0, R.drawable.video_promo_frame);
        }

        return convertView;
    }

}
