package com.giinger.fibu.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.support.v4.content.CursorLoader;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.giinger.fibu.R;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class CameraScreen extends Activity implements
        OnClickListener {

    private Button btnSwap, btnFlash, btnExit, btnTake;
    private SurfaceView camera;
    private RelativeLayout rlBar;
    private Camera mCamera = null;
    private SurfaceHolder holder;
    private int cameraId = 1;
    private int h;
    private int w;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera.getHolder().addCallback(shCallback);
    }

    public void initLayout() {
        setContentView(R.layout.camera_screen);

        btnExit = (Button) findViewById(R.id.btn_exit);
        btnSwap = (Button) findViewById(R.id.btn_switch);
        btnFlash = (Button) findViewById(R.id.btn_flash);
        btnTake = (Button) findViewById(R.id.btn_camera);

        camera = (SurfaceView) findViewById(R.id.camera);
        rlBar = (RelativeLayout) findViewById(R.id.rl_bar);

        btnExit.setOnClickListener(this);
        btnSwap.setOnClickListener(this);
        btnFlash.setOnClickListener(this);
        btnTake.setOnClickListener(this);
        camera.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_exit:
                Constants.imgUri = null;
                finish();
                break;
            case R.id.btn_flash:
                break;
            case R.id.btn_switch:
                switchCamera();
                break;
            case R.id.btn_camera:
                setSize();
                mCamera.takePicture(null, null, pictureCallback);
                break;
            case R.id.camera:
                mCamera.autoFocus(focusCallback);
                break;
        }
    }

    public void switchCamera() {
        startCamera(1 - cameraId);
    }

    protected void startCamera(final int id) {
        releaseCamera();

        new AsyncTask<Integer, Void, Camera>() {

            @Override
            protected Camera doInBackground(Integer... ids) {
                return openCamera(ids[0]);
            }

            @Override
            protected void onPostExecute(Camera c) {
                startPreview(id, c);
            }

        }.execute(id);
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    private static Camera openCamera(int id) {
        Log.e("QueAnh_CameraScreen", "opening camera " + id);
        Camera camera = null;
        try {
            camera = Camera.open(id);
        } catch (Exception e) {
            e.printStackTrace();
            camera.release();
            camera = null;
        }
        return camera;
    }

    private void startPreview(int id, Camera c) {
        if (c != null) {
            try {
                holder = camera.getHolder();
                c.setPreviewDisplay(holder);
                mCamera = c;
                cameraId = id;
                restartPreview();
            } catch (IOException e) {
                e.printStackTrace();
                c.release();
            }
        }
    }

    private void restartPreview() {
        if (mCamera == null) {
            return;
        }
        int degrees = 0;
        switch (getWindowManager().getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }
        Camera.CameraInfo ci = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, ci);
        if (ci.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            degrees += ci.orientation;
            degrees %= 360;
            degrees = 360 - degrees;
        } else {
            degrees = 360 - degrees;
            degrees += ci.orientation;
        }
        mCamera.setDisplayOrientation(degrees % 360);
        mCamera.startPreview();
    }

    public void setSize() {
        List<Camera.Size> supportedSizes;
        Camera.Parameters params = mCamera.getParameters();
        supportedSizes = params.getSupportedPictureSizes();

        if (supportedSizes != null) {
            int countSize = supportedSizes.size();

            int mod = countSize / 2;
            h = supportedSizes.get(mod).height;
            w = supportedSizes.get(mod).width;

            params.setPictureSize(supportedSizes.get(mod).width,
                    supportedSizes.get(mod).height);
            mCamera.setParameters(params);

            Log.e("QueANh_Camera_Screen", " Size  - W : " + w + " H : " + h + " mod : " + mod + " listsize : " + countSize);
        }
    }

    // call back take pic
    private Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            try {
                camera.lock();

                File file = BumpUtils.getFile("avatar.jpg", true);
                FileOutputStream jpg = new FileOutputStream(file);

                jpg.write(data);
                jpg.close();

                rlBar.setVisibility(View.VISIBLE);
                Bitmap bmp = optimizeBitmap(data);
                final String path;


                Matrix matrix = new Matrix();
                if (cameraId == 0)
                    matrix.postRotate(90);
                else
                    matrix.postRotate(-90);
                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp,
                        bmp.getWidth(), bmp.getHeight(), true);
                Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                        scaledBitmap.getWidth(), scaledBitmap.getHeight(),
                        matrix, true);

//                Uri r = getImageUri(getApplicationContext(), rotatedBitmap);
//                path = getRealPathFromURI(r);
//                path = getImageUri(getApplicationContext(), rotatedBitmap);

                path = Utils.getImageUri(rotatedBitmap, false);
                Log.e("QueAnh", "bmp dimensions w : " + rotatedBitmap.getWidth() + "h : "
                        + rotatedBitmap.getHeight());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(CameraScreen.this, EditAvatarScreen.class);
                        i.putExtra("type", "take");
                        i.putExtra("path", path);
                        CameraScreen.this.startActivity(i);
                        rlBar.setVisibility(View.GONE);
                        finish();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    };

    //
    private SurfaceHolder.Callback shCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.e("QA", "SurfaceDestroyed callback");
            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.release();
            }
            mCamera = null;
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            Log.e("QA", "SurfaceCreated callback");
            startCamera(cameraId);
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                   int height) {
            Log.e("QA", "SurfaceChanged callback " + width + "x" + height);
            restartPreview();
        }
    };

    private AutoFocusCallback focusCallback = new AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera cam) {
            if (success) {
                mCamera.cancelAutoFocus();
            }

            float focusDistances[] = new float[3];
            cam.getParameters().getFocusDistances(focusDistances);
        }
    };

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {Images.Media.DATA};

        CursorLoader cursorLoader = new CursorLoader(
                this,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        int column_index =
                cursor.getColumnIndexOrThrow(Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

//    public String getImageUri(Context inContext, Bitmap inImage) {
////        String path = BumpUtils.FOLDER_NAME + "avatar.jpg";
//        File file = new File(path);
//        try {
//            FileOutputStream out = new FileOutputStream(file);
//            inImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        return path;
//    }

    public Bitmap optimizeBitmap(byte[] data) {
        BitmapFactory.Options opt;

        opt = new BitmapFactory.Options();
        opt.inTempStorage = new byte[16 * 1024];

        float mb = (w * h) / 1024000;

        if (mb > 4f)
            opt.inSampleSize = 4;
        else if (mb > 3f)
            opt.inSampleSize = 2;
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, opt);

        Log.e("QueANh_Camera_screen", " w : " + w + " h : " + h + " megapixel : " + mb + " insample : " + opt.inSampleSize);
        return bitmap;
    }
}