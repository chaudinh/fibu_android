package com.giinger.fibu.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.exception.DropboxUnlinkedException;
import com.dropbox.client2.session.AppKeyPair;
import com.giinger.fibu.R;
import com.giinger.fibu.adapter.FileDocumentAdapter;
import com.giinger.fibu.model.FileItem;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.PreferenceUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileDocumentScreen extends BaseFileScreen {

    public static final int MAX_SELECTED_ITEMS = 1;

    private ListView lvChooseDocument;
    private List<FileItem> data;
    private FileDocumentAdapter adapter;
    private View lnListItems, lnEmpty;

    private RelativeLayout layoutBumpAnimation;
    private RelativeLayout layoutAnimLeft;
    private RelativeLayout layoutAnimRight;

    private Animation animLeft;
    private Animation animRight;

    private ProgressBar progressBar;

    @Override
    public void notifyDataSetChange() {
        adapter.notifyDataSetChanged();
    }

    @Override
    protected String getType() {
        return "document";
    }

    @Override
    protected String getSharedType() {
        return "dropbox";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String key = getString(R.string.dropbox_key);
        String secret = getString(R.string.dropbox_secret);
        AppKeyPair appKeys = new AppKeyPair(key, secret);
        AndroidAuthSession session = new AndroidAuthSession(appKeys);
        String token = PreferenceUtils.getString(Constants.DROPBOX_TOKEN, this, null);
        Log.e("Token", token + "");
        if (token != null) {
            session.setOAuth2AccessToken(token);
            dropboxAPI = new DropboxAPI<>(session);

            FileItem item = new FileItem("/", null);
            goToFolder(item);
        } else {
            dropboxAPI = new DropboxAPI<>(session);
            isAuth = true;
            dropboxAPI.getSession().startOAuth2Authentication(this);
        }
    }

    @Override
    protected void initLayout() {
        setContentView(R.layout.file_document_screen);

        lnListItems = findViewById(R.id.lnListItems);
        lnEmpty = findViewById(R.id.lnEmpty);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        lvChooseDocument = (ListView) findViewById(R.id.lvChooseDocument);

        data = new ArrayList<>();
        adapter = new FileDocumentAdapter(data);
        lvChooseDocument.setAdapter(adapter);

        layoutBumpAnimation = (RelativeLayout) findViewById(R.id.layout_bump_action);
        layoutAnimLeft = (RelativeLayout) findViewById(R.id.layout_anim_left);
        layoutAnimRight = (RelativeLayout) findViewById(R.id.layout_anim_right);

        animLeft = AnimationUtils.loadAnimation(this, R.anim.left_to_right);
        animRight = AnimationUtils.loadAnimation(this, R.anim.right_to_left);

        animLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Constants.isAnim = false;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        lvChooseDocument.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FileItem item = data.get(position);
                switch (item.type) {
                    case MEDIA_FILE:
                    case DOCUMENT_FILE:
                        FileDocumentAdapter.ViewHolder viewHolder = (FileDocumentAdapter.ViewHolder) view.getTag();
                        if (item.isSelected) {
                            item.isSelected = false;
                            onItemSelected(item);
                            viewHolder.imgSelected.setImageResource(R.drawable.not_select_icon);
                            if (getCountItemSelected() == 0) {
                                goneLayoutAnimation();
                            }

                        } else {
                            clearSelectedItems();
                            if (Constants.isAnim) {
                                animationBump();
                            }

                            item.isSelected = true;
                            onItemSelected(item);
                            viewHolder.imgSelected.setImageResource(R.drawable.select_icon);
                        }
                        break;
                    case BACK_FOLDER:
                    case FOLDER:
                    case ROOT_FOLDER:
                        goToFolder(item);
                        break;
                }
            }
        });
    }

    private FileItem preItem = null;

    @Override
    public void onBackPressed() {
        if (preItem == null) {
            super.onBackPressed();
        } else {
            goToFolder(preItem);
        }
    }

    @Override
    protected void animationBump() {
        layoutBumpAnimation.setVisibility(View.VISIBLE);
        layoutAnimLeft.startAnimation(animLeft);
        layoutAnimRight.startAnimation(animRight);
    }

    @Override
    protected void goneLayoutAnimation() {
        layoutBumpAnimation.setVisibility(View.GONE);
        Constants.isAnim = true;
    }

    private boolean isAuth = false;

    public void onResume() {
        super.onResume();

        if (dropboxAPI.getSession().authenticationSuccessful()) {
            isAuth = false;
            try {
                dropboxAPI.getSession().finishAuthentication();

                String accessToken = dropboxAPI.getSession().getOAuth2AccessToken();
                Log.e("Token", accessToken);
                PreferenceUtils.saveString(Constants.DROPBOX_TOKEN, this, accessToken);

                FileItem item = new FileItem("/", null);
                goToFolder(item);

            } catch (IllegalStateException e) {
                Log.i("DbAuthLog", "Error authenticating", e);
                handler.sendEmptyMessage(EMPTY);
            }
        } else if (isAuth) {
            isAuth = false;
            handler.sendEmptyMessage(EMPTY);
        }
    }

    public static DropboxAPI<AndroidAuthSession> dropboxAPI;

    public static String getLinkForShare(final String path) {
        DropboxAPI.DropboxLink link = null;
        try {
            link = dropboxAPI.share(path);
        } catch (DropboxException e) {
            e.printStackTrace();
        }
        return link.url;
    }

    private final String ROOT_PATH = "/";

    private static final int EMPTY = 0;
    private static final int RELOAD = 1;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EMPTY:
                    lnListItems.setVisibility(View.GONE);
                    lnEmpty.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    break;
                case RELOAD:
                    clearSelectedItems();
                    adapter.notifyDataSetChanged();
                    lvChooseDocument.smoothScrollToPosition(0);
                    lnListItems.setVisibility(View.VISIBLE);
                    lnEmpty.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    break;
            }
        }
    };

    public void goToFolder(final FileItem root) {

        progressBar.setVisibility(View.VISIBLE);

        Log.e("Goto", root.path);

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<FileItem> items = new ArrayList<>();
                try {
                    DropboxAPI.Entry metadata = dropboxAPI.metadata(root.path, 0, null, true, null);
                    List<DropboxAPI.Entry> entries = metadata.contents;

                    if (!ROOT_PATH.equals(root.path)) {
                        String path = metadata.parentPath();
                        String name = "Up to " + metadata.fileName();
                        FileItem fileItem = new FileItem(path, name, FileItem.TYPE.BACK_FOLDER);
                        items.add(fileItem);
                        preItem = fileItem;
                    } else {
                        preItem = null;
                    }

                    for (DropboxAPI.Entry item : entries) {
                        String path = item.path;
                        String name = item.fileName();

                        FileItem fileItem = new FileItem(path, name);
                        if (ROOT_PATH.equals(root.path)) {
                            fileItem.type = item.isDir ? FileItem.TYPE.ROOT_FOLDER : FileItem.TYPE.DOCUMENT_FILE;
                        } else {
                            fileItem.type = item.isDir ? FileItem.TYPE.FOLDER : FileItem.TYPE.DOCUMENT_FILE;
                            boolean isMediaFile = item.mimeType == null ? false : item.mimeType.contains("");
                            if (isMediaFile) {

                            }
                        }

                        items.add(fileItem);
                    }

                    data.clear();
                    data.addAll(items);
                    Collections.sort(data);

                    handler.sendEmptyMessage(RELOAD);
                } catch (DropboxException e) {
                    e.printStackTrace();

                    if (e instanceof DropboxUnlinkedException) {
                        isAuth = true;
                        dropboxAPI.getSession().startOAuth2Authentication(FileDocumentScreen.this);
                    } else {
                        handler.sendEmptyMessage(EMPTY);
                    }
                }
            }
        }).start();

    }
}
