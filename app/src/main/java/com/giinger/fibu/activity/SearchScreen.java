package com.giinger.fibu.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.giinger.fibu.R;
import com.giinger.fibu.adapter.PlaceAdapter;
import com.giinger.fibu.element.CustomEditText;
import com.giinger.fibu.model.PlaceModel;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by geekup on 4/24/15.
 */
public class SearchScreen extends Activity implements View.OnClickListener {

    private ListView listPlace;
    private RelativeLayout btnClear;
    private RelativeLayout layoutBack;
    private CustomEditText edtSearch;
    private ArrayList<PlaceModel> data;
    private PlaceAdapter adapter;
    private String latLng;
    private String namePlace = "GEEKUp";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_search_screen);

        latLng = getIntent().getExtras().getString("lat_lng", "0,0");

        layoutBack = (RelativeLayout) findViewById(R.id.layout_back);
        edtSearch = (CustomEditText) findViewById(R.id.edt_search);
        btnClear = (RelativeLayout) findViewById(R.id.btn_clear);
        listPlace = (ListView) findViewById(R.id.list_place);

        layoutBack.setOnClickListener(this);
        btnClear.setOnClickListener(this);

        data = new ArrayList<>();
        adapter = new PlaceAdapter(data, getApplicationContext());
        listPlace.setAdapter(adapter);

        listPlace.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    JSONObject lct = new JSONObject();
                    lct.put("longitude", data.get(position).getLng());
                    lct.put("latitude", data.get(position).getLat());
                    lct.put("name", data.get(position).getName());

                    PreferenceUtils.saveString(Constants.JSON_LOC, getApplicationContext(), lct.toString());
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    data.clear();
                    adapter.notifyDataSetChanged();
                    btnClear.setVisibility(View.GONE);
                } else {
                    btnClear.setVisibility(View.VISIBLE);
                }
            }
        });

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.e("QueAnh_send_emty", actionId + "");
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (TextUtils.isEmpty(v.getText())) {
                        Log.e("QueAnh_send_emty", "true");
                    } else {
                        hideKeyBoard();
                        searchPlace(v.getText().toString());
                    }

                    return true;
                }

                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_back:
                finish();
                break;

            case R.id.btn_clear:
                data.clear();
                btnClear.setVisibility(View.GONE);
                edtSearch.setText("");
                adapter.notifyDataSetChanged();
                break;
        }

    }

    public void searchPlace(final String name) {

        String url = AppUtils.getUrlTextPlace(latLng, name);
        namePlace = name;
        data.clear();

        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    try {
                        Log.e("QueAnh_url_get_list_place : ", url);

                        JSONArray results = object.getJSONArray("results");

                        // Add first item
                        PlaceModel item = new PlaceModel();

                        String lat = latLng.substring(0, latLng.indexOf(","));
                        String lng = latLng.substring(latLng.indexOf(",") + 1, latLng.length());

                        item.setName(name);
                        item.setLat(lat);
                        item.setLng(lng);
                        item.setAddress("");
                        item.setImg("");

                        data.add(item);

                        adapter.notifyDataSetChanged();

                        for (int i = 0; i < results.length(); i++) {
                            PlaceModel model = new PlaceModel();

                            JSONObject jOb = results.getJSONObject(i);

                            model.setImg(jOb.getString("icon"));
                            model.setName(jOb.getString("name"));
                            model.setAddress(jOb.getString("formatted_address"));
                            model.setLat(jOb.getJSONObject("geometry").getJSONObject("location").getString("lat"));
                            model.setLng(jOb.getJSONObject("geometry").getJSONObject("location").getString("lng"));

                            data.add(model);
                        }

                        adapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Google API ERROR!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };

        new AQuery(getApplicationContext()).ajax(url, JSONObject.class, callback);
    }

    private void hideKeyBoard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }
}
