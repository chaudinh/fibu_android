package com.giinger.fibu.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.dd.crop.TextureVideoView;
import com.giinger.fibu.R;

import java.io.File;

public class PreviewVideo extends BasePreviewScreen {

    public static final String EXTRA = "video_path";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initLayout() {
        setContentView(R.layout.preview_video_screen);

        final Intent intent = getIntent();
        final String path = intent.getStringExtra(EXTRA);

        final TextureVideoView video = (TextureVideoView) findViewById(R.id.video);
        video.setDataSource(path);

        final View imgPlay = findViewById(R.id.imgPlay);

        video.setScaleType(TextureVideoView.ScaleType.CENTER_CROP);

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isPlaying = imgPlay.getVisibility() == View.GONE;
                if (isPlaying) {
                    video.pause();
                    imgPlay.setVisibility(View.VISIBLE);
                } else {
                    video.play();
                    imgPlay.setVisibility(View.GONE);
                }
            }
        });

        video.setListener(new TextureVideoView.MediaPlayerListener() {
            @Override
            public void onVideoPrepared() {

            }

            @Override
            public void onVideoEnd() {
                imgPlay.setVisibility(View.VISIBLE);
            }
        });

        final View share = findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                File file = new File(path);
                Uri uri = Uri.fromFile(file);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("video/*");
                Intent chooser = Intent.createChooser(shareIntent, "Share");
                startActivity(chooser);
            }
        });

        imgPlay.setVisibility(View.GONE);
        video.play();
    }
}
