package com.giinger.fibu.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.giinger.fibu.R;
import com.giinger.fibu.element.PopupBumpFailHolder;
import com.giinger.fibu.element.PopupConnectingHolder;
import com.giinger.fibu.element.PopupMultipleConnectedHolder;
import com.giinger.fibu.model.ChatBoxItem;
import com.giinger.fibu.model.FileItem;
import com.giinger.fibu.model.Friend;
import com.giinger.fibu.receiver.ParseReceiver;
import com.giinger.fibu.service.GPSTracker;
import com.giinger.fibu.util.AccelerometerManager;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.RushOrmManager;
import com.giinger.fibu.util.Utils;
import com.koushikdutta.async.future.Future;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseFileScreen extends Activity implements AccelerometerManager.AccelerometerListener, BumpUtils.ConnectConfirmCallback {

    public static long MAX_SIZE = 15 * 1024 * 1024;

    protected TextView txtCountSelectedItem, txtBumpFailure;
    protected View imgBack, popupConnecting, popBumpFail, popupMultipleConnected;
    private Animation moveDow;

    protected List<FileItem> selected = new ArrayList<>();
    protected long totalSize = 0;

    public int getCountItemSelected() {
        return selected.size();
    }

    public void onItemSelected(FileItem item) {
        if (item.isSelected) {
            selected.add(item);
        } else {
            selected.remove(item);
        }

        updateCountView();
    }

    public abstract void notifyDataSetChange();

    public void clearSelectedItems() {
        for (FileItem item : selected) {
            item.isSelected = false;
        }

        totalSize = 0;
        selected.clear();
        notifyDataSetChange();
        updateCountView();
        goneLayoutAnimation();
    }

    protected void updateCountView() {
        String type = getType();
        int count = selected.size();
        if (count == 0) {
            txtCountSelectedItem.setVisibility(View.INVISIBLE);
        } else {
            txtCountSelectedItem.setVisibility(View.VISIBLE);
            String text = count + " " + (count > 1 ? type + "s" : type) + " selected";
            txtCountSelectedItem.setText(text);
        }
    }

    protected abstract String getType();

    private TextView txtNotification;

    private Handler handler = new Handler();

    private IntentFilter filter = new IntentFilter(ParseReceiver.NOTIFY_ACTION);

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            String message = intent.getStringExtra(ParseReceiver.KEY_PUSH_MESSAGE);
            txtNotification.setText(message);
            txtNotification.setVisibility(View.VISIBLE);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    txtNotification.setVisibility(View.GONE);
                }
            }, 3000);

            txtNotification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtNotification.setVisibility(View.GONE);
                    txtNotification.setOnClickListener(null);

                    String data = intent.getStringExtra(ParseReceiver.KEY_PUSH_DATA);
                    Intent fibu = new Intent("fibu.action.chat");
                    fibu.putExtra(ParseReceiver.KEY_PUSH_DATA, data);
                    context.sendBroadcast(fibu);
                }
            });


        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initLayout();

        txtNotification = (TextView) findViewById(R.id.txtNotification);

        imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtCountSelectedItem = (TextView) findViewById(R.id.txtCountSelectedItem);

        txtBumpFailure = (TextView) findViewById(R.id.txtBumpFailure);

        moveDow = AnimationUtils.loadAnimation(this, R.anim.move_down);
        moveDow.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (popBumpFail.getVisibility() == View.VISIBLE) {
                            popBumpFail.setVisibility(View.GONE);
                            canBump = true;
                        }
                    }
                }, 2000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        popupConnecting = findViewById(R.id.layoutPopupConnecting);
        popupConnectingHolder = new PopupConnectingHolder(popupConnecting);

        popBumpFail = findViewById(R.id.view_bump_fail);
        popupBumpFailHolder = new PopupBumpFailHolder(popBumpFail);
        popupBumpFailHolder.layoutActionHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popBumpFail.setVisibility(View.GONE);
                canBump = true;
            }
        });

        popupBumpFailHolder.layoutAnimation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popBumpFail.setVisibility(View.GONE);
                canBump = true;
            }
        });

        popupMultipleConnected = findViewById(R.id.layoutMultipleConnect);
        popupMultipleConnectedHolder = new PopupMultipleConnectedHolder(popupMultipleConnected);

        popupMultipleConnectedHolder.txtPopupCancelBumpAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMultipleConnected.setVisibility(View.GONE);
                canBump = true;
            }
        });

        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    private Animation rotate;

    @Override
    public void onResume() {
        super.onResume();

        if (AccelerometerManager.isSupported(this)) {
            int sensitive = PreferenceUtils.getInt(Constants.SETTING_SENSITIVE, getApplicationContext(), Constants.DEFAULT_SENSITIVE);
            Utils.getInstance().setSensitivity(sensitive);
            AccelerometerManager.startListening(this);
        }

        boolean checkLocationEnable = Utils.getInstance().checkLocationEnable(this);
        if (!checkLocationEnable) {
            Utils.getInstance().forcePopupLocation(this);
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);

        clearSelectedItems();
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

        if (AccelerometerManager.isListening()) {
            AccelerometerManager.stopListening();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (AccelerometerManager.isListening()) {
            AccelerometerManager.stopListening();
        }

        Constants.isAnim = true;
    }

    protected abstract void initLayout();

    protected abstract void animationBump();

    protected boolean canBump = true;

    protected abstract void goneLayoutAnimation();

    @Override
    public void onShake(float force) {
        if (canBump) {
            Log.e("QueAnh", "shake");
            doBump();
        }
    }

    private void doBump() {
        GPSTracker gpsTracker = GPSTracker.getInstance();
        Location location = gpsTracker.getLocation();
        Log.e("Location", location + "");
        if (location == null) {
            return;
        }

        canBump = false;

        AQuery aQuery = new AQuery(this);
        Map<String, String> params = new HashMap<>();

        params.put("latitude", location.getLatitude() + "");
        params.put("longitude", location.getLongitude() + "");

//        params.put("latitude", "10.806803" + new Random().nextInt(10));
//        params.put("longitude", "106.667936" + new Random().nextInt(10));

        String userID = PreferenceUtils.getString(Constants.USER_ID, this, null);
        params.put("user_id", userID);

        String bumpType = selected.size() > 0 ? getSharedType() : null;
        params.put("my_shared_type", bumpType);

        Log.e("Params", params + "");

        String url = AppUtils.getBumpUrl(this);
        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    handlerBumpResponse(object);
                } else {
                    canBump = true;
                }
            }
        });
    }

    protected abstract String getSharedType();

    protected Friend friend;

    private void showPopupMultipleConnected() {
        popupMultipleConnected.setVisibility(View.VISIBLE);

        View.OnClickListener l = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMultipleConnected.setVisibility(View.GONE);
                canBump = true;
                doBump();
            }
        };

        popupMultipleConnectedHolder.layoutPopupAction.setOnClickListener(l);
        popupMultipleConnectedHolder.txtPopupBumpAgain.setOnClickListener(l);
    }

    protected void handlerBumpResponse(JSONObject data) {
        try {
            Log.e("Bump", "Response " + data);
            JSONObject response = data.getJSONObject(Constants.RESPONSE);
            boolean success = response.getBoolean(Constants.SUCCESS);
            if (success) {
                friend = new Friend(response);
                showPopupConnecting();
            } else {
                String error = response.getString("error");

//                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
//                String title = getString(R.string.title_bump_dialog);
//                alertBuilder.setTitle(title);

                switch (error) {
                    case "friendNotFound":
                    case "bothNotShare":
//                        alertBuilder.setMessage(R.string.message_bump_no_friend_available);
//                        popBumpFail.setVisibility(View.VISIBLE);
//                        popupBumpFailHolder.layoutAnimation.startAnimation(moveDow);

                        canBump = true;
                        txtBumpFailure.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                txtBumpFailure.setVisibility(View.GONE);
                            }
                        }, 3000);
                        break;
                    case "multiBumped":
                        showPopupMultipleConnected();
                        break;
//                    case "bothShare":
//                        alertBuilder.setMessage(R.string.message_bump_both_share);
//                        alertBuilder.setNeutralButton(R.string.bump_clear_all_choices, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                clearSelectedItems();
//                            }
//                        });
//                        break;
//                    case "bothNotShare":
//                        alertBuilder.setMessage(R.string.message_bump_both_not_share);
//                        popBumpFail.setVisibility(View.VISIBLE);
//                        popupBumpFailHolder.layoutAnimation.startAnimation(moveDow);
//                        break;
                }

//                alertBuilder.setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//                alertBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                    @Override
//                    public void onDismiss(DialogInterface dialog) {
//                        canBump = true;
//                    }
//                });
//                alertBuilder.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private PopupConnectingHolder popupConnectingHolder;
    private PopupBumpFailHolder popupBumpFailHolder;
    private PopupMultipleConnectedHolder popupMultipleConnectedHolder;

    private void showPopupConnecting() {
        popupConnectingHolder.txtFirstName.setText(friend.firstName);
        popupConnectingHolder.txtLastName.setText(friend.lastName);
        popupConnectingHolder.txtJobTitle.setText("null".equals(friend.jobTitle) ? "" : friend.jobTitle);

        popupConnectingHolder.txtPopupAction.setVisibility(View.VISIBLE);
        popupConnectingHolder.imgStatus.setVisibility(View.GONE);

        popupConnectingHolder.txtPopupCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (request != null) {
                    Log.e("Cancel", "Aquery");
                    request.cancel(true);
                }

                if (asyncTask != null) {
                    Log.e("Cancel", "Task");
                    asyncTask.cancel(true);
                }

                hidePopupConnecting();
                BumpUtils.confirmConnect(null, BaseFileScreen.this, friend.connectId, BumpUtils.ConnectConfirmCallback.ACTION_CANCEL, friend.id);
            }
        });

        String avatarUrl = AppUtils.getAvatarByName(this, friend.avatar);
        AppUtils.loadAvatar(this, popupConnectingHolder.imgAvatar, avatarUrl, true);
        popupConnecting.setVisibility(View.VISIBLE);

        View.OnClickListener l = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupConnectingHolder.layoutPopupAction.setOnClickListener(null);
                if (friend.sendType.isEmpty() && friend.receiveType.isEmpty()) {
                    gotoChatBox();
                    hidePopupConnecting();
                } else {
                    transferData();
                }
            }
        };

        popupConnectingHolder.layoutPopupAction.setOnClickListener(l);
        popupConnectingHolder.txtPopupAction.setOnClickListener(l);
    }

    private void transferData() {
        popupConnectingHolder.txtPopupAction.setVisibility(View.GONE);
        popupConnectingHolder.imgStatus.setVisibility(View.VISIBLE);
        popupConnectingHolder.txtPopupAction.setOnClickListener(null);
        popupConnectingHolder.imgStatus.startAnimation(rotate);
        popupConnectingHolder.txtPopupCancel.setOnClickListener(null);

        BumpUtils.confirmConnect(this, this, friend.connectId, BumpUtils.ConnectConfirmCallback.ACTION_CONNECT, friend.id);
    }

    private AsyncTask asyncTask;
    private Future request;

    private void upload() {
        ArrayList<String> selected = new ArrayList<>();

        switch (friend.sendType) {
            case "video":
            case "music":
            case "document":
                for (FileItem item : this.selected) {
                    selected.add(item.path);
                }
                break;
            case "dropbox":
                String path = this.selected.get(0).path;
                selected.add(path);
                break;
        }

        BumpUtils.upload(this, friend, selected, new BumpUtils.Callback() {
            @Override
            public void callback() {
                connectHandler.sendEmptyMessage(0);
            }
        });
    }

    private void download() {
        BumpUtils.download(this, friend, new BumpUtils.Callback() {
            @Override
            public void callback() {
                connectHandler.sendEmptyMessage(0);
            }
        });
    }

    @Override
    public void doTransferData() {

        if (!friend.sendType.isEmpty()) {
            upload();
        } else {
            connectHandler.sendEmptyMessage(1);
        }

        if (!friend.receiveType.isEmpty()) {
            download();
        } else {
            connectHandler.sendEmptyMessage(1);
        }

    }

    public void hidePopupConnecting() {
        request = null;
        asyncTask = null;

        canBump = true;
        popupConnecting.setVisibility(View.GONE);
        popupConnectingHolder.imgStatus.clearAnimation();
    }

    private Handler connectHandler = new Handler() {
        private boolean firstTime = true;

        @Override
        public void handleMessage(Message msg) {
            Log.e("Handler", "Received " + firstTime + " " + msg.what);
            if (msg.what == -1) {
                hidePopupConnecting();
                firstTime = true;
                return;
            }

            if (firstTime) {
                firstTime = false;
            } else {
                firstTime = true;
                gotoChatBox();
                hidePopupConnecting();
            }
        }
    };

    protected void gotoChatBox() {
        clearSelectedItems();

        if (!PreferenceUtils.getString(Constants.FRIEND_ID, getApplicationContext(), "").equals("friend" + friend.id)) {
            //add friend profile
            ChatBoxItem itemFriend = new ChatBoxItem();
            JSONObject jsonFriend = Utils.getInstance().getProfileFriend(friend);

            itemFriend.setFriendId(friend.id);
            itemFriend.setJsonData(jsonFriend);
            RushOrmManager.getInstance().storeSingleChatItem(itemFriend);

            //add seft profile
            ChatBoxItem itemSeft = new ChatBoxItem();
            String selfInfo = PreferenceUtils.getString(Constants.USER_INFO_JSON, BaseFileScreen.this, "{}");
            try {
                JSONObject selfJson = new JSONObject(selfInfo);
                JSONObject jsonSeft = Utils.getInstance().getSelfProfile(friend.id, selfJson, 0);

                itemSeft.setFriendId(friend.id);
                itemSeft.setJsonData(jsonSeft);
                RushOrmManager.getInstance().storeSingleChatItem(itemSeft);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //store friend id
            PreferenceUtils.saveString(Constants.FRIEND_ID, getApplicationContext(), "friend" + friend.id);
        }


        int count = PreferenceUtils.getInt(Constants.TIMES_BUMP_SUCCESS, this, 0) + 1;
        PreferenceUtils.saveInt(Constants.TIMES_BUMP_SUCCESS, this, count);

        Intent intent = new Intent(this, ChatBoxScreen.class);
        intent.putExtra(ChatBoxScreen.EXTRA_ID, friend.id);
        intent.putExtra(ChatBoxScreen.EXTRA_NAME, friend.firstName + " " + friend.lastName);
        intent.putExtra(ChatBoxScreen.EXTRA_AVATAR, friend.avatar);
        intent.putExtra(ChatBoxScreen.EXTRA_FROM_BUM, true);
        startActivity(intent);
    }
}
