package com.giinger.fibu.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.faradaj.blurbehind.BlurBehind;
import com.faradaj.blurbehind.OnBlurCompleteListener;
import com.giinger.fibu.R;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class SplashScreen extends Activity {

    public static final String EXTRA_TYPE = "type";
    public static final String TYPE_CHAT = "chat";

    private boolean checkInternet = false;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (checkInternet) {
                if (intent.getExtras() == null || intent.hasExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY) && intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)) {
                    checkInternet = true;
                } else {
                    connect2Server();
                }
            }
        }
    };

    private IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                finish();
                return;
            }
        }

        setContentView(R.layout.splash_screen);
        connect2Server();
    }

    private void connect2Server() {
        AQuery aQuery = new AQuery(this);
        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                checkInternet = false;

                if (object != null) {
                    try {
                        JSONObject response = object.getJSONObject(Constants.RESPONSE);
                        boolean success = response.getBoolean(Constants.SUCCESS);
                        if (success) {
                            AppUtils.saveAppConfig(SplashScreen.this, response);

                            String userId = PreferenceUtils.getString(Constants.USER_ID, SplashScreen.this, "");
                            boolean firstLaugh = TextUtils.isEmpty(userId);

                            Intent intent;
                            if (firstLaugh) {
                                // delete old avatar
                                File file = BumpUtils.getFile("avatar.jpg", true);
                                if (file.exists()) {
                                    file.delete();
                                }
                                intent = new Intent(SplashScreen.this, CreateProfileScreen.class);
                            } else {
                                intent = new Intent(SplashScreen.this, HomeScreen.class);

                                Intent data = getIntent();
                                if (data.hasExtra(EXTRA_TYPE)) {
                                    String type = data.getStringExtra(EXTRA_TYPE);
                                    switch (type) {
                                        case TYPE_CHAT:
                                            String id = data.getStringExtra(ChatBoxScreen.EXTRA_ID);
                                            String name = data.getStringExtra(ChatBoxScreen.EXTRA_NAME);
                                            String avatar = data.getStringExtra(ChatBoxScreen.EXTRA_AVATAR);

                                            intent.putExtra(EXTRA_TYPE, TYPE_CHAT);
                                            intent.putExtra(ChatBoxScreen.EXTRA_ID, id);
                                            intent.putExtra(ChatBoxScreen.EXTRA_NAME, name);
                                            intent.putExtra(ChatBoxScreen.EXTRA_AVATAR, avatar);
                                            break;
                                    }
                                }
                            }

                            startActivity(intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message = getString(R.string.msg_check_your_internet_connection);
                    Toast.makeText(SplashScreen.this, message, Toast.LENGTH_SHORT).show();

                    checkInternet = true;
                }
            }
        };

        String url = AppUtils.getAppConfigUrl();
        aQuery.ajax(url, JSONObject.class, callback);
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(receiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(receiver, filter);
    }
}
