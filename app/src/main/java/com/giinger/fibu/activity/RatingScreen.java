package com.giinger.fibu.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.faradaj.blurbehind.BlurBehind;
import com.giinger.fibu.R;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.Utils;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RatingScreen extends Activity {

    @OnClick(R.id.shareSmsLayout)
    public void shareSms() {
        Utils.sendSms(this);
    }

    @OnClick(R.id.shareEmailLayout)
    public void shareEmail() {
        String text = PreferenceUtils.getString(Constants.EMAIL_TEMPLATE, this, "");
        Utils.sendEmail(this, null, text);
    }

    @Bind(R.id.txtActionLeft)
    TextView txtActionLeft;

    @Bind(R.id.txtActionRight)
    TextView txtActionRight;

    private boolean reviewing = false, feedback = false;

    @OnClick(R.id.txtActionLeft)
    public void leftAction() {
        if (rated || feedback) {
            Log.e("Rating", "Send feedback");
            String email = PreferenceUtils.getString(Constants.EMAIL_FEEDBACK, this, "");
            Utils.sendEmail(this, null, null, email);
        } else if (reviewing) {
            Log.e("Rating", "Rating");

            Builders.Any.B request = Ion.with(this).load(AppUtils.API_CONFIRM_ACTIVITY);
            String userId = PreferenceUtils.getString(Constants.USER_ID, this, "");
            request.setBodyParameter("userId", userId);
            request.setBodyParameter("action", "rated");

            request.asJsonObject().setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    if (e != null || result == null) {
                        return;
                    }

                    JsonObject response = result.getAsJsonObject("response");
                    if (response != null) {
                        boolean success = response.getAsJsonPrimitive("success").getAsBoolean();
                        if (success) {
                            Log.e("Rating", "Updated");
                            PreferenceUtils.saveBoolean(Constants.RATED, RatingScreen.this, true);
                        }
                    }
                }
            });

            String appPackageName = getPackageName();

            try {
                String uri = getString(R.string.market_link, appPackageName);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
            } catch (ActivityNotFoundException e) {
                String uri = getString(R.string.market_web_link, appPackageName);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
            }

        } else {
            gotoReview();
        }
    }

    @OnClick(R.id.txtActionRight)
    public void rightAction() {
        if (rated) {
            finish();
        } else if (reviewing) {
            gotoThink();
        } else if (feedback) {
            gotoThink();
        } else {
            gotoFeedback();
        }
    }

    private void gotoFeedback() {
        feedback = true;
        txtMessage.setText(R.string.rating_improve_text);

        txtActionLeft.setText(R.string.rating_button_feedback);
        txtActionRight.setText(R.string.rating_button_no_thanks);
    }

    private void gotoReview() {
        reviewing = true;

        txtMessage.setText(R.string.rating_review_text);

        txtActionLeft.setText(R.string.rating_button_review);
        txtActionRight.setText(R.string.rating_button_no_thanks);
    }

    private void gotoThink() {
        feedback = false;
        reviewing = false;

        txtMessage.setText(R.string.rating_think_text);

        txtActionLeft.setText(R.string.rating_button_love);
        txtActionRight.setText(R.string.rating_button_improve);
    }

    @Bind(R.id.txtMessage)
    TextView txtMessage;

    private boolean rated;

    @OnClick(R.id.txtCancel)
    public void onCancel() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.rating_screen);

        int color = Color.parseColor("#B2E4EF");
        BlurBehind.getInstance()
                .withAlpha(50)
                .withFilterColor(color)
                .setBackground(this);

        ButterKnife.bind(this);

        rated = PreferenceUtils.getBoolean(Constants.RATED, this, false);
        if (rated) {
            gotoFeedback();
        }
    }

    @Override
    public void onBackPressed() {
        if (!rated && (reviewing || feedback)) {
            gotoThink();
        } else {
            super.onBackPressed();
        }
    }
}
