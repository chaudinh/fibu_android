package com.giinger.fibu.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.edmodo.cropper.CropImageView;
import com.giinger.fibu.R;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.Utils;

import java.io.File;

public class EditAvatarScreen extends Activity implements
        OnClickListener {

    private Button btnOk, btnExit;
    private CropImageView imgAvatar;
    private File file;
    private ProgressDialog pDialog;

    // Static final constants
    private static final int DEFAULT_ASPECT_RATIO_VALUES = 3;
    private static final String ASPECT_RATIO_X = "ASPECT_RATIO_X";
    private static final String ASPECT_RATIO_Y = "ASPECT_RATIO_Y";
    private static final int ON_TOUCH = 1;

    // Instance variables
    private int mAspectRatioX = DEFAULT_ASPECT_RATIO_VALUES;
    private int mAspectRatioY = DEFAULT_ASPECT_RATIO_VALUES;

    public Bitmap croppedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initLayout();

        if (getIntent().getExtras() != null
                && getIntent().getExtras().containsKey("path")) {
            String path = getIntent().getExtras().getString("path");

            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, option);
            int h = option.outHeight;
            int w = option.outWidth;

            int size = getInSampleSize(w, h);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = size;


//            if (w <= 2048 && h <= 2048) {
//                option.inSampleSize = 2;
//                bm = BitmapFactory.decodeFile(path,option);
//                imgAvatar.setImageBitmap(bm);
//                Log.e("normal", w + " - " + h);
//            } else {
//                BitmapFactory.Options options = new BitmapFactory.Options();
//                options.inSampleSize = 4;
            Bitmap ava = BitmapFactory.decodeFile(path, options);
            imgAvatar.setImageBitmap(ava);
            Log.e("QueANh_Edit_screen", "Dimen : w " + w + " - h : " + h + " insample : " + size);
//            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt(ASPECT_RATIO_X, mAspectRatioX);
        bundle.putInt(ASPECT_RATIO_Y, mAspectRatioY);
    }

    @Override
    protected void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        mAspectRatioX = bundle.getInt(ASPECT_RATIO_X);
        mAspectRatioY = bundle.getInt(ASPECT_RATIO_Y);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_exit:
                Constants.imgUri = null;
                finish();
                break;

            case R.id.btn_ok:
                cropImg();
                btnOk.setBackgroundResource(R.drawable.ava_ok_done);
                finish();
                break;
        }

    }

    public void initLayout() {
        setContentView(R.layout.edit_avatar_screen);

        btnOk = (Button) findViewById(R.id.btn_ok);
        btnExit = (Button) findViewById(R.id.btn_exit);
        imgAvatar = (CropImageView) findViewById(R.id.img_avatar);
        imgAvatar.setAspectRatio(DEFAULT_ASPECT_RATIO_VALUES,
                DEFAULT_ASPECT_RATIO_VALUES);
        imgAvatar.setGuidelines(0);
        imgAvatar.setFixedAspectRatio(true);
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth(); // deprecated
        int height = display.getHeight();
        Rect rec = new Rect(0, 0, width, height
                - (int) ((Utils.getInstance().convertDpToPixel(120,
                getApplicationContext()))));
        imgAvatar.setRect(rec);

        btnExit.setOnClickListener(this);
        btnOk.setOnClickListener(this);
    }

    public void cropImg() {
        croppedImage = imgAvatar.getCroppedCircleImage();
        Constants.imgUri = Utils.getImageUri(croppedImage, true);
        croppedImage.recycle();
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public int getInSampleSize(int w, int h) {
        int inSampleSize = 1;
        float mb = (w * h) / 1024000;

        if (mb > 4f)
            inSampleSize = 4;
        else if (mb > 3f)
            inSampleSize = 2;

        Log.e("QueANh_Edit_screen", "w : " + w + " h : " + h + " megapixel : " + mb + " insample : " + inSampleSize);

        return inSampleSize;
    }
}