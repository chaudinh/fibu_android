package com.giinger.fibu.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.giinger.fibu.R;
import com.giinger.fibu.adapter.PreviewPhotoAdapter;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.File;
import java.util.List;

public class PreviewPhoto extends BasePreviewScreen {

    public static final String EXTRA_PATHS = "photo_paths";
    public static final String EXTRA_DIRS = "photo_dirs";

    private ViewPager pager;
    private CirclePageIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initLayout() {
        setContentView(R.layout.preview_photo_screen);

        pager = (ViewPager) findViewById(R.id.pager);
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);

        final Intent intent = getIntent();
        final List<String> paths = intent.getStringArrayListExtra(EXTRA_PATHS);
        final List<Integer> dirs = intent.getIntegerArrayListExtra(EXTRA_DIRS);

        FragmentManager fragmentManager = getSupportFragmentManager();
        PreviewPhotoAdapter adapter = new PreviewPhotoAdapter(fragmentManager, paths, dirs);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(paths.size());

        indicator.setViewPager(pager);
        indicator.setRadius(15);
        indicator.setStrokeWidth(0);
        int grey = getResources().getColor(R.color.grey);
        int white = getResources().getColor(R.color.white);
        indicator.setFillColor(white);
        indicator.setPageColor(grey);

        final View share = findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);

                int currentItem = pager.getCurrentItem();
                String path = paths.get(currentItem);

                File file = new File(path);
                Uri uri = Uri.fromFile(file);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("image/*");
                Intent chooser = Intent.createChooser(shareIntent, "Share");
                startActivity(chooser);
            }
        });
    }
}
