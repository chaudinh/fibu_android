package com.giinger.fibu.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.giinger.fibu.R;
import com.giinger.fibu.adapter.FileMusicAdapter;
import com.giinger.fibu.model.FileItem;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.ExternalStorage;
import com.giinger.fibu.util.PreferenceUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class FileMusicScreen extends BaseFileScreen {

    public static int MAX_SELECTED_ITEMS = 5;

    private ListView lvChooseMusic;
    private FileMusicAdapter musicAdapter;
    private List<FileItem> musicData;

    private RelativeLayout layoutBumpAnimation;
    private RelativeLayout layoutAnimLeft;
    private RelativeLayout layoutAnimRight;

    private Animation animLeft;
    private Animation animRight;

    @Override
    public void notifyDataSetChange() {
        Collections.sort(musicData, new Comparator<FileItem>() {
            public int compare(FileItem f1, FileItem f2) {
                return f1.name.toLowerCase().compareTo(f2.name.toLowerCase());
            }
        });
        musicAdapter.notifyDataSetChanged();
    }

    @Override
    protected String getType() {
        return "music";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MAX_SELECTED_ITEMS = 5;
        String type = PreferenceUtils.getString(Constants.PURCHASED, this, "");
        if (type.equals(Constants.PURCHASE.BUY_ALL.type) || type.equals(Constants.PURCHASE.BUY_ONE.type)) {
            MAX_SELECTED_ITEMS = MAX_SELECTED_ITEMS + 2;
        }

        loadAllFiles();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            notifyDataSetChange();
        }
    };

    public static final File MUSICS = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);

    private void loadAllFiles() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e("Music", MUSICS.getAbsolutePath());
                listAllFile(MUSICS);

                File appFolder = BumpUtils.getAppFolder();
                listAllFile(appFolder);

                if (ExternalStorage.isAvailable()) {
                    File sdCard = new File("/storage/sdcard1/Music");
                    if (!sdCard.exists()) {
                        Map<String, File> externalLocations = ExternalStorage.getAllStorageLocations();
                        if (externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD) == null) {
                            sdCard = new File("/mnt/sdcard/ext_sd/Music");
                        } else {
                            sdCard = new File(externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD).toString() + "/Music");
                        }
                    }

                    listAllFile(sdCard);
                }

                handler.sendEmptyMessage(0);
            }
        }).start();
    }

    @Override
    protected void initLayout() {
        setContentView(R.layout.file_music_screen);

        lvChooseMusic = (ListView) findViewById(R.id.lvChooseMusic);

        layoutBumpAnimation = (RelativeLayout) findViewById(R.id.layout_bump_action);
        layoutAnimLeft = (RelativeLayout) findViewById(R.id.layout_anim_left);
        layoutAnimRight = (RelativeLayout) findViewById(R.id.layout_anim_right);

        animLeft = AnimationUtils.loadAnimation(this, R.anim.left_to_right);
        animRight = AnimationUtils.loadAnimation(this, R.anim.right_to_left);

        animLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Constants.isAnim = false;
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        musicData = new ArrayList<>();

        musicAdapter = new FileMusicAdapter(musicData);
        lvChooseMusic.setAdapter(musicAdapter);
        lvChooseMusic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FileItem child = musicData.get(position);
                FileMusicAdapter.ViewHolder viewHolder = (FileMusicAdapter.ViewHolder) view.getTag();
                if (child.isSelected) {
                    child.isSelected = false;
                    totalSize -= child.size;
                    onItemSelected(child);
                    viewHolder.imgSelected.setImageResource(R.drawable.not_select_icon);

                    if (getCountItemSelected() == 0) {
                        layoutBumpAnimation.setVisibility(View.GONE);
                        Constants.isAnim = true;
                    }
                } else {
                    long newSize = totalSize + child.size;
                    if (newSize > BaseFileScreen.MAX_SIZE) {
                        String message = getString(R.string.msg_file_selected_too_big, selected.size() < 1 ? "" : "s");
                        new AlertDialog.Builder(FileMusicScreen.this).setMessage(message).setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                        return;
                    }

                    int count = getCountItemSelected();
                    if (count < MAX_SELECTED_ITEMS) {
                        child.isSelected = true;
                        totalSize += child.size;
                        onItemSelected(child);
                        viewHolder.imgSelected.setImageResource(R.drawable.select_icon);

                        if (Constants.isAnim) {
                            animationBump();
                        }

                    } else {
                        Toast.makeText(FileMusicScreen.this, "Maximum files can select once time", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    protected void animationBump() {
        layoutBumpAnimation.setVisibility(View.VISIBLE);
        layoutAnimLeft.startAnimation(animLeft);
        layoutAnimRight.startAnimation(animRight);
    }

    @Override
    protected void goneLayoutAnimation() {
        layoutBumpAnimation.setVisibility(View.GONE);
        Constants.isAnim = true;
    }

    @Override
    protected String getSharedType() {
        return "music";
    }

    private void listAllFile(File root) {
        File[] files = root.listFiles();
        if (files == null) {
            return;
        }

        File cache = BumpUtils.getCacheFolder();
        if (root.getAbsolutePath().equals(cache.getAbsolutePath())) {
            return;
        }

        for (File file : files) {
            boolean isDir = file.isDirectory();
            if (isDir) {
                listAllFile(file);
            } else {
                String name = file.getName();
                boolean isMusic = name.toLowerCase().endsWith(".mp3");
                if (isMusic) {
                    String path = file.getAbsolutePath();
                    FileItem item = new FileItem(path, name, file.length());
                    musicData.add(item);
                }
            }
        }
    }
}
