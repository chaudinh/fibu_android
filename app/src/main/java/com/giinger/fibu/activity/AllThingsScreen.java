package com.giinger.fibu.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;

import com.android.vending.billing.IInAppBillingService;
import com.giinger.fibu.R;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.PreferenceUtils;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

public class AllThingsScreen extends Activity implements View.OnClickListener {

    public static final String EXTRA_USER_ID = "extra_user_id";
    public static final String HIDE = "autoHide";

    private IInAppBillingService iapService;
    private ServiceConnection iapConnection;

    private View popupLayout;
    private View txtBuyAll, txtBuyOne, txtNoThanks;

    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.all_things_screen);

        popupLayout = findViewById(R.id.popupLayout);

        txtBuyAll = findViewById(R.id.txtBuyAll);
        txtBuyOne = findViewById(R.id.txtBuyOne);

        txtNoThanks = findViewById(R.id.txtNoThanks);

        txtNoThanks.setOnClickListener(this);

        txtBuyAll.setOnClickListener(this);
        txtBuyOne.setOnClickListener(this);

        Intent intent = getIntent();
        userId = intent.getStringExtra(EXTRA_USER_ID);

        bindIAPService();
    }

    @Override
    public void onBackPressed() {

        if (popupLayout.isShown()) {
            popupLayout.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    private void bindIAPService() {
        iapConnection = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                iapService = null;
            }

            @Override
            public void onServiceConnected(ComponentName name,
                                           IBinder service) {
                iapService = IInAppBillingService.Stub.asInterface(service);
            }
        };

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, iapConnection, Context.BIND_AUTO_CREATE);
    }

    private static final int RC_PURCHASE_ONE = 1001;
    private static final int RC_PURCHASE_ALL = 1002;

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RC_PURCHASE_ONE:
            case RC_PURCHASE_ALL:
                switch (resultCode) {
                    case RESULT_OK:
                        if (userId != null) {
                            String url = AppUtils.getConfirmUrl(this);
                            Builders.Any.B request = Ion.with(this).load(url);
                            request.setBodyParameter("userId", userId);

                            final String type = requestCode == RC_PURCHASE_ALL ? Constants.PURCHASE.BUY_ALL.type : Constants.PURCHASE.BUY_ONE.type;
                            request.setBodyParameter("action", type);

                            request.asJsonObject().setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {
                                    Log.e("Purchase", result + "");

                                    if (e == null && result != null) {
                                        PreferenceUtils.saveString(Constants.PURCHASED, AllThingsScreen.this, type);

                                        if (type.equals(Constants.PURCHASE.BUY_ALL.type)) {
                                            PreferenceUtils.saveInt(Constants.MAX_SELECTED_CONTACT, getApplicationContext(), 10000);
                                        } else if (type.equals(Constants.PURCHASE.BUY_ONE.type)) {
                                            PreferenceUtils.saveInt(Constants.MAX_SELECTED_CONTACT, getApplicationContext(), 15);
                                        }

                                        setResult(RESULT_OK);
                                        finish();
                                    }
                                }
                            });
                        }
                        break;
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (iapService != null) {
            unbindService(iapConnection);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.txtNoThanks:
                finish();
                break;
            case R.id.txtGoPro:
                popupLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.txtBuyAll:
            case R.id.txtBuyOne:
                boolean isBuyAll = id == R.id.txtBuyAll;
                int requestCode = isBuyAll ? RC_PURCHASE_ALL : RC_PURCHASE_ONE;
//                String purchaseType = isBuyAll ? Constants.PURCHASE.BUY_ALL.type : Constants.PURCHASE.BUY_ONE.type;
                String sku = PreferenceUtils.getString(Constants.IAP_ITEM, this, "");

                try {
                    String packageName = getPackageName();
                    String payload = "inapp_purchase_fibu_v2";
                    String type = "inapp";

                    Bundle buyIntent = iapService.getBuyIntent(3, packageName, sku, type, payload);

                    int responseCode = buyIntent.getInt("RESPONSE_CODE");
                    if (responseCode == 0) {
                        PendingIntent intent = buyIntent.getParcelable("BUY_INTENT");

                        IntentSender sender = intent.getIntentSender();
                        startIntentSenderForResult(sender, requestCode, new Intent(), 0, 0, 0);
                    } else if (responseCode == 7) { //TODO: consume for testing
                        iapService.consumePurchase(3, packageName,
                                "inapp:" + packageName + ":"
                                        + sku);

                        onClick(view);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.txtRestore:
                break;
        }
    }
}
