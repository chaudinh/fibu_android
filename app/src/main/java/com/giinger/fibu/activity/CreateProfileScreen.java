package com.giinger.fibu.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;

import com.giinger.fibu.R;
import com.giinger.fibu.adapter.CreateProfileScreenAdapter;
import com.giinger.fibu.element.CustomViewPager;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.OnRefreshListener;
import com.giinger.fibu.util.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CreateProfileScreen extends FragmentActivity {

    public static CustomViewPager pager;
    private File tempImageFile;
    ProgressDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_profile_screen);
        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading...");
        loadingDialog.setCancelable(false);

        pager = (CustomViewPager) findViewById(R.id.pager);
        pager.setSwipe(false);
        FragmentManager fragmentManager = getSupportFragmentManager();
        CreateProfileScreenAdapter adapter = new CreateProfileScreenAdapter(fragmentManager);
        pager.setAdapter(adapter);
    }

    public interface SetAvatarCallBack {
        void setImageAvatar(String path);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!TextUtils.isEmpty(Constants.imgUri)) {
            Log.e("QueANh", "set image avatar");
            FragmentManager fragmentManager = getSupportFragmentManager();

            int currentItem = pager.getCurrentItem();
            Fragment fragment = fragmentManager.findFragmentByTag("android:switcher:" + R.id.pager + ":" + currentItem);
            if (fragment != null && Constants.imgUri != null && fragment instanceof OnRefreshListener) {
                ((OnRefreshListener) fragment).onSetImageAvatar(Constants.imgUri);
            }
        }
    }

    public static void nextPage(int position) {
        pager.setCurrentItem(position);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri galleryUri = data.getData();
                String path = Utils.getInstance().getRealPathFromURI(galleryUri, CreateProfileScreen.this);
                if (path != null) {
                    Intent i = new Intent(CreateProfileScreen.this, EditAvatarScreen.class);
                    i.putExtra("path", path);
                    i.putExtra("type", "gallery");
                    startActivity(i);
                } else {
                    new PickPhotoTask().execute(galleryUri);
                }

            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private class PickPhotoTask extends AsyncTask<Uri, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!isFinishing() && loadingDialog != null && !loadingDialog.isShowing()) {
                loadingDialog.show();
            }
        }

        @Override
        protected Boolean doInBackground(Uri... params) {
            Uri pickedUri = params[0];
            try {
                // copy image file to temp image file
                tempImageFile = new File(getFilesDir(), "temp.jpg");
                InputStream inputStream = getContentResolver().openInputStream(
                        pickedUri);
                FileOutputStream fileOutputStream = new FileOutputStream(
                        tempImageFile);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
                // prepare pick image file to avoid outOfMemories and wrong
                // rotation
                preparePickImageFile();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (loadingDialog != null && loadingDialog.isShowing()) {
                loadingDialog.dismiss();

                Intent i = new Intent(CreateProfileScreen.this, EditAvatarScreen.class);
                i.putExtra("path", tempImageFile.getAbsolutePath());
                i.putExtra("type", "gallery");
                startActivity(i);
            }
        }
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
        output.flush();
        output.close();
    }

    private File preparePickImageFile() {
        int targetW = Utils.getScreenWidth(CreateProfileScreen.this);
        int targetH = Utils
                .getScreenHeight(CreateProfileScreen.this);

        Bitmap bitmap = null;
        bitmap = Utils.getImageBitmapFromPath(tempImageFile.getAbsolutePath());
        try {
            ExifInterface exif = new ExifInterface(
                    tempImageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            bitmap = Utils.rotateBitmap(bitmap, orientation);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Utils.createImageFile(tempImageFile.getAbsolutePath(),
                Utils.convertBitmapToByteArray(bitmap));
    }
}
