package com.giinger.fibu.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.giinger.fibu.R;
import com.giinger.fibu.adapter.FriendAdapter;
import com.giinger.fibu.fragment.ExtraPowerFragment;
import com.giinger.fibu.fragment.HomeFragment;
import com.giinger.fibu.fragment.ProfileFragment;
import com.giinger.fibu.model.ChatBoxItem;
import com.giinger.fibu.model.FriendItem;
import com.giinger.fibu.service.GPSTracker;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.RushOrmManager;
import com.giinger.fibu.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeScreen extends FragmentActivity implements AdapterView.OnItemClickListener, DrawerLayout.DrawerListener, AdapterView.OnItemLongClickListener {

    public static HomeScreen instance;

    private DrawerLayout drawerLayout;
    private View layoutEmpty, popupAction;
    private ListView lvFriends;
    private FriendAdapter adapter;
    private List<FriendItem> data;
    private List<FriendItem> newData = new ArrayList<>();
    private File tempImageFile;
    private ProgressDialog loadingDialog;

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        popupAction.setVisibility(View.VISIBLE);

        final FriendItem friend = data.get(position);

        popupActionHolder.txtName.setText(friend.name);

        String avatarUrl = AppUtils.getAvatarByName(HomeScreen.this, friend.avatar);
        AppUtils.loadAvatar(this, popupActionHolder.imgAvatar, avatarUrl, false);

        popupActionHolder.txtRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionOnLongClick(position, friend.id, "remove");
            }
        });

        popupActionHolder.txtBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionOnLongClick(position, friend.id, "block");
            }
        });

        return true;
    }

    private void actionOnLongClick(final int position, final String friendId, final String type) {
        popupAction.setVisibility(View.GONE);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String message = null, title = null;
        switch (type) {
            case "block":
                title = getString(R.string.title_confirm_block_friend);
                message = getString(R.string.msg_confirm_block_friend);
                break;
            case "remove":
                title = getString(R.string.title_confirm_remove_friend);
                message = getString(R.string.msg_confirm_remove_friend);
                break;
        }

        builder.setTitle(title).setMessage(message);
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AQuery aQuery = new AQuery(HomeScreen.this);

                AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        Log.e(type, object + " " + PreferenceUtils.getString(Constants.USER_ID, HomeScreen.this, "") + " " + friendId);
                        if (object != null) {
                            try {
                                JSONObject response = object.getJSONObject(Constants.RESPONSE);
                                boolean success = response.getBoolean(Constants.SUCCESS);
                                if (success) {
                                    data.remove(position);
                                    adapter.notifyDataSetChanged();

                                    if (data.size() == 0) {
                                        layoutEmpty.setVisibility(View.VISIBLE);
                                        lvFriends.setVisibility(View.GONE);
                                    }

                                    RushOrmManager.getInstance().deleteChatWithFriend(friendId);
                                    return;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        Toast.makeText(HomeScreen.this, "Can't " + type + " this friend", Toast.LENGTH_LONG).show();
                    }
                };

                Map<String, String> params = new HashMap<>();
                params.put("user_id", PreferenceUtils.getString(Constants.USER_ID, HomeScreen.this, ""));

                String url = null;
                switch (type) {
                    case "block":
                        params.put("block_id", friendId);
                        params.put("action", "block");
                        url = AppUtils.getBlockUserUrl(HomeScreen.this);
                        break;
                    case "remove":
                        params.put("remove_id", friendId);
                        url = AppUtils.getRemoveUserUrl(HomeScreen.this);
                        break;
                }

                aQuery.ajax(url, params, JSONObject.class, callback);
            }
        });

        builder.show();
    }

    private class PopupActionHolder {

        public ImageView imgAvatar;
        public TextView txtName;
        public View txtRemove, txtBlock;

        public PopupActionHolder(final View view) {
            imgAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
            txtName = (TextView) view.findViewById(R.id.txtName);
            txtRemove = view.findViewById(R.id.txtRemove);
            txtBlock = view.findViewById(R.id.txtBlock);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    view.setVisibility(View.GONE);
                }
            });
        }

    }

    private PopupActionHolder popupActionHolder;
    public HomeFragment homeFragment;

    public View txtBumpFailure;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);

        BaseFileScreen.MAX_SIZE = 15 * 1024 * 1024;
        String typePur = PreferenceUtils.getString(Constants.PURCHASED, this, "");
        if (typePur.equals(Constants.PURCHASE.BUY_ALL.type) || typePur.equals(Constants.PURCHASE.BUY_ONE.type)) {
            BaseFileScreen.MAX_SIZE = 30 * 1024 * 1024;
        }
//        RushOrmManager.getInstance().deleteTableChatThread();
        if (PreferenceUtils.getBoolean("collection_info", getApplicationContext(), true)) {
            sendInfoDevice();
            PreferenceUtils.saveBoolean("collection_info", getApplicationContext(), false);
        }


        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading...");
        loadingDialog.setCancelable(false);

        txtBumpFailure = findViewById(R.id.txtBumpFailure);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        drawerLayout.setDrawerListener(this);

        popupAction = findViewById(R.id.popupAction);
        popupActionHolder = new PopupActionHolder(popupAction);

        lvFriends = (ListView) findViewById(R.id.listFriends);
        data = new ArrayList<>();
//        data.add(new FriendItem("Anh Nguyen", "Card", "10:10", null, null));
        adapter = new FriendAdapter(data);
        lvFriends.setAdapter(adapter);
        lvFriends.setOnItemClickListener(this);
        lvFriends.setOnItemLongClickListener(this);

        homeFragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, homeFragment).commit();

        layoutEmpty = findViewById(R.id.layoutEmpty);

        Intent gpsIntent = new Intent(this, GPSTracker.class);
        startService(gpsIntent);

        Intent intent = getIntent();
        if (intent.hasExtra(SplashScreen.EXTRA_TYPE)) {
            String type = intent.getStringExtra(SplashScreen.EXTRA_TYPE);
            switch (type) {
                case SplashScreen.TYPE_CHAT:
                    String id = intent.getStringExtra(ChatBoxScreen.EXTRA_ID);
                    String name = intent.getStringExtra(ChatBoxScreen.EXTRA_NAME);
                    String avatar = intent.getStringExtra(ChatBoxScreen.EXTRA_AVATAR);

                    Intent chatIntent = new Intent(this, ChatBoxScreen.class);
                    chatIntent.putExtra(SplashScreen.EXTRA_TYPE, SplashScreen.TYPE_CHAT);
                    chatIntent.putExtra(ChatBoxScreen.EXTRA_ID, id);
                    chatIntent.putExtra(ChatBoxScreen.EXTRA_NAME, name);
                    chatIntent.putExtra(ChatBoxScreen.EXTRA_AVATAR, avatar);

                    startActivity(chatIntent);
                    break;
            }
        }

        instance = this;
//        getListFriends();
    }

    public boolean showBumpFailure() {
        boolean drawerOpen = drawerLayout.isDrawerOpen(GravityCompat.START);
        if (drawerOpen) {

            txtBumpFailure.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    txtBumpFailure.setVisibility(View.GONE);
                }
            }, 3000);

            return true;
        }

        return false;
    }

    private void showEmptyScreen() {
        layoutEmpty.setVisibility(View.VISIBLE);
        lvFriends.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        boolean drawerOpen = drawerLayout.isDrawerOpen(GravityCompat.START);
        if (drawerOpen) {
            closeDrawer();
            return;
        }

        View settingPopup = homeFragment.getSettingPopup();
        if (settingPopup.getVisibility() == View.VISIBLE) {
            settingPopup.setVisibility(View.GONE);
            return;
        }

        Fragment fragment = homeFragment.getCurrentFragment();
        if (fragment instanceof ExtraPowerFragment) {
            boolean back = ((ExtraPowerFragment) fragment).hidePopupLayout();
            if (back) {
                return;
            }
        } else if (fragment instanceof ProfileFragment) {
            ProfileFragment profileFragment = (ProfileFragment) fragment;
            if (profileFragment.isSelectFieldMode) {
                profileFragment.showSelectFieldView(false);
                return;
            } else if (profileFragment.isEditMode) {
                profileFragment.unselectedAdditionalField();
                profileFragment.showEditView(false);
                profileFragment.initView();

                homeFragment.layoutBumpAnimation.setVisibility(View.VISIBLE);
                Constants.canBump = true;
                return;
            }
        }

        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        getListFriends();

        if (!TextUtils.isEmpty(Constants.imgUri) && ProfileFragment.imgAvatar != null && ProfileFragment.imgAvatar4Edit != null) {
            Log.e("QueANh", "set image avatar");

//            Uri imgUri = Uri.parse(Constants.imgUri);
            Bitmap bitmap = BitmapFactory.decodeFile(Constants.imgUri);
            ProfileFragment.imgAvatar.setImageBitmap(bitmap);
            ProfileFragment.imgAvatar4Edit.setImageBitmap(bitmap);

//            String path = Utils.getInstance().getRealPathFromURI(imgUri, this);
            File file = new File(Constants.imgUri);
            byte[] data = Utils.getInstance().getFileData(file);
            Utils.getInstance().postAvatar(this, data);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        instance = null;
    }

    private void getListFriends() {
        AQuery aQuery = new AQuery(this);
        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                Log.e("Friends", object + "");
                int countBadge = 0;
                if (object != null) {
                    try {
                        JSONObject response = object.getJSONObject(Constants.RESPONSE);
                        boolean success = response.getBoolean(Constants.SUCCESS);
                        if (success) {
                            JSONArray activities = response.getJSONArray("activities");
                            newData.clear();
                            data.clear();

                            if (activities.length() > 0) {
                                boolean isRestoreAccount = PreferenceUtils.getBoolean(Constants.IS_RESTORE_ACCOUNT, HomeScreen.this, false);

                                for (int i = 0; i < activities.length(); i++) {
                                    JSONObject item = activities.getJSONObject(i);
                                    FriendItem friend = new FriendItem(item);
                                    newData.add(friend);
                                    countBadge = countBadge + Integer.parseInt(friend.badge);

                                    if (isRestoreAccount) {
                                        loadChatHistoryFromServer(friend.id);
                                    }

                                    loadFriendProfile(friend.id);
                                }

                                PreferenceUtils.saveBoolean(Constants.IS_RESTORE_ACCOUNT, HomeScreen.this, false);
                                data.addAll(newData);
                            }

                            if (data.size() > 0) {
                                layoutEmpty.setVisibility(View.GONE);
                                lvFriends.setVisibility(View.VISIBLE);
                            } else {
                                layoutEmpty.setVisibility(View.VISIBLE);
                                lvFriends.setVisibility(View.GONE);
                            }

                            adapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                PreferenceUtils.saveInt(Constants.COUNT_BADGE, getApplicationContext(), countBadge);
                Intent intent = new Intent(HomeFragment.ACTION);
                intent.putExtra(HomeFragment.EXTRA_COUNT_BADGE, countBadge);
                LocalBroadcastManager.getInstance(HomeScreen.this).sendBroadcast(intent);
            }
        };

        String url = AppUtils.getFriendsActivityUrl(this, PreferenceUtils.getString(Constants.USER_ID, this, ""));
        aQuery.ajax(url, JSONObject.class, callback);
    }

    private void loadFriendProfile(final String friendId) {
        AQuery aQuery = new AQuery(this);
        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                Log.e("Friend", object + "");
                if (object != null) {
                    try {
                        JSONObject response = object.getJSONObject(Constants.RESPONSE);
                        boolean success = response.getBoolean(Constants.SUCCESS);
                        if (success) {
                            Log.e("lu_friend_id", friendId + "");
                            ChatBoxItem item = RushOrmManager.getInstance().getSingleChatItem("friend" + friendId);

                            if (item == null) {
                                return;
                            }

                            RushOrmManager.getInstance().deleteItem(item);

                            //update profile friend
                            JSONObject friendProfile = new JSONObject();

                            friendProfile.put("id", "friend" + friendId);
                            friendProfile.put("type", "profile");
                            friendProfile.put("gmtTimestamp", item.timeStamp);
                            friendProfile.put("seen", "true");
                            friendProfile.put("liked", "false");
                            friendProfile.put("side", "friend");
                            friendProfile.put("type", "bump");

                            JSONObject friendData = response.getJSONObject("data");

                            JSONObject profile = friendData.put("id", "friend" + friendId);
                            profile.put("type", "profile");

                            friendProfile.put("message", profile);

                            item.setJsonData(friendProfile);
                            RushOrmManager.getInstance().storeSingleChatItem(item);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        String url = AppUtils.getViewProfileUrl(this, friendId);
        aQuery.ajax(url, JSONObject.class, callback);
    }

    public void loadChatHistoryFromServer(final String friendId) {
        AQuery aQuery = new AQuery(this);

        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    try {
                        JSONObject response = object.getJSONObject(Constants.RESPONSE);

                        JSONArray items = response.getJSONArray("items");

                        int size = items.length();

                        for (int i = 0; i < size; i++) {
                            JSONObject item = items.getJSONObject(i);

                            String chatType = item.getString("type");
                            switch (chatType) {
                                case "bump":
                                case "share":
                                    String message = item.getString("message");
                                    JSONObject obj = new JSONObject(message);
                                    String bumpType = obj.getString("type");
                                    if ("profile".equals(bumpType)) {
                                        JSONObject profile = obj.getJSONObject("profile");
                                        JSONArray array = new JSONArray();
                                        array.put(profile);
                                        obj.put("items", array);
                                    }

                                    String serverFile = obj.optString("serverFile", null);
                                    String sf = item.optString("server_file", "null");
                                    if (!sf.isEmpty() && !sf.equals("null")) {
                                        serverFile = sf;
                                    }

                                    int countCt = 0;
                                    if ("contact".equals(bumpType)) {
                                        if ("share".equals(bumpType)) {
                                            countCt = 1;
                                        } else {
                                            String count = obj.getString("count");
                                            if ("all".equals(count)) {
                                                countCt = Integer.MAX_VALUE;
                                            } else {
                                                countCt = Integer.parseInt(count);
                                            }
                                        }
                                    }

                                    if (obj.has("items") && !"photo".equals(bumpType) && !"location".equals(bumpType)) {
                                        JSONArray array = obj.getJSONArray("items");

                                        int length = countCt > 5 ? 1 : array.length();

                                        for (int j = 0; j < length; j++) {
                                            JSONObject element = array.getJSONObject(j);
                                            String[] names = {"id", "side", "type", "seen", "liked", "time", "gmtTimestamp"};
                                            JSONObject clone = new JSONObject(item, names);
                                            JSONObject json = new JSONObject();
                                            String messageType = obj.getString("type");
                                            json.put("type", messageType);
                                            json.put("message", element);
                                            json.put("serverFile", serverFile);
                                            clone.put("message", json);

                                            ChatBoxItem chatItem = new ChatBoxItem(friendId);
                                            chatItem.setJsonData(clone);
                                            chatItem.count = j;

                                            if (countCt > 5) {

                                                boolean isSender = chatItem.isSender;
                                                String msg = isSender ? "Send " : "Received ";
                                                if (countCt == Integer.MAX_VALUE) {
                                                    msg += "all contacts";
                                                } else {
                                                    msg += countCt + " contacts";
                                                }

                                                clone.put("message", msg);
                                                chatItem.setJsonData(clone);
                                                chatItem.count = -1;
                                            }

                                            if (!"profile".equals(bumpType)) {
                                                RushOrmManager.getInstance().storeSingleChatItem(chatItem);
                                                ChatBoxScreen.saveContactItem(HomeScreen.this, chatItem);
                                            }
                                        }
                                    } else {

                                        ChatBoxItem chatItem = new ChatBoxItem(friendId);
                                        chatItem.setJsonData(item);

                                        RushOrmManager.getInstance().storeSingleChatItem(chatItem);
                                    }
                                    break;
                                case "message":

                                    ChatBoxItem chatItem = new ChatBoxItem(friendId);
                                    chatItem.setJsonData(item);

                                    RushOrmManager.getInstance().storeSingleChatItem(chatItem);

                                    break;
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        String url = PreferenceUtils.getString(Constants.ROOT_URL, this, AppUtils.ROOT_URL) + AppUtils.API_CHAT_THREAD
                + "?type=all&user_id=" + PreferenceUtils.getString(Constants.USER_ID, this, "") + "&friend_id=" + friendId;
        aQuery.ajax(url, JSONObject.class, callback);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FriendItem item = data.get(position);

        Intent intent = new Intent(this, ChatBoxScreen.class);
        intent.putExtra(ChatBoxScreen.EXTRA_ID, item.id);
        intent.putExtra(ChatBoxScreen.EXTRA_NAME, item.name);
        intent.putExtra(ChatBoxScreen.EXTRA_AVATAR, item.avatar);
        intent.putExtra(ChatBoxScreen.EXTRA_BADGE, Integer.parseInt(item.badge));
        startActivity(intent);

//        closeDrawer();
    }

    public void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri galleryUri = data.getData();
                String path = Utils.getInstance().getRealPathFromURI(galleryUri, HomeScreen.this);
                if (path != null) {
                    Intent i = new Intent(HomeScreen.this, EditAvatarScreen.class);
                    i.putExtra("path", path);
                    i.putExtra("type", "gallery");
                    startActivity(i);
                } else {
                    new PickPhotoTask().execute(galleryUri);
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    public void lockDrawer(boolean lock) {
        drawerLayout.setDrawerLockMode(lock ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void onDrawerOpened(View drawerView) {
//        Log.e("Bump", "Disable");
//        Constants.canBump = false;
        getListFriends();
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        Log.e("Bump", "Enable");
//        Constants.canBump = true;
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    public void lockNavigation(boolean isLock) {
        if (isLock) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    //profile photo
    private class PickPhotoTask extends AsyncTask<Uri, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!isFinishing() && loadingDialog != null && !loadingDialog.isShowing()) {
                loadingDialog.show();
            }
        }

        @Override
        protected Boolean doInBackground(Uri... params) {
            Uri pickedUri = params[0];
            try {
                // copy image file to temp image file
                tempImageFile = new File(getFilesDir(), "temp.jpg");
                InputStream inputStream = getContentResolver().openInputStream(
                        pickedUri);
                FileOutputStream fileOutputStream = new FileOutputStream(
                        tempImageFile);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
                // prepare pick image file to avoid outOfMemories and wrong
                // rotation
                preparePickImageFile();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (loadingDialog != null && loadingDialog.isShowing()) {
                loadingDialog.dismiss();

                Intent i = new Intent(HomeScreen.this, EditAvatarScreen.class);
                i.putExtra("path", tempImageFile.getAbsolutePath());
                i.putExtra("type", "gallery");
                startActivity(i);
            }
        }
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
        output.flush();
        output.close();
    }

    private File preparePickImageFile() {
        int targetW = Utils.getScreenWidth(HomeScreen.this);
        int targetH = Utils
                .getScreenHeight(HomeScreen.this);

        Bitmap bitmap = null;
        bitmap = Utils.getImageBitmapFromPath(tempImageFile.getAbsolutePath());
        try {
            ExifInterface exif = new ExifInterface(
                    tempImageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            bitmap = Utils.rotateBitmap(bitmap, orientation);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Utils.createImageFile(tempImageFile.getAbsolutePath(),
                Utils.convertBitmapToByteArray(bitmap));
    }

    public void sendInfoDevice() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                AQuery aQuery = new AQuery(HomeScreen.this);
                Map<String, String> params = new HashMap<>();

                String model = Utils.getInstance().getNameModel();
                String deveiceId = Utils.getInstance().getDeviceId(getApplicationContext());
                String osVersion = Utils.getInstance().getOSVersion();
                params.put("user_id", PreferenceUtils.getString(Constants.USER_ID, HomeScreen.this, ""));
                params.put("os_type", "android");
                params.put("model", model);
                params.put("device_id", deveiceId);
                params.put("os_version", osVersion);

                AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, final JSONObject object, AjaxStatus status) {
                        if (object != null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Lu", object.toString());
                                }
                            });

                        }
                    }
                };

                String url = AppUtils.getCollectionInfoUrl(getApplicationContext());
                aQuery.ajax(url, params, JSONObject.class, callback);
            }
        });

        thread.start();
    }

    protected ProgressDialog mProgressDialog;

    public void showProgressDialog(boolean isCancel) {
        showWait(getResources().getString(R.string.text_processing), isCancel);
    }

    public void showLoadingDialog(boolean isCancel) {
        showWait(getResources().getString(R.string.text_loading), isCancel);
    }

    public void showWait(String message, boolean isCancel) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            closeProgressDialog();
        }
        if (!isFinishing()) {
            mProgressDialog = ProgressDialog.show(this, "", message, true, true);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setCancelable(isCancel);
        }
    }

    public void closeProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
