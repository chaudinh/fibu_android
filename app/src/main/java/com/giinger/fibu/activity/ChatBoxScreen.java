package com.giinger.fibu.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.MediaStore.Images;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.faradaj.blurbehind.BlurBehind;
import com.faradaj.blurbehind.OnBlurCompleteListener;
import com.giinger.fibu.R;
import com.giinger.fibu.adapter.ChatBoxAdapter;
import com.giinger.fibu.adapter.TabsAdapter;
import com.giinger.fibu.element.CustomEditText;
import com.giinger.fibu.element.PopupBumpFailHolder;
import com.giinger.fibu.element.PopupConnectingHolder;
import com.giinger.fibu.element.PopupMultipleConnectedHolder;
import com.giinger.fibu.fragment.ContactFragment;
import com.giinger.fibu.fragment.FileFragment;
import com.giinger.fibu.fragment.SharingBarFragment;
import com.giinger.fibu.model.ChatBoxItem;
import com.giinger.fibu.model.ContactItem;
import com.giinger.fibu.model.FileItem;
import com.giinger.fibu.model.Friend;
import com.giinger.fibu.model.PhotoItem;
import com.giinger.fibu.receiver.ParseReceiver;
import com.giinger.fibu.service.GPSTracker;
import com.giinger.fibu.service.UpDownLoadService;
import com.giinger.fibu.util.AccelerometerManager;
import com.giinger.fibu.util.AppUtils;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.EmojiMapUtil;
import com.giinger.fibu.util.OnItemSharing;
import com.giinger.fibu.util.PreferenceUtils;
import com.giinger.fibu.util.RushOrmManager;
import com.giinger.fibu.util.Utils;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import github.ankushsachdeva.emojicon.EmojiconGridView;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.emoji.Emojicon;

public class ChatBoxScreen extends FragmentActivity implements View.OnClickListener, OnItemSharing, SwipeRefreshLayout.OnRefreshListener, AccelerometerManager.AccelerometerListener, BumpUtils.ConnectConfirmCallback {

    public static ChatBoxScreen instance;

    private PopupProfileHolder popupProfileHolder;
    private View layoutBack, layoutPopupProfile;
    private String friendId, avatarUrl;
    private ListView lvChatBox;
    private List<ChatBoxItem> data;
    private ChatBoxAdapter adapter;
    private ImageView imgLeftAction, imgRightAction;
    private CustomEditText txtMessage;
    private FrameLayout contentLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
//    private RelativeLayout layoutProgress;

    public static final String EXTRA_ID = "friend_id";
    public static final String EXTRA_NAME = "friend_name";
    public static final String EXTRA_AVATAR = "friend_avatar";
    public static final String EXTRA_BADGE = "badge_chat";
    public long lastTimeStamp = 0;
    public boolean loadMore = true;
    public static boolean removeLoading = false;

    private int badge = 0;

    private JSONObject jsonFriend = new JSONObject();

    public String getFriendId() {
        return friendId;
    }


    public void addToLoadingList(ChatBoxItem item) {
        item.isLoading = true;
        loadingItems.put(item.id, item);

        boolean inProgressing = UpDownLoadService.isInProgressing(item.id);
        if (inProgressing) {
            int progress = UpDownLoadService.getProgress(item.id);
            if (progress > item.progress) {
                item.progress = progress;
            }
        } else if (item.serverFile != null) {
            Intent download = new Intent(this, UpDownLoadService.class);
            download.putExtra(UpDownLoadService.TYPE, UpDownLoadService.TYPE_DOWNLOAD);
            download.putExtra(UpDownLoadService.EXTRA_SIDE_IS_ME, item.isSender);
            download.putExtra(UpDownLoadService.EXTRA_DOWNLOAD_FILE, item.serverFile);
            download.putExtra(ChatBoxScreen.EXTRA_LOADING_ID, item.id);
            startService(download);
        }
    }

    private int indexOf(ChatBoxItem item) {
        int size = data.size();

        for (int i = 0; i < size; i++) {
            ChatBoxItem chatBoxItem = data.get(i);
            if (chatBoxItem.id != null && chatBoxItem.id.equals(item.id)) {
                return i;
            }
        }

        return -1;
    }

    private boolean checkCount(ChatBoxItem item) {
        for (ChatBoxItem i : data) {
            if (i.id != null && i.id.equals(item.id) && i.count == item.count) {
                return true;
            }
        }

        return false;
    }

    private boolean contains(ChatBoxItem item) {
        int index = indexOf(item);
        return index != -1 && checkCount(item);
    }

    public void addToListChat(String friendId, JSONObject obj) {
        ChatBoxItem item = new ChatBoxItem(friendId, obj);
        item.isLoading = item.isNeedDownload();

        boolean inList = contains(item);
        if (inList) {
            int position = indexOf(item);
            data.remove(position);
            data.add(position, item);
        } else {
            data.add(item);
        }

        if (item.isLoading) {
            item.progress = ChatBoxItem.PROGRESS_WAITING;
            loadingItems.put(item.id, item);
        }

        adapter.notifyDataSetChanged();

        if (!inList) {
            lvChatBox.smoothScrollToPosition(data.size() - 1);
        }

        String url = AppUtils.getChatThread(this, PreferenceUtils.getString(Constants.USER_ID, this, ""), friendId);
        new AQuery(this).ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                // mark read for new message
            }
        });
    }

    public static Map<String, ChatBoxItem> loadingItems = new HashMap<>();

    public static void addBumpItem(ChatBoxItem item) {
        loadingItems.put(item.id, item);
    }

    public static Map<ChatBoxItem, List<ChatBoxItem>> newItems = new HashMap<>();

    public static void updateSendItem(String key, File file, String message) {
        ChatBoxItem item = loadingItems.get(key);
        if (item != null) {
            String filePath = file == null ? null : file.getAbsolutePath();
            item.setResendInfo(filePath, message);
            item.save();
        }
    }

    public static String updateBumpItem(Context context, String key, String friendId, JSONObject data) {
        try {

            ChatBoxItem chatBoxItem = loadingItems.get(key);

            if (chatBoxItem == null) {
                return null;
            }

            String message = data.getString("message");
            JSONObject obj = new JSONObject(message);

            String type = obj.getString("type");
            int countCt = 0;
            if ("contact".equals(type)) {
                String count = obj.getString("count");
                if ("all".equals(count)) {
                    countCt = Integer.MAX_VALUE;
                } else {
                    countCt = Integer.parseInt(count);
                }
            }

//            if ("profile".equals(type)) {
//                JSONObject profile = obj.getJSONObject("profile");
//                JSONArray array = new JSONArray();
//                array.put(profile);
//                obj.put("items", array);
//            }

            List<ChatBoxItem> items = new ArrayList<>();
            JSONObject _data = null;
            if (obj.has("items") && !"photo".equals(type) && !"location".equals(type)) {
                JSONArray array = obj.getJSONArray("items");
                for (int j = 0; j < array.length(); j++) {
                    JSONObject element = array.getJSONObject(j);
                    String[] names = {"id", "serverFile", "seen", "liked", "chat_time"};
                    JSONObject clone = new JSONObject(data, names);
                    JSONObject json = new JSONObject();
                    String messageType = obj.getString("type");
                    json.put("type", messageType);
                    json.put("message", element);
                    clone.put("message", json);

                    if (_data == null) {
                        _data = clone;
                    } else {
                        ChatBoxItem item = new ChatBoxItem(friendId, chatBoxItem.isSender);
                        item.update(clone);
                        items.add(item);
                    }
                }
            }

            if (chatBoxItem.type == 5 || chatBoxItem.type == 3 && countCt > 0) {
                ChatBoxItem item = new ChatBoxItem(friendId, chatBoxItem.isSender);
                item.update(_data);

                saveContactItem(context, item);
            }

            if ("contact".equals(type) && countCt > 5) {
                try {

                    removeLoading = true;
                    JSONObject messageContact = new JSONObject();

                    boolean isSender = chatBoxItem.isSender;

                    String side = isSender ? "me" : "friend";
                    messageContact.put("side", side);

                    String msg = isSender ? "Send " : "Received ";
                    if (countCt == Integer.MAX_VALUE) {
                        msg += "all contacts";
                    } else {
                        msg += countCt + " contacts";
                    }

                    messageContact.put("message", msg);
                    messageContact.put("id", _data.getString("id"));
                    messageContact.put("liked", "0");
                    messageContact.put("type", "message");
                    messageContact.put("seen", "true");
                    messageContact.put("time", "");
                    messageContact.put("gmtTimestamp", _data.getString("chat_time"));
//                    dataMessageContact.put("data",messageContact);

                    ChatBoxItem itemContact = new ChatBoxItem();
                    itemContact.setFriendId(friendId);
                    itemContact.setJsonData(messageContact);
                    itemContact.count = -1;

                    RushOrmManager.getInstance().deleteItem(chatBoxItem);
                    RushOrmManager.getInstance().storeSingleChatItem(itemContact);

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            } else {
                chatBoxItem.update(_data == null ? data : _data);
                chatBoxItem.save();
            }


            int count = items.size();
            if (count > 0) {
                for (int i = 0; i < count; i++) {
                    ChatBoxItem item = items.get(i);
                    item.count = i + 1;
                }

                List<ChatBoxItem> saved = new ArrayList<>();
                saved.addAll(items);
                saveContactItems(context, saved, countCt);

                if ("contact".equals(type) && countCt > 5) {
                    JSONObject messageContact = new JSONObject();
                    boolean isSender = chatBoxItem.isSender;
                    String side = isSender ? "me" : "friend";
                    messageContact.put("side", side);

                    String msg = isSender ? "Send " : "Received ";
                    if (countCt == Integer.MAX_VALUE) {
                        msg += "all contacts";
                    } else {
                        msg += countCt + " contacts";
                    }

                    messageContact.put("message", msg);

                    messageContact.put("id", _data.getString("id"));
                    messageContact.put("liked", "false");
                    messageContact.put("type", "message");
                    messageContact.put("seen", "true");
                    messageContact.put("time", "Just Now");
                    messageContact.put("gmtTimestamp", _data.getString("chat_time"));

                    ChatBoxItem itemContact = new ChatBoxItem();
                    itemContact.setFriendId(friendId);
                    itemContact.setJsonData(messageContact);
//
                    items.clear();
                    items.add(itemContact);
                    newItems.clear();
                }

                newItems.put(chatBoxItem, items);
                if (countCt <= 5) {
                    RushOrmManager.getInstance().storeListChatItems(items);
                }
            }

            return chatBoxItem.id;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static void saveContactItems(final Context context, final List<ChatBoxItem> items, final int countCt) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < items.size(); i++) {
                    ChatBoxItem item = items.get(i);
                    if (item.type == 5 || item.type == 3 && countCt > 5) {
                        try {
                            saveContactItem(context, item);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

    public static void saveContactItem(final Context context, final ChatBoxItem chatItem) throws JSONException {
        if (!chatItem.isSender && chatItem.type == 5) {
            JSONObject dt = chatItem.data;
            if (dt.has("name")) {
                dt = new JSONObject();
                JSONArray phones = chatItem.data.getJSONArray("phones");
                JSONArray emails = chatItem.data.getJSONArray("emails");
                String name = chatItem.data.getString("name");
                dt.put("first_name", name);

                if (phones.length() > 0) {
                    String phone = phones.getString(0);
                    if (!TextUtils.isEmpty(phone)) {
                        dt.put("mobile", phone);
                    }
                }

                if (emails.length() > 0) {
                    String email = emails.getString(0);
                    if (!TextUtils.isEmpty(email)) {
                        dt.put("email", email);
                    }
                }
            }

            String avatar = dt.has("avatar") ? dt.getString("avatar") : null;
            String avatarUrl = AppUtils.getAvatarByName(context, avatar);

            Log.e("Save contact", dt + "");

            if (avatarUrl == null) {
                saveContact(context, dt, null);
            } else {
                final JSONObject finalData = dt;
                BitmapAjaxCallback callback = new BitmapAjaxCallback() {
                    @Override
                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                        saveContact(context, finalData, bm);
                    }
                };

                ImageView img = new ImageView(context);
                new AQuery(context).id(img).image(avatarUrl, true, false, 0, 0, callback);
            }
        }
    }

    public static final String EXTRA_LOADING_ID = "extra_loading_id";
    public static final String EXTRA_LOADING_PROGRESS = "extra_loading_progress";

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int type = intent.getIntExtra(UpDownLoadService.TYPE, -1);

            String id = intent.getStringExtra(EXTRA_LOADING_ID);
            int progress = intent.getIntExtra(EXTRA_LOADING_PROGRESS, -1);

            switch (type) {
                case UpDownLoadService.TYPE_RESEND:
                case UpDownLoadService.TYPE_UPLOAD:
                case UpDownLoadService.TYPE_BUMP_SEND:
                    updateProgress(progress, id, true);
                    break;
                case UpDownLoadService.TYPE_BUMP_RECEIVE:
                case UpDownLoadService.TYPE_DOWNLOAD:
                    updateProgress(progress, id, false);
                    break;
            }
        }
    };

    private void updateProgress(int progress, String loadingId, boolean isUpload) {
        Log.e("Update progress", loadingId + " " + progress);

        final ChatBoxItem item = loadingItems.get(loadingId);
        if (item == null) {
            return;
        }

        Log.e("Update progress", item.id + " " + progress + " " + item.progress);

        if (item != null) {
            if (progress > item.progress) {
                item.progress = progress;
                switch (item.progress) {
                    case ChatBoxItem.PROGRESS_SUCCESS:
                        item.isLoading = false;
                        loadingItems.remove(loadingId);

                        int i = indexOf(item);
                        if (i != -1) {
                            List<ChatBoxItem> items = newItems.get(item);
                            if (removeLoading) {
                                data.remove(i);
                                removeLoading = false;
                            }

                            if (items != null) {
                                data.addAll(i, items);
                                newItems.remove(item);
                            }

                            adapter.notifyDataSetChanged();
                        }
                        break;
                    default:
                        int childCount = lvChatBox.getChildCount();
                        int firstTime = lvChatBox.getFirstVisiblePosition();
                        int index = indexOf(item);
                        if (index == -1) {
                            Log.e("Update progress", "Not found");
                            break;
                        }


                        int realIndex = index - firstTime;
                        if (realIndex < childCount) {
                            View view = lvChatBox.getChildAt(realIndex);
                            if (view == null) {
                                Log.e("Update", "View null 1");
                                break;
                            }

                            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
                            TextView txtStatus = (TextView) view.findViewById(R.id.txtStatus);
                            View imgFailed = view.findViewById(R.id.imgFailed);
                            View layout = view.findViewById(R.id.layout);

                            if (progressBar == null || txtStatus == null || imgFailed == null) {
                                Log.e("Update", "View null 2");
                                break;
                            }

                            switch (item.progress) {
                                case ChatBoxItem.PROGRESS_EXTRACTING:
                                    imgFailed.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                    txtStatus.setVisibility(View.VISIBLE);
                                    txtStatus.setTag(R.string.is_loading, true);
                                    txtStatus.setTag(R.string.resource_id, R.string.extracting);
                                    txtStatus.setText(R.string.extracting_sp);

                                    ChatBoxAdapter.loading(txtStatus, 0);
                                    break;
                                case ChatBoxItem.PROGRESS_FAIL:
                                    progressBar.setVisibility(View.GONE);
                                    imgFailed.setVisibility(View.VISIBLE);
                                    txtStatus.setVisibility(View.VISIBLE);
                                    txtStatus.setText(isUpload ? R.string.send_failed_message : R.string.receive_failed_message);
                                    txtStatus.setTag(R.string.is_loading, false);
                                    if (isUpload) {
                                        tapToSendAgain(layout, item);
                                    }
                                    break;
                                default:
                                    imgFailed.setVisibility(View.GONE);
                                    txtStatus.setVisibility(View.GONE);
                                    txtStatus.setTag(R.string.is_loading, false);
                                    progressBar.setVisibility(View.VISIBLE);
                                    progressBar.setProgress(item.progress);
                                    break;
                            }

                            Log.e("Update", "Updated");
                        }
                        break;
                }
            }
        }
    }

    private void tapToSendAgain(View view, final ChatBoxItem item) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setOnClickListener(null);
                item.progress = ChatBoxItem.PROGRESS_INIT;

                if (item.messageForResend != null) {
                    Intent intent = new Intent(ChatBoxScreen.this, UpDownLoadService.class);
                    intent.putExtra(UpDownLoadService.TYPE, UpDownLoadService.TYPE_RESEND);
                    intent.putExtra(EXTRA_LOADING_ID, item.id);
                    intent.putExtra(UpDownLoadService.EXTRA_UPLOAD_USER_ID, PreferenceUtils.getString(Constants.USER_ID, ChatBoxScreen.this, ""));
                    intent.putExtra(UpDownLoadService.EXTRA_FRIEND_ID, friendId);
                    intent.putExtra(UpDownLoadService.EXTRA_RESEND_FILE, item.fileToResend);
                    intent.putExtra(UpDownLoadService.EXTRA_RESEND_MESSAGE, item.messageForResend);
                    startService(intent);
                }
            }
        });
    }

    private TextView txtName;
    private ImageView imgAvatar;

    private TextView txtNotification;

    private Handler handler = new Handler();

    private IntentFilter filter = new IntentFilter(ParseReceiver.NOTIFY_ACTION);

    private BroadcastReceiver receiverNotify = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            String message = intent.getStringExtra(ParseReceiver.KEY_PUSH_MESSAGE);
            txtNotification.setText(message);
            txtNotification.setVisibility(View.VISIBLE);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    txtNotification.setVisibility(View.GONE);
                }
            }, 3000);

            txtNotification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtNotification.setVisibility(View.GONE);
                    txtNotification.setOnClickListener(null);

                    String data = intent.getStringExtra(ParseReceiver.KEY_PUSH_DATA);
                    Intent fibu = new Intent("fibu.action.chat");
                    fibu.putExtra(ParseReceiver.KEY_PUSH_DATA, data);
                    context.sendBroadcast(fibu);
                }
            });


        }
    };

    public static final String EXTRA_FROM_BUM = "extra_from_bump";

    private Animation rotate;
    private View popupBumpFail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.chatbox_screen);

        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);

        badge = getIntent().getIntExtra(EXTRA_BADGE, 0);

        txtBumpFailure = (TextView) findViewById(R.id.txtBumpFailure);
        txtNotification = (TextView) findViewById(R.id.txtNotification);

        popupConnecting = findViewById(R.id.layoutPopupConnecting);
        popupConnectingHolder = new PopupConnectingHolder(popupConnecting);

        popupBumpFail = findViewById(R.id.view_bump_fail);
        popupBumpFailHolder = new PopupBumpFailHolder(popupBumpFail);
        popupBumpFailHolder.layoutActionHide.setOnClickListener(this);
        popupBumpFailHolder.layoutAnimation.setOnClickListener(this);

        popupMultipleConnected = findViewById(R.id.layoutMultipleConnect);
        popupMultipleConnectedHolder = new PopupMultipleConnectedHolder(popupMultipleConnected);
        popupMultipleConnectedHolder.txtPopupCancelBumpAgain.setOnClickListener(this);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        txtName = (TextView) findViewById(R.id.txtName);
        imgAvatar = (ImageView) findViewById(R.id.imgAvatar);

        layoutBack = findViewById(R.id.layoutBack);
        layoutBack.setOnClickListener(this);

        contentLayout = (FrameLayout) findViewById(R.id.content_frame);
//        layoutProgress = (RelativeLayout) findViewById(R.id.layout_progress_sync_chat);

        Fragment fragment = new SharingBarFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content_frame, fragment).commit();

        layoutPopupProfile = findViewById(R.id.layoutPopupProfile);
        popupProfileHolder = new PopupProfileHolder(layoutPopupProfile);

        View imgClose = layoutPopupProfile.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);

        View viewHide = layoutPopupProfile.findViewById(R.id.layoutPopupProfile);
        viewHide.setOnClickListener(this);

        lvChatBox = (ListView) findViewById(R.id.lvChatBox);
        data = new ArrayList<>();
        adapter = new ChatBoxAdapter(data, this);
        lvChatBox.setAdapter(adapter);

        imgLeftAction = (ImageView) findViewById(R.id.imgLeftAction);
        imgLeftAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emojiconsPopup.isShowing()) {
                    emojiconsPopup.dismiss();
                } else {
                    if (emojiconsPopup.isKeyBoardOpen()) {
                        emojiconsPopup.showAtBottom();
                    } else {
                        txtMessage.setFocusableInTouchMode(true);
                        txtMessage.requestFocus();
                        emojiconsPopup.showAtBottomPending();

                        showKeyBoard(txtMessage);
                    }

                    imgLeftAction.setImageResource(R.drawable.action_keyboard);
                }
            }
        });

        imgRightAction = (ImageView) findViewById(R.id.imgRightAction);
        imgRightAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = txtMessage.getText().toString();
                if (keyboardOpened && !message.isEmpty()) {
                    sendMessage(message);
                    resetTextBox();
                } else {
                    Constants.isChat = true;
                    hideKeyBoard(v);
                    contentLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        txtMessage = (CustomEditText) findViewById(R.id.txtMessage);
        txtMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                keyboardOpened();

                if (emojiconsPopup.isKeyBoardOpen()) {
                    if (emojiconsPopup.isShowing()) {
                        emojiconsPopup.dismiss();
                    }
                }
            }
        });

        txtMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if (focus) {
                    keyboardOpened();
                }
            }
        });

        txtMessage.setBackPressedListener(new CustomEditText.BackPressedListener() {
            @Override
            public void onImeBack(CustomEditText editText) {
                resetTextBox();
                keyboardClosed();
            }
        });

        txtMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    imgRightAction.setImageResource(R.drawable.plus_chat_icon);
                } else {
                    imgRightAction.setImageResource(R.drawable.send_on);

                    int lineCount = txtMessage.getLineCount();

                    if (lineCount >= 3 && lineCount < 7) {
                        int lineHeight = txtMessage.getLineHeight();

                        int height = lineCount * lineHeight;

                        ViewGroup.LayoutParams layoutParams = txtMessage.getLayoutParams();
                        layoutParams.height = height + 4;

                        txtMessage.setLayoutParams(layoutParams);
                    }
                }
            }
        });

        txtMessage.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    String text = txtMessage.getText().toString();
                    if (TextUtils.isEmpty(text)) {
                        Log.e("QueAnh_send_emty", "true");
                    } else {
//                        hideKeyBoard(v);
//                        keyboardClosed();

                        sendMessage(text);
                        resetTextBox();
                    }

                    return true;
                }

                return false;
            }
        });

        txtMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.e("QueAnh_send_emty", actionId + "");
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (TextUtils.isEmpty(v.getText())) {
                        Log.e("QueAnh_send_emty", "true");
                    } else {
//                        hideKeyBoard(v);
//                        keyboardClosed();

                        sendMessage(v.getText().toString());
                        resetTextBox();
                    }

                    return true;
                }

                return false;
            }
        });

        Intent intent = getIntent();
        loadFriend(intent);

        instance = this;

        setUpEmojiPopup();

        boolean fromBump = intent.hasExtra(EXTRA_FROM_BUM);
        if (fromBump) {
            checkForRating();
        }
    }

    private void checkForRating() {
        int times = PreferenceUtils.getInt(Constants.TIMES_BUMP_SUCCESS, this, 0);
        boolean rated = PreferenceUtils.getBoolean(Constants.RATED, this, false);
        boolean ok = times == 3 || !rated && times == 5 || times % 50 == 0;
        Log.e("Check rating", times + " " + rated + " " + ok);
        if (ok) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    BlurBehind.getInstance().execute(ChatBoxScreen.this, new OnBlurCompleteListener() {
                        @Override
                        public void onBlurComplete() {
                            Intent intent = new Intent(ChatBoxScreen.this, RatingScreen.class);
                            startActivity(intent);
                        }
                    });
                }
            }, 1000);
        }
    }

    private EmojiconsPopup emojiconsPopup;

    private void setUpEmojiPopup() {
        final View rootView = findViewById(R.id.chatboxLayout);
        emojiconsPopup = new EmojiconsPopup(rootView, this);
        emojiconsPopup.setSizeForSoftKeyboard();

        emojiconsPopup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {
            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (emojiconsPopup.isShowing()) {
                    emojiconsPopup.dismiss();
                }
            }
        });

        emojiconsPopup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {
            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (txtMessage == null || emojicon == null) {
                    return;
                }

                int start = txtMessage.getSelectionStart();
                int end = txtMessage.getSelectionEnd();
                String emoji = emojicon.getEmoji();
                if (start < 0) {
                    txtMessage.append(emoji);
                } else {
                    int from = Math.min(start, end);
                    int to = Math.max(start, end);
                    int length = emoji.length();
                    txtMessage.getText().replace(from, to, emoji, 0, length);
                }
            }
        });

        emojiconsPopup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {
            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                txtMessage.dispatchKeyEvent(event);
            }
        });

        emojiconsPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                imgLeftAction.setImageResource(R.drawable.smiley);
            }
        });

    }

    private void resetTextBox() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.weight = 1;
        txtMessage.setLayoutParams(layoutParams);
        txtMessage.getText().clear();
    }

    public void loadFriend(Intent intent) {
        friendId = intent.getStringExtra(EXTRA_ID);
        String name = intent.getStringExtra(EXTRA_NAME);
        txtName.setText(name);
        String avatar = intent.getStringExtra(EXTRA_AVATAR);
        avatarUrl = AppUtils.getAvatarByName(this, avatar);
        AppUtils.loadAvatar(this, imgAvatar, avatarUrl, false);
        imgAvatar.setOnClickListener(this);

        try {
            jsonFriend.put("id", friendId);
            jsonFriend.put("name", name);
            jsonFriend.put("avatar", avatar);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        clearChatBox();

        loadFriendProfile();
    }

    private void clearChatBox() {
        currentPage = 1;
        friendData = null;
        data.clear();
        adapter.notifyDataSetChanged();
    }


    private int currentPage = 1;

    private void sendMessage(final String message) {
        try {
            JSONObject json = new JSONObject();
            json.put("message", message);

            final ChatBoxItem item = new ChatBoxItem(friendId, 1, json);
            data.add(item);
            adapter.notifyDataSetChanged();
            lvChatBox.smoothScrollToPosition(data.size() - 1);

            AQuery aQuery = new AQuery(this);

            AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    int location = data.lastIndexOf(item);
                    data.remove(location);

                    if (object != null) {
                        try {
                            JSONObject response = object.getJSONObject(Constants.RESPONSE);
                            boolean success = response.getBoolean(Constants.SUCCESS);

                            if (success) {
                                JSONObject obj = response.getJSONObject("item");
                                ChatBoxItem newItem = new ChatBoxItem(friendId, obj);
                                data.add(location, newItem);
                                RushOrmManager.getInstance().storeSingleChatItem(newItem);

//                                Toast.makeText(ChatBoxScreen.this, "Sent", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ChatBoxScreen.this, "Error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(ChatBoxScreen.this, "Error", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }

                    adapter.notifyDataSetChanged();
                }
            };

            String msg = EmojiMapUtil.replaceUnicodeEmojis(message);

            Map<String, String> params = new HashMap<>();
            params.put("user_id", PreferenceUtils.getString(Constants.USER_ID, this, ""));
            params.put("friend_id", friendId);
            params.put("message", msg);
            params.put("chat_type", "2");

            String url = AppUtils.getChatSendUrl(this);
            aQuery.ajax(url, params, JSONObject.class, callback);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        instance = null;

        if (AccelerometerManager.isListening()) {
            AccelerometerManager.stopListening();
        }

        resetTextBox();
        keyboardClosed();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiverNotify);
    }

    private boolean firstTime = true;

    @Override
    protected void onResume() {
        super.onResume();

        instance = this;

        if (AccelerometerManager.isSupported(this)) {
            int sensitive = PreferenceUtils.getInt(Constants.SETTING_SENSITIVE, getApplicationContext(), Constants.DEFAULT_SENSITIVE);
            Utils.getInstance().setSensitivity(sensitive);
            AccelerometerManager.startListening(this);
        }

        boolean checkLocationEnable = Utils.getInstance().checkLocationEnable(this);
        if (!checkLocationEnable) {
            Utils.getInstance().forcePopupLocation(this);
        }

        showListChat();
        loadChatHistoryFromServer();

        IntentFilter intentFilter = new IntentFilter("fibu.action.loading");
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, intentFilter);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiverNotify, filter);

        try {
            String locationParam = PreferenceUtils.getString(Constants.JSON_LOC, this, "");
            PreferenceUtils.saveString(Constants.JSON_LOC, this, "");

            if (!TextUtils.isEmpty(locationParam)) {
                onShare("location", new JSONObject(locationParam));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private boolean keyboardOpened = false;

    private void keyboardOpened() {
        keyboardOpened = true;
    }

    private void keyboardClosed() {
        keyboardOpened = false;
    }

    private void showKeyBoard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    private void hideKeyBoard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            IBinder windowToken = view.getWindowToken();
            inputMethodManager.hideSoftInputFromWindow(windowToken, 0);
        }
    }

    private boolean isPreviewing = false;

    public void showPreview(Intent intent) {
        isPreviewing = true;
        startActivity(intent);
    }

    private JSONObject friendData = null;

    private void loadFriendProfile() {
        AQuery aQuery = new AQuery(this);
        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                Log.e("Friend", object + "");
                if (object != null) {
                    try {
                        JSONObject response = object.getJSONObject(Constants.RESPONSE);
                        boolean success = response.getBoolean(Constants.SUCCESS);
                        if (success) {
                            friendData = response.getJSONObject("data");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        String url = AppUtils.getViewProfileUrl(this, friendId);
        aQuery.ajax(url, JSONObject.class, callback);
    }

    public static void saveContact(Context context, JSONObject friendData, Bitmap bm) {
        Intent intent = new Intent(ContactFragment.ACTION);
        intent.putExtra(ContactFragment.DATA, friendData.toString());
        intent.putExtra("bit_map_contact", bm);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    protected boolean canBump = true;

    @Override
    public void onShake(float force) {
        if (canBump) {
            Log.e("QueAnh", "shake");
            doBump();
        }
    }

    private void doBump() {
        GPSTracker gpsTracker = GPSTracker.getInstance();
        Location location = gpsTracker.getLocation();
        Log.e("Location", location + "");
        if (location == null) {
            return;
        }

        canBump = false;

        AQuery aQuery = new AQuery(this);
        Map<String, String> params = new HashMap<>();

        params.put("latitude", location.getLatitude() + "");
        params.put("longitude", location.getLongitude() + "");

        String userID = PreferenceUtils.getString(Constants.USER_ID, this, null);
        params.put("user_id", userID);

        params.put("my_shared_type", "profile");

        Log.e("Params", params + "");

        String url = AppUtils.getBumpUrl(this);
        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    handlerBumpResponse(object);
                } else {
                    canBump = true;
                }
            }
        });
    }

    protected Friend friend;

    private void showPopupMultipleConnected() {
        popupMultipleConnected.setVisibility(View.VISIBLE);

        View.OnClickListener l = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMultipleConnected.setVisibility(View.GONE);
                canBump = true;
                doBump();
            }
        };

        popupMultipleConnectedHolder.layoutPopupAction.setOnClickListener(l);
        popupMultipleConnectedHolder.txtPopupBumpAgain.setOnClickListener(l);
    }

    private PopupConnectingHolder popupConnectingHolder;
    private PopupBumpFailHolder popupBumpFailHolder;
    private PopupMultipleConnectedHolder popupMultipleConnectedHolder;

    private void showPopupConnecting() {
        popupConnectingHolder.txtFirstName.setText(friend.firstName);
        popupConnectingHolder.txtLastName.setText(friend.lastName);
        popupConnectingHolder.txtJobTitle.setText("null".equals(friend.jobTitle) ? "" : friend.jobTitle);

        popupConnectingHolder.txtPopupAction.setVisibility(View.VISIBLE);
        popupConnectingHolder.imgStatus.setVisibility(View.GONE);

        popupConnectingHolder.txtPopupCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (request != null) {
                    Log.e("Cancel", "Aquery");
                    request.cancel(true);
                }

                if (asyncTask != null) {
                    Log.e("Cancel", "Task");
                    asyncTask.cancel(true);
                }

                hidePopupConnecting();
                BumpUtils.confirmConnect(null, ChatBoxScreen.this, friend.connectId, BumpUtils.ConnectConfirmCallback.ACTION_CANCEL, friend.id);
            }
        });

        String avatarUrl = AppUtils.getAvatarByName(this, friend.avatar);
        AppUtils.loadAvatar(this, popupConnectingHolder.imgAvatar, avatarUrl, true);
        popupConnecting.setVisibility(View.VISIBLE);

        View.OnClickListener l = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupConnectingHolder.layoutPopupAction.setOnClickListener(null);
                if (friend.sendType.isEmpty() && friend.receiveType.isEmpty()) {
                    hidePopupConnecting();
                    gotoChatBox();
                } else {
                    transferData();
                }
            }
        };

        popupConnectingHolder.layoutPopupAction.setOnClickListener(l);
        popupConnectingHolder.txtPopupAction.setOnClickListener(l);
    }

    private void transferData() {
        popupConnectingHolder.txtPopupAction.setVisibility(View.GONE);
        popupConnectingHolder.imgStatus.setVisibility(View.VISIBLE);
        popupConnectingHolder.txtPopupAction.setOnClickListener(null);
        popupConnectingHolder.imgStatus.startAnimation(rotate);
        popupConnectingHolder.txtPopupCancel.setOnClickListener(null);

        BumpUtils.confirmConnect(this, this, friend.connectId, BumpUtils.ConnectConfirmCallback.ACTION_CONNECT, friend.id);
    }

    private AsyncTask asyncTask;
    private Future request;

    private void upload() {
        ArrayList<String> selected = new ArrayList<>();
        BumpUtils.upload(this, friend, selected, new BumpUtils.Callback() {
            @Override
            public void callback() {
                connectHandler.sendEmptyMessage(0);
            }
        });

    }

    private void download() {
        BumpUtils.download(this, friend, new BumpUtils.Callback() {
            @Override
            public void callback() {
                connectHandler.sendEmptyMessage(0);
            }
        });
    }

    @Override
    public void doTransferData() {

        if (!friend.sendType.isEmpty()) {
            upload();
        } else {
            connectHandler.sendEmptyMessage(1);
        }

        if (!friend.receiveType.isEmpty()) {
            download();
        } else {
            connectHandler.sendEmptyMessage(1);
        }

    }

    public void hidePopupConnecting() {
        request = null;
        asyncTask = null;

        canBump = true;
        popupConnecting.setVisibility(View.GONE);
        popupConnectingHolder.imgStatus.clearAnimation();
    }

    private Handler connectHandler = new Handler() {
        private boolean firstTime = true;

        @Override
        public void handleMessage(Message msg) {
            Log.e("Handler", "Received " + firstTime + " " + msg.what);
            if (msg.what == -1) {
                hidePopupConnecting();
                firstTime = true;
                return;
            }

            if (firstTime) {
                firstTime = false;
            } else {
                firstTime = true;
                gotoChatBox();
                hidePopupConnecting();
            }
        }
    };

    protected void gotoChatBox() {
        if (!PreferenceUtils.getString(Constants.FRIEND_ID, getApplicationContext(), "").equals("friend" + friend.id)) {
            //add friend profile
            ChatBoxItem itemFriend = new ChatBoxItem();
            JSONObject jsonFriend = Utils.getInstance().getProfileFriend(friend);

            itemFriend.setFriendId(friend.id);
            itemFriend.setJsonData(jsonFriend);
            RushOrmManager.getInstance().storeSingleChatItem(itemFriend);

            //add seft profile
            ChatBoxItem itemSeft = new ChatBoxItem();
            String selfInfo = PreferenceUtils.getString(Constants.USER_INFO_JSON, ChatBoxScreen.this, "{}");
            try {
                JSONObject selfJson = new JSONObject(selfInfo);
                JSONObject jsonSeft = Utils.getInstance().getSelfProfile(friend.id, selfJson, 0);

                itemSeft.setFriendId(friend.id);
                itemSeft.setJsonData(jsonSeft);
                RushOrmManager.getInstance().storeSingleChatItem(itemSeft);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //store friend id
            PreferenceUtils.saveString(Constants.FRIEND_ID, getApplicationContext(), "friend" + friend.id);
        }


        int count = PreferenceUtils.getInt(Constants.TIMES_BUMP_SUCCESS, this, 0) + 1;
        PreferenceUtils.saveInt(Constants.TIMES_BUMP_SUCCESS, this, count);

        Intent intent = new Intent(this, ChatBoxScreen.class);
        intent.putExtra(ChatBoxScreen.EXTRA_ID, friend.id);
        intent.putExtra(ChatBoxScreen.EXTRA_NAME, friend.firstName + " " + friend.lastName);
        intent.putExtra(ChatBoxScreen.EXTRA_AVATAR, friend.avatar);
        intent.putExtra(ChatBoxScreen.EXTRA_FROM_BUM, true);
        startActivity(intent);

        if (friendId.equals(friend.id)) {
            finish();
        }
    }

    protected TextView txtBumpFailure;
    protected View popupConnecting, popupMultipleConnected;

    protected void handlerBumpResponse(JSONObject data) {
        try {
            Log.e("Bump", "Response " + data);
            JSONObject response = data.getJSONObject(Constants.RESPONSE);
            boolean success = response.getBoolean(Constants.SUCCESS);
            if (success) {
                friend = new Friend(response);
                showPopupConnecting();
            } else {
                String error = response.getString("error");

//                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
//                String title = getString(R.string.title_bump_dialog);
//                alertBuilder.setTitle(title);

                switch (error) {
                    case "friendNotFound":
                    case "bothNotShare":
//                        alertBuilder.setMessage(R.string.message_bump_no_friend_available);
//                        popBumpFail.setVisibility(View.VISIBLE);
//                        popupBumpFailHolder.layoutAnimation.startAnimation(moveDow);

                        canBump = true;
                        txtBumpFailure.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                txtBumpFailure.setVisibility(View.GONE);
                            }
                        }, 3000);
                        break;
                    case "multiBumped":
                        showPopupMultipleConnected();
                        break;
//                    case "bothShare":
//                        alertBuilder.setMessage(R.string.message_bump_both_share);
//                        alertBuilder.setNeutralButton(R.string.bump_clear_all_choices, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                clearSelectedItems();
//                            }
//                        });
//                        break;
//                    case "bothNotShare":
//                        alertBuilder.setMessage(R.string.message_bump_both_not_share);
//                        popBumpFail.setVisibility(View.VISIBLE);
//                        popupBumpFailHolder.layoutAnimation.startAnimation(moveDow);
//                        break;
                }

//                alertBuilder.setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//                alertBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                    @Override
//                    public void onDismiss(DialogInterface dialog) {
//                        canBump = true;
//                    }
//                });
//                alertBuilder.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class PopupProfileHolder {
        public ImageView imgAvatar;
        public TextView txtFirstName, txtLastName, txtJobTitle;

        public View layoutPhone[], layoutEmail[], layoutAddress[], layoutSocial[], layoutOther;
        public TextView txtPhone[], txtEmail[], txtAddress[], txtSocial[];

        public View layoutLink;
        public TextView txtLink;

        public PopupProfileHolder(View view) {
            imgAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
            txtFirstName = (TextView) view.findViewById(R.id.txtFirstName);
            txtLastName = (TextView) view.findViewById(R.id.txtLastName);
            txtJobTitle = (TextView) view.findViewById(R.id.txtJobTitle);

            layoutPhone = new View[4];
            txtPhone = new TextView[4];

            layoutPhone[0] = view.findViewById(R.id.layoutPhone);
            txtPhone[0] = (TextView) view.findViewById(R.id.txtPhone);

            layoutPhone[1] = view.findViewById(R.id.layoutPhone2);
            txtPhone[1] = (TextView) view.findViewById(R.id.txtPhone2);

            layoutPhone[2] = view.findViewById(R.id.layoutPhone3);
            txtPhone[2] = (TextView) view.findViewById(R.id.txtPhone3);

            layoutPhone[3] = view.findViewById(R.id.layoutPhone4);
            txtPhone[3] = (TextView) view.findViewById(R.id.txtPhone4);

            layoutEmail = new View[2];
            txtEmail = new TextView[2];

            layoutEmail[0] = view.findViewById(R.id.layoutEmail);
            txtEmail[0] = (TextView) view.findViewById(R.id.txtEmail);

            layoutEmail[1] = view.findViewById(R.id.layoutEmail2);
            txtEmail[1] = (TextView) view.findViewById(R.id.txtEmail2);

            layoutLink = view.findViewById(R.id.layoutLink);
            txtLink = (TextView) view.findViewById(R.id.txtLink);

            layoutAddress = new View[3];
            txtAddress = new TextView[3];

            layoutAddress[0] = view.findViewById(R.id.layoutAddress);
            txtAddress[0] = (TextView) view.findViewById(R.id.txtAddress);

            layoutAddress[1] = view.findViewById(R.id.layoutAddress2);
            txtAddress[1] = (TextView) view.findViewById(R.id.txtAddress2);

            layoutAddress[2] = view.findViewById(R.id.layoutAddress3);
            txtAddress[2] = (TextView) view.findViewById(R.id.txtAddress3);

            layoutSocial = new View[3];
            txtSocial = new TextView[3];

            layoutSocial[0] = view.findViewById(R.id.layoutFacebook);
            txtSocial[0] = (TextView) view.findViewById(R.id.txtFacebook);

            layoutSocial[1] = view.findViewById(R.id.layoutTwitter);
            txtSocial[1] = (TextView) view.findViewById(R.id.txtTwitter);

            layoutSocial[2] = view.findViewById(R.id.layoutLinkedIn);
            txtSocial[2] = (TextView) view.findViewById(R.id.txtLinkedIn);

            layoutOther = view.findViewById(R.id.layoutOther);
        }
    }

    private void fillProfileFriend(final JSONObject data) {
        try {
            boolean hasInfo = false;

            if (data.has("avatar")) {
                String avatar = data.getString("avatar");
                String avatarUrl = AppUtils.getAvatarByName(this, avatar);
                AppUtils.loadAvatar(this, popupProfileHolder.imgAvatar, avatarUrl, false);
                popupProfileHolder.imgAvatar.setVisibility(View.VISIBLE);
            } else {
                popupProfileHolder.imgAvatar.setVisibility(View.GONE);
            }

            String firstName = data.getString("first_name");
            popupProfileHolder.txtFirstName.setText(firstName);

            if (data.has("last_name")) {
                String lastName = data.getString("last_name");
                popupProfileHolder.txtLastName.setText(lastName);
                popupProfileHolder.txtLastName.setVisibility(View.VISIBLE);
            } else {
                popupProfileHolder.txtLastName.setVisibility(View.GONE);
            }

            if (data.has("job_title")) {
                String jobTitle = data.getString("job_title");
                popupProfileHolder.txtJobTitle.setText(jobTitle);
                popupProfileHolder.txtJobTitle.setVisibility(View.VISIBLE);
            } else {
                popupProfileHolder.txtJobTitle.setVisibility(View.GONE);
            }

            String[] phonesKey = {"mobile", "home_phone", "office_phone", "other_phone"};
            for (int i = 0; i < phonesKey.length; i++) {
                String phoneKey = phonesKey[i];
                if (data.has(phoneKey)) {
                    String phone = data.getString(phoneKey);

                    if (!TextUtils.isEmpty(phone) && !phone.equals("null")) {
                        popupProfileHolder.txtPhone[i].setText(phone);
                        popupProfileHolder.layoutPhone[i].setVisibility(View.VISIBLE);
                        hasInfo = true;
                    } else {
                        popupProfileHolder.layoutPhone[i].setVisibility(View.GONE);
                    }
                } else {
                    popupProfileHolder.layoutPhone[i].setVisibility(View.GONE);
                }
            }

            String[] emailsKey = {"email", "other_email"};
            for (int i = 0; i < emailsKey.length; i++) {
                String emailKey = emailsKey[i];
                if (data.has(emailKey)) {
                    String email = data.getString(emailKey);
                    if (!TextUtils.isEmpty(email) && !email.equals("null")) {
                        popupProfileHolder.txtEmail[i].setText(email);
                        popupProfileHolder.layoutEmail[i].setVisibility(View.VISIBLE);
                        hasInfo = true;
                    } else {
                        popupProfileHolder.layoutEmail[i].setVisibility(View.GONE);
                    }
                } else {
                    popupProfileHolder.layoutEmail[i].setVisibility(View.GONE);
                }
            }

//            ContactResult result = checkInContactsList(phone, email, null, null);
//            int textId = result != null ? R.string.popup_profile_friend_in_contact : R.string.popup_profile_friend_not_in_contact;

            int textId = R.string.popup_profile_friend_in_contact;
            TextView txtInContactList = (TextView) layoutPopupProfile.findViewById(R.id.txtInContactList);
            txtInContactList.setText(textId);

            if (data.has("link")) {
                String website = data.getString("link");
                popupProfileHolder.txtLink.setText(website);
                popupProfileHolder.layoutLink.setVisibility(View.VISIBLE);
                hasInfo = true;
            } else {
                popupProfileHolder.layoutLink.setVisibility(View.GONE);
            }

            String[] addressesKey = {"home_address", "office_address", "other_address"};
            for (int i = 0; i < addressesKey.length; i++) {
                String addressKey = addressesKey[i];
                if (data.has(addressKey)) {
                    String json = data.getString(addressKey);
                    JSONObject obj = new JSONObject(json);
                    String address = obj.optString("address") + " " + obj.optString("postal_code") + " " + obj.optString("city") + " " + obj.optString("street");
                    popupProfileHolder.txtAddress[i].setText(address);
                    popupProfileHolder.layoutAddress[i].setVisibility(View.VISIBLE);
                    hasInfo = true;
                } else {
                    popupProfileHolder.layoutAddress[i].setVisibility(View.GONE);
                }
            }

            String[] socialsKey = {"facebook", "twitter", "linkedin"};
            for (int i = 0; i < socialsKey.length; i++) {
                String socialKey = socialsKey[i];
                if (data.has(socialKey)) {
                    String nick = data.getString(socialKey);
                    boolean isVisibleView = true;
                    if (socialKey.equals("facebook")) {
                        if (AccessToken.getCurrentAccessToken() == null) {
                            isVisibleView = false;
                        } else {
                            final int indexField = i;
                            final String facebookId = data.getString("facebook");
//                            Ion.with(this).load("https://graph.facebook.com/?ids=" + facebookId
//                                    + "&access_token=1524270291224917|c38c8ef6632f8452a1b0de330985d028")
//                                    .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
//                                @Override
//                                public void onCompleted(Exception e, JsonObject result) {
//                                    if (e != null || result == null) {
//                                        return;
//                                    }
//                                    if (result.has(facebookId)) {
//                                        JsonObject response = result.getAsJsonObject(facebookId);
//                                        popupProfileHolder.txtSocial[indexField].setText(response.getAsJsonPrimitive("name").getAsString());
//                                    }
//                                }
//                            });
                            new GraphRequest(
                                    AccessToken.getCurrentAccessToken(),
                                    "/" + facebookId,
                                    null,
                                    HttpMethod.GET,
                                    new GraphRequest.Callback() {
                                        public void onCompleted(GraphResponse response) {
                                            if (response.getJSONObject() != null) {
                                                try {
                                                    popupProfileHolder.txtSocial[indexField].setText(response.getJSONObject().getString("name"));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                            ).executeAsync();
                        }
                    } else {
                        popupProfileHolder.txtSocial[i].setText(nick);
                    }
                    if (isVisibleView) {
                        popupProfileHolder.layoutSocial[i].setVisibility(View.VISIBLE);
                    } else {
                        popupProfileHolder.layoutSocial[i].setVisibility(View.GONE);
                    }
                    hasInfo = true;
                } else {
                    popupProfileHolder.layoutSocial[i].setVisibility(View.GONE);
                }
            }

            popupProfileHolder.layoutOther.setVisibility(hasInfo ? View.GONE : View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.layoutBack:
                finish();
                break;
            case R.id.imgAvatar:
                if (friendData != null) {
                    showProfileFriend(friendData);
                } else {
                    Toast.makeText(this, "Can't load profile", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.imgClose:
                layoutPopupProfile.setVisibility(View.GONE);
                break;
            case R.id.layoutPopupProfile:
                layoutPopupProfile.setVisibility(View.GONE);
                break;
            case R.id.view_bump_fail:
                popupBumpFail.setVisibility(View.GONE);
                canBump = true;
                break;
            case R.id.layout_animation:
                popupBumpFail.setVisibility(View.GONE);
                canBump = true;
                break;
            case R.id.txtPopupCancelBumpAgain:
                popupMultipleConnected.setVisibility(View.GONE);
                canBump = true;
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (Constants.isChat) {
            if (TabsAdapter.index == 1 && !Constants.offBar) {
                if (FileFragment.layoutContent.getVisibility() == View.VISIBLE) {
                    FragmentManager supportFragmentManager = getSupportFragmentManager();
                    FileFragment.backAction(supportFragmentManager);
                    return;
                }
            }

            contentLayout.setVisibility(View.GONE);
            Constants.isChat = false;
            Constants.offBar = false;
        } else if (layoutPopupProfile.getVisibility() == View.VISIBLE) {
            layoutPopupProfile.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    public void showProfileFriend(JSONObject data) {
        layoutPopupProfile.setVisibility(View.VISIBLE);
        fillProfileFriend(data);
    }

    @Override
    public void onShare(final String type, final JSONObject json) {
        boolean up = false;
        if (TabsAdapter.index == 1) {
            onBackPressed();
            TabsAdapter.index = 0;
            up = true;
        }

        onBackPressed();
        if (up) {
            TabsAdapter.index = 1;
        }

        Log.e("Sharing", type + " " + json);

        int progress = 0;

        try {
            int tp = 0;
            JSONObject dt = new JSONObject();
            dt.put("type", type);

            JSONArray items = new JSONArray();

            switch (type) {
                case "photo":
                    progress = ChatBoxItem.PROGRESS_COMPRESSING;
                    tp = 2;
                    JSONObject photo = new JSONObject();
                    String path = json.getString("data");
                    photo.put("path", path);
                    items.put(photo);
                    dt.put("items", items);
                    break;
                case "music":
                case "video":
                case "document":
                    progress = ChatBoxItem.PROGRESS_COMPRESSING;
                    switch (type) {
                        case "music":
                            tp = 4;
                            break;
                        case "video":
                            tp = 3;
                            break;
                        case "document":
                            tp = 7;
                            break;
                    }

                    dt = new JSONObject();
                    String p = json.getString("data");
                    dt.put("path", p);
                    break;
                case "contact":
                    tp = 5;
                    dt = json;
                    break;
                case "location":
                    tp = 9;
                    JSONObject a = new JSONObject();
                    a.put("type", "location");
                    a.put("item", json);
                    dt.put("message", a.toString());
                    break;
                case "dropbox":
                    progress = ChatBoxItem.PROGRESS_GETTING_LINK;
                    tp = 6;
                    dt = json;
                    break;
            }

            ChatBoxItem chatboxItem;
            if (tp != 0) {
                chatboxItem = new ChatBoxItem(friendId, tp, dt);
                String key = chatboxItem.toString();
                chatboxItem.progress = progress;
                chatboxItem.isLoading = true;

                chatboxItem.save();

                data.add(chatboxItem);
                adapter.notifyDataSetChanged();
                int position = data.size() - 1;
                lvChatBox.smoothScrollToPosition(position);

                loadingItems.put(key, chatboxItem);

                Intent intent = new Intent(this, UpDownLoadService.class);
                intent.putExtra(UpDownLoadService.TYPE, UpDownLoadService.TYPE_UPLOAD);
                intent.putExtra(EXTRA_LOADING_ID, key);
                intent.putExtra(UpDownLoadService.EXTRA_UPLOAD_USER_ID, PreferenceUtils.getString(Constants.USER_ID, this, ""));
                intent.putExtra(UpDownLoadService.EXTRA_FRIEND_ID, friendId);
                intent.putExtra(UpDownLoadService.EXTRA_UPLOAD_TYPE, type);

                String info = json.toString();
                intent.putExtra(UpDownLoadService.EXTRA_UPLOAD_INFO, info);
                startService(intent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
//        loadChatHistory(false);
        Log.e("refresh", "last time stamp : " + lastTimeStamp);
        if (loadMore) {
            loadListPaging(lastTimeStamp);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }

    }

    public static final int RC_CAPTURE_IMAGE = 1001;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RC_CAPTURE_IMAGE:
                if (resultCode == Activity.RESULT_OK) {
                    String path = PreferenceUtils.getString(Constants.PATH_SHARING, this, "");
                    if (new File(path).exists()) {
                        try {
                            Bitmap bitmap = decodeFile(path);

                            Uri uri = getRealPath(getApplicationContext(), bitmap);
                            String pth = Utils.getInstance().getRealPathFromURI(uri, ChatBoxScreen.this);

                            Log.e("Take_photo : ", pth);
                            JSONObject dataPhoto = new JSONObject();
                            dataPhoto.put("data", pth);
                            onShare("photo", dataPhoto);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    public static Bitmap decodeFile(String path) {
        int orientation;
        try {
            if (path == null) {
                return null;
            }
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 70;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 0;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale++;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bm = BitmapFactory.decodeFile(path, o2);
            Bitmap bitmap = bm;

            ExifInterface exif = new ExifInterface(path);

            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Matrix m = new Matrix();

            if ((orientation == ExifInterface.ORIENTATION_ROTATE_180)) {
                m.postRotate(180);
                Log.e("QueANh in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
                return bitmap;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                m.postRotate(90);
                Log.e("QueANh in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
                return bitmap;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                m.postRotate(270);
                Log.e("QueANh in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
                return bitmap;
            }

            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Uri getRealPath(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    // for offline data function

    private void showListChat() {
        List<ChatBoxItem> listChat = RushOrmManager.getInstance().getListChatItems(friendId);

        boolean change = false;

        for (ChatBoxItem item : listChat) {
            if (!contains(item)) {

                if (item.id != null && (item.id.startsWith("friend") || item.id.startsWith("self"))) {
                    data.add(0, item);
                } else {
                    data.add(item);
                }

                change = true;
            }
        }

        if (change) {
            Collections.sort(data);
            adapter.notifyDataSetChanged();
//	        layoutProgress.setVisibility(View.GONE);
        }

        ChatBoxItem lastItem;
        int sizeList = listChat.size();
        if (sizeList == 10) {
            loadMore = true;
            lastItem = listChat.get(0);
            lastTimeStamp = lastItem.timeStamp;
        } else {
            loadMore = false;
        }

        if (firstTime) {
            lvChatBox.setSelection(data.size() - 1);
            firstTime = false;
        } else {
            boolean refreshing = swipeRefreshLayout.isRefreshing();
            if (refreshing) {
                swipeRefreshLayout.setRefreshing(false);
            }
        }

        Log.e("first item", "last time stamp : " + lastTimeStamp);
    }

    private void loadListPaging(long timeStamp) {
        boolean refreshing = swipeRefreshLayout.isRefreshing();
        List<ChatBoxItem> listChat = RushOrmManager.getInstance().getListChatItemsPaging(friendId, timeStamp);
        data.addAll(0, listChat);
        adapter.notifyDataSetChanged();

        ChatBoxItem lastItem;
        int sizeList = listChat.size();
        if (sizeList == 10) {
            loadMore = true;
            lastItem = listChat.get(0);
            lastTimeStamp = lastItem.timeStamp;
        } else {
            loadMore = false;
        }

        if (!refreshing) {
            lvChatBox.setSelection(data.size() - 1);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void loadChatHistoryFromServer() {
        AQuery aQuery = new AQuery(this);

        AjaxCallback<JSONObject> callback = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    try {
                        JSONObject response = object.getJSONObject(Constants.RESPONSE);

                        JSONArray items = response.getJSONArray("items");

                        int size = items.length();

                        for (int i = 0; i < size; i++) {
                            JSONObject item = items.getJSONObject(i);

                            String chatType = item.getString("type");
                            switch (chatType) {
                                case "bump":
                                case "share":
                                    String message = item.getString("message");
                                    JSONObject obj = new JSONObject(message);
                                    String bumpType = obj.getString("type");
                                    if ("profile".equals(bumpType)) {
                                        JSONObject profile = obj.getJSONObject("profile");
                                        JSONArray array = new JSONArray();
                                        array.put(profile);
                                        obj.put("items", array);
                                    }

                                    String serverFile = obj.optString("serverFile", null);
                                    String sf = item.optString("server_file", "null");
                                    if (!sf.isEmpty() && !sf.equals("null")) {
                                        serverFile = sf;
                                    }

                                    int countCt = 0;
                                    if ("contact".equals(bumpType)) {
                                        if ("share".equals(bumpType)) {
                                            countCt = 1;
                                        } else {
                                            String count = obj.getString("count");
                                            if ("all".equals(count)) {
                                                countCt = Integer.MAX_VALUE;
                                            } else {
                                                countCt = Integer.parseInt(count);
                                            }
                                        }
                                    }

                                    if (obj.has("items") && !"photo".equals(bumpType) && !"location".equals(bumpType)) {
                                        JSONArray array = obj.getJSONArray("items");

                                        int length = countCt > 5 ? 1 : array.length();

                                        for (int j = 0; j < length; j++) {
                                            JSONObject element = array.getJSONObject(j);
                                            String[] names = {"id", "side", "type", "seen", "liked", "time", "gmtTimestamp"};
                                            JSONObject clone = new JSONObject(item, names);
                                            JSONObject json = new JSONObject();
                                            String messageType = obj.getString("type");
                                            json.put("type", messageType);
                                            json.put("message", element);
                                            json.put("serverFile", serverFile);
                                            clone.put("message", json);

                                            ChatBoxItem chatItem = new ChatBoxItem(friendId);
                                            chatItem.setJsonData(clone);
                                            chatItem.count = j;

                                            if (countCt > 5) {

                                                boolean isSender = chatItem.isSender;
                                                String msg = isSender ? "Send " : "Received ";
                                                if (countCt == Integer.MAX_VALUE) {
                                                    msg += "all contacts";
                                                } else {
                                                    msg += countCt + " contacts";
                                                }

                                                clone.put("message", msg);
                                                chatItem.setJsonData(clone);
                                                chatItem.count = -1;
                                            }

                                            if (!"profile".equals(bumpType)) {
                                                RushOrmManager.getInstance().storeSingleChatItem(chatItem);
                                                saveContactItem(ChatBoxScreen.this, chatItem);
                                            }
                                        }
                                    } else {

                                        ChatBoxItem chatItem = new ChatBoxItem(friendId);
                                        chatItem.setJsonData(item);

                                        RushOrmManager.getInstance().storeSingleChatItem(chatItem);
                                    }
                                    break;
                                case "message":

                                    ChatBoxItem chatItem = new ChatBoxItem(friendId);
                                    chatItem.setJsonData(item);

                                    RushOrmManager.getInstance().storeSingleChatItem(chatItem);

                                    break;
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        String url = AppUtils.getChatThread(this, PreferenceUtils.getString(Constants.USER_ID, this, ""), friendId);
        aQuery.ajax(url, JSONObject.class, callback);
    }
}
