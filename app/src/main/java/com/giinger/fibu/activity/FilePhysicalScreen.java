package com.giinger.fibu.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.giinger.fibu.R;
import com.giinger.fibu.adapter.FileDocumentAdapter;
import com.giinger.fibu.model.FileItem;
import com.giinger.fibu.util.Constants;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FilePhysicalScreen extends BaseFileScreen {

    public static final int MAX_SELECTED_ITEMS = 5;

    private ListView lvChooseDocument;
    private List<FileItem> data;
    private String rootPath;
    private FileDocumentAdapter adapter;
    private View lnListItems, lnEmpty;

    private RelativeLayout layoutBumpAnimation;
    private RelativeLayout layoutAnimLeft;
    private RelativeLayout layoutAnimRight;

    private Animation animLeft;
    private Animation animRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rootPath = ROOT.getAbsolutePath();
        FileItem root = new FileItem(rootPath, null);
        goToFolder(root);
    }

    @Override
    protected void initLayout() {
        setContentView(R.layout.file_document_screen);

        ImageView imgIcon = (ImageView) findViewById(R.id.imgIcon);
        imgIcon.setImageResource(R.drawable.files_icon);

        lnListItems = findViewById(R.id.lnListItems);
        lnEmpty = findViewById(R.id.lnEmpty);

        lvChooseDocument = (ListView) findViewById(R.id.lvChooseDocument);

        data = new ArrayList<>();
        adapter = new FileDocumentAdapter(data);
        lvChooseDocument.setAdapter(adapter);

        layoutBumpAnimation = (RelativeLayout) findViewById(R.id.layout_bump_action);
        layoutAnimLeft = (RelativeLayout) findViewById(R.id.layout_anim_left);
        layoutAnimRight = (RelativeLayout) findViewById(R.id.layout_anim_right);

        animLeft = AnimationUtils.loadAnimation(this, R.anim.left_to_right);
        animRight = AnimationUtils.loadAnimation(this, R.anim.right_to_left);

        animLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Constants.isAnim = false;
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        lvChooseDocument.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FileItem item = data.get(position);
                switch (item.type) {
                    case MEDIA_FILE:
                    case DOCUMENT_FILE:
                        FileDocumentAdapter.ViewHolder viewHolder = (FileDocumentAdapter.ViewHolder) view.getTag();
                        if (item.isSelected) {
                            item.isSelected = false;
                            totalSize -= item.size;
                            onItemSelected(item);
                            viewHolder.imgSelected.setImageResource(R.drawable.not_select_icon);

                            if (getCountItemSelected() == 0) {
                                goneLayoutAnimation();
                            }
                        } else {
                            long newSize = totalSize + item.size;
                            if (newSize > BaseFileScreen.MAX_SIZE) {
                                String message = getString(R.string.msg_file_selected_too_big, selected.size() < 1 ? "" : "s");
                                new AlertDialog.Builder(FilePhysicalScreen.this).setMessage(message).setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }).show();
                                return;
                            }

                            int count = getCountItemSelected();
                            if (count < MAX_SELECTED_ITEMS) {
                                item.isSelected = true;
                                totalSize += item.size;
                                onItemSelected(item);
                                viewHolder.imgSelected.setImageResource(R.drawable.select_icon);

                                if (Constants.isAnim) {
                                    animationBump();
                                }
                            } else {
                                Toast.makeText(FilePhysicalScreen.this, "Maximum files can select once time", Toast.LENGTH_SHORT).show();
                            }
                        }
                        Log.e("Path", item.path);
                        break;
                    case BACK_FOLDER:
                    case FOLDER:
                    case ROOT_FOLDER:
                        totalSize = 0;
                        goToFolder(item);
                        break;
                }
            }
        });
    }

    private FileItem preItem = null;

    @Override
    public void onBackPressed() {
        if (preItem == null) {
            super.onBackPressed();
        } else {
            totalSize = 0;
            goToFolder(preItem);
        }
    }

    @Override
    protected void animationBump() {
        layoutBumpAnimation.setVisibility(View.VISIBLE);
        layoutAnimLeft.startAnimation(animLeft);
        layoutAnimRight.startAnimation(animRight);
    }

    @Override
    protected void goneLayoutAnimation() {
        layoutBumpAnimation.setVisibility(View.GONE);
        Constants.isAnim = true;
    }

    public static final File ROOT = Environment.getExternalStorageDirectory().getParentFile().getParentFile();
    private static final int EMPTY = 0;
    private static final int RELOAD = 1;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EMPTY:
                    lnListItems.setVisibility(View.GONE);
                    lnEmpty.setVisibility(View.VISIBLE);
                    break;
                case RELOAD:
                    clearSelectedItems();
                    adapter.notifyDataSetChanged();
                    lvChooseDocument.smoothScrollToPosition(0);
                    lnListItems.setVisibility(View.VISIBLE);
                    lnEmpty.setVisibility(View.GONE);
                    break;
            }
        }
    };

    public void goToFolder(final FileItem root) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<FileItem> items = new ArrayList<>();
                File file = new File(root.path);

                if (!rootPath.equals(root.path)) {
                    String path = file.getParentFile().getAbsolutePath();
                    String name = "Up to " + file.getName();
                    FileItem fileItem = new FileItem(path, name, FileItem.TYPE.BACK_FOLDER);
                    items.add(fileItem);
                    preItem = fileItem;
                } else {
                    preItem = null;
                }

                File[] files = file.listFiles();
                if (files != null) {
                    for (File item : files) {
                        String path = item.getAbsolutePath();
                        String name = item.getName();

                        FileItem fileItem = new FileItem(path, name, item.length());
                        boolean isDir = item.isDirectory();
                        if (rootPath.equals(root.path)) {
                            fileItem.type = isDir ? FileItem.TYPE.ROOT_FOLDER : FileItem.TYPE.DOCUMENT_FILE;
                        } else {
                            fileItem.type = isDir ? FileItem.TYPE.FOLDER : FileItem.TYPE.DOCUMENT_FILE;
//                            boolean isMediaFile = item.get == null ? false : item.mimeType.contains("");
//                            if (isMediaFile) {
//
//                            }
                        }

                        items.add(fileItem);
                    }
                }

                data.clear();
                data.addAll(items);
                Collections.sort(data);

                handler.sendEmptyMessage(RELOAD);
            }
        }).start();

    }

    @Override
    public void notifyDataSetChange() {
        adapter.notifyDataSetChanged();
    }

    @Override
    protected String getType() {
        return "file";
    }

    @Override
    protected String getSharedType() {
        return "document";
    }
}
