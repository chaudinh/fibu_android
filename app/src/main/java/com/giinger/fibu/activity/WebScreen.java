package com.giinger.fibu.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.giinger.fibu.R;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.PreferenceUtils;

public class WebScreen extends Activity {

    public static final String EXTRA_URL = "extra_url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.web_screen);

        Intent intent = getIntent();
        String url = intent.getStringExtra(EXTRA_URL);

        WebView webView = (WebView) findViewById(R.id.webView);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

        webView.setWebViewClient(new WebViewClient());

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        webView.loadUrl(url);

        int times = PreferenceUtils.getInt(Constants.FB_ADD_FRIEND, this, 0);
        if (times < 2) {
            showAddFriendFB();
            PreferenceUtils.saveInt(Constants.FB_ADD_FRIEND, this, ++times);
        }
    }

    public void showAddFriendFB() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Facebook Suggestion");
        alertDialog.setMessage("Tap again \"Add Friend\" to confirm your friend request");
        alertDialog.setPositiveButton("Got it",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        alertDialog.show();
    }
}
