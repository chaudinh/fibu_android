package com.giinger.fibu.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.giinger.fibu.R;
import com.giinger.fibu.adapter.FileVideoAdapter;
import com.giinger.fibu.model.FileItem;
import com.giinger.fibu.util.BumpUtils;
import com.giinger.fibu.util.Constants;
import com.giinger.fibu.util.ExternalStorage;
import com.giinger.fibu.util.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class FileVideoScreen extends BaseFileScreen {

    public static final int MAX_SELECTED_ITEMS = 5;

    private RelativeLayout layoutBumpAnimation;
    private RelativeLayout layoutAnimLeft;
    private RelativeLayout layoutAnimRight;

    private Animation animLeft;
    private Animation animRight;

    @Override
    public void notifyDataSetChange() {
        adapter.notifyDataSetChanged();
    }

    @Override
    protected String getType() {
        return "video";
    }

    @Override
    protected String getSharedType() {
        return "video";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadAllFiles();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            Collections.sort(data, new Comparator<FileItem>() {
                @Override
                public int compare(FileItem lhs, FileItem rhs) {

                    if (lhs.getMili() > rhs.getMili()) {
                        return -1;
                    } else if (lhs.getMili() < rhs.getMili()) {
                        return +1;
                    } else {
                        return 0;
                    }
                }
            });

            adapter.notifyDataSetChanged();
        }
    };

    private static final File MOVIES = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

    private void loadAllFiles() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e("Video", MOVIES.getAbsolutePath());
                listAllFiles(MOVIES);

                if (ExternalStorage.isAvailable()) {
                    File sdCard = new File("/storage/sdcard1/DCIM");
                    if (!sdCard.exists()) {
                        Map<String, File> externalLocations = ExternalStorage.getAllStorageLocations();
                        if (externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD) == null) {
                            sdCard = new File("/mnt/sdcard/ext_sd/DCIM");
                        } else {
                            sdCard = new File(externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD).toString() + "/DCIM");
                        }
                    }

                    listAllFiles(sdCard);
                }

                handler.sendEmptyMessage(0);
            }
        }).start();
    }

    private GridView gvChooseVideo;
    private List<FileItem> data;
    private FileVideoAdapter adapter;

    @Override
    protected void initLayout() {
        setContentView(R.layout.file_video_screen);

        layoutBumpAnimation = (RelativeLayout) findViewById(R.id.layout_bump_action);
        layoutAnimLeft = (RelativeLayout) findViewById(R.id.layout_anim_left);
        layoutAnimRight = (RelativeLayout) findViewById(R.id.layout_anim_right);

        animLeft = AnimationUtils.loadAnimation(this, R.anim.left_to_right);
        animRight = AnimationUtils.loadAnimation(this, R.anim.right_to_left);

        animLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Constants.isAnim = false;
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        gvChooseVideo = (GridView) findViewById(R.id.gvChooseVideo);
        gvChooseVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
                FileItem fileItem = data.get(position);
                FileVideoAdapter.ViewHolder viewHolder = new FileVideoAdapter.ViewHolder(view, position);

                Integer integer = new Integer(position);
                if (compressing.contains(integer)) {
                    compressing.remove(integer);
                    fileItem.progress = -1;
                    viewHolder.progressBar.setVisibility(View.GONE);
                    viewHolder.progressBar.setProgress(0);
                } else {
                    if (fileItem.isSelected) {
                        fileItem.isSelected = false;
                        totalSize -= fileItem.size;
                        FileVideoScreen.this.onItemSelected(fileItem);
                        viewHolder.imgSelected.setImageResource(R.drawable.video_not);

                        if (getCountItemSelected() == 0) {
                            goneLayoutAnimation();
                        }
                    } else {
                        final int count = getCountItemSelected() + compressing.size();
                        if (count > 0 && firstTime && !fileItem.isSelected) {
                            firstTime = false;

                            new AlertDialog.Builder(FileVideoScreen.this)
                                    .setTitle("Notice")
                                    .setMessage(getString(R.string.notice_for_multiple_videos_selected))
                                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            onItemSelect(position);
                                        }
                                    })
                                    .setNegativeButton("NO", null).show();
                        } else if (count > MAX_SELECTED_ITEMS) {
                            Toast.makeText(FileVideoScreen.this, "Maximum files can select once time", Toast.LENGTH_SHORT).show();
                        } else if (fileItem.size > 20 * 1024 * 1014) { // Too big
                            String message = getString(R.string.msg_file_selected_too_big, selected.size() < 1 ? "" : "s");
                            new AlertDialog.Builder(FileVideoScreen.this)
                                    .setMessage(message)
                                    .setPositiveButton(R.string.text_ok, null)
                                    .show();
                        } else {
                            onItemSelect(position);
                        }
                    }
                }
            }
        });

        data = new ArrayList<>();
        adapter = new

                FileVideoAdapter(data);

        gvChooseVideo.setAdapter(adapter);
    }

    @Override
    protected void animationBump() {
        layoutBumpAnimation.setVisibility(View.VISIBLE);
        layoutAnimLeft.startAnimation(animLeft);
        layoutAnimRight.startAnimation(animRight);
    }

    @Override
    protected void goneLayoutAnimation() {
        layoutBumpAnimation.setVisibility(View.GONE);
        Constants.isAnim = true;
    }

    private boolean firstTime = true;

    private void onItemSelect(final int position) {
        final Integer integer = new Integer(position);
        final FileItem child = data.get(position);

        compressing.add(integer);
        startCompressing(child.size, position);
    }

    private List<Integer> compressing = new ArrayList<>();

    private IntentFilter filter = new IntentFilter(ACTION);

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int position = intent.getIntExtra(EXTRA_POSITION, -1);
            int progress = intent.getIntExtra(EXTRA_PROGRESS, -1);

            if (position != -1 && progress != -1) {
                Integer integer = new Integer(position);
                if (compressing.contains(integer)) {
                    FileItem item = data.get(position);
                    if (progress > item.progress) {
                        item.progress = progress;
                        int childCount = gvChooseVideo.getChildCount();
                        int firstTime = gvChooseVideo.getFirstVisiblePosition();

                        int realIndex = position - firstTime;
                        if (realIndex < childCount) {
                            View view = gvChooseVideo.getChildAt(realIndex);
                            if (view != null) {
                                FileVideoAdapter.ViewHolder viewHolder = new FileVideoAdapter.ViewHolder(view, position);
                                switch (progress) {
                                    case 0:
                                        viewHolder.progressBar.setVisibility(View.VISIBLE);
                                        viewHolder.progressBar.setProgress(0);
                                        break;
                                    case 100:
                                        compressing.remove(integer);
                                        viewHolder.progressBar.setVisibility(View.GONE);
                                        checkingForSelecting(item, viewHolder);
                                        break;
                                    default:
                                        viewHolder.progressBar.setProgress(progress);
                                        break;
                                }

                                return;
                            }
                        }

                        if (progress == 100) {
                            compressing.remove(integer);
                            checkingForSelecting(item, null);
                        }
                    }
                }
            }
        }
    };

    private AlertDialog dialog;

    private void checkingForSelecting(FileItem item, FileVideoAdapter.ViewHolder viewHolder) {
        long newSize = totalSize + item.size;
        if (newSize > BaseFileScreen.MAX_SIZE) {
            if (dialog == null || !dialog.isShowing()) {
                String message = getString(R.string.msg_file_selected_too_big, selected.size() < 1 ? "" : "s");
                AlertDialog.Builder builder = new AlertDialog.Builder(FileVideoScreen.this)
                        .setMessage(message)
                        .setPositiveButton(R.string.text_ok, null);
                dialog = builder.create();
                dialog.show();
            }

            item.progress = -1;
        } else {
            item.isSelected = true;
            totalSize += item.size;

            item.isSelected = true;
            item.progress = -1;
            FileVideoScreen.this.onItemSelected(item);

            if (viewHolder != null) {
                viewHolder.imgSelected.setImageResource(R.drawable.video_selected);
                viewHolder.progressBar.setProgress(0);
            }

            if (Constants.isAnim) {
                animationBump();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.unregisterReceiver(receiver);
    }

    private void startCompressing(long size, int position) {
        new CompressVideoTask(size, position).start();
    }

    private static final String ACTION = "action.compress.video";
    private static final String EXTRA_POSITION = "extra.compress.video.position";
    private static final String EXTRA_PROGRESS = "extra.compress.video.progress";

    class CompressVideoTask extends Thread {
        private long size;
        private int position;

        private CompressVideoTask(long size, int position) {
            this.size = size;
            this.position = position;
        }

        @Override
        public void run() {
            long time = 30 * size / (5 * 1024 * 1024);

            int count = 0;

            Integer integer = new Integer(position);
            do {
                if (compressing.contains(integer)) {

                    updateProgress(count);

                    try {
                        Thread.sleep(time);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    count++;
                } else {
                    break;
                }
            } while (count <= 100);
        }

        private void updateProgress(final int progress) {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(FileVideoScreen.this);
            Intent intent = new Intent(ACTION);
            intent.putExtra(EXTRA_POSITION, position);
            intent.putExtra(EXTRA_PROGRESS, progress);
            broadcastManager.sendBroadcast(intent);
        }

    }

    private void listAllFiles(File root) {
        File[] files = root.listFiles();
        if (files == null) {
            return;
        }

        File cache = BumpUtils.getCacheFolder();
        if (root.getAbsolutePath().equals(cache.getAbsolutePath())) {
            return;
        }

        for (File file : files) {
            boolean isDir = file.isDirectory();
            if (isDir) {
                listAllFiles(file);
            } else {
                String name = file.getName();
                boolean isVideo = name.toLowerCase().endsWith(".mp4");
                if (isVideo) {
                    String path = file.getAbsolutePath();
                    long time = file.lastModified();
                    String[] dt = Utils.getCurrentTime(time);

                    long size = file.length();
                    FileItem item = new FileItem(path, name, dt[1], dt[0], size);
                    item.setMili(time);
                    data.add(item);
                }
            }
        }
    }

}
