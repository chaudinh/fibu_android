package com.giinger.fibu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.giinger.fibu.R;
import com.giinger.fibu.fragment.ExtraPowerFragment;

public class ExtraPowerScreen extends FragmentActivity {

    public static final String EXTRA_USER_ID = "extra_user_id";

    private ExtraPowerFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.extra_power_screen);

        Intent intent = getIntent();
        String userId = intent.getStringExtra(EXTRA_USER_ID);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        fragment = new ExtraPowerFragment();
        Bundle args = new Bundle();
        args.putString(ExtraPowerFragment.EXTRA_USER_ID, userId);
        fragment.setArguments(args);

        transaction.add(R.id.fragment, fragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (fragment == null || !fragment.hidePopupLayout()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
