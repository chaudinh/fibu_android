package com.giinger.fibu.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.TextView;

import com.giinger.fibu.R;
import com.giinger.fibu.receiver.ParseReceiver;

public abstract class BasePreviewScreen extends FragmentActivity {

    public static BasePreviewScreen instance;

    private TextView txtNotification;

    private Handler handler = new Handler();

    private IntentFilter filter = new IntentFilter(ParseReceiver.NOTIFY_ACTION);

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            String message = intent.getStringExtra(ParseReceiver.KEY_PUSH_MESSAGE);
            txtNotification.setText(message);
            txtNotification.setVisibility(View.VISIBLE);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    txtNotification.setVisibility(View.GONE);
                }
            }, 3000);

            txtNotification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtNotification.setVisibility(View.GONE);
                    txtNotification.setOnClickListener(null);

                    String data = intent.getStringExtra(ParseReceiver.KEY_PUSH_DATA);
                    Intent fibu = new Intent("fibu.action.chat");
                    fibu.putExtra(ParseReceiver.KEY_PUSH_DATA, data);
                    context.sendBroadcast(fibu);
                }
            });

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initLayout();

        instance = this;

        txtNotification = (TextView) findViewById(R.id.txtNotification);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    public abstract void initLayout();

    @Override
    protected void onDestroy() {
        super.onDestroy();

        instance = null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }
}
